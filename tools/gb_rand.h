/* -*-c++-*-
 * 
 *    Source     $RCSfile: gb_rand.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __GB_RAND_H__
#define __GB_RAND_H__

#if __GNUC__ > 3 || \
	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
#include <iostream>
#include <fstream>
using namespace std;
#endif

//I have extracted and adapted this from an indirect 
//copy of a socalled Stanford graph-base library

static const long two_to_the_31 = (unsigned long)0x80000000;


class GB_rand
{
private:
    long A[56];
    int ind;

    long mod_diff(long x,long y){return ((x-y)&0x7fffffff);}

    long flip_cycle ()
    {
     register long *ii, *jj;
     for (ii = &A[1], jj = &A[32]; jj <= &A[55]; ii++, jj++)
         *ii = ((*ii-*jj) & 0x7fffffff);
     for (jj = &A[1]; ii <= &A[55]; ii++, jj++)
         *ii = ((*ii-*jj) & 0x7fffffff);
     ind = 54;
     return A[55];
    }
    
    //long next_rand(){return (*gb_fptr>=0?(*gb_fptr--):flip_cycle());}

public:
    void init(long seed=1)
    {
     register long i;
     register long prev = seed, next = 1;
     seed = prev = (prev & 0x7fffffff);
     A[55] = prev;
     for (i = 21; i; i = (i + 21) % 55) {
         A[i] = next;

         next = ((prev-next) & 0x7fffffff);
         if (seed & 1)
             seed = 0x40000000 + (seed >> 1);
         else
             seed >>= 1;
         next = ((next- seed) & 0x7fffffff);
         
         prev = A[i];
     }

     (void) flip_cycle ();
     (void) flip_cycle ();
     (void) flip_cycle ();
     (void) flip_cycle ();
     (void) flip_cycle ();

    }
    
    GB_rand(long seed=1)
        {ind=0;A[0]=-1;init(seed);}
    ~GB_rand(){}
    
    long unif_long(long m)
    {
     register unsigned long t = two_to_the_31 - (two_to_the_31 % m);
     register long r;
     do {
         r = (A[ind]>=0?(A[ind--]):flip_cycle());
     } while (t <= (unsigned long) r);
     return r % m;
    }

    double next()
    {
     long r=unif_long(0x40000000);
     return double(r+.5)/double(0x40000000);
    }
    
    ostream& save(ostream& out) const
    {
     for(int i=0;i<56;i++) out<<A[i]<<"\n";
     out<<ind<<"\n"; return out;
    }
   
    istream& restore(istream& in)
    {
     for(int i=0;i<56;i++)
         in>>A[i];
     in>>ind; return in;
    }

};


#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: gb_rand.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:51  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:48  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/tools/gb_rand.h,v $
 --------------------------------------------------------------------------- */
