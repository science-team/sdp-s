/*
* Date: 30/08/2002
* Author : Geraud Delaporte
* creates an graphical output of the convergence
* of the SDP solver SB (C.Helmberg)
* from an output file of SDP_S
* CAUTION : gnuplot and ghostview must be installed
*/

/*This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#define NB_READ 1000


double boundFinMin = 0x7fffffff;
double boundFinMax = -0x7fffffff;
int courbeCroissante = 0;
int courbeDecroissante = 0;
int Ymin = 0;
int Ymax = 0;
int Tmin = 0;
int Tmax = 0;
int affiche = 1;

char *go_parenthese(char * buf)
{
  int i = 0;
  while(1){
    if(buf[i] == 0)
      return NULL;
    if(buf[i] == '('){
      return &(buf[i]); 
    }
    else{
      i++;
    } 
  }
}


void genere_gnuplot_file(int nb_graf,char **grafs)
{
  FILE *fp;
  char buf[100];
  char buf2[100];
  int i;
  double Tmax_d = (double)Tmax;
  double Ymax_d = (double)Ymax;
  double Ymin_d = (double)Ymin;

  sprintf(buf,"%s",grafs[1]);
  sprintf(&(buf[strlen(grafs[1])]),".plot");
  fp = fopen(buf,"w");
  buf[strlen(buf)] = 0;
  sprintf(buf2,"%s",buf);

  fprintf(fp,"set term postscript color\n");
  fprintf(fp,"set output \"%s.ps\"\n",buf);
  fprintf(fp,"set multiplot\n");
  fprintf(fp,"set xlabel \"CPU time (s)\"\n");
  fprintf(fp,"set ylabel \"SDP bound\"\n");
  fprintf(fp,"set xrange [%d:%d]\n",Tmin,Tmax);
  fprintf(fp,"set yrange [%d:%d]\n",Ymin,Ymax);
  for(i=1;i<nb_graf;i++)
  {
    sprintf(buf,"%s",grafs[i]);
    fprintf(fp,"set key %f,%f\n",(Tmax_d-Tmax_d/10),Ymax_d-(double)i*(Ymax_d-Ymin_d)/30);
    fprintf(fp,"plot \"%s.xy\" 7\n",buf);
    fprintf(fp,"plot \"%s.xy\" with lines %d\n",buf,i);
  }

  fclose(fp);
  fflush(NULL);
  sprintf(buf,"gnuplot %s",buf2);
  if(fork() == 0){
    system(buf);
    exit(0);
  }
  fflush(NULL);
  sleep(3);
  sprintf(buf,"gv %s.ps",buf2);
  if((fork() == 0) && (affiche !=0)){
    system(buf);
    exit(0);
  }

}

void gen_xy_file(char *filename)
{
  FILE *fpr;
  FILE *fpw;
  char buf[NB_READ];
  char *ptr = NULL;
  int nb_lus;
  int cent,sec,min, h,time;
  int bound_int,bound_frac;
  int bound_int_1,bound_frac_1;
  float bound_float,bound_float_1;

  fpr = fopen(filename,"r");
  if(!fpr){
    printf("Error while opening the file %s\n",filename);
    exit(-1);
  }

  sprintf(buf,"%s",filename);
  sprintf(&(buf[strlen(filename)]),".xy");
  fpw = fopen(buf,"w");
  if(!fpr){
    printf("Error while opening the file %s\n",buf);
    exit(-1);
  }

  //positionnement au debut des donn�es
  while(ptr == NULL){
    ptr = fgets(buf,NB_READ-1,fpr);
    if(ptr == NULL){
      printf("String 'constraints' not found : %s\n",filename);
      exit(-1);
    }
    ptr = strstr(buf,"constraints =");
  }
  // passage au debut des donn�es
  ptr = fgets(buf,NB_READ-1,fpr);

  //obtention du premier point
  ptr = fgets(buf,NB_READ-1,fpr);
  nb_lus = sscanf(buf,"%d:%d:%d.%d",&h,&min,&sec,&cent);
  if(nb_lus !=4 ) 
    printf("CPU time not found : %s\n",filename);
  ptr = go_parenthese(buf);
  nb_lus = sscanf(ptr,"(%f)",&bound_float_1);
  if(nb_lus !=1 ) 
    printf("Parenthesis not found : %s\n",filename);
  fprintf(fpw,"%d.%.2d %f\n",(time = 3600*h+60*min+sec),cent,bound_float_1);
  bound_int_1 = (int) ceil(bound_float_1);


  //debut traitement
  while(ptr != NULL){
    ptr = fgets(buf,NB_READ-1,fpr);
    if(ptr == NULL) break;
    nb_lus = sscanf(buf,"%d:%d:%d.%d",&h,&min,&sec,&cent);
    if(nb_lus !=4 ) break;
    ptr = go_parenthese(buf);
    nb_lus = sscanf(ptr,"(%f)",&bound_float);
    bound_int = (int) ceil(bound_float);
    if(nb_lus !=1 ) break;
    fprintf(fpw,"%d.%.2d %f\n",(time = 3600*h+60*min+sec),cent,bound_float);
    bound_int = (int) ceil(bound_float);
  }

  if(time >= Tmax) Tmax = time+1;

  if(bound_int_1>bound_int)
  {
    courbeDecroissante = 1;
    boundFinMin = bound_int;
    boundFinMax = bound_int_1;
  }
  else
  {
   courbeCroissante = 1;
   boundFinMin = bound_int_1;
   boundFinMax = bound_int;

  }

  fclose(fpr);
  fclose(fpw);
}


void calculY()
{
  double delta;
  double milieu;

  delta = fabs(boundFinMax-boundFinMin)*1.2; 
  milieu = (boundFinMax+boundFinMin)*0.5;
  Ymin = -1 + (int)(milieu-0.5*delta);
  Ymax = 1 + (int)(milieu+0.5*delta);
}

/* string to integer */
int value (char * string)
{
  int number;
  int i;

  number = 0;
  for (i=0;i<strlen(string);i++)
    {
      number = (number * 10) + ((int)(string[i]) - (int)('0'));
    }
  return number;
}


int trait_option(char ** option,int i)
{
  if (! strcmp(option[i],"-tmax"))
    {
      Tmax = value(option[i+1]);
      return i+1;
    }
  else
    {
      if (! strcmp(option[i],"-tmin"))
	{
	  Tmin = value(option[i+1]);
	  return i+1;
	}
      else
	{
	  if (! strcmp(option[i],"-bmax"))
	    {
	      Ymax = value(option[i+1]);
	      return i+1;
	    }
	  else
	    {
	      if (! strcmp(option[i],"-bmin"))
		{
		  Ymin = value(option[i+1]);
		  return i+1;
		}
	      else
		{
		  if (! strcmp(option[i],"-noplot"))
		    {
		      affiche = 0;
		      return i;
		    }
		  else
		    {
		      return -1;
		    }
		}
	    }
	}
    }
}


int main(int argc, char **argv)
{
  int i;
  char * error;

  error = (char *)malloc(400*sizeof(char));

  error = strcpy(error,"\nusage : ./res2Plot <filename> [option] where filename is an output file of SDP_S.\n\nOptions are:\n   -tmax <integer> : \n   -tmin <integer> : \n   -bmin <integer> : \n   -bmax <integer> : \n   -noplot :\n\n");

  if(argc < 2){
    fprintf(stdout,error);
    return -1;
  }

  gen_xy_file(argv[1]);

  calculY();

  i = 2;
  if (argc > 1)
    {
      while ((i<argc) && (i>0))
	{
	  i = trait_option(argv,i);
	  i++;
	}
    }

  if (i == 0)
    fprintf(stdout,error);
  else
    genere_gnuplot_file(2,argv);

  return 0;
}





