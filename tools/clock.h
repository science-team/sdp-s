/* -*-c++-*-
 * 
 *    Source     $RCSfile: clock.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:49 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __MYTIME_H__
#define __MYTIME_H__

#include <iostream>
#include <iomanip>
extern "C"{
#include <time.h>
}
#ifdef __unix
extern "C"{
#include <sys/resource.h>
//we need extern int getrusage(int who, struct rusage *r_usage);
}
#endif

#if __GNUC__ > 3 || \
	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 3))
using namespace std;
#endif


class Microseconds{
    bool infinity;     //value infinity
    long seconds;
    long microsecs;

public:
    Microseconds(){seconds=0;microsecs=0;infinity=false;}
    Microseconds(bool infty){seconds=0;microsecs=0;infinity=infty;}
    Microseconds(const Microseconds& m)
    {seconds=m.seconds; microsecs=m.microsecs;infinity=m.infinity;}
    Microseconds(long secs, long msecs=0)
    {seconds=secs; microsecs=msecs;infinity=false;}
    Microseconds(int secs, int msecs=0)
    {seconds=secs; microsecs=msecs;infinity=false;}
    Microseconds(long hours,long minutes,long secs, long micros)
    {seconds=hours*3600+minutes*60+secs; microsecs=micros;infinity=false;}
    Microseconds(int hours,int minutes,int secs, int micros)
    {seconds=hours*3600+minutes*60+secs; microsecs=micros;infinity=false;}

    void set_infinity(bool infty){infinity=infty;}
    bool get_infinity() const {return infinity;}

    operator double()
    { return double(seconds)+double(microsecs)/1000000.; }
    
    Microseconds& operator=(const Microseconds& m)
    {seconds=m.seconds; microsecs=m.microsecs; infinity=m.infinity;return *this;}
    
    Microseconds& operator+=(const Microseconds& m)
    {
     microsecs+=m.microsecs;
     seconds+=m.seconds;
     while (microsecs>1000000){
         seconds++;
         microsecs-=1000000;
     }
     return *this;
    }
     
    Microseconds& operator-=(const Microseconds& m)
    {
     microsecs-=m.microsecs;
     seconds-=m.seconds;
     while (microsecs<0) {
         seconds--; microsecs+=1000000;
     }
     return *this;
    }

    Microseconds operator-(Microseconds& m)
    {Microseconds s(*this); return s-=m;}
    Microseconds operator+(Microseconds& m)
    {Microseconds s(*this); return s+=m;}

    bool operator<(const Microseconds& m)
    {
      if (infinity) return false;
      if (m.infinity) return true;
      return ((seconds<m.seconds)||((seconds==m.seconds)&&(microsecs<m.microsecs)));
    }
    bool operator>(const Microseconds& m)
    {
      if (infinity&&(!m.infinity)) return true;
      if (m.infinity) return false;
      return ((seconds>m.seconds)||((seconds==m.seconds)&&(microsecs>m.microsecs)));}
    bool operator<=(const Microseconds& m)
    {
      if (infinity&&(!m.infinity)) return false;
      if (m.infinity) return true;
      return ((seconds<m.seconds)||((seconds==m.seconds)&&(microsecs<=m.microsecs)));}
    bool operator>=(const Microseconds& m)
    {
      if (infinity) return true;
      if (m.infinity) return false;
      return ((seconds>m.seconds)||((seconds==m.seconds)&&(microsecs>=m.microsecs)));}
    bool operator==(const Microseconds& m)
    {
     if ((infinity&&(!m.infinity))||(m.infinity)) return false;
     return ((seconds==m.seconds)&&(microsecs==m.microsecs));}

    void hhmmss(long& hours,long& minutes,long& secs) const
    {
     long s=seconds;
     if (microsecs>=500000) s++;
     hours=s/3600;
     minutes=(s%3600)/60;
     secs=(s%60);
    }
    void hhmmssdd(long& hours,long& minutes,long& secs,long& hund) const
    {
     hund=(microsecs+5000)/10000;
     long s=seconds;
     if (hund==100) {s++;hund=0;}
     hours=s/3600;
     minutes=(s%3600)/60;
     secs=(s%60);
    }

    long roundsecs() const
    {
     if (microsecs>=500000) return seconds+1;
     return seconds;
    }
    
    long roundhundreds() const
    {
     long hund=(microsecs+5000)/10000;
     hund+=seconds*100;
     return hund;
    }

    friend ostream& operator<<(ostream& out,const Microseconds& m)
    {if (m.infinity) return out<<"-1.000000";
     out<<m.seconds<<".";out.fill('0');out.width(6);out<<m.microsecs;
     out.fill(' ');return out;}

    friend istream& operator>>(istream& in,Microseconds& m)
    {
     char c; m.infinity=false;
     in>>m.seconds>>c>>m.microsecs;
     if (m.seconds<0) {m.seconds=0; m.infinity=true;}
     return in;
    }     
};

inline void print_time(ostream& out,const Microseconds& m,int secondsonly=0)
{
 long hours,minutes,seconds,hunds;
 out.fill('0');
 if (secondsonly){
     m.hhmmss(hours,minutes,seconds);
     out.width(2);out<<hours;
     out<<":";out.width(2);out<<minutes;
     out<<":";out.width(2);out<<seconds;
 }
 else {
     m.hhmmssdd(hours,minutes,seconds,hunds);
     out.width(2);out<<hours;
     out<<":";out.width(2);out<<minutes;
     out<<":";out.width(2);out<<seconds;
     out<<".";out.width(2);out<<hunds;
 }
 out.fill(' ');
}

class Clock
{
  private:
    Microseconds t_start;
    Microseconds offset;
#ifdef __unix
#else
    long l_start;
#endif

  public:
    void start(){
#ifdef __unix
        struct rusage r_usage;
        getrusage(RUSAGE_SELF,&r_usage);
        t_start=Microseconds(r_usage.ru_utime.tv_sec,r_usage.ru_utime.tv_usec);
        offset=Microseconds(0,0);
#else
        t_start=Microseconds(time(0),0);
#endif
    }

    Clock(){start();}
    ~Clock(){}

    void set_offset(Microseconds offs){
        offset=offs;
    }

    Microseconds time() const{
#ifdef __unix
        struct rusage r_usage;
        getrusage(RUSAGE_SELF,&r_usage);
        Microseconds elapsed(r_usage.ru_utime.tv_sec,r_usage.ru_utime.tv_usec);
#else
        Microseconds elapsed(time(0),0);
#endif
        elapsed-=t_start;
        elapsed+=offset;
        return elapsed;
    }

    void elapsed_time(ostream& out) const
    {
        time_t current=::time(0);
        struct tm *loctime;                     
        Microseconds spent=time();
        out<<"elapsed time: ";
        print_time(out,spent);
        loctime=localtime(&current);
        out<<"   ----   "<<asctime(loctime)<<flush;
    }

    friend inline ostream& operator<<(ostream& out,const Clock& cl)
    {
        print_time(out,cl.time());
        return out;
    }
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: clock.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:49  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:48  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/tools/clock.h,v $
 --------------------------------------------------------------------------- */
