/* -*-c++-*-
 * 
 *    Source     $RCSfile: heapsort.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:48 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __HEAPSORT_H__
#define __HEAPSORT_H__

/* given:                                                      */
/*    n     ... number of indices to be sorted                 */
/*    ind   ... class indexable by integers,                   */
/*              a function swap(ind[i],ind[j]) is required     */
/*              which swaps the contents of ind[i] and ind[j]  */
/*    val   ... class indexable by elements of ind,            */
/*              for two vals val[i] and val[j] operator <      */
/*              (val[i]<val[j]) yielding 0/1 must be defined   */
/*                                                             */
/* Sorts the first n entries in the index array such that      */
/*                                                             */
/*        val[ind[i]]<=val[ind[j]] for 0<=i<j<n                */
/*                                                             */

template<class I,class V> void heapify(int i,int n,I& ind,const V& val);
     /* Heapify element i in index array of size n. */
     /* after this operation d[ind[i]] is a maximal */
     /* element of the values in the subtree with   */
     /* root i.                                     */

template<class I,class V> void build_heap(int n,I& ind,const V& val);
     /* Construct heap from index array of size n.  */
     /* After this operation d[ind[0]] is a maximal */
     /* element of the first n indexed values.      */

template<class I,class V> void heapsort(int n,I& ind,const V& val);
     /* sort index array */



//--------------------------------------------------------------
//                    heapsort -- implementation
//--------------------------------------------------------------

#include "mymath.h"

template<class I,class V> inline void heapify(int i,int n,I& ind,const V& val)
{
 int k,j;
 k=i;
 while(2*k+1<n){
     if (2*k==n-2){
         if (val[ind[k]]<val[ind[2*k+1]])
             swap(ind[k],ind[2*k+1]);
         break;
     }
     if ((val[ind[k]]<val[ind[2*k+1]])||
         (val[ind[k]]<val[ind[2*k+2]])){
         if (val[ind[2*k+1]]>val[ind[2*k+2]]) j=2*k+1;
         else j=2*k+2;
         swap(ind[k],ind[j]);
         k=j;
     }
     else break;
 }
}

template<class I,class V> inline void build_heap(int n,I& ind,const V& val)
{
 for(int i=n/2;--i>=0;) heapify(i,n,ind,val);
}

template<class I,class V> inline void heapsort(int n,I& ind,const V& val)
{
 int i;

 //build heap
 build_heap(n,ind,val);

 //extract greatest and rebuild heap
 for(i=n;--i>=1;){
     swap(ind[i],ind[0]);
     heapify(0,i,ind,val);
 }
}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: heapsort.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:48  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/tools/heapsort.h,v $
 --------------------------------------------------------------------------- */
