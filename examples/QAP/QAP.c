
/*
* Date: 30/08/2002
* Author : Frederic Roupin
* creates the files .pb, .C, .eq for SDP_S
* from a data file of the QAP problem
*/

/*This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *fp;// Fichier contenant les donnees de l'instance
int n,n2,tmp;
double **C;
int nb_non_nuls; //nombre d'elements non nuls dans C :
                 //utilise pour le stockage en format "creux" (sparse)


void lectureFichier()
{
  int i,j,l,k;
  int **A,**B;

// Dimension du probleme
  fscanf(fp,"%d",&n);
  n2 = n*n;

// Matrice definissant la fonction objectif : min C.Y(=Cijlk.Xij.Xlk)
  C = (double **)malloc(n2*sizeof(double*));
  for(i=0; i<n2; i++){
    C[i] = (double *)malloc(n2*sizeof(double));
  }
 for(j=0; j<n2; j++)
    for(i=0; i<n2; i++) C[i][j] = 0.0;

// LECTURE DES MATRICES DE DONNEES
 A = (int **)malloc(n*sizeof(int *));
 B = (int **)malloc(n*sizeof(int *));
 for(i=0;i<n;i++)
 	{
	 A[i] = (int *)malloc(n*sizeof(int));
	 B[i] = (int *)malloc(n*sizeof(int));
	}
 /* Lecture de la matrice A */
 for(i=0;i<n;i++)
 	{
 	 for(j=0;j<n;j++)
	 	{
		 fscanf(fp, "%d", &tmp);
		 A[i][j] = tmp;
		}
	}
 /* Lecture de la matrice B */
 for(i=0;i<n;i++)
 	{
 	 for(j=0;j<n;j++)
	 	{
		 fscanf(fp, "%d", &tmp);
		 B[i][j] = tmp;
		}
	}
// Construction de C : matrice de la fonction objectif "min C.X"
// et comptage des elements non nuls dans C

 nb_non_nuls = 0; 
 for(i=0;i<n;i++)
   for(j=0;j<n;j++)
     for(k=0;k<n;k++)
       for(l=0;l<n;l++)
 	{
  	 C[i*n+j][k*n+l] += A[i][k]*B[j][l];
	 if ((C[i*n+j][k*n+l]) && (i*n+j<k*n+l)) nb_non_nuls++;
	}
}

void ecriturePb(char *filename)
{
  FILE *filePb;
  char *buf;
  int i,j;

  filePb = fopen(filename,"w");
  if(filePb == NULL){
    printf("Error while opening %s.\n",filename);
    exit(-1);
  }

  /*Min CX
    
    C    : file Sparse : filename.C
    Dim  : n*n
    Trace : n 
    Type : {0,1}
    AddMoreConstr : 1 //Xij>=0 pour tout i<j

    Constraints : 
    File Sparse : contraintes d'affectation
    ProductConstrVect(contraintes d'affectations) // Contraintes "produits"
                                                  // a la "Adams-Sherali"
  
  */
  buf = malloc(strlen(filename)*sizeof(char));
  sprintf(buf,"%s",filename);
  buf[strlen(filename)-2]=0;
  fprintf(filePb,"min CX\n\n");
  fprintf(filePb,"C\t: file Sparse : %sC\n",buf);
  fprintf(filePb,"Dim\t: %d\n",n2);
  fprintf(filePb,"Trace\t: %d\n",n);
  fprintf(filePb,"Type\t: {0,1}\n");
// On impose Yijlk>=0 pour tout (i,j,k,l)
  fprintf(filePb,"AddMoreConstr\t: 1\n\n");
  fprintf(filePb,"Constraints\t:\n\n");
// CONTRAINTES D'AFFECTATION
  fprintf(filePb,"File Sparse\t: %seq\n",buf); 
// CONTRAINTES "PRODUITS" i.e. Xlk*somme(Xij)=Xij sur i puis j
  fprintf(filePb,"ProductConstrVect(%seq)\n",buf);
  free(buf);
}

void ecritureConstr(char *filename)
{
  FILE *fileConstr;
  char *buf;
  int i,j,p,k,t;
  int compt = 0;

  buf = malloc((strlen(filename)+2)*sizeof(char));
  sprintf(buf,"%s",filename);
  sprintf(&(buf[strlen(filename)-3]),"eq");
  fileConstr = fopen(buf,"w");
  if(fileConstr == NULL){
    printf("Error while writting the constraints.\n");
    exit(-1);
  }

 fprintf(fileConstr,"l=\n\n");
 fprintf(fileConstr,"%d %d\n\n",n2,2*n);
 
// somme des x_ij = 1 sur j
 for(i=0;i<n;i++)
	{
	  fprintf(fileConstr,"1\n"); //second membre
	  fprintf(fileConstr,"%d\n",n); //nombre d'elements non nuls
          // indices et valeur des elements non nuls
 	  for(j=0;j<n;j++) fprintf(fileConstr,"%d 1 ",j+1+i*n);
	  fprintf(fileConstr,"\n");
	}
  fprintf(fileConstr,"\n\n");
  
// somme des x_ij = 1 sur i
 for(i=0;i<n;i++)
	{
	  fprintf(fileConstr,"1\n"); //second membre
	  fprintf(fileConstr,"%d\n",n); //nombre d'elements non nuls
          // indices et valeur des elements non nuls
 	  for(j=0;j<n;j++) fprintf(fileConstr,"%d 1 ",j*n+i+1);
	  fprintf(fileConstr,"\n");
	}
  fprintf(fileConstr,"\n\n");
  fclose(fileConstr);
}

void ecritureQuad(char *filename)
{
  int i,j,k,l;
  FILE *fileQuad;
  fileQuad = fopen(filename,"w");
  if(fileQuad == NULL){
    printf("Error while writting the objective function.\n");
    exit(-1);
  }

  fprintf(fileQuad,"%d\n",n2);
  fprintf(fileQuad,"%d\n",nb_non_nuls);
  for(i=0; i<n2; i++)
  {
    for(j=i; j<n2; j++)
    {
      if (C[i][j]) fprintf(fileQuad,"%d %d %f \n",i+1,j+1, C[i][j]);
    }
  }
  fclose(fileQuad);
}

int main(int argc,char **argv)
{
  char buf[100];

  if(argc != 2){
    printf("\n\t Usage : QAP <filename.dat>\n");
    exit(-1);
  }

  fp = fopen(argv[1],"r");
  if(fp == NULL){
    printf("Error while opening %s\n",argv[1]);
    exit(-2);
  }

  lectureFichier();
  sprintf(buf,"%s",argv[1]);
  sprintf(buf+(strlen(argv[1])-4),".pb");
  ecriturePb(buf);
  sprintf(buf+(strlen(argv[1])-4),".C");
  ecritureQuad(buf);
  ecritureConstr(argv[1]);
  printf("Files .pb .C and .eq  written.\n\n");
  return 0;
}
