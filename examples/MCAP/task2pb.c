/****
* Date : 31/08/2002
* Author : G.Delaporte
* The Constrained-Memory Allocation Problem : (MCAP)
* Creates the files .pb, .C, .l, .eq and .ineq
* from the file tass*.dat
*****/


/* This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */






#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *fp;
int nbTaches,nbProcesseurs;
int *coutsExecutions;
int **coutsCommunication;
int *taillesTaches;
int *capasProcesseurs;
double **C;

void lectureFichier()
{
  int i,j;

  fscanf(fp,"%d",&nbTaches);
  fscanf(fp,"%d",&nbProcesseurs);

  coutsExecutions = (int *)malloc(nbProcesseurs*nbTaches*sizeof(int));
  for(i=0; i<nbProcesseurs*nbTaches;i++)
	 fscanf(fp,"%d",&(coutsExecutions[i])); 
  coutsCommunication = (int **)malloc(nbTaches*sizeof(int*));
  for(i=0; i<nbTaches; i++)
	 coutsCommunication[i] = (int *)malloc(nbTaches*sizeof(int));
  C = (double **)malloc(nbTaches*nbProcesseurs*sizeof(double*));
  for(i=0; i<nbTaches*nbProcesseurs; i++)
	 C[i] = (double *)malloc(nbTaches*nbProcesseurs*sizeof(double)); 
 for(j=0; j<nbTaches*nbProcesseurs; j++)
    for(i=0; i<nbTaches*nbProcesseurs; i++) C[i][j] = 0.0;

  for(i=0; i<nbTaches; i++)
   {
    coutsCommunication[i][i] = 0;
    for(j=i+1; j<nbTaches; j++)
    {
     fscanf(fp,"%d",&(coutsCommunication[i][j]));
     coutsCommunication[j][i] = coutsCommunication[i][j];
    }
  }

  taillesTaches = (int*)malloc(nbTaches*sizeof(int));
  for(i=0; i<nbTaches; i++) fscanf(fp,"%d",&(taillesTaches[i])); 
  capasProcesseurs = (int*)malloc(nbProcesseurs*sizeof(int));
  for(i=0; i<nbProcesseurs; i++) fscanf(fp,"%d",&(capasProcesseurs[i]));
}

void ecriturePb(char *filename)
{
  FILE *filePb;
  char *buf;
  int sumCtt=0;
  int i,j;

  filePb = fopen(filename,"w");
  if(filePb == NULL){
    printf("Error while creating the problem file.\n");
    exit(-1);
  }

  for(i=0; i<nbTaches;i++)
    for(j=i+1;j<nbTaches;j++)
      sumCtt += coutsCommunication[i][j];

  buf = malloc(strlen(filename)*sizeof(char));
  sprintf(buf,"%s",filename);
  buf[strlen(filename)-2]=0;
  fprintf(filePb,"min CX + lx + %d\n\n",sumCtt);
  fprintf(filePb,"C\t: file Sparse : %sC\n",buf);
  fprintf(filePb,"l\t: file Dense : %sl\n",buf);
  fprintf(filePb,"Dim\t: %d\n",nbTaches*nbProcesseurs);
  fprintf(filePb,"Trace\t: %d\n",nbTaches);
  fprintf(filePb,"Type\t: {0,1}\n");
  fprintf(filePb,"AddMoreConstr\t: 1\n\n");
  fprintf(filePb,"Constraints\t:\n\n");
  fprintf(filePb,"File Sparse\t: %seq\n",buf);
  fprintf(filePb,"_File Sparse\t: %sineq\n",buf);
  fprintf(filePb,"ProductConstrVect(%seq)\n",buf);
  fprintf(filePb,"ProductConstrVectRight(%sineq)\n",buf);

  free(buf);
}

void ecritureLin(char *filename)
{
  FILE *fileLin;
  int i,j,ce;
  fileLin = fopen(filename,"w");
  if(fileLin == NULL){
    printf("Error while creating the file describing the linear part of the objective function\n");
    exit(-1);
  }

  fprintf(fileLin,"%d\n",nbProcesseurs*nbTaches);
  for(i=0;i<nbTaches;i++){
    for(j=0;j<nbProcesseurs;j++){
      ce = coutsExecutions[nbTaches*j+i];
      fprintf(fileLin,"%d\n",ce);
    }
  }
  fclose(fileLin);
}

void ecritureQuad(char *filename)
{
  int i,j,k,l;
  int nb_non_nuls = 0;

  FILE *fileQuad;
  fileQuad = fopen(filename,"w");
  if(fileQuad == NULL)
  {
    printf("Error while creating the file describing the quadratic part of the objective function.\n");
    exit(-1);
  }

  fprintf(fileQuad,"%d\n",nbTaches*nbProcesseurs);
  for(i=0; i<nbTaches; i++)
  {
    for(l=0; l<nbProcesseurs; l++)
    {
      for(j=0; j<nbTaches; j++)
      {
        C[i*nbProcesseurs+l][j*nbProcesseurs+l] = -0.5*(double)coutsCommunication[i][j];
        if ((coutsCommunication[i][j]) && (i<j)) nb_non_nuls++;
     }
    }
  }
  
  fprintf(fileQuad,"%d\n",nb_non_nuls);
  for(i=0; i<nbTaches*nbProcesseurs; i++)
  {
    for(j=i; j<nbTaches*nbProcesseurs; j++)
    {
      if (C[i][j]) fprintf(fileQuad,"%d %d %f \n",i+1,j+1, C[i][j]);
    }
  }
  fclose(fileQuad);
}

void ecritureConstr(char *filename)
{
  FILE *fileConstr;
  char *buf;
  int i,j,p,k,t;
  int compt = 0;

  buf = malloc((strlen(filename)+2)*sizeof(char));
  sprintf(buf,"%s",filename);
  sprintf(&(buf[strlen(filename)-3]),"eq");
  fileConstr = fopen(buf,"w");
  if(fileConstr == NULL){
    printf("Error while creating the constraints file (equalities)\n");
    exit(-1);
  }

  fprintf(fileConstr,"l=\n\n");
  fprintf(fileConstr,"%d %d\n\n",nbProcesseurs*nbTaches,nbTaches);
  for(i=0; i<nbTaches; i++){
    fprintf(fileConstr,"1\n%d\n",nbProcesseurs);
    for(j=0; j<nbProcesseurs; j++){
      fprintf(fileConstr,"%d 1 ",i*nbProcesseurs+j+1);
    }
    fprintf(fileConstr,"\n\n");
  }
  fclose(fileConstr);

  sprintf(&(buf[strlen(filename)-3]),"ineq");
  fileConstr = fopen(buf,"w");
  if(fileConstr == NULL){
    printf("Error while creating the constraints file (inequalities)\n");
    exit(-1);
  }

  fprintf(fileConstr,"l<>\n\n");
  fprintf(fileConstr,"%d %d\n\n",nbProcesseurs*nbTaches,nbProcesseurs);
  for(i=0; i<nbProcesseurs; i++){
    fprintf(fileConstr,"0 %d\n",capasProcesseurs[i]);
    fprintf(fileConstr,"%d\n",nbTaches);
    for(j=0; j<nbTaches; j++){
      fprintf(fileConstr,"%d %d ",j*nbProcesseurs+i+1,taillesTaches[j]);
    }
    fprintf(fileConstr,"\n\n");
  }
  fclose(fileConstr);

}

int main(int argc,char **argv)
{
  char buf[100];

  if(argc != 2){
    printf("\n\t Usage : task2pb <filename.dat>\n");
    exit(-1);
  }

  fp = fopen(argv[1],"r");
  if(fp == NULL){
    printf("Error while opening %s\n",argv[1]);
    exit(-2);
  }

  lectureFichier();
  sprintf(buf,"%s",argv[1]);
  sprintf(buf+(strlen(argv[1])-4),".pb");
  ecriturePb(buf);
  sprintf(buf+(strlen(argv[1])-4),".l");
  ecritureLin(buf);
  sprintf(buf+(strlen(argv[1])-4),".C");
  ecritureQuad(buf);
  ecritureConstr(argv[1]);
  return 0;
}
