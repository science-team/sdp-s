/* -*-c++-*-
 * 
 *    Source     $RCSfile: memarray.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:54 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __MEMARRAY_H__
#define __MEMARRAY_H__

class Memarray{
    private:

      class Entry{
          public:
              Entry *next;
              char *addr;
              long size;
              int index;
                   
              Entry(){next=0; addr=0; size=0;}
              ~Entry(){delete[] addr;}
      };

      long max_entries;
      long max_sizes;
      long max_addr_entr;
      unsigned long addr_mask;
      unsigned long in_use;

      Entry first_taken;
      Entry first_empty;
      Entry* entry_store;
      Entry* first_free;
      Entry* first_used;

      int size_index(register long size); //compute index for size class to "size"
      long index_size(register int index); //compute size of size class "index"
      int addr_index(register const char *addr);
          //compute index into address class to retrieve entry of "addr"
      int get_more_entries();

    public:
      Memarray(long number_of_entries,int number_of_sizes,int address_bits);
      ~Memarray();
      unsigned long get_in_use() const {return in_use;}

      long get(register long size,char *& addr);
      long get(long size,double *& addr)
          {return get(size*long(sizeof(double)),(char*&)addr)/long(sizeof(double));}
      long get(long size,float *& addr)
          {return get(size*long(sizeof(float)),(char*&)addr)/long(sizeof(float));}
      long get(long size,long *& addr)
          {return get(size*long(sizeof(long)),(char*&)addr)/long(sizeof(long));}
      long get(long size,int *& addr)
          {return get(size*long(sizeof(int)),(char*&)addr)/long(sizeof(int));}
      int free(register void *addr);
};

class Memarrayuser
{
 protected: static Memarray memarray;
 public:
   Memarrayuser(){}
   virtual ~Memarrayuser(){};   
};

template<class T>
inline int mem_provide(Memarray& memarray,long provide,long in_use,long& avail,T*& store)
{
 if (provide<avail) return 0;
 T* tmpstore;
 long tmpavail=memarray.get(((provide>2*avail)?provide:2*avail)*long(sizeof(T)),(char *&)tmpstore)/long(sizeof(T));
 if ((tmpstore==0)&&(provide<2*avail)){
     tmpavail=memarray.get(provide*long(sizeof(T)),(char *&)tmpstore)/long(sizeof(T));
 }
 if (tmpstore==0) return 1;
 long i;
 for(i=0;i<in_use;i++){
     tmpstore[i]=store[i];
 }
 memarray.free((void *)store);
 store=tmpstore;
 avail=tmpavail;
 return 0;
}

template<class T>
inline int mem_provide_init0(Memarray& memarray,long provide,long& avail,T*& store)
{
 if (provide<avail) return 0;
 T* tmpstore;
 long tmpavail=memarray.get(((provide>2*avail)?provide:2*avail)*long(sizeof(T)),(char *&)tmpstore)/long(sizeof(T));
 if ((tmpstore==0)&&(provide<2*avail)){
     tmpavail=memarray.get(provide*long(sizeof(T)),(char *&)tmpstore)/long(sizeof(T));
 }
 if (tmpstore==0) return 1;
 long i;
 for(i=0;i<avail;i++){
     tmpstore[i]=store[i];
 }
 for(;i<tmpavail;i++){
     tmpstore[i]=0;
 }
 memarray.free((void *)store);
 store=tmpstore;
 avail=tmpavail;
 return 0;
}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: memarray.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/memarray.h,v $
 --------------------------------------------------------------------------- */
