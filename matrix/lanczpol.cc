/* -*-c++-*-
 * 
 *    Source     $RCSfile: lanczpol.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:19 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: lanczpol.cc,v 1.1.1.1.2.1 2000/02/28 13:55:19 bzfhelmb Exp $"

#include <cmath>
#include "mymath.h"
#include "lanczpol.h"

// *************************************************************************
//                              constructor(s)
// *************************************************************************

Lanczpol::Lanczpol()
{
 ierr=0;
 eps=1.e-5;
 minval=0.;
 maxop=-1;       //flag for choose default
 maxiter=-1;     //flag for infinite
 guessmult=10;
 maxguessiter=1;
 do_out=0;
 myout=&cout;
 choicencheb=-1;
 nchebit=20;
 polval=1000;
 iter=0;
 nmult=0;
 blocksz=0;
 neigfound=0;
 choicenbmult=-1;
 nblockmult=20;
 retlanvecs=-1;
 stop_above=0;
 upper_bound=0.;

 nlanczvecs=0;
 mcheps=eps_Real;
 //while(1.+mcheps>1.) mcheps/=2.;
 //mcheps*=2.;

 ncalls=0;
 
 time_mult=Microseconds(long(0));    
 time_mult_sum=Microseconds(long(0));
 time_iter=Microseconds(long(0));
 time_sum=Microseconds(long(0));
}

Lanczpol::~Lanczpol()
{
}

// *************************************************************************
//                            get_lanczosvecs
// *************************************************************************

int Lanczpol::get_lanczosvecs(Matrix& val,Matrix& vecs) const
{
 if (nmult==0) return 1;
 Integer n,m;
 X.dim(n,m);
 m=min(m,nlanczvecs);
 if (retlanvecs>0) m=min(m,retlanvecs);
 val.init(m,1,d.get_store());
 vecs.init(n,m,X.get_store());
 return 0;
}
 

// *************************************************************************
//                              compute
// *************************************************************************

int Lanczpol::compute(Lanczosmatrix* bigmat,Matrix& eigval,Matrix& eigvec,
                     Integer nreig,Integer in_blocksz,Integer maxj)
{
  ncalls++;
 
 //--- check and set input parameters
 bigmatrix=bigmat;
 Integer n;
 n=bigmatrix->lanczosdim();
 
 if (nreig<=0) return 0;
 else nreig=min(nreig,n);
 
 if (choicenbmult<=0) nblockmult=15;
 else nblockmult=min(max(Integer(6),choicenbmult),n/2);

 if (choicencheb<0) nchebit=0;
 else nchebit=choicencheb;

 if (in_blocksz<=0) blocksz=1;
 else blocksz=min(in_blocksz,max(Integer(1),n/nblockmult));

 neigfound=0;
 time_mult_sum=Microseconds(long(0));
 time_sum=Microseconds(long(0));
 myclock.start();

 //--- initialize size parameters
 mymaxj=neigfound+max((3*nblockmult)/2*blocksz+nreig,2*(guessmult+1));
 if ((choicenbmult<0)&&(choicencheb<0)) {
   Integer k; 
   if (n<300) k=50; //expect to need this number of Lanczos steps per restart
   else if (n<1000) k=100;
   else k=200;
   Integer multflops=bigmat->lanczosflops();
   //cout<<"\n mult/dim="<<double(multflops)/double(n);
   if (multflops<k*n){
     //start with small Chebychev
     nchebit=20;
     nblockmult=min(Integer(15),n/2);
   }
   else {
     nchebit=0;
     nblockmult=min(Integer(50),n/2);
   }
   //cout<<" nchebit="<<nchebit<<" nblockmult="<<nblockmult<<endl;
   mymaxj=max(mymaxj,neigfound+min(Integer(200),n/2));
 }
 maxj=mymaxj;
 Integer maxmult=maxop;
 
 X.newsize(n,maxj); chk_set_init(X,1);
 C.newsize(maxj,maxj); chk_set_init(C,1);
 d.newsize(maxj,1); chk_set_init(d,1);
 e.newsize(maxj,1); chk_set_init(e,1);
 u.newsize(n,max(blocksz,Integer(2))); chk_set_init(u,1);
 v.newsize(n,max(blocksz,Integer(2))); chk_set_init(v,1);
 w.newsize(n,max(blocksz,Integer(2))); chk_set_init(w,1);

 nmult=0;
 iter=0;
 errc=0.;
 maxval=-1e40;
 //do_out=1;

 //--- generate starting vectors from proposed vectors
 Integer m,i,nproposed;
 eigvec.dim(m,nproposed);
 if (m==n){
     for(i=0;i<n*neigfound;i++)
         X(i)=eigvec(i);
     nproposed-=neigfound;
     //- copy the proposed vectors that fit into storage
     nproposed=min(nproposed,maxj-1-neigfound);
     for(i=n*neigfound;i<n*(neigfound+nproposed);i++)
         X(i)=eigvec(i);
     //- if there are still too many, take the eigenvectors
     //- to the largest eigenvalues of the projected matrix
     if (nproposed>blocksz){
         orthog(neigfound,nproposed,X,C);
	 time_mult=Microseconds(long(0)); 
         sectn(X,neigfound,nproposed,C,d,u,v,minval);
         time_mult_sum+=time_mult;
         nmult+=nproposed;
         if (nproposed>=3){
             maxval=sum(d(Range(neigfound,neigfound+nproposed-1)))/nproposed;
         }
         else {
             maxval=d(neigfound+1);
         }
	 minval=min(d);
         rotate(neigfound,nproposed,blocksz,C,X,v);
     }
     nproposed=blocksz;
     //- perturb the vectors a little bit
     Real randfac=0.05/sqrt((double)n);
     for(i=n*neigfound;i<n*(neigfound+blocksz);i++)
         X(i)+=randfac*(2.*mat_randgen.next()-1.);
 }
 else {
     neigfound=0;
     nproposed=0;
 }
     
 //--- compute guess for interval of Chebychev polynomial
 if (nchebit>0){
     if ((minvec.rowdim()==n)&&(minvec.coldim()==1)){
         nproposed=min(nproposed,maxj-2-neigfound);
         for(i=0;i<n;i++)
             X(n*(neigfound+nproposed)+i)=minvec(i);
         nproposed+=1;
     }
     ierr=guess_extremes(nproposed);
     minval= d(neigfound+1);
     minvec= X.col(neigfound+1);
     //fill remaining vectors again with starting vectors
     if (m==n){
         eigvec.dim(m,nproposed);
         nproposed-=neigfound;
         Real randfac=0.05/sqrt((double)n);
         nproposed=min(nproposed,blocksz-1);
         for(i=n*(neigfound+1);i<n*(neigfound+1+nproposed);i++)
             X(i)=eigvec(i)+randfac*(2.*mat_randgen.next()-1.);
         nproposed+=1;
     }
 }
 
 //--- call lanczos
 if (do_out) {
     myout->precision(8);
     (*myout)<<" maxval="<<maxval<<" minval="<<minval<<" ncheb="<<nchebit;
     (*myout)<<" nblock="<<nblockmult<<" relprec="<<eps<<endl;
 }
 ierr=dhua(nreig,nproposed,maxmult);
 if (ierr>0){
   time_sum=myclock.time();     
   return 1;
 }

 //--- store the parameters now known
 eigvec.init(n,neigfound,X.get_store());
 eigval.init(neigfound,1,d.get_store());
 time_sum=myclock.time();  
 return (ierr!=0);
}

// *****************************************************************************
//                                guess_extremes
// *****************************************************************************

//iterates a few time in order to obtain a good guess for minimal and
//maximal eigenvalue. These will later be used to determine the
//interval of the Chebychef polynomial.
//The routine performs a "guessmult" steps Lanczosapproximation and
//returns the largest value in d(neigfound) and the smallest in d(neigfound+1)

int Lanczpol::guess_extremes(Integer nproposed)
{

 Integer n,q;
 X.dim(n,q);
 Integer blocksz=2;
 time_mult=Microseconds(long(0)); 

 //fill empty starting vectors with random vectors
 for(Integer i=neigfound+nproposed;i<neigfound+blocksz;i++){ 
     random(X,i);
 } 
  
 orthog(neigfound,max(blocksz,nproposed),X,C);
 sectn(X,neigfound,max(blocksz,nproposed),C,d,u,v,minval);
 nmult+=max(nproposed,blocksz);
 rotate_extremes(neigfound,max(blocksz,nproposed),d,C,X,v);

 time_mult_sum+=time_mult;
 
 do {
     if (do_out){
         (*myout)<<"G: ";myout->width(2);(*myout)<<iter<<":";
         (*myout)<<"  blocksz=";myout->width(2);(*myout)<<blocksz;
	 (*myout)<<"  s=";myout->width(2);(*myout)<<guessmult;
         (*myout)<<"  neigfound=";myout->width(2);(*myout)<<neigfound;
	 (*myout)<<"  nmult=";myout->width(4);(*myout)<<nmult;
         (*myout)<<"  minval="<<minval<<endl;
     }

     iter++;
     Microseconds secs=myclock.time();
     time_mult=Microseconds(long(0)); 
   
     Integer sbs=guessmult*blocksz;
     bklanc(neigfound,blocksz,guessmult,d,C,X,e,u,v);
     nmult+=sbs;
     eigen(neigfound,blocksz,sbs,C,d,u,v,minval);
     maxval=max(maxval,(d(1)+d(2))/2.);
     if (do_out){
         (*myout)<<"GE: blocksz="<<blocksz;
         (*myout)<<"  nmult=";myout->width(4);(*myout)<<nmult<<"\n  ";
         (*myout)<<neigfound<<":";
         for(Integer i=0;i<neigfound;i++) (*myout)<<" "<<d(i);
         (*myout)<<"\n  "<<sbs<<":";
         {for(Integer i=0;i<sbs;i++) (*myout)<<" "<<d(neigfound+i);}
         (*myout)<<endl;
     }   
     Integer nconv;
     cnvtst(neigfound,blocksz,errc,eps,d,e,nconv);
     rotate_extremes(neigfound,sbs,d,C,X,v);
     
     time_mult_sum+=time_mult;
     time_iter=myclock.time()-secs;
 }while(iter<maxguessiter);

 if (do_out){
         (*myout)<<"G__";myout->width(2);(*myout)<<iter<<":";
         (*myout)<<"  blocksz=";myout->width(2);(*myout)<<blocksz;
	 (*myout)<<"  s=";myout->width(2);(*myout)<<guessmult;
         (*myout)<<"  neigfound=";myout->width(2);(*myout)<<neigfound;
	 (*myout)<<"  nmult=";myout->width(4);(*myout)<<nmult;
     (*myout)<<"  guessvals:";
     for(Integer i=0;i<neigfound+blocksz;i++) (*myout)<<" "<<d(i);
     (*myout)<<"  minval="<<minval<<endl;
 }

 return 0;
}

// *****************************************************************************
//                                dhua
// *****************************************************************************

// X       Matrix(n,q), n dimension der sym Matrix.
// pinit      initial block size
// nreig      number of eigenvalues to be computed
// maxmult    maximal number of matrix vector computations 
// neigfound  number of eigenvalues found (initially zero unless some known)
// nmult      counts number of matrix vector computations
// iter       number of program iterations
// nchebit    number of chebychev iterations
// randseed   
// eps        relative precision for eigenvalues
// errc       accumulated error, set to 0. initally
// minval         block Cheb parameter (best least eigenval of A), 0. ok

/*
c on input: x (of size (n,q) contains eigenvector estimates
c on output: vec ... eigenvector
c            lambda ... eigenvalue
c	     ie (integer): 0 if everything ok, else 1
c
C       This program implements the Block Chebychev-Lanczos method
C     for computing the largest R eigenvalues and eigenvectors of
C     an NxN symmetric matrix A. If the least R eigenvalues and
C     corresponding eigenvectors of A are found, then the Block
C     Chebychev-Lanczos method is applied to symmetric matrix -A. 
C
      INTEGER n,Q,PINIT,R,S,PS,P,ZZ, nmax, ie
	parameter( nmax = 2001, q = 20)
	double precision x(nmax,q), c(q,q),u(nmax),v(nmax),w(nmax)
	double precision lambda, vec(n), vecin( n)
	double precision EPS,ERRC,AF,D(q),E(q)     
      EXTERNAL OP 
C 
C     Description of parameters:
C
C       N: Integer variable. The order of the symmetric matrix A.
C
C       Q: Integer variable. The number of column vectors of length N 
C          contained in array X.     
C
C       PINIT: Integer variable. The initial block size to be used in
C              the Block Chebychev-Lanczos method.
C
C       R: Integer variable. The number of eigenvalues and eigenvectors
C          being computed.
C
C       MMAX: Integer variable. The maximum number of matrix-vector 
C             products A*X where X is a vector. MMAX should be given
C             a very large value.
C
C       M: Integer variable. M gives the number of eigenvalues and
C          eigenvectors already computed. Thus, initially, M should be
C          zero. If M is greater than zero, then columns one through M
C          of the array X are assumed to contain the computed 
C          approximations to the eigenvectors corresponding to the
C          largest M eigenvalues. At exit, M contains a value equal to 
C          the total number of eigenvalues computed including any
C          already computed when the program was entered. Thus, at exit,
C          the first M elements of D and the first m columns of X will
C          contain approximations to the largest M eigenvalues and 
C          corresponding eigenvectors of A, respectively.
C
C       IE: Integer variable. The value of IE indicates whether the
C           program terminated successfully. If IE=1, the value of
C           MMAX was exceeded before R eigenvalues were computed.
C
C       IMM: Integer variable. IMM counts the number of matrix-vector
C            products computed which is the number of times the 
C            Subroutine named by OP is called. Thus, initially, IMM
C            must be set to zero.
C
C       ITER: Integer variable. ITER counts the number of the program
C             iterations. Thus, initially, ITER must be set to zero.
C
C       IM: Integer variable. IM is the number of internal cycle in
C           one Block Chebychev iteration. Commonly, IM is taken as
C           the value between 20 and 50.
C
C       ZZ: Integer variable. ZZ is used to produce random number.
C           ZZ may be taken as any value. Commonly set ZZ=0 or 1.
C
C       EPS: Double precision variable. Initially, EPS should contain
C            a value indicating the relative precision to which the
C            program will attempt to compute the eigenvalues and
C            eigenvectors of matrix A. For eigenvalues less in modulus
C            than 1, EPS will be an absolute tolerance.
C
C       ERRC: Double precision variable. ERRC measures the accumuted
C             error in the eigenvalues and eigenvectors. Initially,
C             ERRC must be set to zero.
C
C       AF: Double precision variable. AF is a parameter in the Block
C           Chebychev iteration. It is the best to take AF as the
C           least eigenvalue of matrix A. Initially, AF may be taken
C           as zero.
C
C       X: Double precision variable. X contains the computed eigenvectors.
C          X should be an array containing at least N*Q elements. X is
C          used not only to store the eigenvectors computed by the
C          program, but also as working storage for the Block Chebychev-
C          Lanczos method. At exit, the first M columns of X contain
C          the corresponding eigenvector approximations.
C 
C      OP: Subroutine name. The actual argument corresponding to OP
C          should be the name of a subroutine used to define the matrix A.
C          This subroutine should have three arguments N, U and V, where
C          N is the order of the matrix A, U and V are two one dimensional
C          arrays of length N. Then the statement 
C                          CALL OP(N,U,V)
C          should result in vector A*U being computed and stored in V.
C
*/

int Lanczpol::dhua(Integer nreig,Integer nproposed, Integer maxmult)
{
 Integer n,q;
 X.dim(n,q);
 time_mult=Microseconds(long(0)); 

 nproposed=min(blocksz,nproposed);
 for(Integer i=neigfound+nproposed;i<neigfound+blocksz;i++){ 
     random(X,i);
 } 
 
 orthog(neigfound,blocksz,X,C);

 time_mult_sum+=time_mult;
 
 Integer nconv;
 Integer sbs;
 do {

     iter++;
     Microseconds secs=myclock.time();
     time_mult=Microseconds(long(0));

     if (do_out){
         (*myout)<<"L: ";myout->width(2);(*myout)<<iter<<":";
         (*myout)<<"  blocksz=";myout->width(2);(*myout)<<blocksz;
	 (*myout)<<"  s=";myout->width(2);(*myout)<<nblockmult;
         (*myout)<<"  neigfound=";myout->width(2);(*myout)<<neigfound;
	 (*myout)<<"  nmult=";myout->width(4);(*myout)<<nmult;
         if (nchebit) (*myout)<<"  nchebit="<<nchebit<<" maxval="<<maxval;
         if (stop_above) (*myout)<<" stop>"<<upper_bound;
         (*myout)<<endl;
     }

     sbs=nblockmult*blocksz;
     bklanccheb(neigfound,blocksz,nblockmult,C,X,e,v);
     nmult+=sbs*max(Integer(1),nchebit);
     Real dummy=-10.;
     eigen(neigfound,blocksz,sbs,C,d,u,v,dummy);
     if (do_out){
         (*myout)<<"LE: blocksz="<<blocksz<<"\n  "<<neigfound<<":";
         for(Integer i=0;i<neigfound;i++) (*myout)<<" "<<d(i);
         (*myout)<<"\n  "<<sbs<<":";
         {for(Integer i=0;i<sbs;i++) (*myout)<<" "<<d(neigfound+i);}
         (*myout)<<endl;
     }
     
     if (nchebit>0){
         //sectn(X,0,neigfound+blocksz,C,d,u,v,minval);
         rotate(neigfound,sbs,sbs,C,X,v);
         sectn(X,neigfound,sbs,C,d,u,v,minval);
         //maxval=min(d(neigfound+min(Integer(4),sbs)),
	 //	    minval+(d(neigfound)-minval)/(1.+1./Real(nchebit)));
         nmult+=sbs;
	 if (do_out){
	   (*myout)<<"  {"<<sbs<<":";
	   {for(Integer i=0;i<sbs;i++) (*myout)<<" "<<d(neigfound+i);}
	   (*myout)<<"}\n  [";
	   {for(Integer i=0;i<sbs;i++) (*myout)<<" "<<e(neigfound+i);}
	   (*myout)<<"]"<<endl;
	 }
	 cnvtst(neigfound,sbs,errc,eps,d,e,nconv);
     }
     else {
         cnvtst(neigfound,blocksz,errc,eps,d,e,nconv);
     }
     nlanczvecs=neigfound+sbs;
     neigfound+=nconv;

     if (neigfound>=nreig) {
       time_iter=myclock.time()-secs;
       time_mult_sum+=time_mult;
       break;
     } 
              
     if ((nchebit>0)&&(nconv>0)) maxval=d(min(nreig+1,nlanczvecs-1));

     if ((stop_above)&&
         (nlanczvecs>=nreig)&&
         (upper_bound<d(nreig-1))){
         time_iter=myclock.time()-secs;
	 time_mult_sum+=time_mult;
         break;
     }

     if ((maxmult>=0)&&(nmult>maxmult)){
         if (do_out){
             (*myout)<<"\nLanczpol: nmult>maxmult: "<<nmult<<">"<<maxmult<<endl;
             (*myout)<<"         neigfound="<<neigfound<<"  nreig="<<nreig<<endl;
             (*myout)<<"         iter="<<iter<<"  s="<<nblockmult<<"  blocksz="<<blocksz<<endl;
             d.display(*myout);
         }
         rotate(neigfound-nconv,sbs,sbs,C,X,v);
	 time_iter=myclock.time()-secs;
	 time_mult_sum+=time_mult;
         return 1;
     }
     if ((maxiter>=0)&&(iter>=maxiter)){
         if (do_out){
             (*myout)<<"\nLanczpol: iter>maxiter: "<<iter<<">"<<maxiter<<endl;
             (*myout)<<"         neigfound="<<neigfound<<"  nreig="<<nreig<<endl;
             (*myout)<<"         iter="<<iter<<"  s="<<nblockmult<<"  blocksz="<<blocksz<<endl;
             d.display(*myout);
         }
         rotate(neigfound-nconv,sbs,sbs,C,X,v);
	 time_iter=myclock.time()-secs;
	 time_mult_sum+=time_mult;
         return 2;
     }

     rotate(neigfound-nconv,sbs,nconv+blocksz,C,X,v);
       
     //if (nconv>0){
       //perturb non block vectors slightly to get out of subspace
     //  Real randfac=0.5/sqrt(n);
     //  for(Integer i=n*(neigfound+max(Integer(0),blocksz-nconv));i<n*(neigfound+blocksz);i++)
     //    X(i)+=randfac*(2.*mat_randgen.next()-1.);
     //  orthog(neigfound+max(Integer(0),blocksz-nconv),blocksz-max(Integer(0),blocksz-nconv),X,C);
     //}       

    //--- determine new nblockmult and ncheb if default
    time_iter=myclock.time()-secs;
    time_mult_sum+=time_mult;
    //cout<<" L"<<iter<<" nblm="<<nblockmult<<" nch="<<nchebit<<" bls="<<blocksz;
    //cout<<" stm="<<time_mult_sum<<" sti="<<myclock.time()<<setprecision(2);
    //cout<<" rel="<<double(time_mult_sum)/max(double(myclock.time()),eps_Real)<<endl;
    if ((choicenbmult<0)&&(choicencheb<0)){
      if (nchebit>0){
	if ((iter==2)||(nconv>0)){ 
	  nchebit=30;
	  nblockmult=min(Integer(25),(mymaxj-neigfound)/blocksz);
	  continue;
	}
	if (iter%2==0){ 
	  nchebit=min(nchebit+10,Integer(200));
	}  
	continue;
      }
      if (nconv>0) {
	nblockmult=min(Integer(50),(mymaxj-neigfound)/blocksz);
	nblockmult=min(nblockmult,max(n/2,min(n,Integer(6))));
      }
      else if (iter%2==0){
	  nblockmult=min(nblockmult+50,(mymaxj-neigfound)/blocksz);
          nblockmult=min(nblockmult,max(n/2,min(n,Integer(6))));
      }
      continue;
    }
    if ((iter==2)&&(choicenbmult<=0)){
      nblockmult*=3;
      nblockmult/=2;
      nblockmult=min(nblockmult,(mymaxj-neigfound)/blocksz);
      nblockmult=min(nblockmult,max(n/2,min(n,Integer(6))));
    }
      
 }while(1);

 rotate(neigfound-nconv,sbs,sbs,C,X,v);

 if (do_out){
   (*myout)<<"L__";myout->width(2);(*myout)<<iter<<":";
   (*myout)<<"  blocksz=";myout->width(2);(*myout)<<blocksz;
   (*myout)<<"  s=";myout->width(2);(*myout)<<nblockmult;
   (*myout)<<"  neigfound=";myout->width(2);(*myout)<<neigfound;
   (*myout)<<"  nmult=";myout->width(4);(*myout)<<nmult;
   (*myout)<<"  minval="<<minval<<endl;
   (*myout)<<"LE: blocksz="<<blocksz<<"\n  "<<neigfound<<":";
   for(Integer i=0;i<neigfound;i++) (*myout)<<" "<<d(i);
   (*myout)<<"\n other vecs("<<nlanczvecs-neigfound<<"):";
   {for(Integer i=neigfound;i<nlanczvecs;i++) (*myout)<<" "<<d(i);}
   (*myout)<<endl;
 }

 return 0;
}

// *****************************************************************************
//                                bklanc
// *****************************************************************************

//Input: neigfound,blocksz,s,d,X
//output: X,e,C
//usus: u,v

int Lanczpol::bklanc(Integer neigfound,Integer blocksz,Integer s,
                    const Matrix &d,Matrix &C,Matrix &X,
                    Matrix &e,Matrix &u,Matrix &v)
{
 Integer n=X.rowdim();
 Integer i,j,k,l;
 Real t;
 for(l=0;l<s;l++){

     //--- roughly:
     //---  v = A * X(:,block l)
     //---  C = (v' * X(:,block l)'
     //---  X(:,block l+1) = v - X(:,block l)*C
     Integer ll=neigfound+l*blocksz;
     Integer lu=neigfound+(l+1)*blocksz;
     u.newsize(n,blocksz); chk_set_init(u,1);
     v.newsize(n,blocksz); chk_set_init(v,1);
     mat_xey(n*blocksz,u.get_store(),X.get_store()+ll*n);
     Microseconds secs=myclock.time();
     bigmatrix->lanczosmult(u,v);
     time_mult+=myclock.time()-secs; 
     for(k=ll;k<lu;k++){
         //for(i=0;i<n;i++) u(i)=X(i,k);
         if (l==0){
             //for(i=k+1;i<lu;i++) C(i,k)=0.;
             mat_xea(lu-k+1,C.get_store()+k*C.rowdim()+k+1,0.);
             C(k,k)=d(k);
             //--- v = v - X(:,k:lu-1)*C(k:lu-1,k)
             //for(i=0;i<n;i++) v(i)-=d(k)*X(i,k);
             mat_xpeya(n,v.get_store()+(k-ll)*n,X.get_store()+n*k,-d(k));
         }
         else {
             //--- C(k:lu-1,k)=(v'*X(:,k:lu-1))'
             for(i=k;i<lu;i++){
                 //t=0.; for(j=0;j<n;j++) t+=v(j)*X(j,i);
                 C(i,k)=mat_ip(n,v.get_store()+(k-ll)*n,X.get_store()+n*i);
             }
             //--- v = v - X(:,k-blocksz:lu-1)*C(k-blocksz:lu-1,k)
             for(i=0;i<n;i++){
                 //t=0.;for(j=k-blocksz;j<=k;j++) t+=X(i,j)*C(k,j);
                 //for(j=k+1;j<lu;j++) t+=X(i,j)*C(j,k);
                 t=mat_ip(blocksz+1,X.get_store()+(k-blocksz)*n+i,n,
                          C.get_store()+(k-blocksz)*C.rowdim()+k,C.rowdim());
                 t+=mat_ip(lu-k-1,X.get_store()+(k+1)*n+i,n,
                           C.get_store()+k*C.rowdim()+k+1,1);
                 v(i,k-ll)-=t;
             }
         }
         if (l==s-1) continue;
         //for(i=0;i<n;i++) X(i,k+blocksz)=v(i);
         mat_xey(n,X.get_store()+(k+blocksz)*n,v.get_store()+(k-ll)*n);
     }
     if (l==0) err(neigfound,blocksz,X,e);
     if (l==s-1) continue;
     orthog(lu,blocksz,X,C);  //orthogonalize new vectors
     //--- copy R (C) of QR factorization to correct position
     Integer it=lu-1;
     for(j=0;j<blocksz;j++){
         it++;
         //for(i=lu;i<=it;i++) C(i,it-blocksz)=C(i,it);
         mat_xey(it-lu+1,C.get_store()+(it-blocksz)*C.rowdim()+lu,
                 C.get_store()+it*C.rowdim()+lu);
     }
 }
 return 0;
}


// *****************************************************************************
//                                bklanccheb
// *****************************************************************************

//Input: neigfound,blocksz,s,d,X
//output: X,e,C
//usus: u,v

int Lanczpol::bklanccheb(Integer neigfound,Integer blocksz,Integer s,
                    Matrix &C,Matrix &X,Matrix &e,Matrix &v)
{
 Integer n=X.rowdim();
 Integer i,j,k,l;
 Real t;
 for(l=0;l<s;l++){

     Integer ll=neigfound+l*blocksz;
     Integer lu=neigfound+(l+1)*blocksz;
     blockcheby(ll,X,v);
     for(k=ll;k<lu;k++){
         if (l==0){
             //--- C(k:lu-1,k)=(v'*X(:,k:lu-1))'
             for(i=k;i<lu;i++){
                 C(i,k)=mat_ip(n,v.get_store()+(k-ll)*n,X.get_store()+n*i);
             }
             //--- v = v - X(:,k:lu-1)*C(k:lu-1,k)
             for(i=0;i<n;i++){
                 //for(j=k;j<lu;j++) t+=X(i,j)*C(j,k);
                 t=mat_ip(lu-k,X.get_store()+k*n+i,n,
                           C.get_store()+k*C.rowdim()+k,1);
                 v(i,k-ll)-=t;
             }             
         }
         else {
             //--- C(k:lu-1,k)=(v'*X(:,k:lu-1))'
             for(i=k;i<lu;i++){
                 C(i,k)=mat_ip(n,v.get_store()+(k-ll)*n,X.get_store()+n*i);
             }
             //--- v = v - X(:,k-blocksz:lu-1)*C(k-blocksz:lu-1,k)
             for(i=0;i<n;i++){
                 //t=0.;for(j=k-blocksz;j<=k;j++) t+=X(i,j)*C(k,j);
                 //for(j=k+1;j<lu;j++) t+=X(i,j)*C(j,k);
                 t=mat_ip(blocksz+1,X.get_store()+(k-blocksz)*n+i,n,
                          C.get_store()+(k-blocksz)*C.rowdim()+k,C.rowdim());
                 t+=mat_ip(lu-k-1,X.get_store()+(k+1)*n+i,n,
                           C.get_store()+k*C.rowdim()+k+1,1);
                 v(i,k-ll)-=t;
             }
         }
         if (l==s-1) continue;
         //for(i=0;i<n;i++) X(i,k+blocksz)=v(i);
         mat_xey(n,X.get_store()+(k+blocksz)*n,v.get_store()+(k-ll)*n);
     }
     if (l==0) err(neigfound,blocksz,X,e);
     if (l==s-1) continue;
     orthog(lu,blocksz,X,C);  //orthogonalize new vectors
     //--- copy R (C) of QR factorization to correct position
     Integer it=lu-1;
     for(j=0;j<blocksz;j++){
         it++;
         //for(i=lu;i<=it;i++) C(i,it-blocksz)=C(i,it);
         mat_xey(it-lu+1,C.get_store()+(it-blocksz)*C.rowdim()+lu,
                 C.get_store()+it*C.rowdim()+lu);
     }
 }
 return 0;
}


// *****************************************************************************
//                                err
// *****************************************************************************

// e(k)=norm_2(X(:,k))   k=neigfound+blocksz:neigfound+2*blocksz-1;
//Input:  neigfound,blocksz,X
//Output: e

int Lanczpol::err(Integer neigfound,Integer blocksz,const Matrix& X,Matrix &e)
{
 Integer n=X.rowdim();
 for(Integer k=neigfound+blocksz;k<neigfound+2*blocksz;k++){
     //t=0.; for(i=0;i<n;i++) t+=X(i,k)*X(i,k);
     Real t=mat_ip(n,X.get_store()+n*k,X.get_store()+n*k);
     e(k-blocksz)=sqrt(t);
 }
 return 0;
}

// *****************************************************************************
//                                cnvtst
// *****************************************************************************

//Input: neigfound, blocksz,errc,eps,d,e
//output: nconv,errc

int Lanczpol::cnvtst(Integer neigfound,Integer blocksz,Real& errc,Real eps,
                    const Matrix& d,const Matrix &e,Integer &nconv)
{
 //const Real mcheps=2.22e-16;
 Integer n=d.dim();
 Integer i,k=0;
 Real t=1.;
 for(i=0;i<neigfound;i++){t=max(t,my_abs(d(i)));}
 for(i=0;i<blocksz;i++){
     t=max(t,my_abs(d(i+neigfound)));
     if (e(i+neigfound)>t*(eps+10.*Real(n)*mcheps)+errc) break;
     k=i+1;
 }
 nconv=k;
 if (k==0) return 0;
 t=0.;
 for (i=0;i<k;i++){
     t+=e(i+neigfound)*e(i+neigfound);
 }
 errc=sqrt(errc*errc+t);
 return 0;
}
               
 
// *****************************************************************************
//                                eigen
// *****************************************************************************

//Input: neigfound,blocksz,sbsz,C,minval
//Output: C,d,minval

int Lanczpol::eigen(Integer neigfound,Integer blocksz,Integer sbsz,Matrix& C,
                   Matrix& d,Matrix& u,Matrix& v,Real& minval)
{
 Integer i,j;
 Integer lim;
 for(i=0;i<sbsz;i++){
     lim=i-blocksz;
     if (i<blocksz) lim=0;
     for(j=0;j<lim;j++)             
         C(i,j)=0.;               //C(i,0:lim)=0
     for(j=lim;j<=i;j++)
         C(i,j)=C(i+neigfound,j+neigfound);
 }
 tred2(sbsz,C,u,v,C);
 if ((i=tql2(sbsz,u,v,C))){
     (*myout)<<"\n"<<i<<endl;
     error("eigen failed on computing tql2");
 }
 for(i=0;i<sbsz;i++){
     d(i+neigfound)=u(i);
 }
 minval=min(minval,d(neigfound+sbsz-1));
 return 0;
}

// *****************************************************************************
//                                sectn
// *****************************************************************************



//Input: X,neigfound,blocksz,minval,
//Output: X,minval,d

int Lanczpol::sectn(Matrix& X,Integer neigfound,Integer blocksz,
                   Matrix& C,Matrix& d,Matrix& u,Matrix& v,
                   Real& minval)
{
 Integer n=X.rowdim();

 //---  C=X'*A*X
 //    lower triangle of C(neigfound+1:neigfound+blocksz,neigfound:nef+blocksz)
 //    X is only X(:,neigfound+1,neigfound:blocksz)
 Integer col1,col2;
 col1=neigfound-1;
 u.newsize(n,blocksz);
 v.newsize(n,blocksz);
 w.newsize(n,1);
 chk_set_init(u,1);
 chk_set_init(v,1);
 chk_set_init(w,1);
 mat_xey(n*blocksz,u.get_store(),X.get_store()+neigfound*n);
 Microseconds secs=myclock.time();
 bigmatrix->lanczosmult(u,v);
 time_mult+=myclock.time()-secs;
 for(Integer j=0;j<blocksz;j++){
     col1++;
     col2=neigfound-1;
     for(Integer i=0;i<=j;i++){
         col2++;
         C(col1,col2)=mat_ip(n,v.get_store()+j*n,X.get_store()+col2*n);
         if (i==j){
             mat_xeya(n,w.get_store(),X.get_store()+col2*n,C(col1,col2));
             mat_xmey(n,w.get_store(),v.get_store()+j*n);
             e(neigfound+i)=norm2(w);
         }
             
     }
 }

 //--- diagonalize shifted C
 //==> diag d, C trafo matrix, minval updated small eig est 
 eigen(neigfound,blocksz,blocksz,C,d,u,v,minval);
 //cout<<"    #";
 //for(i=0;i<min(blocksz,Integer(3));i++){
 //    cout<<" ["<<d(neigfound+i)<<","<<e(neigfound+i)<<"]";
 //}
 //cout<<endl;

 return 0;
}


// *****************************************************************************
//                                rotate_extremes
// *****************************************************************************

//rotates the eigenvectors of the largest and smallest eigenvalues of
//the tridiagonal matrix.
// X(:,neigfound[+0,+sbs-1])=X(:,neigfound:neigfound+sbs-1)*C(:,[0,sbs-1])

// Input: neigfound,sbs,C,X
// Output: X
// uses: v as storage

int Lanczpol::rotate_extremes(Integer neigfound,Integer sbs,Matrix& d,const Matrix& C,Matrix &X,Matrix& v)
{
 Integer q=C.rowdim();
 Integer n=X.rowdim();
 for(Integer i=0;i<n;i++){ //for each row
     Real *xi=X.get_store()+neigfound*n+i;
     v(0)=mat_ip(sbs,xi,n,C.get_store(),1);            //rotate largest EV
     v(1)=mat_ip(sbs,xi,n,C.get_store()+(sbs-1)*q,1);  //rotate smallest EV
     mat_xey(2,xi,n,v.get_store(),1);
 }
 d(neigfound+1)=d(neigfound+sbs-1);
 return 0;
}
 
           
// *****************************************************************************
//                                rotate
// *****************************************************************************

// X(:,neigfound:neigfound+l-1)=X(:,neigfound:neigfound+sbs-1)*C

// Input: neigfound,sbs,l,C,X
// Output: X
// uses: v as storage

int Lanczpol::rotate(Integer neigfound,Integer sbs,Integer l,
                    const Matrix& C,Matrix &X,Matrix& v)
{
 Integer q=C.rowdim();
 Integer n=X.rowdim();
 for(Integer i=0;i<n;i++){
     Real *xi=X.get_store()+neigfound*n+i; Real *vp=v.get_store();const Real *cp=C.get_store();
     for(Integer k=0;k<l;k++){
         //t=0.; for(j=0;j<sbs;j++) t+=X(i,j+neigfound)*C(j,k);
         *vp++=mat_ip(sbs,xi,n,cp,1); cp+=q;
     }
     //for(k=0;k<l;k++) X(i,k+neigfound)=v(k);
     mat_xey(l,xi,n,v.get_store(),1);
 }
 return 0;
}
 
           
// *****************************************************************************
//                                random
// *****************************************************************************

//assigns a random vector to column j of X

//Input: X,j,randomseed
//Output: X,randomseed

int Lanczpol::random(Matrix& X,Integer j)
{
 Integer n=X.rowdim();
 for(Integer i=0;i<n;i++){
     X(i,j)=2.*mat_randgen.next()-1.;
 }
 return 0;
}

// *****************************************************************************
//                                orthog
// *****************************************************************************

// orthonormalize columns X(:,offset:offset+blocksz-1) also with respect to
// the first offset columns of X (these are already assumed to be orthonormal). 
// On output the upper triangle of B(:,offset:offset+blocksz-1) 
// contains the coefficients of the 
// components (B is upper triangular with diagonal but only for the new vectors)
// uses Gram-Schmidt for the QR factorization
// returns rank deficiency

//Input: X,blocksz,offset
//Output: X,B

Integer Lanczpol::orthog(Integer offset,Integer blocksz,Matrix &X,Matrix& B)
{
 Integer n=X.rowdim();
 int orig;
 Integer  rankdef=0;
 for(Integer k=offset;k<offset+blocksz;k++){
     Real *xk=X.get_store()+k*X.rowdim();
     orig=1;
     Real t,t1;
     do{
         // compute projection on X(:,i) and subtract this 
         t=0.;
	 Real *xi=X.get_store();
         for(Integer i=0;i<k-rankdef;i++){
             //t1=0.; for(j=0;j<n;j++) t1+=X(j,i)*X(j,k);
             t1=mat_ip(n,xi,xk);
             if ((orig)&&(i>=offset)) B(i,k)=t1;
             t+=t1*t1;
             //for(j=0;j<n;j++) X(j,k)-=t1*X(j,i);
             mat_xpeya(n,xk,xi,-t1);
             xi+=n;
         }
         //t1=0.;for(j=0;j<n;j++) t1+=X(j,k)*X(j,k);
         t1=mat_ip(n,xk,xk);
         t+=t1;
         orig=0;
     }while(t1<t/100.);
     t1=sqrt(t1);
     for(Integer i=k-rankdef+1;i<=k;i++) B(i,k)=0.;
     if (t1>eps_Real) {
         B(k-rankdef,k)=t1;
         t1=1./t1;
         //for(j=0;j<n;j++) X(j,k-rankdef)=X(j,k)*t1;
         if (rankdef==0) {
	     mat_xmultea(n,xk,t1);
         }
         else {
	     mat_xeya(n,X.get_store()+(k-rankdef)*X.rowdim(),xk,t1);
         }
     }
     else { //residual is zero -> rank deficiency, eliminate column
         B(k-rankdef,k)=0.;
         rankdef++;
     }
 }  
 //fill up rank deficiency by random columns and orthonormalize them
 for(Integer k=offset+blocksz-rankdef;k<offset+blocksz;k++){
     random(X,k);
     Real *xk=X.get_store()+k*X.rowdim();
     orig=1;
     Real t,t1;
     do{
         // compute projection on X(:,i) and subtract this 
         t=0.;
	 Real *xi=X.get_store();
         for(Integer i=0;i<k;i++){
             //t1=0.; for(j=0;j<n;j++) t1+=X(j,i)*X(j,k);
             t1=mat_ip(n,xi,xk);
             t+=t1*t1;
             //for(j=0;j<n;j++) X(j,k)-=t1*X(j,i);
             mat_xpeya(n,xk,xi,-t1);
             xi+=n;
         }
         //t1=0.;for(j=0;j<n;j++) t1+=X(j,k)*X(j,k);
         t1=mat_ip(n,xk,xk);
         t+=t1;
         orig=0;
     }while(t1<t/100.);
     t1=sqrt(t1);
     if (t1>eps_Real) {
         B(k-rankdef,k)=t1;
         t1=1./t1;
         //for(j=0;j<n;j++) X(j,k)*=t1;
	 mat_xmultea(n,xk,t1);
     }
     else { //random column was again rank deficient, try again
       //(k<n) is guaranteed in the intilialization of the parameters
       k--;
     }
 }  
 return rankdef;
}      

// *****************************************************************************
//                                blockcheby
// *****************************************************************************

//The routine takes the blocksz first vectors of X after colum col_offset
//and applies the matrix cheb polynomial of order nchebit with respect to the
//interval [minval, maxval] to these vectors. The result is returned in v.
//Input: X, col_offset
//global params: maxval,minval,nchebit, blocksz, polval
//output: v
//uses: u,w

int Lanczpol::blockcheby(Integer col_offset,const Matrix& X,Matrix& v)
{
 //T0: u=X(:,col_offset:col_offset+blocksz-1)
 Integer n=X.rowdim();
 u.newsize(n,blocksz); chk_set_init(u,1);
 v.newsize(n,blocksz); chk_set_init(v,1);
 mat_xey(n*blocksz,u.get_store(),X.get_store()+n*col_offset);
 Microseconds secs=myclock.time();
 bigmatrix->lanczosmult(u,v);
 time_mult+=myclock.time()-secs;
 if ((maxval-minval<1e-8)||(nchebit<=0)){
     return 0;
 }

 //determine interval limits [a,b] for polynomial
 if ((nchebit%2)==0) nchebit++;
 Real a=minval-1e-3*(maxval-minval);
 Real b=maxval;
 //Real b=(2*maxval+(cosh(acosh(polval)/nchebit)-1)*a)/(cosh(acosh(polval)/nchebit)+1);
 
 //shift away known eigenvalues
 for(Integer i=0;i<neigfound;i++){
   for(Integer k=0;k<blocksz;k++){
     Real dip=mat_ip(n,u.get_store()+k*n,X.get_store()+i*n);
     mat_xpeya(n,v.get_store()+k*n,X.get_store()+i*n,(-d(i)+(a+b)/2.)*dip);
   }
 }
 
 //T1: v=(a+b)/(a-b)*u-2./(a-b)*v
 mat_xmultea(n*blocksz,v.get_store(),-2./(a-b));
 mat_xpeya(n*blocksz,v.get_store(),u.get_store(),(a+b)/(a-b));
 Real c1=2.*(a+b)/(a-b);
 Real c2=4./(a-b);

 //T_n = c1*T_{n-1} - c2 * bigmatrix * T_{n-1} - T_{n-2}
 w.newsize(n,blocksz); chk_set_init(w,1);
 Integer j;
 for(j=2;j<nchebit;j+=2){
     secs=myclock.time();
     bigmatrix->lanczosmult(v,w);
     time_mult+=myclock.time()-secs;
     for(Integer i=0;i<neigfound;i++){
       for(Integer k=0;k<blocksz;k++){
	 Real dip=mat_ip(n,v.get_store()+k*n,X.get_store()+i*n);
	 mat_xpeya(n,w.get_store()+k*n,X.get_store()+i*n,(-d(i)+(a+b)/2.)*dip);
       }
     }
     //T_{2n}= u = c1*v-c2*w-u;
     mat_xemx(n*blocksz,u.get_store());
     mat_xpeya(n*blocksz,u.get_store(),w.get_store(),-c2);
     mat_xpeya(n*blocksz,u.get_store(),v.get_store(),c1);
     secs=myclock.time();
     bigmatrix->lanczosmult(u,w);
     time_mult+=myclock.time()-secs;
     for(Integer i=0;i<neigfound;i++){
       for(Integer k=0;k<blocksz;k++){
	 Real dip=mat_ip(n,u.get_store()+k*n,X.get_store()+i*n);
	 mat_xpeya(n,w.get_store()+k*n,X.get_store()+i*n,(-d(i)+(a+b)/2.)*dip);
       }
     }
     //T_{2n+1}= v = c1*u-c2*w-v;
     mat_xemx(n*blocksz,v.get_store());
     mat_xpeya(n*blocksz,v.get_store(),w.get_store(),-c2);
     mat_xpeya(n*blocksz,v.get_store(),u.get_store(),c1);
 }
 return 0;
}

// *****************************************************************************
//                                scalarcheby
// *****************************************************************************

//computes the same Chebycheff polynomial as blockcheby but for scalar values
//Input: xval, col_offset
//global params: maxval,minval,nchebit, blocksz, polval
//output: v
//uses: u,w

Real Lanczpol::scalarcheby(Real xval)
{
 if ((maxval-minval<1e-8)||(nchebit==0)){
     return xval;
 }
 //determine interval limits [a,b] for polynomial
 if ((nchebit%2)==0) nchebit++;
 Real a=minval-1e-3*(maxval-minval);
 Real b=(2*maxval+(cosh(acosh(polval)/nchebit)-1)*a)/
         (cosh(acosh(polval)/nchebit)+1);

 Real ru,rv;
 //T0= ru = 1;
 ru=1;

 //T1: v=(a+b)/(a-b)-2./(a-b)*xval
 rv=(a+b)/(a-b)-2./(a-b)*xval;

 Real c1=2.*(a+b)/(a-b);
 Real c2=4./(a-b);
 //T_n = c1*T_{n-1} - c2 * xval * T_{n-1} - T_{n-2}
 Integer j;
 for(j=2;j<nchebit;j+=2){
     //T_{2n}= u = v*(c1-c2*xval)-u;
     ru=rv*(c1-c2*xval)-ru;

     //T_{2n+1}= v = c1*u-c2*w-v;
     rv=ru*(c1-c2*xval)-rv;
 }
 return rv;
}

// *****************************************************************************
//                                tred2
// *****************************************************************************

//transform symmetric input matrix C (stored in the lower triagonal) into
//tridiagonal form (diagonal u and subdiagonal v), the orthogonal 
//transformation matrix is stored in Z; C and Z may be the same  
//the subdiagonal elements v(i) are stored in positions 1 to n,
//v(0) is set to zero.

//uses householder transformation

int Lanczpol::tred2(Integer n,const Matrix& C,Matrix& u,Matrix& v,Matrix& Z)
{
 Integer i,j,k,l;
 u.init(n,1,0.);
 v.init(n,1,0.);
 
 //---- copy lower triangular part from C to Z (may be the same)
 if (Z.get_store()!=C.get_store()){
     for(i=0;i<n;i++)
         for(j=0;j<=i;j++)
             Z(i,j)=C(i,j);
 }
 
 //---- compute householder recursively starting with the last column 
 for(i=n-1;i>0;i--){
     l=i-1;             
     Real h=0.;
     Real scale=0.;
     if (l<1){
         v(i)=Z(i,l);
         u(i)=h;
         continue;
     }
     for(k=0;k<=l;k++)
         scale+=my_abs(Z(i,k));
     if(scale==0.){
         v(i)=Z(i,l);
         u(i)=h;
         continue;
     }                                //scale=sum(abs(Z(i,0:l)) != 0.

     //---- compute Z(i,0:l) /= scale
     //---- compute h = Z(i,0:l)*Z(i,0:l)'
     for(k=0;k<=l;k++){               //scaling Z(i,0:l)
         Z(i,k)/=scale;
         h+=Z(i,k)*Z(i,k);
     }

     //---- compute householder vector into Z(i,0:l)
     //       Z(i,0:l) = Z(i,0:l) + sign(Z(i,l))*norm(Z(i,0:l))*e_l;
     //       h = 2*Z(i,0:l)*Z(i,0:l)';
     Real f=Z(i,l);
     Real g=-d_sign(sqrt(h),f);       //g=-sign(f)*norm(Z(i,0:l))
     v(i)=scale*g;                    //offdiagonal element is now fixed
     h-=f*g;                         
     Z(i,l)=f-g;

     //---- compute v(0:l) = Z(0:l,0:l) * Z(i,0:l)' /h;
     //                  f = v(0:l)' * Z(i,0:l)';
     f=0.;
     Real *zi=Z.get_store()+i;
     Real *zj=Z.get_store();
     Integer Zrowdim=Z.rowdim();
     for(j=0;j<=l;j++,zj++){
         //Z(j,i)=Z(i,j)/(scale*h);      // stores Z(i,0:l) in upper triangle
         *(zj+i*Zrowdim)=*(zi+j*Zrowdim)/(scale*h);
         //g=0.;
         //for(k=0;k<=j;k++) g+=Z(j,k)*Z(i,k);
         //for(;k<=l;k++)    g+=Z(k,j)*Z(i,k);
         g= mat_ip(j+1,zj,Zrowdim,zi,Zrowdim);
         g+=mat_ip(l-j,zj+j*Zrowdim+1,1,
                   zi+(j+1)*Zrowdim,Zrowdim);
         //v(j)=g/h;
         //f+=v(j)*Z(i,j);
         f+=(v(j)=g/h)*(*(zi+j*Zrowdim));
     }

     //---- compute   v = v - Z(i,0:l)'*f/2h;   (second vector for rank2-update)
     //---- compute   Z(0:l,0:l) -= Z(i,0:l)'*v'+ v*Z(i,0:l);     (rank2-update)
     Real hh=f/(h+h);
     for(j=0;j<=l;j++){
         zj=Z.get_store()+j;
         f=Z(i,j);
         g=(v(j)-=hh*f);
         //for(k=0;k<=j;k++) Z(j,k)-=f*v(k)+g*Z(i,k);
         mat_xpeya(j+1,zj,Z.rowdim(),zi,Z.rowdim(),-g);
         mat_xpeya(j+1,zj,Z.rowdim(),v.get_store(),1,-f);
     } 

     //---- unscale Z(i,0:l);
     //---- store squared norm of householdervec in u(i)
     //for(k=0;k<=l;k++) Z(i,k)*=scale;
     mat_xmultea(l+1,zi,Z.rowdim(),scale);
     u(i)=h;
 }
 u(0)=0.;
 v(0)=0.;

 //---- reconstruct orthogonal transformation by succesively computing
 //----  Z= Z*(I-2vv'/v'v) for the householder vectors v (starting with Z=I)

 Real g;
 
 for(i=0;i<n;i++){
     l=i-1;
     if (u(i)!=0.){ //if the householder vector is not the zero vector
         //--- compute Z(0:l,0:l) -= Z(i,0:l)*Z(0:l,0:l)*Z(0:l,j)
         //---                     (== z'*Z*z/scale/scale/(2*z'*z)) 
         for(j=0;j<=l;j++){
             //--- g= Z(0:l,j)*Z(i,0:l)
             //g=0.; for(k=0;k<=l;k++) g+= Z(i,k)*Z(k,j);
             g=mat_ip(l+1,Z.get_store()+i,Z.rowdim(),
                      Z.get_store()+j*Z.rowdim(),1);
             //--- Z(0:l,j) -= g*Z(0:l,i)
             //for(k=0;k<=l;k++) Z(k,j)-=g*Z(k,i);
             mat_xpeya(l+1,Z.get_store()+j*Z.rowdim(),
                       Z.get_store()+i*Z.rowdim(),-g);
         }
     }
    
     //---- store the diagonal of tridiagonal matrix in u before overwriting 
     u(i)=Z(i,i);

     //--- replace used householder vector by identity matrix part
     Z(i,i)=1.;
     for(j=0;j<=l;j++){ 
         Z(i,j)=0.;
         Z(j,i)=0.;
     }
 }
 return 0;
}

// *****************************************************************************
//                                tql2
// *****************************************************************************

//computes diagonal representation of the tridiagnal symmetric matrix given by
//the diagonal u and the subdiagonal v (v(0) is empty) by the Symmetric
//QR Algorithm (Golub/van Loan 2nd p423). The transformations are added to
//Z which is assumed to be the transoformation matrix bringing the
//original matrix into tridiagonal form.
//on output eigenvalues appear in u, eigenvectors are the columns of Z
//Input: u,v,Z
//Output: u,Z (v is destroyed)

int Lanczpol::tql2(Integer n,Matrix &u,Matrix &v, Matrix& Z)
{

 if (n<=1) return 0;

 const Real machep=2.e-47;
 Integer i,j,iter,l,m;
 Integer k;
 
 //shift subdiagonal elements to position 0 to n-2
 //for (i=1;i<n;i++) v(i-1)=v(i);
 mat_xey(n-1,v.get_store(),v.get_store()+1);

 Real f=0.;
 Real subdiagzero=machep;
 v(n-1)=0;
 Integer l1;
 Real g;
 Real h;
 Real t2;
 Real t5;
 Real t3,t4;
 
 for(l=0;l<n;l++){  
     iter=0;
     h=machep*(my_abs(u(l))+my_abs(v(l)));
     if (subdiagzero<h) subdiagzero=h;
     for(m=l;m<n;m++){
         if (my_abs(v(m))<subdiagzero) break;
     }
     //for(m=l;m<n-1;m++){
     //    Real tst1=my_abs(u(m))+my_abs(u(m+1));
     //    Real tst2=tst1+my_abs(v(m));
     //    if (tst1==tst2) break;
     //}
     if (m==l) {
         u(l)+=f;
         continue;
     }
     do{
         if (iter==30) return l+1;   //at most 30 iterations
         iter++;
         l1=l+1;

         //---- comput shift mu (Golub/van Loan p423)
         g=u(l);
         t3=(u(l1)-g)/(2.*v(l));      //t3= u(l+1)-u(l)/(2.*v(l))
         t4=sqrt(t3*t3+1.);           //=sqrt((u(l+1)-u(l))^2/4+v(l)^2)/|v(l)|
         u(l)=v(l)/(t3+d_sign(t4,t3)); 
         h=g-u(l);                    //=mu

         //---- subtract shift from remaining diagonal elements
         for(i=l1;i<n;i++) u(i)-=h;
         //mat_xpea(n-l1,u.get_store()+l1,-h);
         f+=h;

         
         t3=u(m);
         t2=1.;
         t5=0.;
         Real *vi=v.get_store()+m-1;
         Real *vi1=vi+1;
         Real *ui=u.get_store()+m-1;
         Real *ui1=ui+1;
         for(i=m-1;i>=l;i--,vi--,vi1--,ui--,ui1--){

             //---- compute Givens coefficients (Golub/van Loan p202) 
             g=t2* (*vi);               //g=t2*v(i);     
             h=t2*t3;
             if (my_abs(t3)>=my_abs(*vi)){    //if (abs(t3)>=abs(v(i))){
                 t2=(*vi)/t3;           //t2=v(i)/t3;
                 t4=sqrt(t2*t2+1.);
                 *vi1=t5*t3*t4;         //v(i+1)=t5*t3*t4;
                 t5=t2/t4;
                 t2=1./t4;
             }
             else {
                 t2=t3/(*vi);           //t2=t3/v(i);
                 t4=sqrt(t2*t2+1.);
                 *vi1=t5*(*vi)*t4;      //v(i+1)=t5*v(i)*t4;
                 t5=1./t4;
                 t2=t2*t5;
             }
             t3=t2*(*ui)-t5*g;          //t3=t2*u(i)-t5*g;
             *ui1=h+t5*(t2*g+t5*(*ui)); //u(i+1)=h+t5*(t2*g+t5*u(i));

             //---- change vectors
             Real *zi=Z.get_store()+i*Z.rowdim();
             Real *zi1=zi+Z.rowdim();
             for(k=n;--k>=0;){
                 //g=Z(k,i); h=Z(k,i+1);
                 g=*zi;
                 h=*zi1;
                 (*zi1++) = t5*g+t2*h;      //Z(k,i+1)=t5*Z(k,i)+t2*h;
                 (*zi++)  = t2*g-t5*h;      //Z(k,i)=t2*Z(k,i)-t5*h;
             }
         }
         v(l)=t5*t3;
         u(l)=t2*t3;
     } while(my_abs(v(l))>subdiagzero); 
     u(l)+=f;
 }

 //--- sort eigenvalues nonincreasingly and rearrange eigenvectors
 //    (quite stupid, should probably be replaced) ?does it work at all? 
 for(i=0;i<n-1;i++){
     k=i;
     t3=u(i);
     //--- find remaining maximal eigenvalue 
     for(j=i+1;j<n;j++){
         if(u(j)<=t3) continue;
         k=j;
         t3=u(j);
     }
     if (k==i) continue;   //i is already maximal, skip rest

     //--- swap maximal eigenvalue and eigenvector k with i
     u(k)=u(i);            
     u(i)=t3;
     //for(j=0;j<n;j++){
     //    t3=Z(j,i); Z(j,i)=Z(j,k); Z(j,k)=t3;
     //}
     mat_swap(n,Z.get_store()+i*Z.rowdim(),Z.get_store()+k*Z.rowdim());
 }
 return 0;
}


// *****************************************************************************
//                                save
// *****************************************************************************

ostream& Lanczpol::save(ostream& out) const
{
 out<<ierr<<"\n"<<maxop<<"\n"<<maxiter<<"\n";
 out<<maxguessiter<<"\n"<<guessmult<<"\n"<<choicencheb<<"\n"; 
 out<<nchebit<<"\n"<<choicenbmult<<"\n"<<nblockmult<<"\n";
 out<<nlanczvecs<<"\n"<<neigfound<<"\n"<<blocksz<<"\n";
 out<<iter<<"\n"<<nmult<<"\n"<<errc<<"\n"<<eps<<"\n";
 out<<mcheps<<"\n"<<maxval<<"\n"<<minval<<"\n"<<polval<<"\n"; 
 out<<X<<C<<d<<e<<u<<v<<w<<minvec;      
 out<<stop_above<<"\n"<<upper_bound<<"\n"<<do_out<<"\n"<<mymaxj<<"\n";
 out<<time_mult<<"\n"<<time_mult_sum<<"\n"<<time_iter<<"\n"<<time_sum<<"\n";
 return out;
}

// *****************************************************************************
//                                restore
// *****************************************************************************

istream& Lanczpol::restore(istream& in)
{
 in>>ierr>>maxop>>maxiter;
 in>>maxguessiter>>guessmult>>choicencheb; 
 in>>nchebit>>choicenbmult>>nblockmult;
 in>>nlanczvecs>>neigfound>>blocksz;
 in>>iter>>nmult>>errc>>eps;
 in>>mcheps>>maxval>>minval>>polval; 
 in>>X>>C>>d>>e>>u>>v>>w>>minvec;      
 in>>stop_above>>upper_bound>>do_out>>mymaxj;
 in>>time_mult>>time_mult_sum>>time_iter>>time_sum;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: lanczpol.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:19  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/lanczpol.cc,v $
 --------------------------------------------------------------------------- */
