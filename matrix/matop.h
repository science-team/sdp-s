/* -*-c++-*-
 * 
 *    Source     $RCSfile: matop.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:54 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __MATOP_H__
#define __MATOP_H__

#include <climits>
#include <cfloat>

typedef long Integer;
const Integer max_Integer= LONG_MAX;
const Integer min_Integer= LONG_MIN;

typedef double Real;
const Real max_Real= DBL_MAX;
const Real min_Real= -DBL_MAX;
const Real eps_Real= DBL_EPSILON;

template<class Val>
inline void mat_xey( Integer len, Val *x, const Val* y)
{
 for(;--len>=0;) (*x++)=(*y++);
}

template<class Val>
inline void mat_xey( Integer len, Val *x, const Integer incx,
                     const Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)=(*y);
}

template<class Val>
inline void mat_xemx( Integer len, Val *x)
{
 for(;--len>=0;x++) (*x)=-(*x);
}

template<class Val>
inline void mat_xemx( Integer len, Val *x, const Integer incx)
{
 for(;--len>=0;x+=incx) (*x)=-(*x);
}

template<class Val>
inline void mat_xemy( Integer len, Val *x, const Val *y)
{
 for(;--len>=0;) (*x++)=-(*y++);
}

template<class Val>
inline void mat_xemy( Integer len, Val *x, const Integer incx,
                          const Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)=-(*y);
}

template<class Val>
inline void mat_xeya( Integer len, Val *x,
                     const Val *y, Val d)
{
 for(;--len>=0;) (*x++)=d*(*y++);
}

template<class Val>
inline void mat_xeya( Integer len, Val *x, const Integer incx,
                     const Val *y, const Integer incy, Val d)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)=d*(*y);
}

template<class Val>
inline void mat_xpey( Integer len, Val *x, const Val *y)
{
 for(;--len>=0;) (*x++)+=(*y++);
}

template<class Val>
inline void mat_xpey( Integer len, Val *x, const Integer incx,
                         const Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)+=(*y);
}

template<class Val>
inline void mat_xmey( Integer len, Val *x, const Val *y)
{
 for(;--len>=0;) (*x++)-=(*y++);
}

template<class Val>
inline void mat_xmey( Integer len, Val *x, const Integer incx,
                          const Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)-=(*y);
}

template<class Val>
inline void mat_xhadey( Integer len, Val *x, const Val *y)
{
 for(;--len>=0;) (*x++)*=(*y++);
}

template<class Val>
inline void mat_xhadey( Integer len, Val *x, const Integer incx,
                          const Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)*=(*y);
}

template<class Val>
inline void mat_xpeya( Integer len, Val *x,
                         const Val *y, Val d)
{
 for(;--len>=0;) (*x++)+=d*(*y++);
}

template<class Val>
inline void mat_xpeya( Integer len, Val *x, const Integer incx,
                         const Val *y, const Integer incy, Val d)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)+=d*(*y);
}

template<class Val>
inline void mat_xbpeya( Integer len, Val *x,
                         const Val *y, Val a,Val b)
{
 for(;--len>=0;x++) (*x)=b*(*x)+a*(*y++);
}

template<class Val>
inline void mat_xbpeya( Integer len, Val *x, const Integer incx,
                         const Val *y, const Integer incy, Val a,Val b)
{
 for(;--len>=0;x+=incx,y+=incy) (*x)+=b*(*x)+a*(*y);
}

template<class Val>
inline void mat_xea( Integer len, Val *x, Val d)
{
 for(;--len>=0;) (*x++)=d;
}

template<class Val>
inline void mat_xea( Integer len, Val *x, const Integer incx,
                     Val d)
{
 for(;--len>=0;x+=incx) (*x)=d;
}

template<class Val>
inline void mat_xpea( Integer len, Val *x, Val d)
{
 for(;--len>=0;) (*x++)+=d;
}

template<class Val>
inline void mat_xpea( Integer len, Val *x, const Integer incx,
                         Val d)
{
 for(;--len>=0;x+=incx) (*x)+=d;
}

template<class Val>
inline void mat_xmultea( Integer len, Val *x, Val d)
{
 for(;--len>=0;) (*x++)*=d;
}

template<class Val>
inline void mat_xmultea( Integer len, Val *x, const Integer incx,
                         Val d)
{
 for(;--len>=0;x+=incx) (*x)*=d;
}

template<class Val>
inline void mat_xdivea( Integer len, Val *x, Val d)
{
 for(;--len>=0;) (*x++)/=d;
}

template<class Val>
inline void mat_xdivea( Integer len, Val *x, const Integer incx,
                         Val d)
{
 for(;--len>=0;x+=incx) (*x)/=d;
}

template<class Val>
inline void mat_xmodea( Integer len, Val *x, Val d)
{
 for(;--len>=0;) (*x++)%=d;
}

template<class Val>
inline void mat_xmodea( Integer len, Val *x, const Integer incx,
                         Val d)
{
 for(;--len>=0;x+=incx) (*x)%=d;
}

template<class Val>
inline void mat_xeypz( Integer len, Val *x,
                      const Val *y, const Val *z)
{
 for(;--len>=0;) (*x++)=(*y++)+(*z++);
}

template<class Val>
inline void mat_xeypz( Integer len, Val *x, const Integer incx,
                       const Val *y, const Integer incy,
                       const Val *z, const Integer incz)
{
 for(;--len>=0;x+=incx,y+=incy,z+=incz) (*x)=(*y)+(*z);
}

template<class Val>
inline void mat_xeymz( Integer len, Val *x,
                       const Val *y, const Val *z)
{
 for(;--len>=0;) (*x++)=(*y++)-(*z++);
}

template<class Val>
inline void mat_xeymz( Integer len, Val *x, const Integer incx,
                       const Val *y, const Integer incy,
                       const Val *z, const Integer incz)
{
 for(;--len>=0;x+=incx,y+=incy,z+=incz) (*x)=(*y)-(*z);
}

template<class Val>
inline void mat_xeyapzb( Integer len, Val *x,
                      const Val *y, const Val *z,Val a,Val b)
{
 for(;--len>=0;) (*x++)=a*(*y++)+b*(*z++);
}

template<class Val>
inline void mat_xeyapzb( Integer len, Val *x, const Integer incx,
                       const Val *y, const Integer incy,
                       const Val *z, const Integer incz,Val a,Val b)
{
 for(;--len>=0;x+=incx,y+=incy,z+=incz) (*x)=a*(*y)+b*(*z);
}

template<class Val>
inline Val mat_ip( Integer len, const Val *x, const Val *y)
{
  Val sum=0;
 for(;--len>=0;) sum+=(*x++)*(*y++);
 return sum;
}

template<class Val>
inline Val mat_ip( Integer len, const Val *x, const Integer incx,
                   const Val *y, const Integer incy)
{
  Val sum=0;
 for(;--len>=0;x+=incx,y+=incy) sum+=(*x)*(*y);
 return sum;
}

template<class Val>
inline void mat_swap( Integer len, Val *x, Val *y)
{
 for(;--len>=0;) {Val h=*x;(*x++)=*y;(*y++)=h;}
}

template<class Val>
inline void mat_swap( Integer len, Val *x, const Integer incx,
                      Val *y, const Integer incy)
{
 for(;--len>=0;x+=incx,y+=incy) {Val h=*x;(*x)=*y;(*y)=h;}
}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: matop.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/matop.h,v $
 --------------------------------------------------------------------------- */
