/* -*-c++-*-
 * 
 *    Source     $RCSfile: chol.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:26 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: chol.cc,v 1.1.1.1.2.1 2000/02/28 13:55:26 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "matop.h"
#include "symmat.h"

int Symmatrix::Chol_factor(Indexmatrix& piv,Real tol)
{
 piv.init(nr,1,Integer(0));
 Integer rank=0;
 register Integer i,j,k;
 register Real d;
 register Real *mp;
 register Real *mp2;
 for(k=0;k<nr;k++){

     //---- find maximal diagonal element or stop if a diagonal element is negative
     Integer maxind=k;
     mp=m+nr*k-((k-1)*k)/2;
     Real maxval=(*mp);
     for(i=k+1;i<nr;i++){
         mp+=nr-i+1;
         d=(*mp);
         if (d<0.) {
             piv.nr=rank;
             return i;
         }
         if (d>maxval){
             maxind=i;
             maxval=d;
         }
     }

     //---- check whether maxval is still large enough
     if (maxval<tol) break;
     piv(rank)=maxind;
     rank++;

     //---- exchange row k and row maxind
     if (maxind!=k){
         mp=m+k;
         mp2=m+maxind;
         for(i=0;i<k;i++){
             d=*mp; *mp=*mp2; *mp2=d;
             mp+=nr-i-1; mp2+=nr-i-1;
         }
         d=*mp; *mp=*(mp2=(m+nr*maxind-((maxind-1)*maxind)/2));*mp2=d;
         mp2=mp+(maxind-k)+nr-k-1; mp++;
         for(i=k+1;i<maxind;i++){
             d=*mp;*mp=*mp2;*mp2=d;
             mp++; mp2+=nr-i-1;        
         }
         for(i=maxind+1;i<nr;i++){
             mp++; mp2++;
             d=*mp;*mp=*mp2;*mp2=d;
         }
     }

     //---- compute factorization
     /*
     (*this)(k,k)=sqrt((*this)(k,k));
     for(i=k+1;i<nr;i++) (*this)(k,i)/=(*this)(k,k);
     for(i=k+1;i<nr;i++) {
         for(j=i;j<nr;j++){
             (*this)(i,j)-=(*this)(k,j)*(*this)(k,i);
         }
     }
     */
     mp=m+nr*k-((k-1)*k)/2;
     d=(*mp=sqrt(*mp));
     mp++;
     for(i=nr-k-1;--i>=0;){
         (*mp++)/=d;
     } 
     mp2=mp;
     for(i=nr-k-1;--i>=0;){
         mp-=(j=i+1);
         d=*mp;
         for(;--j>=0;){
             (*mp2++)-=(*mp++)*d;
         }
     }
 }
 piv.nr=rank;
 return 0;
}
 
int Symmatrix::Chol_solve(Matrix& x,const Indexmatrix& piv) const
{
 chk_mult(*this,x);

 register Real f;
 register Integer i,j,h;
 register Real *mp;
 register Real *rp;

 //permute x
 for(i=0;i<nr;i++){
     h=piv(i);
     if (h==i) continue;
     mp=x.m+i;
     rp=x.m+h;
     mat_swap(x.coldim(),mp,nr,rp,nr);
 }

 Integer k;
 Real *xbase;

 for(k=0;k<x.coldim();k++){ //solve for and overwrite column k of x
     xbase=x.m+k*nr;
     //---- solve Lr=xbase
     for(i=0;i<nr;i++){
         f=xbase[i];
         rp=xbase;
         mp=m+i;
         h=nr-i;
         for(j=nr;--j>=h;) {
             f-=(*rp++)*(*mp);
             mp+=j;
         }
         *rp=f/(*mp);
     }

     //---- solve L'r=r
     for(i=nr;--i>=0;){
         rp=xbase+i;
         f=(*rp++);
         mp=m+i*nr-(i*(i-1))/2+1;
         for(j=nr;--j>i;) f-=(*rp++)*(*mp++);
         xbase[i]=f/(*(mp-(nr-i)));
     }
 }

 //repermute x
 for(i=nr;--i>=0;){
     h=piv(i);
     if (h==i) continue;
     mp=x.m+i;
     rp=x.m+h;
     mat_swap(x.coldim(),mp,nr,rp,nr);
 }
 
 return 0;
}

int Symmatrix::Chol_inverse(Symmatrix &S,const Indexmatrix& piv) const
{
 chk_init(*this);
 S.newsize(nr);
 Real *sp=S.m;
 register Real f;
 register Real *mp;
 register Real* sp1;
 register Integer e,i,j,indi;
 for(e=0;e<nr;e++){
     
     indi=e*nr-((e-1)*e)/2-e;
     sp=S.m+indi;
     
     //---- solve Lr=v
     indi+=e;
     sp[e]=1./(*(m+indi));
     indi++;
     mat_xeya(nr-e-1,sp+e+1,m+indi,-sp[e]);
     for(i=e+1;i<nr;i++){
         indi+=nr-i;
         sp[i]/=(*(m+indi));
         indi++;
         mat_xpeya(nr-i-1,sp+i+1,m+indi,-sp[i]);
     }
  
     //---- solve L'r=r
     indi=(nr*(nr+1))/2-1;
     for(i=nr;--i>=e;){
         mp=m+indi;
         sp1=sp+i;
         *sp1/=*mp;
         mp-=nr-i;
         f=*sp1;
         for(j=i;--j>=e;) {
             (*(--sp1))-=f*(*mp);
             mp-=nr-j;
         }
         indi-=nr-i+1;
     } 
 }
 chk_set_init(S,1);

 //repermute S
 for(i=nr;--i>=0;){
     e=piv(i);
     if (e==i) continue;
     mp=S.m+i;
     sp1=S.m+e;
     for(j=0;j<i;j++){
         f=*mp; *mp=*sp1; *sp1=f;
         mp+=nr-j-1; sp1+=nr-j-1;
     }
     f=*mp; *mp=*(sp1=(S.m+nr*e-((e-1)*e)/2));*sp1=f;
     sp1=mp+(e-i)+nr-i-1; mp++;
     for(j=i+1;j<e;j++){
         f=*mp;*mp=*sp1;*sp1=f;
         mp++; sp1+=nr-j-1;        
     }
     for(j=e+1;j<nr;j++){
         mp++; sp1++;
         f=*mp;*mp=*sp1;*sp1=f;
     }
 }
 
 return 0;
}


/* ---------------------------------------------------------------------------
 *    Change log $Log: chol.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:26  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/chol.cc,v $
 --------------------------------------------------------------------------- */
