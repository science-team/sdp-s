/* -*-c++-*-
 * 
 *    Source     $RCSfile: sparsmat.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:22 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: sparsmat.cc,v 1.1.1.1.2.1 2000/02/28 13:55:22 bzfhelmb Exp $"

#include <cstdlib>
#include "sparsmat.h"
#include "heapsort.h"

Sparsemat& Sparsemat::xeya(const Sparsemat& A,Real d)
{
 chk_init(A);
 if (d==0.) return init(A.nr,A.nc);
 nr=A.nr;
 nc=A.nc;
 colinfo=A.colinfo;
 rowinfo=A.rowinfo;
 colindex=A.colindex;
 rowindex=A.rowindex;
 colval.xeya(A.colval,d);
 rowval.xeya(A.rowval,d);
 chk_set_init(*this,1);
 return *this;
}

Sparsemat& Sparsemat::xeya(const Matrix& A,Real d)
  //returns (*this)=A*d;
{
 chk_init(A);
 if (d==0.) return init(A.nr,A.nc);
 Integer i,nz=0;
 Integer di=A.nr*A.nc;
 const Real *m=A.get_store();
 for(i=0;i<di;i++){
     if (my_abs(*m++)>tol) nz++;
 }
 Indexmatrix I(nz,1),J(nz,1);
 Matrix val(nz,1);
 chk_set_init(I,1);
 chk_set_init(J,1);
 chk_set_init(val,1);
 m=A.get_store();
 nz=0;
 for(i=0;i<di;i++,m++){
     if (my_abs(*m)>tol){
         I(nz)=i%A.nr;
         J(nz)=i/A.nr;
         val(nz++)=*m*d;
     }
 }
 init(A.nr,A.nc,nz,I,J,val);
 return *this;
}

Sparsemat& Sparsemat::xeya(const Indexmatrix& A,Real d)
  //returns (*this)=A*d;
{
 chk_init(A);
 if (d==0.) return init(A.nr,A.nc);
 Integer i,nz=0;
 Integer di=A.nr*A.nc;
 const Integer *m=A.get_store();
 for(i=0;i<di;i++){
     if (*m++!=0) nz++;
 }
 Indexmatrix I(nz,1),J(nz,1);
 Matrix val(nz,1);
 chk_set_init(I,1);
 chk_set_init(J,1);
 chk_set_init(val,1);
 m=A.get_store();
 nz=0;
 for(i=0;i<di;i++,m++){
     if (*m!=0){
         I(nz)=i%A.nr;
         J(nz)=i/A.nr;
         val(nz++)=Real(*m*d);
     }
 }
 init(A.nr,A.nc,nz,I,J,val);
 return *this; 
}

  
Matrix& genmult(const Sparsemat& A,const Matrix& B,Matrix &C,
                       Real alpha,Real beta, int atrans,int btrans)
{
 chk_init(A);
 chk_init(B);
 Integer nr,nm,nc;
 if (atrans) {nr=A.nc;nm=A.nr;}
 else { nr=A.nr; nm=A.nc;}
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.nr)||(nc!=C.nc)) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (atrans){
     if (btrans){
         const Integer *aip=A.colindex.get_store();
         const Real *avp=A.colval.get_store();
         for(Integer i=0;i<A.colinfo.rowdim();i++){
             Integer ii=A.colinfo(i,0);
             Real *cp=C.get_store()+ii;
             for(Integer j=A.colinfo(i,1);--j>=0;){
                 mat_xpeya(nc,cp,nr,B.get_store()+(*aip++)*nc,1,alpha*(*avp++));
             }
         }
     }
     else {
         const Integer *aip=A.colindex.get_store();
         const Real *avp=A.colval.get_store();
         for(Integer i=0;i<A.colinfo.rowdim();i++){
             Integer ii=A.colinfo(i,0);
             Real *cp=C.get_store()+ii;
             for(Integer j=A.colinfo(i,1);--j>=0;){
                 mat_xpeya(nc,cp,nr,B.get_store()+*aip++,nm,
                           alpha*(*avp++));
             }
         }
     }
 }
 else {
     if (btrans){
         const Integer *aip=A.rowindex.get_store();
         const Real *avp=A.rowval.get_store();
         for(Integer i=0;i<A.rowinfo.rowdim();i++){
             Integer ii=A.rowinfo(i,0);
             Real *cp=C.get_store()+ii;
             for(Integer j=A.rowinfo(i,1);--j>=0;){
                 mat_xpeya(nc,cp,nr,B.get_store()+(*aip++)*nc,1,alpha*(*avp++));
             }
         }
     }
     else {
         const Integer *aip=A.rowindex.get_store();
         const Real *avp=A.rowval.get_store();
         for(Integer i=0;i<A.rowinfo.rowdim();i++){
             Integer ii=A.rowinfo(i,0);
             Real *cp=C.get_store()+ii;
             for(Integer j=A.rowinfo(i,1);--j>=0;){
                 mat_xpeya(nc,cp,nr,B.get_store()+*aip++,nm,
                           alpha*(*avp++));
             }
         }
     }
 }
 return C;
}

Matrix& genmult(const Matrix& A,const Sparsemat& B,Matrix &C,
                       Real alpha,Real beta, int atrans,int btrans)
{
 chk_init(A);
 chk_init(B);
 Integer nr,nm,nc;
 if (atrans) {nr=A.nc;nm=A.nr;}
 else { nr=A.nr; nm=A.nc;}
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.nr)||(nc!=C.nc)) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (atrans){
     if (btrans){ //A^T*B^T
         const Integer *bip=B.rowindex.get_store();
         const Real *bvp=B.rowval.get_store();
         for(Integer i=0;i<B.rowinfo.rowdim();i++){
             Integer ii=B.rowinfo(i,0);
             Real *cp=C.get_store()+ii*nr;
             for(Integer j=B.rowinfo(i,1);--j>=0;){
                 mat_xpeya(nr,cp,1,A.get_store()+(*bip++),nm,alpha*(*bvp++));
             }
         }
     }
     else { //A^T*B
         const Integer *bip=B.colindex.get_store();
         const Real *bvp=B.colval.get_store();
         for(Integer i=0;i<B.colinfo.rowdim();i++){
             Integer ii=B.colinfo(i,0);
             Real *cp=C.get_store()+ii*nr;
             for(Integer j=B.colinfo(i,1);--j>=0;){
                 mat_xpeya(nr,cp,1,A.get_store()+(*bip++),nm,alpha*(*bvp++));
             }
         }
     }
 }
 else {
     if (btrans){ // A*B^T
         const Integer *bip=B.rowindex.get_store();
         const Real *bvp=B.rowval.get_store();
         for(Integer i=0;i<B.rowinfo.rowdim();i++){
             Integer ii=B.rowinfo(i,0);
             Real *cp=C.get_store()+ii*nr;
             for(Integer j=B.rowinfo(i,1);--j>=0;){
                 mat_xpeya(nr,cp,A.get_store()+(*bip++)*nr,alpha*(*bvp++));
             }
         }
     }
     else { // A*B
         const Integer *bip=B.colindex.get_store();
         const Real *bvp=B.colval.get_store();
         for(Integer i=0;i<B.colinfo.rowdim();i++){
             Integer ii=B.colinfo(i,0);
             Real *cp=C.get_store()+ii*nr;
             for(Integer j=B.colinfo(i,1);--j>=0;){
                 mat_xpeya(nr,cp,A.get_store()+(*bip++)*nr,alpha*(*bvp++));
             }
         }
     }
 }
 return C;
}

Matrix& genmult(const Sparsemat& A,const Sparsemat& B,Matrix &C,
                       Real alpha,Real beta, int atrans,int btrans)
{
 chk_init(A);
 chk_init(B);
 Integer nr,nc;
 const Indexmatrix* ainf;
 const Indexmatrix* aind;
 const Matrix* aval;
#ifdef DEBUG
 Integer nm;
#endif 
 if (atrans) {
     nr=A.nc;
#ifdef DEBUG
     nm=A.nr;
#endif 
     ainf=&(A.colinfo);
     aind=&(A.colindex);
     aval=&(A.colval);
 }
 else {
     nr=A.nr;
#ifdef DEBUG
     nm=A.nc;
#endif 
     ainf=&(A.rowinfo);
     aind=&(A.rowindex);
     aval=&(A.rowval);
 }

 const Indexmatrix* binf;
 const Indexmatrix* bind;
 const Matrix* bval;
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
     binf=&(B.rowinfo);
     bind=&(B.rowindex);
     bval=&(B.rowval);
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
     binf=&(B.colinfo);
     bind=&(B.colindex);
     bval=&(B.colval);
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.rowdim())||(nc!=C.coldim())) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsparse));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 for(Integer i=0;i<ainf->rowdim();i++){
     for(Integer j=0;j<binf->rowdim();j++){
         Real d=0.;
         Integer rk=(*ainf)(i,2); 
         Integer ru=rk+(*ainf)(i,1);
         Integer ck=(*binf)(j,2);
         Integer cu=ck+(*binf)(j,1);
         while((rk<ru)&&(ck<cu)){
             Integer eval=(*aind)(rk)-(*bind)(ck);
             if (eval==0){
                 d+=(*aval)(rk)*(*bval)(ck);
                 rk++;
                 ck++;
             }
             else if (eval<0){
                 rk++;
             }
             else {
                 ck++;
             }
         }
         C((*ainf)(i,0),(*binf)(j,0))+=alpha*d;
     }
 }
 return C;
}

Sparsemat& Sparsemat::init(Integer in_nr,Integer in_nc,Integer nz,
              const Integer *ini,const Integer *inj,const Real* va)
{
 return init(in_nr,in_nc,nz,Indexmatrix(nz,1,ini),Indexmatrix(nz,1,inj),Matrix(nz,1,va));
}


Sparsemat& Sparsemat::init(Integer in_nr,Integer in_nc,Integer nz,
          const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va)
{
 chk_init(ini);
 chk_init(inj);
 chk_init(va);
#ifdef DEBUG
 if (nz<0) {
     MEmessage(MatrixError(ME_unspec,"Sparsemat::init(nr,nc,nz,indi,indj,val): nz<0",MTsparse));
     exit(1);
 }
#endif
 if (nz==0) {
     return init(in_nr,in_nc);
 }
#ifdef DEBUG
 if ((ini.dim()<nz)||(inj.dim()<nz)||(va.dim()<nz)) {  
     MEmessage(MatrixError(ME_unspec,"Sparsemat::init(nr,nc,nz,indi,indj,val): indi, indj, or val has <nz elements",MTsparse));
     exit(1);
 }
 if ((min(ini(Range(0,nz-1)))<0)||(max(ini(Range(0,nz-1)))>=in_nr)||
     (min(inj(Range(0,nz-1)))<0)||(max(inj(Range(0,nz-1)))>=in_nc)) {  
     MEmessage(MatrixError(ME_unspec,"Sparsemat::init(nr,nc,nz,indi,indj,val): indices in indi or indj exceed range",MTsparse));
     exit(1);
 }
#endif

 nr=in_nr;
 nc=in_nc;

 if (nz==1) {
     colinfo.newsize(1,3); chk_set_init(colinfo,1);
     colinfo(0)=inj(0);
     colinfo(1)=1;
     colinfo(2)=0;
     colindex.init(1,1,ini(0));
     colval.init(1,1,va(0));
     rowinfo.newsize(1,3); chk_set_init(rowinfo,1);
     rowinfo(0)=ini(0);
     rowinfo(1)=1;
     rowinfo(2)=0;
     rowindex.init(1,1,inj(0));
     rowval.init(1,1,va(0));
     return *this;
 }
     
 colindex.newsize(nz,1); chk_set_init(colindex,1);
 colval.newsize(nz,1); chk_set_init(colval,1);
 rowindex.newsize(nz,1); chk_set_init(rowindex,1);
 rowval.newsize(nz,1); chk_set_init(rowval,1);
 
 Integer i;

 //--- generate structure for columns and rows
 Indexmatrix sindc,sindr;

 //--- find number of columns and elements in each column and row by sorting
 colindex.init(nz,1,inj.get_store());   //has to be copied, because lenght of inj unknown
                                        //values inj*nr+ini might be too big for large nr and nc
 sortindex(colindex,sindc);             
 rowindex.init(nz,1,ini.get_store());   //has to be copied, because lenght of inj unknown
 sortindex(rowindex,sindr);             
 
 Integer lastcval=inj(sindc(0));  //holds last column index encountered
 Integer lastrval=ini(sindr(0));  //holds last row index encountered
 Integer colcnt=0;                //counts number of columns
 Integer rowcnt=0;                //counts number of rows
 Integer cntcelems=1;             //counts number of elements in current column
                                  //number will be stored in colindex temporarily
 Integer cntrelems=1;             //counts number of elements in current row
                                  //number will be stored in rowindex temporarily
 for(i=1;i<nz;i++){
     if (lastcval!=inj(sindc(i))){  //new column starts
         colindex(colcnt)=cntcelems;
         //sort indices in this column in incereasing rowindex order
         Integer *sindp=sindc.get_store()+i-cntcelems;
         heapsort(cntcelems,sindp,ini.get_store());
         //start new column
         lastcval=inj(sindc(i));
         cntcelems=1;
         colcnt++;
     }
     else {
         cntcelems++;
     }
     if (lastrval!=ini(sindr(i))){  //new column starts
         rowindex(rowcnt)=cntrelems;
         //sort indices in this row in increasing colindex order
         Integer *sindp=sindr.get_store()+i-cntrelems;
         heapsort(cntrelems,sindp,inj.get_store());
         lastrval=ini(sindr(i));
         cntrelems=1;
         rowcnt++;
     }
     else {
         cntrelems++;
     } 
 }
 colindex(colcnt)=cntcelems;
 //sort indices in this column in incereasing rowindex order
 Integer *sindp=sindc.get_store()+i-cntcelems;
 heapsort(cntcelems,sindp,ini.get_store());
 colcnt++;
 rowindex(rowcnt)=cntrelems;
 //sort indices in this row in increasing colindex order
 sindp=sindr.get_store()+i-cntrelems;
 heapsort(cntrelems,sindp,inj.get_store());
 rowcnt++;
 
 //--- initialize colinfo and rowinfo to final values
 colinfo.newsize(colcnt,3); chk_set_init(colinfo,1);
 rowinfo.newsize(rowcnt,3); chk_set_init(rowinfo,1);
 colinfo(0,0)=inj(sindc(0));
 colinfo(0,1)=colindex(0);
 colinfo(0,2)=0;
 for(i=1;i<colcnt;i++){
     colinfo(i,1)=colindex(i);
     colinfo(i,2)=colinfo(i-1,2)+colinfo(i-1,1);
     colinfo(i,0)=inj(sindc(colinfo(i,2)));
 }
 rowinfo(0,0)=ini(sindr(0));
 rowinfo(0,1)=rowindex(0);
 rowinfo(0,2)=0;
 for(i=1;i<rowcnt;i++){
     rowinfo(i,1)=rowindex(i);
     rowinfo(i,2)=rowinfo(i-1,2)+rowinfo(i-1,1);
     rowinfo(i,0)=ini(sindr(rowinfo(i,2)));
 }

 //--- copy indices and values
 for(i=0;i<nz;i++){
     colindex(i)=ini(sindc(i));
     colval(i)=va(sindc(i));
     rowindex(i)=inj(sindr(i));
     rowval(i)=va(sindr(i));
 }
 return *this;
}

void Sparsemat::get_edge_rep(Indexmatrix& I, Indexmatrix& J, Matrix& val) const
{
 I.newsize(rowindex.dim(),1); chk_set_init(I,1);
 J.newsize(rowindex.dim(),1); chk_set_init(J,1);
 val.newsize(rowindex.dim(),1); chk_set_init(val,1);
 Integer i,j;
 Integer nz=0;
 for(i=0;i<rowinfo.rowdim();i++){
     Integer row=rowinfo(i,0);
     Integer nrcols=rowinfo(i,1);
     for(j=0;j<nrcols;j++){
         I(nz)=row;
         J(nz)=rowindex(nz);
         val(nz)=rowval(nz);
         nz++;
     }
 }
}

void Sparsemat::get_edge(Integer e,Integer& indi,Integer& indj,Real& val) const
{
  if (colinfo.rowdim()<=rowinfo.rowdim()){ //constant over calls on same matrix
    val=colval(e);//includes initialization and range check for e
    indi=colindex(e); 
    //binary search for column of e
    Integer lb=0;
    Integer ub=colinfo.rowdim()-1;
    Integer ii;
    while (lb<=ub){
      ii=(lb+ub)/2;
      if (colinfo(ii,2)>e) {
	ub=ii-1;
	continue;
      }
      if (colinfo(ii,2)<e) { 
	lb=ii+1;
	continue;
      }
      break;
    }
    if (lb>ub){ //then ub holds the correct index
      indj=colinfo(ub,0);
    }
    else {
      indj=colinfo(ii);
    }
  }
  else {
    val=rowval(e);//includes initialization and range check for e
    indj=rowindex(e); 
    //binary search for row of e
    Integer lb=0;
    Integer ub=rowinfo.rowdim()-1;
    Integer ii;
    while (lb<=ub){
      ii=(lb+ub)/2;
      if (rowinfo(ii,2)>e) {
	ub=ii-1;
	continue;
      }
      if (rowinfo(ii,2)<e) { 
	lb=ii+1;
	continue;
      }
      break;
    }
    if (lb>ub){ //then ub holds the correct index
      indi=rowinfo(ub,0);
    }
    else {
      indi=rowinfo(ii);
    }
  }
}

    
// ============================================================================
//                               contains_support
// ============================================================================

int Sparsemat::contains_support(const Sparsemat& A) const
{
 chk_init(*this);
 chk_init(A);
 if ((nr!=A.nr)||(nc!=A.nc)) return 0;
 if (A.colinfo.rowdim()==0) return 1;
 if ((colinfo.rowdim()==0) && (A.colinfo.rowdim()==0)) return 1;
 if (colval.rowdim()<A.colval.rowdim()) return 0;
 if (colinfo.rowdim()<A.colinfo.rowdim()) return 0;
 if (rowinfo.rowdim()<A.rowinfo.rowdim()) return 0;
 Integer cind1=0;
 Integer cind2=0;
 Integer cv1=colinfo(cind1);
 Integer cv2=A.colinfo(cind2);
 Integer nz1=0;
 Integer nz2=0;
 do {
     if (cv1<cv2){
         nz1+=colinfo(cind1,1);
         cind1++;
         if (cind1<colinfo.rowdim()){
             cv1=colinfo(cind1,0);
         }
         else {
             cv1=nc;
         }
     }
     else if (cv2<cv1) {
         return 0;
     }
     else { //cv1==cv2, both work on the same column
         Integer uind1=colinfo(cind1,2)+colinfo(cind1,1);
         Integer uind2=A.colinfo(cind2,2)+A.colinfo(cind2,1);
         Integer rind1,rind2;
         rind1= (nz1<uind1)? colindex(nz1):nr;
         rind2= (nz2<uind2)? A.colindex(nz2):nr;
         while((rind1<nr)||(rind2<nr)){
             if (rind1<rind2){
                 nz1++;
                 rind1= (nz1<uind1)? colindex(nz1):nr;
             }
             else if(rind2<rind1){
                 return 0;
             }
             else {
                 nz1++;
                 rind1= (nz1<uind1)? colindex(nz1):nr;
                 nz2++;
                 rind2= (nz2<uind2)? A.colindex(nz2):nr;
             }
         }
         cind1++;
         if (cind1<colinfo.rowdim()){
             cv1=colinfo(cind1,0);
         }
         else {
             cv1=nc;
         }
         cind2++;
         if (cind2<A.colinfo.rowdim()){
             cv2=A.colinfo(cind2,0);
         }
         else {
             cv2=nc;
         }
     }
 }while((cv1<nc)||(cv2<nc));
 
 return 1;
}


Real Sparsemat::operator()(Integer i,Integer j) const
{
 chk_init(*this);
 chk_range(i,j,nr,nc);
 Integer nrows=rowinfo.rowdim();
 if (nrows==0) return 0.;
 Integer ncols=colinfo.rowdim();
 if ((i<rowinfo(0,0))||(i>rowinfo(nrows-1,0))) return 0.;
 if ((j<colinfo(0,0))||(j>colinfo(ncols-1,0))) return 0.;
 if (nrows<=ncols){
     Integer li=0; 
     Integer ui=nrows-1;
     Integer mi=0;
     while(li<=ui) {
         mi=(li+ui)/2;
         if (rowinfo(mi,0)==i) break;
         if (rowinfo(mi,0)<i){
             li=mi+1;
         }
         else {
             ui=mi-1;
         }
     }
     if (li>ui) return 0.;
     li=rowinfo(mi,2);
     ui=li+rowinfo(mi,1)-1;
     while(li<=ui) {
         mi=(li+ui)/2;
         if (rowindex(mi)==j) break;
         if (rowindex(mi)<j){
             li=mi+1;
         }
         else {
             ui=mi-1;
         }
     }
     if (li>ui) return 0.;
     return rowval(mi);
 }
 Integer li=0; 
 Integer ui=ncols-1;
 Integer mi=0;
 while(li<=ui) {
     mi=(li+ui)/2;
     if (colinfo(mi,0)==j) break;
     if (colinfo(mi,0)<j){
         li=mi+1;
     }
     else {
         ui=mi-1;
     }
 }
 if (li>ui) return 0.;
 li=colinfo(mi,2);
 ui=li+colinfo(mi,1)-1;
 while(li<=ui) {
     mi=(li+ui)/2;
     if (colindex(mi)==i) break;
     if (colindex(mi)<i){
         li=mi+1;
     }
     else {
         ui=mi-1;
     }
 } 
 if (li>ui) return 0.;
 return colval(mi);
}

Sparsemat Sparsemat::col(Integer ci) const
{
  chk_init(*this);
  if ((nr==0)||(nc==0)){ 
     MEmessage(MatrixError(ME_unspec,"Sparsemat::col(Integer ci): n==0 or m==0",MTsparse));
     exit(1);
  }
  chk_range(0,ci,nr,nc);
  Sparsemat A;
  A.nr=nr;
  A.nc=1;
  A.tol=tol;
  chk_set_init(A,1);
  if (colinfo.dim()==0) return A; //no nonzero columns
  if ((ci<colinfo(0,0))||(ci>colinfo(colinfo.rowdim()-1,0))) return A;
  //binary search for column ci
  Integer lb=0;
  Integer ub=colinfo.rowdim()-1;
  Integer ii;
  while (lb<=ub){
    ii=(lb+ub)/2;
    if (colinfo(ii,0)<ci) {
      lb=ii+1;
      continue;
    }
    if (colinfo(ii,0)>ci) { 
      ub=ii-1;
      continue;
    }
    break;
  }
  if (lb>ub) return A; //column ci is zero
  //--- column ci is stored in ii
  Integer nz=colinfo(ii,1);
  A.colinfo.newsize(1,3); chk_set_init(A.colinfo,1);
  A.colinfo(0,0)=0;
  A.colinfo(0,1)=nz;
  A.colinfo(0,2)=0;
  A.colindex.init(nz,1,colindex.get_store()+colinfo(ii,2));
  A.colval.init(nz,1,colval.get_store()+colinfo(ii,2));
  A.rowinfo.newsize(nz,3); chk_set_init(A.rowinfo,1);
  A.rowindex.init(nz,1,Integer(0)); //all go into column 0
  A.rowval=A.colval;
  for (Integer i=0;i<nz;i++){
    A.rowinfo(i,0)=A.colindex(i);
    A.rowinfo(i,1)=1;
    A.rowinfo(i,2)=i;
  }
  return A;
}

Sparsemat Sparsemat::row(Integer ri) const
{
  chk_init(*this);
  if ((nr==0)||(nc==0)){ 
     MEmessage(MatrixError(ME_unspec,"Sparsemat::row(Integer ri): n==0 or m==0",MTsparse));
     exit(1);
  }
  chk_range(ri,0,nr,nc);
  Sparsemat A;
  A.nr=1;
  A.nc=nc;
  A.tol=tol;
  chk_set_init(A,1);
  if (rowinfo.dim()==0) return A; //no nonzero rows
  if ((ri<rowinfo(0,0))||(ri>rowinfo(rowinfo.rowdim()-1,0))) return A;
  //binary search for row ri
  Integer lb=0;
  Integer ub=rowinfo.rowdim()-1;
  Integer ii;
  while (lb<=ub){
    ii=(lb+ub)/2;
    if (rowinfo(ii,0)<ri) {
      lb=ii+1;
      continue;
    }
    if (rowinfo(ii,0)>ri) { 
      ub=ii-1;
      continue;
    }
    break;
  }
  if (lb>ub) return A; //rowumn ri is zero
  //--- row ri is stored in ii
  Integer nz=rowinfo(ii,1);
  A.rowinfo.newsize(1,3); chk_set_init(A.rowinfo,1);
  A.rowinfo(0,0)=0;
  A.rowinfo(0,1)=nz;
  A.rowinfo(0,2)=0;
  A.rowindex.init(nz,1,rowindex.get_store()+rowinfo(ii,2));
  A.rowval.init(nz,1,rowval.get_store()+rowinfo(ii,2));
  A.colinfo.newsize(nz,3); chk_set_init(A.colinfo,1);
  A.colindex.init(nz,1,Integer(0));
  A.colval=A.rowval;
  for (Integer i=0;i<nz;i++){
    A.colinfo(i,0)=A.rowindex(i);
    A.colinfo(i,1)=1;
    A.colinfo(i,2)=i;
  }
  return A;
}

Sparsemat operator*(const Sparsemat &A,const Sparsemat &B) 
{
 chk_mult(A,B);
 Sparsemat C;
 C.nr=A.nr;
 C.nc=B.nc;
 C.rowinfo=A.rowinfo;
 C.colinfo=B.colinfo;

 //first run to find all nonzeros
 Integer i,j,nz=0;
 for(i=0;i<B.colinfo.rowdim();i++){
     C.colinfo(i,1)=0;
 }
 for(i=0;i<C.rowinfo.rowdim();i++){
     C.rowinfo(i,1)=0;
     for(j=0;j<C.colinfo.rowdim();j++){
         Real d=0.;
         Integer rk=A.rowinfo(i,2); 
         Integer ru=rk+A.rowinfo(i,1);
         Integer ck=B.colinfo(j,2);
         Integer cu=ck+B.colinfo(j,1);
         while((rk<ru)&&(ck<cu)){
             if (A.rowindex(rk)==B.colindex(ck)){
                 d+=A.rowval(rk)*B.colval(ck);
                 rk++;
                 ck++;
             }
             else if (A.rowindex(rk)<B.colindex(ck)){
                 rk++;
             }
             else {
                 ck++;
             }
         }
         if (my_abs(d)>C.tol){
             nz++;
             C.rowinfo(i,1)++;
             C.colinfo(j,1)++;
         }
     }
 }

 //initialize index and val to correct size
 if (nz==0) {
     C.colinfo.init(0,0,Integer(0));
     C.rowinfo.init(0,0,Integer(0));
     return C;
 }
 C.colval.newsize(nz,1); chk_set_init(C.colval,1);
 C.rowval.newsize(nz,1); chk_set_init(C.rowval,1);
 C.colindex.newsize(nz,1); chk_set_init(C.colindex,1);
 C.rowindex.newsize(nz,1); chk_set_init(C.rowindex,1);
 
 //second run to fill with correct contents
 nz=0;
 for(i=0;i<C.colinfo.rowdim();i++){
     //set start index for each column and clear column counter
     //so that both col and row can be stored simultaneously afterwards
     C.colinfo(i,2)=nz;
     nz+=C.colinfo(i,1);
     C.colinfo(i,1)=0;
 }
 nz=0;
 for(i=0;i<C.rowinfo.rowdim();i++){
     C.rowinfo(i,2)=nz;
     for(j=0;j<C.colinfo.rowdim();j++){
         Real d=0.;
         Integer rk=A.rowinfo(i,2); 
         Integer ru=rk+A.rowinfo(i,1);
         Integer ck=B.colinfo(j,2);
         Integer cu=ck+B.colinfo(j,1);
         while((rk<ru)&&(ck<cu)){
             if (A.rowindex(rk)==B.colindex(ck)){
                 d+=A.rowval(rk)*B.colval(ck);
                 rk++;
                 ck++;
             }
             else if (A.rowindex(rk)<B.colindex(ck)){
                 rk++;
             }
             else {
                 ck++;
             }
         }
         if (my_abs(d)>C.tol){
             C.rowindex(nz)=B.colinfo(j,0);
             C.rowval(nz)=d;
             C.colindex(C.colinfo(j,2)+C.colinfo(j,1))=A.rowinfo(i,0);
             C.colval(C.colinfo(j,2)+C.colinfo(j,1))=d;
             C.colinfo(j,1)++;
             nz++;
         }
     }
 }

 //check for empty columns or rows in info
 Indexmatrix delind=(C.rowinfo.col(1)).find_number(Integer(0));
 C.rowinfo.delete_rows(delind);
 delind=(C.colinfo.col(1)).find_number(Integer(0));
 C.colinfo.delete_rows(delind);

 return C;
}
    
void Sparsemat::display(ostream& out,
                  int precision,int width,int screenwidth) const
           //for variables of value zero default values are used
           //precision=4,width=precision+6,screenwidth=80
{
 chk_init(*this);
 if (precision==0) precision=4;
 if (width==0) width=precision+6;
 if (screenwidth==0) screenwidth=80;
 out.precision(precision);
 out<<"Sparsemat:"<<nr<<" "<<nc<<" "<<rowindex.dim()<<"\n";
 Integer i,j,nz=0;
 for(i=0;i<rowinfo.rowdim();i++){
     Integer rind=rowinfo(i,0);
     for(j=0;j<rowinfo(i,1);j++){
         out.width(5);out<<rind<<" ";
         out.width(5);out<<rowindex(nz)<<" ";
         out.width(width);out<<rowval(nz)<<"\n";
         nz++;
     }
 }
}

// *****************************************************************************
//                  Constructors from Sparsemat to other classes
// *****************************************************************************


// ******************************    Matrix   ******************************

Matrix& Matrix::xeya(const Sparsemat &A,Real d)
{
 chk_init(A);
 init(A.nr,A.nc,0.);
 if (d==0.) return *this;
 if (d==1.){
     const Real *avp=A.colval.get_store();
     const Integer *aip=A.colindex.get_store();
     for(Integer i=0;i<A.colinfo.rowdim();i++){
         Real *mp=m+A.colinfo(i,0)*nr;
         for(Integer j=A.colinfo(i,1);--j>=0;){
             *(mp+*aip++)=(*avp++);
         }
     }
     return *this;
 }
 if (d==-1.){
     const Real *avp=A.colval.get_store();
     const Integer *aip=A.colindex.get_store();
     for(Integer i=0;i<A.colinfo.rowdim();i++){
         Real *mp=m+A.colinfo(i,0)*nr;
         for(Integer j=A.colinfo(i,1);--j>=0;){
             *(mp+*aip++)=-(*avp++);
         }
     }
     return *this;
 }
 const Real *avp=A.colval.get_store();
 const Integer *aip=A.colindex.get_store();
 for(Integer i=0;i<A.colinfo.rowdim();i++){
     Real *mp=m+A.colinfo(i,0)*nr;
     for(Integer j=A.colinfo(i,1);--j>=0;){
         *(mp+*aip++)=d*(*avp++);
     }
 }
 return *this;    
}
 
Matrix& Matrix::xpeya(const Sparsemat &A,Real d)
{
 chk_add(*this,A);
 if (d==0.) return *this;
 if (d==1.){
     const Real *avp=A.colval.get_store();
     const Integer *aip=A.colindex.get_store();
     for(Integer i=0;i<A.colinfo.rowdim();i++){
         Real *mp=m+A.colinfo(i,0)*nr;
         for(Integer j=A.colinfo(i,1);--j>=0;){
             *(mp+*aip++)+=(*avp++);
         }
     }
     return *this;
 }
 if (d==-1.){
     const Real *avp=A.colval.get_store();
     const Integer *aip=A.colindex.get_store();
     for(Integer i=0;i<A.colinfo.rowdim();i++){
         Real *mp=m+A.colinfo(i,0)*nr;
         for(Integer j=A.colinfo(i,1);--j>=0;){
             *(mp+*aip++)-=(*avp++);
         }
     }
     return *this;
 }
 const Real *avp=A.colval.get_store();
 const Integer *aip=A.colindex.get_store();
 for(Integer i=0;i<A.colinfo.rowdim();i++){
     Real *mp=m+A.colinfo(i,0)*nr;
     for(Integer j=A.colinfo(i,1);--j>=0;){
         *(mp+*aip++)+=d*(*avp++);
     }
 }
 return *this;    
}

// ******************************************************************************
//                    friends
// ******************************************************************************

    
Real trace(const Sparsemat& A)
{
 chk_init(A);
 Integer i;
 Real s=0.;
 if (A.rowinfo.rowdim()<=A.colinfo.rowdim()){
     for(i=0;i<A.rowinfo.rowdim();i++){
         Integer rind=A.rowinfo(i,0);
         if (rind>=A.nc) return s;
         Integer li=A.rowinfo(i,2);
         Integer ui=li+A.rowinfo(i,1)-1;
         Integer mi=0;
         while(li<=ui){
             mi=(li+ui)/2;
             if (A.rowindex(mi)==rind) break;
             if (A.rowindex(mi)<rind){
                 li=mi+1;
             }
             else {
                 ui=mi-1;
             }
         } ;
         if (li<=ui) s+=A.rowval(mi);
     }         
 }
 else { 
     for(i=0;i<A.colinfo.rowdim();i++){
         Integer cind=A.colinfo(i,0);
         if (cind>=A.nr) return s;
         Integer li=A.colinfo(i,2);
         Integer ui=li+A.colinfo(i,1)-1;
         Integer mi=0;
         while(li<=ui){
             mi=(li+ui)/2;
             if (A.colindex(mi)==cind) break;
             if (A.colindex(mi)<cind){
                 li=mi+1;
             }
             else {
                 ui=mi-1;
             }
         }
         if (li<=ui) s+=A.colval(mi);
     }         
 }
 return s;
}
    
Real ip(const Sparsemat& A, const Sparsemat& B)
{
 chk_add(A,B);
 if (&A==&B)
     return mat_ip(A.colval.dim(),A.colval.get_store(),A.colval.get_store());
 Integer ai=0,bi=0;
 Real s=0.;
 while((ai<A.rowinfo.rowdim())&&(bi<B.rowinfo.rowdim())){
     if (A.rowinfo(ai,0)==B.rowinfo(bi,0)){
         Integer aj=A.rowinfo(ai,2);
         Integer au=aj+A.rowinfo(ai,1);
         Integer bj=B.rowinfo(bi,2);
         Integer bu=bj+B.rowinfo(bi,1);
         while((aj<au)&&(bj<bu)){
             if(A.rowindex(aj)==B.rowindex(bj)){
                 s+=A.rowval(aj)*B.rowval(bj);
                 aj++;
                 bj++;
             }
             else if (A.rowindex(aj)<B.rowindex(bj)){
                 aj++;
             }
             else {
                 bj++;
             }
         }
         ai++;
         bi++;
     }
     else if (A.rowinfo(ai,0)<B.rowinfo(bi,0)){
         ai++;
     }
     else {
         bi++;
     }
 }
 return s;
}
             
Real ip(const Sparsemat& A, const Matrix& B)
{
 chk_add(A,B);
 Real s=0.;
 const Real *avp=A.colval.get_store();
 const Integer *aip=A.colindex.get_store();
 for(Integer i=0;i<A.colinfo.rowdim();i++){
     const Real *bbp=B.get_store()+A.colinfo(i,0)*A.rowdim();
     for(Integer j=A.colinfo(i,1);--j>=0;){
         s+=*(bbp+*aip++)*(*avp++);
     }
 }
 return s;
}
             
 
Sparsemat abs(const Sparsemat& A)
{
 chk_init(A);
 Sparsemat B(A.nr,A.nc);
 B.rowinfo=A.rowinfo;
 B.colinfo=A.colinfo;
 B.rowindex=A.rowindex;
 B.colindex=A.colindex;
 B.rowval.newsize(A.rowval.dim(),1); chk_set_init(B.rowval,1);
 B.colval.newsize(A.colval.dim(),1); chk_set_init(B.colval,1);
 Real *br=B.rowval.get_store();
 const Real *ar=A.rowval.get_store();
 Real *bc=B.colval.get_store();
 const Real *ac=A.colval.get_store();
 Integer i=A.rowval.dim();
 for(;--i>=0;){
     *br++= my_abs(*ar++);
     *bc++= my_abs(*ac++);
 }
 return B;
}

ostream& operator<<(ostream& o,const Sparsemat &A)
{
 chk_init(A);
 o<<A.nr<<" "<<A.nc<<" "<<A.rowval.dim()<<"\n";
 Integer i,j,nz=0;
 for(i=0;i<A.rowinfo.rowdim();i++){
     Integer rind=A.rowinfo(i,0);
     for(j=0;j<A.rowinfo(i,1);j++){
         o<<rind<<" ";
         o<<A.rowindex(nz)<<" ";
         o<<A.rowval(nz)<<"\n";
         nz++;
     }
 }
 return o;
}

istream& operator>>(istream& in,Sparsemat &A)
{
 const char* format="         format: nrows ncols nz i_1 j_1 val_1 ... i_nz j_nz val_nz\n         nrows>=0, ncols>=0, nz>=0, 0<=i<nrows, 0<=j<ncols "; 
 Integer nc,nr,nz;
 if (!(in>>nr)){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" failed in reading number of rows"<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if(nr<0){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" number of rows must be nonnegative but is "<<nr<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>nc)){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" failed in reading number of columns"<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if(nc<0){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" number of columns must be nonnegative but is "<<nc<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>nz)){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" failed in reading number of nonzero elements"<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if(nz<0){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" number of nonzeros must be nonnegative but is "<<nz<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 if(((nr==0)||(nc==0))&&(nz>0)){
     cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
     cerr<<" zero row or column dimension but positive number of nonzeros"<<endl;
     cerr<<format;
     in.clear(ios::failbit);
     return in;
 }
 Integer i;
 Indexmatrix I(nz,1),J(nz,1);
 Matrix val(nz,1);
 chk_set_init(I,1);
 chk_set_init(J,1);
 chk_set_init(val,1);
 for(i=0;i<nz;i++){
     if (!(in>>I(i)>>J(i)>>val(i))){
         cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
         cerr<<" failed in reading nonzero element (i,j,val) #";
         cerr<<i<<endl;
         in.clear(ios::failbit);
         return in;
     }
     if ((0>I(i))||(I(i)>=nr)){
         cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
         cerr<<" row index of nonzero element #"<<i<<"exceeds range: ";
         cerr<<0<<"<="<<I(i)<<"<"<<nr<<endl;
         in.clear(ios::failbit);
         return in;
     }
     if ((0>J(i))||(J(i)>=nc)){
         cerr<<"*** ERROR: operator>>(istream&,Sparsemat&): ";
         cerr<<" column index of nonzero element #"<<i<<"exceeds range: ";
         cerr<<0<<"<="<<J(i)<<"<"<<nc<<endl;
         in.clear(ios::failbit);
         return in;
     }
 }
 A.init(nr,nc,nz,I,J,val);
 return in;
}
 
/* ---------------------------------------------------------------------------
 *    Change log $Log: sparsmat.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:22  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/sparsmat.cc,v $
 --------------------------------------------------------------------------- */
