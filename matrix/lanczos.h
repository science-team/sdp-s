/* -*-c++-*-
 * 
 *    Source     $RCSfile: lanczos.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:18 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __LANCZOS_H__
#define __LANCZOS_H__

#include "matrix.h"

class Lanczosmatrix
{
 public:
    virtual Integer lanczosdim() const=0;
    virtual Integer lanczosflops() const=0;
        //returns estimate of number of flops for lanczosmult for vectors
    virtual int lanczosmult(const Matrix& A,Matrix& B) const=0;
        //computes  B = bigMatrix * A
        //A and B must not be the same object!
};

class Lanczos
{
public:
    virtual void set_do_out(int o)=0;
    virtual void set_myout(ostream& o)=0;

    virtual int get_err() const=0;
    virtual Integer get_iter() const=0;
    virtual Integer get_nmult() const=0;
    virtual int get_lanczosvecs(Matrix& val,Matrix& vecs) const=0;
        //if compute was already run, the first neigfound+blocksz
        //Lanczos vectors and their values have to be returned

    virtual void set_mineig(Real ie)=0;
    virtual void set_maxmult(Integer mop)=0;
    virtual void set_maxiter(Integer mi)=0;
    virtual void set_relprec(Real relprec)=0;  //relative precision
    virtual void set_nblockmult(Integer nb)=0;
    virtual void set_nchebit(Integer nc)=0;
    virtual void enable_stop_above(Real ub)=0;
    virtual void disable_stop_above()=0;
    virtual void set_retlanvecs(Integer nl)=0; //return at most this number of 
                                               //Lanczosvectors in get_lanczosvecs
    
    virtual Real get_relprec(void)=0;

    virtual int compute(Lanczosmatrix* bigmat,Matrix& eigval,Matrix& eigvec,
                Integer nreig,Integer in_blocksz=0,Integer maxcol=0)=0;

    virtual ostream& save(ostream& out) const =0;
    virtual istream& restore(istream& in) =0;
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: lanczos.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:18  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/lanczos.h,v $
 --------------------------------------------------------------------------- */
