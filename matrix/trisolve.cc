/* -*-c++-*-
 * 
 *    Source     $RCSfile: trisolve.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:20 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: trisolve.cc,v 1.1.1.1.2.1 2000/02/28 13:55:20 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "matrix.h"
#ifdef WITH_SYMMATRIX
#include "symmat.h"
#endif
#include "heapsort.h"
#include "gb_rand.h"


// **************************************************************************
//                                 triu_solve
// **************************************************************************

//On input matrix X is the right hand side of an equation (*this)*X=rhs where
//only the upper triangle of the first n lines of (*this) forming a square matrix
//is used. On output X is the solution.
//Back substitution.
//returns:
//  0      if all diagonal elements were larger than tol in absolut value ->ok
//  index+1  of the first diag elem violating this condition -> error

int Matrix::triu_solve(Matrix& X,Real tol)
{
 chk_init(*this);
 chk_init(X);
 Integer n=min(nr,nc); 
#ifdef DEBUG
 if ((X.nr!=n)||(X.nc<1)) MEmessage(MEdim(nr,nc,X.nr,X.nc,"Matrix::triu_solve(): rhs does no match min(nr,nc)",MTmatrix));
#endif
 Integer i,j;
 Real d;
 Real *xp;
 for(i=n;--i>=0;){  //solve for variable row i
     d=(*this)(i,i);
     if (my_abs(d)<tol) {
         xp=X.m+i;
         j=0;
         while((j<X.nc)&&(my_abs(*xp)<tol)) {*xp=0.; j++; xp+=X.nr;}
         if (j<X.nc) return i+1;
         continue;
     }
     xp=X.m+i;
     for(j=0;j<X.nc;j++,xp+=X.nr){
         (*xp)/=d;
         mat_xpeya(i,X.m+j*X.nr,m+i*nr,-(*xp));
     }
 }
 return 0;
}

// **************************************************************************
//                                 tril_solve
// **************************************************************************

//On input matrix X is the right hand side of an equation (*this)*X=rhs where
//only the lower triangle of the first n lines of (*this) forming a square matrix
//is used. On output X is the solution.
//Forward substitution.
//returns:
//  0      if all diagonal elements were larger than tol in absolut vlaue ->ok
//  index+1  of the first diag elem violating this condition -> error

int Matrix::tril_solve(Matrix& X,Real tol)
{
 chk_init(*this);
 chk_init(X);
 Integer n=min(nr,nc); 
#ifdef DEBUG
 if ((X.nr!=n)||(X.nc<1)) MEmessage(MEdim(nr,nc,X.nr,X.nc,"Matrix::tril_solve(): rhs does not match min(nr,nc)"));
#endif
 chk_rowseq(*this,X);
 Integer i,j;
 Real d;
 Real *xp;
 for(i=0;i<n;i++){  //solve for variable row i
     d=(*this)(i,i);
     if (my_abs(d)<tol)  {
         xp=X.m+i;
         j=0;
         while((j<X.nc)&&(my_abs(*xp)<tol)) {*xp=0.; j++; xp+=X.nr;}
         if (j<X.nc) return i+1;
         continue;
     }
     xp=X.m+i;
     for(j=0;j<X.nc;j++,xp+=X.nr){
         (*xp)/=d;
         mat_xpeya(nr-i-1,X.m+j*X.nr+i+1,m+i*nr+i+1,-(*xp));
     }
 }
 return 0;
}



/* ---------------------------------------------------------------------------
 *    Change log $Log: trisolve.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:20  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/trisolve.cc,v $
 --------------------------------------------------------------------------- */
