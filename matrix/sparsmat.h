/* -*-c++-*-
 * 
 *    Source     $RCSfile: sparsmat.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:23 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SPARSMAT_H__
#define __SPARSMAT_H__

#include "matrix.h"

#define SPARSE_ZERO_TOL 1e-60

//non inline included by #ifdef WITH_SYMMATRIX is implemented in "symmat.cc"
//non inline included by #ifdef WITH_SPARSESYM is implemented in "sparssym.cc"

#ifdef WITH_SYMMATRIX
class Symmatrix;
#endif

#ifdef WITH_SPARSESYM
class Sparsesym;
#endif

class Sparsemat: protected Memarrayuser
{
    friend class Indexmatrix;
    friend class Matrix;
#ifdef WITH_SYMMATRIX
    friend class Symmatrix;
#endif
#ifdef WITH_SPARSESYM
    friend class Sparsesym;
#endif
    
private:

    Mtype mtype;   //name of this type to recognize special kinds of matrices
    Integer nr,nc;     //number rows, number columns

    //column by column representation
    Indexmatrix colinfo;  //k by 3 matrix, gives for nonzero columns
                          //index of column, nr of nonzero elements
                          //and first element in colindex/colval
    Indexmatrix colindex; //gives the rowindex of the element at position i
                          //(sorted increasingly in each column)
    Matrix colval;        //gives the value    of the element at position i

    //row by row representation (see column by column)
    Indexmatrix rowinfo;
    Indexmatrix rowindex;
    Matrix rowval;
        
    Real tol;          //>0, if abs(number)<tol, then number is taken to be zero

    #ifdef DEBUG
    Integer is_init;      //flag whether memory is initialized
    #endif

    void init_to_zero();            // (inline), initialization to zero

public:

  //----------------------------------------------------
  //----  constructors, destructor, and initialization
  //----------------------------------------------------

  inline Sparsemat();                      
  inline Sparsemat(const Sparsemat&,Real d=1.);       
  inline Sparsemat(const Matrix&, Real d=1.);          
  inline Sparsemat(const Indexmatrix&,Real d=1.);     
  inline Sparsemat(Integer nr,Integer nc);  //zero-matrix of size nr*nc
  inline Sparsemat(Integer nr,Integer nc,Integer nz,
		   const Integer *ini,const Integer *inj,const Real* va);
  inline Sparsemat(Integer nr,Integer nc,Integer nz,
		   const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va);

#ifdef DEBUG
    void set_init(Integer i){is_init=i;}
    int get_init() const {return is_init;} 
#endif 

  inline Sparsemat& init(const Sparsemat&,Real d=1.);       
  inline Sparsemat& init(const Matrix&,Real d=1.);          
  inline Sparsemat& init(const Indexmatrix&,Real d=1.);     
  inline Sparsemat& init(Integer nr,Integer nc);  //zero-matrix of size nr*nc
  Sparsemat& init(Integer nr,Integer nc,Integer nz,
		  const Integer *ini,const Integer *inj,const Real* va);
  Sparsemat& init(Integer nr,Integer nc,Integer nz,
		  const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va);


  void set_tol(Real t){tol=t;}


  //----------------------------------------------------
  //----  size and type information
  //----------------------------------------------------

  void dim(Integer& r, Integer& c) const {r=nr; c=nc;}
  Integer dim() const {return nr*nc;}
  Integer rowdim() const {return nr;}
  Integer coldim() const {return nc;}
  Integer nonzeros() const {return colval.dim();}
  
  Mtype get_mtype() const {return mtype;}

    
  //--------------------------------
  //----  Indexing and Submatrices
  //--------------------------------

  const Indexmatrix& get_colinfo() const {return colinfo;}
  const Indexmatrix& get_colindex() const {return colindex;}
  const Matrix& get_colval() const {return colval;}
  const Indexmatrix& get_rowinfo() const {return rowinfo;}
  const Indexmatrix& get_rowindex() const {return rowindex;}
  const Matrix& get_rowval() const {return rowval;}
  
  void get_edge_rep(Indexmatrix& I, Indexmatrix& J, Matrix& val) const;
  void get_edge(Integer i,Integer& indi,Integer& indj, Real& val) const;
  int contains_support(const Sparsemat& A) const;
    
  Real operator()(Integer i,Integer j) const;     
  inline Real operator()(Integer i) const;           

  inline Real operator[](Integer i) const; 

  Sparsemat col(Integer i) const;
  Sparsemat row(Integer i) const;
  
  //------------------------------
  //----  BLAS-like Routines
  //------------------------------

  Sparsemat& xeya(const Sparsemat& A,Real d=1.);
  //returns (*this)=A*d;
  Sparsemat& xeya(const Matrix& A,Real d=1.);
  //returns (*this)=A*d;
  Sparsemat& xeya(const Indexmatrix& A,Real d=1.);
  //returns (*this)=A*d;

  friend Matrix& genmult(const Sparsemat& A,const Matrix& B,Matrix &C,
			 Real alpha=1.,Real beta=0., int atrans=0,int btrans=0);
      //C=beta*C+alpha A B

  friend Matrix& genmult(const Matrix& A,const Sparsemat& B,Matrix &C,
			 Real alpha=1.,Real beta=0., int atrans=0,int btrans=0);
      //C=beta*C+alpha A B
  
  friend Matrix& genmult(const Sparsemat& A,const Sparsemat& B,Matrix &C,
			 Real alpha=1.,Real beta=0., int atrans=0,int btrans=0);
      //C=beta*C+alpha A B


  //------------------------------
  //----  usual operators
  //------------------------------

  Sparsemat& operator=(const Sparsemat& A);
  Sparsemat operator-() const;
  
  Sparsemat& operator*=(Real d);
  Sparsemat& operator/=(Real d);
  
  Sparsemat& operator=(const Matrix& A);
  Sparsemat& operator=(const Indexmatrix& A);


  friend Sparsemat operator*(const Sparsemat& A,const Sparsemat& B);

  friend inline Sparsemat operator*(const Sparsemat& A,Real d);
  friend inline Sparsemat operator*(Real d,const Sparsemat& A);
  friend inline Sparsemat operator/(const Sparsemat& A,Real d);
  
  friend inline Matrix operator*(const Sparsemat& A,const Matrix& B);
  friend inline Matrix operator*(const Matrix& A,const Sparsemat& B);
  friend inline Matrix operator+(const Sparsemat& A,const Matrix& B);
  friend inline Matrix operator+(const Matrix& A,const Sparsemat& B);
  friend inline Matrix operator-(const Sparsemat& A,const Matrix& B);
  friend inline Matrix operator-(const Matrix& A,const Sparsemat& B);

  Sparsemat& transpose();            //transposes itself
  friend inline Sparsemat transpose(const Sparsemat& A);


  //------------------------------------------
  //----  Connections to other Matrix Classes
  //------------------------------------------
  
#ifdef WITH_SYMMATRIX
  friend Matrix& genmult(const Symmatrix& A,const Sparsemat& B,Matrix& C,
			 Real alpha,Real beta,int btrans);
      //returns C=beta*C+alpha*A*B, where A and B may be transposed

  friend Matrix& genmult(const Sparsemat& A,const Symmatrix& B,Matrix& C,
			 Real alpha,Real beta, int atrans);

  friend Symmatrix& rankadd(const Sparsemat& A,Symmatrix& C,
			    Real alpha,Real beta,int trans);
      // returns C=beta*C+alpha* A*A^T, where A may be transposed
    
  friend Symmatrix& rank2add(const Sparsemat& A,const Matrix& B,Symmatrix& C,
			     Real alpha,Real beta,int trans);
      // returns C=beta*C+alpha* sym(A*B^T) [or sym(A^T*B)]

  inline Sparsemat(const Symmatrix&,Real d=1.); 
  inline Sparsemat& init(const Symmatrix&,Real d=1.);     
  inline Sparsemat& operator=(const Symmatrix &);
  friend inline Matrix operator*(const Symmatrix& A,const Sparsemat& B);
  friend inline Matrix operator*(const Sparsemat& A,const Symmatrix& B);
#endif

#ifdef WITH_SPARSESYM
  Sparsemat& xeya(const Sparsesym& S,Real d=1.);
  friend Matrix& genmult(const Sparsesym& A,const Sparsemat& B,Matrix &C,
			 Real alpha,Real beta, int btrans);
  friend Matrix& genmult(const Sparsemat& A,const Sparsesym& B,Matrix &C,
			 Real alpha,Real beta,int atrans);
  inline Sparsemat(const Sparsesym&,Real d=1.);
  inline Sparsemat& init(const Sparsesym&,Real d=1.);    
  inline Sparsemat& operator=(const Sparsesym &);
#endif
    

  //------------------------------
  //----  Elementwise Operations
  //------------------------------

  friend Sparsemat abs(const Sparsemat& A);                 

  //----------------------------
  //----  Numerical Methods
  //----------------------------

  friend Real trace(const Sparsemat& A);               //=sum(diag(A))
  friend Real ip(const Sparsemat& A, const Sparsemat& B); //=trace(B^t*A)
  friend Real ip(const Sparsemat& A, const Matrix& B); //=trace(B^t*A)
  friend inline Real ip(const Matrix& A, const Sparsemat& B);

  //---------------------------------------------
  //----  Comparisons / Max / Min / sort / find
  //---------------------------------------------


  //--------------------------------
  //----  Input / Output
  //--------------------------------

    void display(ostream& out,
                  int precision=0,int width=0,int screenwidth=0) const;
           //for variables of value zero default values are used
           //precision=4,width=precision+6,screenwidth=80

    friend ostream& operator<<(ostream& o,const Sparsemat &v);
    friend istream& operator>>(istream& i,Sparsemat &v);

};


// **************************************************************************
//                   implementation of inline functions
// **************************************************************************

inline void Sparsemat::init_to_zero()
{
 mtype=MTsparse;
 nr=nc=0;
 tol=SPARSE_ZERO_TOL;
#ifdef DEBUG
 is_init=1;
#endif
}

//initialize

inline Sparsemat& Sparsemat::init(const Sparsemat& A,Real d)
{ return xeya(A,d);}
inline Sparsemat& Sparsemat::init(const Matrix& A,Real d)
{ return xeya(A,d);}
inline Sparsemat& Sparsemat::init(const Indexmatrix& A,Real d)
{ return xeya(A,d);}


inline Sparsemat& Sparsemat::init(Integer r,Integer c)
{
nr=r; nc=c;
colinfo.init(0,0,Integer(0)); colindex.init(0,0,Integer(0)); colval.init(0,0,0.);
rowinfo.init(0,0,Integer(0)); rowindex.init(0,0,Integer(0)); rowval.init(0,0,0.);
chk_set_init(*this,1);
return *this;
}

inline Sparsemat::Sparsemat()
{ init_to_zero(); chk_set_init(*this,1);}

inline Sparsemat::Sparsemat(const Sparsemat& A,double d)
{ init_to_zero(); xeya(A,d);}
inline Sparsemat::Sparsemat(const Matrix& A,double d)
{ init_to_zero(); xeya(A,d);}
inline Sparsemat::Sparsemat(const Indexmatrix& A,double d)
{ init_to_zero(); xeya(A,d);}

inline Sparsemat::Sparsemat(Integer r,Integer c)
{ init_to_zero(); init(r,c); }

inline Sparsemat::Sparsemat(Integer in_nr,Integer in_nc,Integer nz,
       const Integer *ini,const Integer *inj,const Real* va)
{ init_to_zero(); init(in_nr,in_nc,nz,ini,inj,va);}

inline Sparsemat::Sparsemat(Integer in_nr,Integer in_nc,Integer nz,
              const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va)
{ init_to_zero(); init(in_nr,in_nc,nz,ini,inj,va);}

inline Real Sparsemat::operator()(Integer i) const
{return (*this)(i%nr,i/nr);}           
inline Real Sparsemat::operator[](Integer i) const
{return (*this)(i%nr,i/nr);}
    
inline Sparsemat& Sparsemat::transpose()
{chk_init(*this);Integer h=nr; nr=nc;nc=h;swap(colinfo,rowinfo);
 swap(colindex,rowindex);swap(colval,rowval);return *this;}          
    

inline Sparsemat& Sparsemat::operator=(const Sparsemat &A)
{ return xeya(A); }
inline Sparsemat& Sparsemat::operator*=(Real d)
{chk_init(*this); colval*=d; rowval*=d; return *this;}
inline Sparsemat& Sparsemat::operator/=(Real d)
{chk_init(*this);colval/=d;rowval/=d;return *this;}
inline Sparsemat Sparsemat::operator-() const
{return Sparsemat(*this,-1.); }
inline Sparsemat operator*(const Sparsemat& A,Real d)
{return Sparsemat(A,d);}
inline Sparsemat operator*(Real d,const Sparsemat& A) 
{return Sparsemat(A,d);}
inline Sparsemat operator/(const Sparsemat& A,Real d) 
{return Sparsemat(A,1./d);}

inline Sparsemat& Sparsemat::operator=(const Matrix &A)
{return xeya(A);}
inline Sparsemat& Sparsemat::operator=(const Indexmatrix &A)
{return xeya(A);}


inline Matrix operator*(const Sparsemat& A,const Matrix& B) 
    {Matrix C;return genmult(A,B,C);}
inline Matrix operator*(const Matrix& A,const Sparsemat& B) 
    {Matrix C;return genmult(A,B,C);}

inline Matrix operator+(const Sparsemat& A,const Matrix& B)
    {Matrix C(B);return C.xpeya(A);}
inline Matrix operator+(const Matrix& A,const Sparsemat& B)
    {Matrix C(A);return C.xpeya(B);}
inline Matrix operator-(const Sparsemat& A,const Matrix& B)
    {Matrix C(B,-1.);return C.xpeya(A);}
inline Matrix operator-(const Matrix& A,const Sparsemat& B)
    {Matrix C(A);return C.xpeya(B,-1.);}


#ifdef WITH_SYMMATRIX
inline Sparsemat::Sparsemat(const Symmatrix& A,Real d)
{ init_to_zero(); xeya(Matrix(A));}
inline Sparsemat& Sparsemat::init(const Symmatrix &A,Real d)
{return xeya(Matrix(A),d);}
inline Sparsemat& Sparsemat::operator=(const Symmatrix &A)
{return xeya(Matrix(A));}
#endif

#ifdef WITH_SPARSESYM
inline Sparsemat::Sparsemat(const Sparsesym& A,Real d)
{ init_to_zero(); xeya(A,d);}
inline Sparsemat& Sparsemat::init(const Sparsesym& A,Real d) 
{ return xeya(A,d);}   
inline Sparsemat& Sparsemat::operator=(const Sparsesym &A)
{ return xeya(A);}  
inline Matrix operator*(const Symmatrix& A,const Sparsemat& B)
{ Matrix C; return genmult(A,B,C,1.,0.,0);} 
inline Matrix operator*(const Sparsemat& A,const Symmatrix& B)
{ Matrix C; return genmult(A,B,C,1.,0.,0);}  
#endif


inline Real ip(const Matrix& A, const Sparsemat& B)
    {return ip(B,A);}
inline Sparsemat transpose(const Sparsemat& A)
    {Sparsemat B(A); B.transpose(); return B;}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: sparsmat.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:23  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/sparsmat.h,v $
 --------------------------------------------------------------------------- */
