/* -*-c++-*-
 * 
 *    Source     $RCSfile: indexmat.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:17 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */
// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __INDEXMAT_H__
#define __INDEXMAT_H__

#include <iostream>
#include <iomanip>
#include "memarray.h"
#include "matop.h"
#include "gb_rand.h"
#include "mymath.h"

//#define WITH_MATRIX
//everything included by #ifdef WITH_MATRIX is implemented in "matrix.cc"
//#define WITH_SYMMATRIX
//everything included by #ifdef WITH_SYMMATRIX is implemented in "symmat.cc"
//#define WITH_SPARSEMAT
//everything included by #ifdef WITH_SPARSEMAT is implemented in "sparsmat.cc"
//#define WITH_SPARSESYM
//everything included by #ifdef WITH_SPARSESYM is implemented in "sparssym.cc"

// **************************************************************************
//                               Matrix Types
// **************************************************************************

enum Mtype {
    MTindexmatrix,
    MTmatrix,        
    MTsymmetric,
    MTsparse,
    MTsparsesym
};

// **************************************************************************
//                                mat_randgen
// **************************************************************************

//common random number generator used by all matrix-type implementations

extern GB_rand mat_randgen;

// **************************************************************************
//                               "exceptions"
// **************************************************************************

//the following are not really exceptions but only ment to facilitate
//error detection.
//In a debugger catch the Matrix Errors by setting a breakpoint in MEmessage

enum MEcode {ME_unspec,ME_range,ME_mem,ME_dim,ME_num,ME_warning};

class MatrixError
{
public:
    MEcode code;      
    const char *message;     //must point to existing object, does not get freed.
    Mtype mtype;
    MatrixError(MEcode c=ME_unspec,const char *mes=0,Mtype mt=MTmatrix):
        code(c),message(mes),mtype(mt){}
};

//--- some index is out of range
class MErange:public MatrixError
{
public:
    Integer nr,r;   
    Integer nc,c;
    MErange(Integer inr,Integer ir,Integer inc,Integer ic,const char *mes=0,Mtype mt=MTmatrix):
        MatrixError(ME_range,mes,mt),nr(inr),r(ir),nc(inc),c(ic){}
};

//--- cannot allocate memory
class MEmem:public MatrixError
{
public:
    Integer size;
    MEmem(Integer s,const char *mes=0,Mtype mt=MTmatrix):
        MatrixError(ME_mem,mes,mt),size(s){}
};

//--- matrix dimensions do not agree for desired operation
class MEdim:public MatrixError
{
public:
    Integer nr1, nc1, nr2, nc2;
    MEdim(Integer r1,Integer c1,Integer r2,Integer c2,const char *mes=0,Mtype mt=MTmatrix):
        MatrixError(ME_dim,mes,mt),nr1(r1),nc1(c1),nr2(r2),nc2(c2){}
};

int MEmessage(const MatrixError&); //displays error message and terminates
                                   //or returns 1 in case of warnings

// **************************************************************************
//                         DEBUG error checking templates
// **************************************************************************

#ifdef DEBUG

template<class Mat>
inline void chk_init(const Mat& A)
{
 if (!A.get_init())
     MEmessage(MatrixError(ME_unspec,"Matrix not initialized",A.get_mtype()));
}

template<class Mat1,class Mat2>
inline void chk_add(const Mat1& A,const Mat2& B)
{
 chk_init(A); chk_init(B);
 if ((A.rowdim()!=B.rowdim())||(A.coldim()!=B.coldim()))
     MEmessage(MEdim(A.rowdim(),A.coldim(),B.rowdim(),B.coldim(),
                     "dimensions do not match in additive expression",
                     A.get_mtype()));
}

template<class Mat1,class Mat2>
inline void chk_mult(const Mat1& A,const Mat2& B)
{
 chk_init(A); chk_init(B);
 if (A.coldim()!=B.rowdim())
     MEmessage(MEdim(A.rowdim(),A.coldim(),B.rowdim(),B.coldim(),
                     "dimensions do not match in multiplicative expression",
                     A.get_mtype()));
}

template<class Mat1,class Mat2>
inline void chk_rowseq(const Mat1& A,const Mat2& B)
{
 chk_init(A); chk_init(B);
 if (A.rowdim()!=B.rowdim())
     MEmessage(MEdim(A.rowdim(),A.coldim(),B.rowdim(),B.coldim(),
                     "number of rows do not agree",
                     A.get_mtype()));
}

template<class Mat1,class Mat2>
inline void chk_colseq(const Mat1& A,const Mat2& B)
{
 chk_init(A); chk_init(B);
 if (A.coldim()!=B.coldim())
     MEmessage(MEdim(A.rowdim(),A.coldim(),B.rowdim(),B.coldim(),
                     "number of columns do not agree",
                     A.get_mtype()));
}

inline void chk_range(Integer r,Integer c,Integer ubr,Integer ubc)
{
 if ((r<0)||(c<0)||
     ((ubr>=0)&&(r>=ubr))||
     ((ubc>=0)&&(c>=ubc)))
     MEmessage(MErange(r,ubr,c,ubc,"index out of range or negative dimension"));
}

template<class Mat>
inline void chk_set_init(Mat &A,int val){A.set_init(val);}

#else

// if DEBUG is not definied all chk_functions are mapped to the null string

#define chk_init(x)
#define chk_add(x,y)
#define chk_mult(x,y)
#define chk_rowseq(x,y)
#define chk_colseq(x,y)
#define chk_range(u,v,w,x)
#define chk_set_init(x,y)

#endif 
 
// **************************************************************************
//                               Range
// **************************************************************************

//specifies a range of numbers: {i: i=from+k*step for some k\in N_0 and i<=to}

class Range
{
public:
    Integer from;
    Integer to;
    Integer step;
    Range(Integer f,Integer t,Integer s=1):from(f),to(t),step(s){}
    ~Range(){} 
};

// **************************************************************************
//                              Indexmatrix
// **************************************************************************

#ifdef WITH_MATRIX
    class Matrix;
#endif
#ifdef WITH_SYMMATRIX
    class Symmatrix;
#endif
#ifdef WITH_SPARSEMAT
    class Sparsemat;
#endif
#ifdef WITH_SPARSESYM
    class Sparsesym;
#endif

class Indexmatrix: protected Memarrayuser
{
    
#ifdef WITH_MATRIX
    friend class Matrix;
#endif
#ifdef WITH_SYMMATRIX
    friend class Symmatrix;
#endif
#ifdef WITH_SPARSEMAT
    friend class Sparsemat;
#endif
#ifdef WITH_SPARSESYM
    friend class Sparsesym;
#endif

    
private:
    Mtype mtype;
    Integer mem_dim;   //amount of memory currently allocated
    Integer nr,nc;     //number rows, number columns
    Integer *m;     //stored columnwise (a11,a21,...,anr1,a12,a22,.....)

    #ifdef DEBUG
    Integer is_init;      //flag whether memory is initialized
    #endif

    inline void init_to_zero();    // (inline)
    
public:

  //----------------------------------------------------
  //----  constructors, destructor, and initialization
  //----------------------------------------------------

  inline Indexmatrix();                              
  inline Indexmatrix(const Indexmatrix&,Integer d=1);       
  inline Indexmatrix(const Range&);              
  inline Indexmatrix(Integer nr,Integer nc);         
  inline Indexmatrix(Integer nr,Integer nc,Integer d);  
  inline Indexmatrix(Integer nr,Integer nc,const Integer* dp,Integer incr=1); 
  ~Indexmatrix(){memarray.free(m);}
  
#ifdef DEBUG
  void set_init(Integer i){is_init=i;}
  int get_init() const {return is_init;}
#endif
  
  inline Indexmatrix& init(const Indexmatrix &A,Integer d=1);                   
  Indexmatrix& init(const Range&);
  inline Indexmatrix& init(Integer r,Integer c,Integer d);                    
  inline Indexmatrix& init(Integer r,Integer c,const Integer *dp,Integer incr=1); 
    
  void newsize(Integer r,Integer c);  //resize matrix without initialization


  //----------------------------------------------------
  //----  size and type information
  //----------------------------------------------------

  void dim(Integer& r, Integer& c) const {r=nr; c=nc;}
  Integer dim() const {return nr*nc;}
  Integer rowdim() const {return nr;}
  Integer coldim() const {return nc;}
  
  Mtype get_mtype() const {return mtype;}

    
  //--------------------------------
  //----  Indexing and Submatrices
  //--------------------------------

  inline Integer& operator()(Integer i,Integer j);      
  inline Integer& operator()(Integer i);      //index to vector of stacked columns
  inline Integer operator()(Integer i,Integer j) const; 
  inline Integer operator()(Integer i) const; //index to vector of stacked columns
  Indexmatrix operator()(const Indexmatrix& vecrow,const Indexmatrix& veccol) const;
      //extract submatrix, both indexmatrices are interpreted as vectors
      //repeated elements are allowed
  Indexmatrix operator()(const Indexmatrix& A) const;
      //*this matrix is interpreted as vector, the resulting matrix has
      //has the same shape as A
  
  inline Integer& operator[](Integer i);      //{return (*this)(i);}
  inline Integer operator[](Integer i) const; //{return (*this)(i);}
  
  Indexmatrix col(Integer i) const;          //returns column i as column vector
  Indexmatrix row(Integer i) const;          //returns row i as row vector
  Indexmatrix cols(const Indexmatrix &vec) const;  //returns cols as indexed by vec
  Indexmatrix rows(const Indexmatrix &vec) const;  //returns rows as indexed by vec

  friend Indexmatrix diag(const Indexmatrix& A);      //=(A(1,1),A(2,2),...)^t

  Indexmatrix& triu(Integer d=0);    // (*this)(i,j)=0 for i<j+d
  Indexmatrix& tril(Integer d=0);    // (*this)(i,j)=0 for i>j+d
  friend inline Indexmatrix triu(const Indexmatrix& A,Integer i=0);
  friend inline Indexmatrix tril(const Indexmatrix& A,Integer i=0);
  
  Indexmatrix& subassign(const Indexmatrix& vecrow,const Indexmatrix& veccol,
		    const Indexmatrix& A);
     //(*this)(vecrow(i),veccol(j))=A(i,j) for all i,j
  Indexmatrix& subassign(const Indexmatrix& vec,const Indexmatrix& A);
     //(*this)(vec(i))=A(i);
     //*this, vec, and A may be rect matrices but will be used as vectors
  
  Indexmatrix& delete_rows(const Indexmatrix& ind);
  Indexmatrix& delete_cols(const Indexmatrix& ind);

  inline Indexmatrix& reduce_length(Integer n);  
     //interpret *this as a column vector and cut at n 

  Indexmatrix& concat_right(const Indexmatrix& A);
  Indexmatrix& concat_below(const Indexmatrix& A);
  friend inline Indexmatrix concat_right(const Indexmatrix& A,const Indexmatrix& B);
  friend inline Indexmatrix concat_below(const Indexmatrix& A,const Indexmatrix& B);
  
  friend void swap(Indexmatrix &A,Indexmatrix &B);

  Integer* get_store() {return m;}       //use cautiously, do not use delete!
  const Integer* get_store() const {return m;}   //use cautiously

  
  //------------------------------
  //----  BLAS-like Routines
  //------------------------------

  Indexmatrix& xeya(const Indexmatrix& A,Integer d=1);   //*this=d*A
  Indexmatrix& xpeya(const Indexmatrix& A,Integer d=1);  //*this+=d*A;
    
  friend Indexmatrix& xbpeya(Indexmatrix& x,const Indexmatrix& y,Integer alpha=1,Integer beta=0,int ytrans=0);
  //returns x= alpha*y+beta*x, where y may be transposed (ytrans=1)
  //if beta==0. then x is initialized to the correct size
  
  friend Indexmatrix& xeyapzb(Indexmatrix& x,const Indexmatrix& y,const Indexmatrix& z,Integer alpha=1,Integer beta=1);
  //returns x= alpha*y+beta*z
  //x is initialized to the correct size
  
  friend Indexmatrix& genmult(const Indexmatrix& A,const Indexmatrix& B,Indexmatrix& C,
			 Integer alpha=1,Integer beta=0,int atrans=0,int btrans=0);
  //returns C=beta*C+alpha*A*B, where A and B may be transposed
  //C may neither be equal to A nor B
  //if beta==0 then C is initialized to the correct size
    

  //------------------------------
  //----  usual operators
  //------------------------------

  inline Indexmatrix& operator=(const Indexmatrix &A);
  inline Indexmatrix& operator*=(const Indexmatrix &s);
  inline Indexmatrix& operator+=(const Indexmatrix &v);
  inline Indexmatrix& operator-=(const Indexmatrix &v);
  inline Indexmatrix& operator%=(const Indexmatrix &A);   //Hadamard product!!
  inline Indexmatrix operator-() const;
  
  inline Indexmatrix& operator*=(Integer d);
  inline Indexmatrix& operator/=(Integer d);
  inline Indexmatrix& operator%=(Integer d); //modulo
  inline Indexmatrix& operator+=(Integer d);
  inline Indexmatrix& operator-=(Integer d);
  
  Indexmatrix& transpose();            //transposes itself
  
  friend inline Indexmatrix operator*(const Indexmatrix &A,const Indexmatrix& B);
  friend inline Indexmatrix operator+(const Indexmatrix &A,const Indexmatrix& B);
  friend inline Indexmatrix operator-(const Indexmatrix &A,const Indexmatrix& B);
  friend inline Indexmatrix operator%(const Indexmatrix &A,const Indexmatrix& B);
  friend inline Indexmatrix operator*(const Indexmatrix &A,Integer d);
  friend inline Indexmatrix operator*(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator/(const Indexmatrix& A,Integer d);
  friend inline Indexmatrix operator%(const Indexmatrix& A,Integer d);
  friend inline Indexmatrix operator+(const Indexmatrix& A,Integer d);
  friend inline Indexmatrix operator+(Integer d,const Indexmatrix& A);
  friend inline Indexmatrix operator-(const Indexmatrix& A,Integer d);
  friend inline Indexmatrix operator-(Integer d,const Indexmatrix& A);

  friend Indexmatrix transpose(const Indexmatrix& A);

  //------------------------------------------
  //----  Connections to other Matrix Classes
  //------------------------------------------

#ifdef WITH_MATRIX
    Indexmatrix(const Matrix&);          //copy with rounding
#endif
#ifdef WITH_SYMMATRIX
    Indexmatrix(const Symmatrix&);       //copy with rounding
#endif
#ifdef WITH_SPARSEMAT
    //Indexmatrix(const Sparsemat&);     //copy with rounding
#endif
#ifdef WITH_SPARSESYM
    Indexmatrix(const Sparsesym&);       //copy with rounding
#endif

  //------------------------------
  //----  Elementwise Operations
  //------------------------------

  Indexmatrix& rand(Integer nr,Integer nc,Integer lowerb,Integer upperb);
  Indexmatrix& sign(void);
  Indexmatrix& abs(void);        

  friend inline Indexmatrix rand(Integer nr,Integer nc,Integer lb,Integer ub);
  friend inline Indexmatrix sign(const Indexmatrix& A);
  friend Indexmatrix abs(const Indexmatrix& A);                 

  //----------------------------
  //----  Numerical Methods
  //----------------------------

  friend Integer trace(const Indexmatrix& A);               //=sum(diag(A))
  friend inline Integer ip(const Indexmatrix& A, const Indexmatrix& B); //=trace(B^t*A)
  friend inline Real norm2(const Indexmatrix& A);               //=sqrt(ip(A,A));

  Indexmatrix& scale_rows(const Indexmatrix& vec);    //A=diag(vec)*A
  Indexmatrix& scale_cols(const Indexmatrix& vec);    //A=A*diag(vec)
  
  friend Indexmatrix sumrows(const Indexmatrix& A);   //=(1 1 1 ... 1)*A
  friend Indexmatrix sumcols(const Indexmatrix& A);   //=A*(1 1 ... 1)^t
  friend Integer sum(const Indexmatrix& A);         //=(1 1 ... 1)*A*(1 1 ... 1)^t

  //---------------------------------------------
  //----  Comparisons / Max / Min / sort / find
  //---------------------------------------------

  friend Indexmatrix operator<(const Indexmatrix &A,const Indexmatrix &B);
  friend inline Indexmatrix operator>(const Indexmatrix &A,const Indexmatrix &B);
  friend Indexmatrix operator<=(const Indexmatrix &A,const Indexmatrix &B);
  friend inline Indexmatrix operator>=(const Indexmatrix &A,const Indexmatrix &B);
  friend Indexmatrix operator==(const Indexmatrix &A,const Indexmatrix &B);
  friend Indexmatrix operator!=(const Indexmatrix &A,const Indexmatrix &B);
  friend Indexmatrix operator<(const Indexmatrix &A,Integer d);
  friend Indexmatrix operator>(const Indexmatrix &A,Integer d);
  friend Indexmatrix operator<=(const Indexmatrix &A,Integer d);
  friend Indexmatrix operator>=(const Indexmatrix &A,Integer d);
  friend Indexmatrix operator==(const Indexmatrix &A,Integer d);
  friend Indexmatrix operator!=(const Indexmatrix &A,Integer d);
  friend inline Indexmatrix operator<(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator>(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator<=(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator>=(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator==(Integer d,const Indexmatrix &A);
  friend inline Indexmatrix operator!=(Integer d,const Indexmatrix &A);
    
  friend Indexmatrix minrows(const Indexmatrix& A);   //min of each column (over the rows)
  friend Indexmatrix mincols(const Indexmatrix& A);   //min of each row (over the columns)
  friend Integer min(const Indexmatrix& A,Integer *iindex=0,Integer *jindex=0);
  friend Indexmatrix maxrows(const Indexmatrix& A);   //similar
  friend Indexmatrix maxcols(const Indexmatrix& A);   //similar
  friend Integer max(const Indexmatrix& A,Integer *iindex=0,Integer *jindex=0);
  
  friend Indexmatrix sortindex(const Indexmatrix& vec);
  //first element of Indexmatrix points to smallest value of A
  friend void sortindex(const Indexmatrix& vec,Indexmatrix &ind);
  //first element of Indexmatrix points to smallest value of A
  
  Indexmatrix find() const;   //finds nonzeros
  Indexmatrix find_number(Integer num=0) const; 
  friend inline Indexmatrix find(const Indexmatrix& A);
  friend inline Indexmatrix find_number(const Indexmatrix& A,Integer num=0);


  //--------------------------------
  //----  Input / Output
  //--------------------------------

  void display(ostream& out,
	       int precision=0,int width=0,int screenwidth=0) const;
  //for variables of value zero default values are used
  //precision=4,width=precision+6,screenwidth=80
  

  friend ostream& operator<<(ostream& o,const Indexmatrix &v);
  friend istream& operator>>(istream& i,Indexmatrix &v);
};

// **************************************************************************
//                   implementation of inline functions
// **************************************************************************

inline void Indexmatrix::init_to_zero()
{
 mtype=MTindexmatrix;
 nr=nc=0;mem_dim=0;m=0;
 chk_set_init(*this,1);
}

inline Indexmatrix& Indexmatrix::init(Integer inr,Integer inc,Integer d)
{
 newsize(inr,inc);
 mat_xea(nr*nc,m,d);
 chk_set_init(*this,1);
 return *this;
}

inline Indexmatrix& Indexmatrix::init(Integer inr,Integer inc,const Integer* d,Integer incr)
{
 newsize(inr,inc);
 if (incr==1) mat_xey(nr*nc,m,d);
 else mat_xey(nr*nc,m,Integer(1),d,incr);
 chk_set_init(*this,1);
 return *this;
}

inline Indexmatrix& Indexmatrix::init(const Indexmatrix& A,Integer d)
{
 return xeya(A,d);
}

inline Indexmatrix::Indexmatrix()
{
 init_to_zero();
}

inline Indexmatrix::Indexmatrix(const Indexmatrix &A,Integer d)
{
 init_to_zero();
 xeya(A,d);
}

inline Indexmatrix::Indexmatrix(Integer inr,Integer inc)
{
 init_to_zero();
 newsize(inr,inc);
}

inline Indexmatrix::Indexmatrix(const Range& range)
{
 init_to_zero();
 init(range);
}

inline Indexmatrix::Indexmatrix(Integer inr,Integer inc,Integer d)
{
 init_to_zero();
 init(inr,inc,d);
}

inline Indexmatrix::Indexmatrix(Integer inr,Integer inc,const Integer *d,Integer incr)
{
 init_to_zero();
 init(inr,inc,d,incr);
}

inline Integer& Indexmatrix::operator()(Integer i,Integer j)
{
 chk_range(i,j,nr,nc);
 return m[j*nr+i];
}

inline Integer& Indexmatrix::operator()(Integer i) 
{
 chk_range(i,0,nr*nc,1);
 return m[i];
}

inline Integer Indexmatrix::operator()(Integer i,Integer j) const
{
 chk_range(i,j,nr,nc);
 return m[j*nr+i];
}

inline Integer Indexmatrix::operator()(Integer i) const
{
 chk_range(i,0,nr*nc,1);
 return m[i];
}

inline Integer& Indexmatrix::operator[](Integer i)
{return (*this)(i);}

inline Integer Indexmatrix::operator[](Integer i) const 
{return (*this)(i);}

inline Indexmatrix& Indexmatrix::reduce_length(Integer n) 
{ nr=min(nr*nc,max(Integer(0),n)); nc=1; return *this;}


inline Integer ip(const Indexmatrix& A, const Indexmatrix& B)
{
 chk_add(A,B);
 return mat_ip(A.nc*A.nr,A.m,B.m);
}

inline Real norm2(const Indexmatrix& A)
{
 chk_init(A);
 return ::sqrt(mat_ip(A.nc*A.nr,A.m,A.m));
}

inline Indexmatrix& Indexmatrix::operator=(const Indexmatrix &A)
{ return xeya(A);}

inline Indexmatrix& Indexmatrix::operator*=(const Indexmatrix &A)
{ Indexmatrix C; return xeya(genmult(*this,A,C));}

inline Indexmatrix& Indexmatrix::operator+=(const Indexmatrix &A)
{ return xpeya(A); }

inline Indexmatrix& Indexmatrix::operator-=(const Indexmatrix &A)
{ return xpeya(A,-1); }

inline Indexmatrix& Indexmatrix::operator%=(const Indexmatrix &A)
{ chk_add(*this,A); mat_xhadey(nr*nc,m,A.m); return *this; }

inline Indexmatrix Indexmatrix::operator-() const
{ return Indexmatrix(*this,-1); }

inline Indexmatrix& Indexmatrix::operator*=(register Integer d)
{ chk_init(*this); mat_xmultea(nr*nc,m,d); return *this; }

inline Indexmatrix& Indexmatrix::operator/=(register Integer d)
{ chk_init(*this); mat_xdivea(nr*nc,m,d); return *this; }

inline Indexmatrix& Indexmatrix::operator%=(register Integer d)
{ chk_init(*this); mat_xmodea(nr*nc,m,d); return *this; }

inline Indexmatrix& Indexmatrix::operator+=(register Integer d)
{ chk_init(*this); mat_xpea(nr*nc,m,d); return *this; }

inline Indexmatrix& Indexmatrix::operator-=(register Integer d)
{ chk_init(*this); mat_xpea(nr*nc,m,-d); return *this; }


inline Indexmatrix operator*(const Indexmatrix &A,const Indexmatrix &B) 
    {Indexmatrix C; return genmult(A,B,C);}
inline Indexmatrix operator+(const Indexmatrix &A,const Indexmatrix &B)
    {Indexmatrix C; return xeyapzb(C,A,B,1,1);}
inline Indexmatrix operator-(const Indexmatrix &A,const Indexmatrix &B)
    {Indexmatrix C; return xeyapzb(C,A,B,1,-1);}
inline Indexmatrix operator%(const Indexmatrix &A,const Indexmatrix &B) 
    {Indexmatrix C(A); return C%=B;}
inline Indexmatrix operator*(const Indexmatrix &A,Integer d)          
    {return Indexmatrix(A,d);}
inline Indexmatrix operator*(Integer d,const Indexmatrix &A)
    {return Indexmatrix(A,d);}
inline Indexmatrix operator/(const Indexmatrix &A,Integer d)
    {Indexmatrix B(A); return B/=d;}
inline Indexmatrix operator%(const Indexmatrix &A,Integer d)
    {Indexmatrix B(A); return B%=d;}
inline Indexmatrix operator+(const Indexmatrix &A,Integer d)           
    {Indexmatrix B(A); return B+=d;}
inline Indexmatrix operator+(Integer d,const Indexmatrix &A)           
    {Indexmatrix B(A); return B+=d;}
inline Indexmatrix operator-(const Indexmatrix &A,Integer d)       
    {Indexmatrix B(A); return B-=d;}
inline Indexmatrix operator-(Integer d,const Indexmatrix &A)       
    {Indexmatrix B(A,-1); return B+=d;}

inline Indexmatrix triu(const Indexmatrix& A,Integer i)
          {Indexmatrix B(A); B.triu(i); return B;}
inline Indexmatrix tril(const Indexmatrix& A,Integer i)
          {Indexmatrix B(A); B.tril(i); return B;}

inline Indexmatrix concat_right(const Indexmatrix& A,const Indexmatrix& B)
          {Indexmatrix C(A.dim()+B.dim(),1);C=A;C.concat_right(B);return C;}
inline Indexmatrix concat_below(const Indexmatrix& A,const Indexmatrix& B)
          {Indexmatrix C(A.dim()+B.dim(),1);C=A;C.concat_below(B);return C;}
    
inline void swap(Indexmatrix &A,Indexmatrix &B)
{ 
 Integer *hm=A.m;A.m=B.m;B.m=hm;
 Integer hi=A.nr;A.nr=B.nr;B.nr=hi;
 hi=A.nc;A.nc=B.nc;B.nc=hi;
 hi=A.mem_dim;A.mem_dim=B.mem_dim;B.mem_dim=hi;
#ifdef DEBUG
 hi=A.is_init;A.is_init=B.is_init;B.is_init=hi;
#endif
}

inline Indexmatrix rand(Integer rows,Integer cols,Integer lb,Integer ub)
          {Indexmatrix A; return A.rand(rows,cols,lb,ub);}
inline Indexmatrix sign(const Indexmatrix& A)
          {Indexmatrix B(A); return B.sign();}

inline Indexmatrix operator>(const Indexmatrix &A,const Indexmatrix &B)
{return B<A;}
inline Indexmatrix operator>=(const Indexmatrix &A,const Indexmatrix &B)
{return B<=A;}
inline Indexmatrix operator<(Integer d,const Indexmatrix &A)
{return A>d;}
inline Indexmatrix operator>(Integer d,const Indexmatrix &A)
{return A<d;}
inline Indexmatrix operator<=(Integer d,const Indexmatrix &A)
{return A>=d;}
inline Indexmatrix operator>=(Integer d,const Indexmatrix &A)
{return A<=d;}
inline Indexmatrix operator==(Integer d,const Indexmatrix &A)
{return A==d;}
inline Indexmatrix operator!=(Integer d,const Indexmatrix &A)
{return A!=d;}

inline Indexmatrix find(const Indexmatrix& A)
          {return A.find();}
inline Indexmatrix find_number(const Indexmatrix& A,Integer num)
          {return A.find_number(num);}
#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: indexmat.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:17  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:53  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/indexmat.h,v $
 --------------------------------------------------------------------------- */
