/* -*-c++-*-
 * 
 *    Source     $RCSfile: matrix.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:20 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: matrix.cc,v 1.1.1.1.2.1 2000/02/28 13:55:20 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "matrix.h"
#ifdef WITH_SYMMATRIX
#include "symmat.h"
#endif
#include "heapsort.h"
#include "gb_rand.h"

// **************************************************************************
//                                Constructors
// **************************************************************************

Matrix& Matrix::xeya(const Matrix& A,Real d)
{
 chk_init(A);
 newsize(A.nr,A.nc);
 chk_set_init(*this,1);
 if (d==1.) { mat_xey(nr*nc,m,A.m); return *this;}
 if (d==0.) { mat_xea(nr*nc,m,0.); return *this;}
 if (d==-1.) { mat_xemy(nr*nc,m,A.m); return *this;}
 mat_xeya(nr*nc,m,A.m,d);
 return *this;
}

Matrix& Matrix::xpeya(const Matrix& A,Real d)
{
 chk_add(*this,A);
 if (d==1.) { mat_xpey(nr*nc,m,A.m); return *this;}
 if (d==0.) { return *this;}
 if (d==-1.) { mat_xmey(nr*nc,m,A.m); return *this;}
 mat_xpeya(nr*nc,m,A.m,d);
 return *this;
}

Matrix& Matrix::xeya(const Indexmatrix& A,Real d)
{
 chk_init(A);
 newsize(A.nr,A.nc);
 chk_set_init(*this,1);
 if (d==1.) { for(Integer i=0;i<nr*nc;i++) m[i]=Real(A.m[i]); return *this;}
 if (d==0.) { mat_xea(nr*nc,m,0.); return *this;}
 if (d==-1.) { for(Integer i=0;i<nr*nc;i++) m[i]=Real(-A.m[i]); return *this;}
 for(Integer i=0;i<nr*nc;i++) m[i]=d*A.m[i];
 return *this;
}

Matrix& Matrix::xpeya(const Indexmatrix& A,Real d)
{
 chk_add(*this,A);
 if (d==1.) { for(Integer i=0;i<nr*nc;i++) m[i]+=Real(A.m[i]); return *this;}
 if (d==0.) { return *this;}
 if (d==-1.) { for(Integer i=0;i<nr*nc;i++) m[i]-=A.m[i]; return *this;}
 for(Integer i=0;i<nr*nc;i++) m[i]+=d*A.m[i];
 return *this;
}

Matrix& xbpeya(Matrix& x,const Matrix& y,Real alpha,Real beta,int ytrans)
  //returns x= alpha*y+beta*x, where y may be transposed (ytrans=1)
  //if beta==0. then x is initialized to the correct size
{
  chk_init(y);
  if (beta==0.){
    if (!ytrans){ //y is not transposed
      x.newsize(y.nr,y.nc);
      if (alpha==0.){
	mat_xea(x.dim(),x.m,0.);
      }
      else if (alpha==1.){
	mat_xey(y.dim(),x.m,y.m);
      }
      else {
	mat_xeya(y.dim(),x.m,y.m,alpha);
      }
      chk_set_init(x,1);
      return x;
    }
    else{ //y is transposed
      x.newsize(y.nc,y.nr);
      if (alpha==0.){
	mat_xea(x.dim(),x.m,0.);
      }
      else if (alpha==1.){
	if (x.nr>x.nc){
	  for (Integer i=0;i<x.nc;i++){
	    mat_xey(x.nr,x.m+i*x.nr,1,y.m+i,x.nc);
	  }
	} 
	else {
	  for (Integer i=0;i<x.nr;i++){
	    mat_xey(x.nc,x.m+i,x.nr,y.m+i*x.nc,1);
	  }
	}
      }
      else {
	if (x.nr>x.nc){
	  for (Integer i=0;i<x.nc;i++){
	    mat_xeya(x.nr,x.m+i*x.nr,1,y.m+i,x.nc,alpha);
	  }
	} 
	else {
	  for (Integer i=0;i<x.nr;i++){
	    mat_xeya(x.nc,x.m+i,x.nr,y.m+i*x.nc,1,alpha);
	  }
	}
      }
      chk_set_init(x,1);
      return x;
    }
  }
  //Now beta!=0
  if (!ytrans) { //y is not transposed
    chk_add(x,y);
    if (alpha==0.){
      if (beta==1.) return x;
      else if (beta==-1.) {
	mat_xemx(x.dim(),x.m);
        return x;
      }
      mat_xmultea(x.dim(),x.m,beta);
      return x;   
    }
    else if (beta==1.){
      if (alpha==1.){
	mat_xpey(x.dim(),x.m,y.m);
        return x;
      }
      else if (alpha==-1.){
	mat_xmey(x.dim(),x.m,y.m);
        return x;
      }
      mat_xpeya(x.dim(),x.m,y.m,alpha);
      return x; 
    }
  } 
  //Now y transposed
  chk_init(x);
#ifdef DEBUG
  if ((x.nr!=y.nc)||(x.nc!=y.nr)){
    MEmessage(MatrixError(ME_dim,"xbpeya: dimensions don't match",MTmatrix));;
    exit(1);
  }
#endif 
  if (alpha==0.){
    if (beta==1.) return x;
    if (beta==-1.) {
      mat_xemx(x.dim(),x.m);
      return x;
    }
    mat_xmultea(x.dim(),x.m,beta);
    return x;   
  }
  if (beta==1.){
    if (alpha==1.){
      if (x.nr>x.nc){
	for (Integer i=0;i<x.nc;i++){
	  mat_xpey(x.nr,x.m+i*x.nr,1,y.m+i,x.nc);
	}
      } 
      else {
	for (Integer i=0;i<x.nr;i++){
	  mat_xpey(x.nc,x.m+i,x.nr,y.m+i*x.nc,1);
	}
      }
      return x;
    }
    if (alpha==-1.){
      if (x.nr>x.nc){
	for (Integer i=0;i<x.nc;i++){
	  mat_xmey(x.nr,x.m+i*x.nr,1,y.m+i,x.nc);
	}
      } 
      else {
	for (Integer i=0;i<x.nr;i++){
	  mat_xmey(x.nc,x.m+i,x.nr,y.m+i*x.nc,1);
	}
      }
      return x;
    }
      
    if (x.nr>x.nc){
      for (Integer i=0;i<x.nc;i++){
	mat_xpeya(x.nr,x.m+i*x.nr,1,y.m+i,x.nc,alpha);
      }
    } 
    else {
      for (Integer i=0;i<x.nr;i++){
	mat_xpeya(x.nc,x.m+i,x.nr,y.m+i*x.nc,1,alpha);
      }
    }
    return x;
  }
  //now beta!=1. 
  if (x.nr>x.nc){
    for (Integer i=0;i<x.nc;i++){
      mat_xbpeya(x.nr,x.m+i*x.nr,1,y.m+i,x.nc,alpha,beta);
    }
  } 
  else {
    for (Integer i=0;i<x.nr;i++){
      mat_xbpeya(x.nc,x.m+i,x.nr,y.m+i*x.nc,1,alpha,beta);
    }
  }
  return x;
}
  
Matrix& xeyapzb(Matrix& x,const Matrix& y,const Matrix& z,Real alpha,Real beta)
  //returns x= alpha*y+beta*z,
  //x is initialized to the correct size
{
  chk_add(y,z);
  x.newsize(y.nr,y.nc);
  chk_set_init(x,1);
  if (alpha==1.){
    if(beta==1.){
      mat_xeypz(x.dim(),x.m,y.m,z.m);
      return x;
    }
    if (beta==-1.){
      mat_xeymz(x.dim(),x.m,y.m,z.m);
      return x;
    }
  }
  if ((beta==1.)&&(alpha==-1.)){
    mat_xeymz(x.dim(),x.m,z.m,y.m);
    return x;
  }
  mat_xeyapzb(x.dim(),x.m,y.m,z.m,alpha,beta);
  return x;
}

Matrix& genmult(const Matrix& A,const Matrix& B,Matrix& C,
                    Real alpha,Real beta,int atrans,int btrans)
            //returns C=beta*C+alpha*A*B, where A and B may be transposed
            //C may neither be equal to A nor B
{
 chk_init(A);
 chk_init(B);
 Integer nr,nm,nc;
 if (atrans) {nr=A.nc;nm=A.nr;}
 else { nr=A.nr; nm=A.nc;}
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTmatrix));;
         exit(1);
     }
#endif
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTmatrix));;
         exit(1);
     }
#endif
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.nr)||(nc!=C.nc)) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTmatrix));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (atrans){
     if (btrans){
         Real *cp=C.m;
         for(Integer j=0;j<nc;j++){
             for(Integer i=0;i<nr;i++){
                 *cp++ +=alpha*mat_ip(nm,A.m+i*nm,1,B.m+j,nc);
             }
         }
     }
     else {
         Real *cp=C.m;
         for(Integer j=0;j<nc;j++){
             for(Integer i=0;i<nr;i++){
                 *cp++ +=alpha*mat_ip(nm,A.m+i*nm,B.m+j*nm);
             }
         }
     }
 }
 else {
     if (btrans){
         Real *cp=C.m;
         for(Integer j=0;j<nc;j++){
             for(Integer i=0;i<nr;i++){
                 *cp++ +=alpha*mat_ip(nm,A.m+i,nr,B.m+j,nc);
             }
         }
     }
     else {
         Real *cp=C.m;
         for(Integer j=0;j<nc;j++){
             for(Integer i=0;i<nr;i++){
                 *cp++ +=alpha*mat_ip(nm,A.m+i,nr,B.m+j*nm,1);
             }
         }
     }
 }
 return C;
}
         

Matrix& Matrix::init(const Realrange& r)
{
 Integer i;
 if (r.step>=0) {
     if (r.from>r.to+r.tol) i=0;
     else i=Integer((r.to+r.tol-r.from)/r.step)+1;
 }
 else {
     if (r.from<r.to-r.tol) i=0;
     else i=Integer((r.to-r.tol-r.from)/r.step)+1;
 }
 newsize(i,1);
 Real d=r.from;
 Real *mp=m;
 for(i=nr;--i>=0;){
     (*mp++)=d;
     d+=r.step;
 }
 chk_set_init(*this,1);
 return *this;
}

void Matrix::newsize(Integer inr,Integer inc)
{
 chk_range(inr,inc,-1,-1);
 chk_set_init(*this,0);
 if ((inr==0)||(inc==0)){
     nr=0;
     nc=0;
     return;
 }
 if ((inr!=nr)||(inc!=nc)) {
     nr=inr;
     nc=inc;
     if (nr*nc>mem_dim){
         memarray.free(m); m=0;
         mem_dim=memarray.get(nr*nc,m);
         if (mem_dim<nr*nc)
             MEmessage(MEmem(nr*nc,
                         "Matrix::Matrix(Integer,Integer,Real) not enough memory",MTmatrix));
     }
 }
}

Matrix Matrix::operator()(const Indexmatrix &rv,const Indexmatrix &cv) const
{
 chk_init(rv);
 chk_init(cv);
 chk_init(*this);
 if ((rv.dim()==0)||(cv.dim()==0)) return Matrix(0,0,0.);
 chk_range(min(rv),min(cv),nr,nc);
 chk_range(max(rv),max(cv),nr,nc);
 Matrix A(rv.dim(),cv.dim());
 Integer i,j;
 Real *ap=A.m;
 Integer *rp;
 Integer *cp=cv.m;
 Real *mcp;           //points to current column indexed by cv
 for(j=A.nc;--j>=0;){
     mcp=m+nr*(*cp++);
     rp=rv.m;
     for(i=A.nr;--i>=0;)
         (*ap++)=mcp[*rp++];
 }
 chk_set_init(A,1);
 return A;
}

Matrix Matrix::operator()(const Indexmatrix &v) const
{
 chk_init(v);
 chk_init(*this);
 if (v.dim()==0) return Matrix(0,0,0.);
 chk_range(min(v),max(v),nr*nc,nr*nc);
 Matrix A(v.nr,v.nc);
 Integer i;
 Real *ap=A.m;
 Integer *vp=v.m;
 for(i=A.nr*A.nc;--i>=0;){
         (*ap++)=m[*vp++];
 }
 chk_set_init(A,1);
 return A;
}

Matrix Matrix::col(Integer c) const
{
 chk_init(*this);
 chk_range(c,0,nc,1);
 return Matrix(nr,1,m+c*nr);
}

Matrix Matrix::row(Integer r) const
{
 chk_init(*this);
 chk_range(r,0,nr,1);
 return Matrix(1,nc,m+r,nr);
}

Matrix Matrix::cols(const Indexmatrix& v) const
{
 chk_init(v);
 chk_init(*this);
 if (v.dim()==0) return Matrix(0,0,0.);
 chk_range(min(v),max(v),nc,nc);
 Matrix A(nr,v.dim());
 Real *mp;
 Real *ap=A.m;
 Integer *vp=v.m;
 Integer i,j;
 for(i=A.nc;--i>=0;){
     mp=m+nr*(*vp++);
     for(j=nr;--j>=0;)
         *ap++=*mp++;
 }
 chk_set_init(A,1);
 return A;
}

Matrix Matrix::rows(const Indexmatrix& v) const
{
 chk_init(v);
 chk_init(*this);
 if (v.dim()==0) return Matrix(0,0,0.);
 chk_range(min(v),max(v),nr,nr);
 Matrix A(v.dim(),nc);
 Integer *vp=v.m;
 for(Integer i=0;i<A.nr;i++){
     mat_xey(nc,A.m+i,A.nr,m+(*vp++),nr);
 }
 chk_set_init(A,1);
 return A;
}

Matrix& Matrix::transpose()
{
 chk_init(*this);
 if ((nr<=1)||(nc<=1)){
     Integer h=nr;nr=nc;nc=h;
     return *this;
 }
 Real *nm;
 Integer nmem_dim=memarray.get(nr*nc,nm);
 if (nmem_dim<nr*nc)
     MEmessage(MEmem(nr*nc,"Matrix::transpose() not enough memory",MTmatrix));
 Integer i,j;
 Real *mp=m;
 Real *nmp;
 for(j=0;j<nc;j++){
     nmp=nm+j;
     for(i=0;i<nr;i++,nmp+=nc)
         *nmp=*mp++;
 }
 i=nr;nr=nc;nc=i;
 memarray.free(m);
 m=nm;
 mem_dim=nmem_dim;
 return *this;
}

Matrix& Matrix::subassign(const Indexmatrix &rv,const Indexmatrix &cv,
                          const Matrix& A)
{
 chk_init(rv);
 chk_init(cv);
 chk_init(A);
 if ((rv.dim()==0)||(cv.dim()==0)) return *this;
 chk_range(min(rv),min(cv),nr,nc);
 chk_range(max(rv),max(cv),nr,nc);
#ifdef DEBUG
 if ((rv.dim()!=A.nr)||(cv.dim()!=A.nc))
     MEmessage(MEdim(rv.dim(),cv.dim(),A.nr,A.nc,
                 "Matrix::subassign(const Indexmatrix&,const Indexmatrix&,const Matrix&) dimensions do not match",
                 MTmatrix));
#endif
 Integer i,j;
 Real *ap=A.m;
 Integer *rp;
 Integer *cp=cv.m;
 Real *mcp;           //points to current column indexed by cv
 for(j=A.nc;--j>=0;){
     mcp=m+nr*(*cp++);
     rp=rv.m;
     for(i=A.nr;--i>=0;)
         mcp[*rp++]=(*ap++);
 }
 return *this;
}

Matrix& Matrix::subassign(const Indexmatrix &v,const Matrix& A)
{
 chk_init(v);
 chk_init(A);
 if (v.dim()==0) return *this;
 chk_range(min(v),max(v),nr*nc,nr*nc);
#ifdef DEBUG
 if (v.dim()!=A.dim())
     MEmessage(MEdim(v.dim(),1,A.dim(),1,
                 "Matrix::subassign(const Indexmatrix&,const Matrix&) dimensions do not match",
                 MTmatrix));
#endif
 Integer i;
 Real *ap=A.m;
 Integer *vp=v.m;
 for(i=A.nr*A.nc;--i>=0;){
         m[*vp++]=(*ap++);
 }
 return *this;
}

Matrix operator<(const Matrix& A,const Matrix& B) 
{
 chk_add(A,B);
 Matrix C(A.nr,A.nc);
 Real *ap=A.m;
 Real *bp=B.m;
 Real *cp=C.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*cp++)=Real((*ap++)<(*bp++));
 chk_set_init(C,1);
 return C;
}

Matrix operator<=(const Matrix& A,const Matrix& B) 
{
 chk_add(A,B);
 Matrix C(A.nr,A.nc);
 Real *ap=A.m;
 Real *bp=B.m;
 Real *cp=C.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*cp++)=Real((*ap++)<=(*bp++));
 chk_set_init(C,1);
 return C;
}

Matrix operator==(const Matrix& A,const Matrix& B) 
{
 chk_add(A,B);
 Matrix C(A.nr,A.nc);
 Real *ap=A.m;
 Real *bp=B.m;
 Real *cp=C.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*cp++)=Real((*ap++)==(*bp++));
 chk_set_init(C,1);
 return C;
}

Matrix operator!=(const Matrix& A,const Matrix& B) 
{
 chk_add(A,B);
 Matrix C(A.nr,A.nc);
 Real *ap=A.m;
 Real *bp=B.m;
 Real *cp=C.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*cp++)=Real((*ap++)!=(*bp++));
 chk_set_init(C,1);
 return C;
}


Matrix operator<(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)<d);
 chk_set_init(B,1);
 return B;
}

Matrix operator>(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)>d);
 chk_set_init(B,1);
 return B;
}

Matrix operator<=(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)<=d);
 chk_set_init(B,1);
 return B;
}

Matrix operator>=(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)>=d);
 chk_set_init(B,1);
 return B;
}

Matrix operator==(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)==d);
 chk_set_init(B,1);
 return B;
}

Matrix operator!=(const Matrix& A,Real d)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 Real *bp=B.m;
 Real *ap=A.m;
 Integer i=A.nr*A.nc;
 while(--i>=0)
     (*bp++)=Real((*ap++)!=d);
 chk_set_init(B,1);
 return B;
}


// **************************************************************************
//                                 triu
// **************************************************************************

Matrix& Matrix::triu(Integer i)
{
 chk_init(*this);
 Integer j;
 for(j=0;j<nc;j++){
     mat_xea(nr-max(Integer(0),j+1-i),m+j*nr+max(Integer(0),j+1-i),0.);
 }
 return *this;
}

Matrix& Matrix::tril(Integer i)
{
 chk_init(*this);
 Integer j;
 for(j=0;j<nc;j++){
     mat_xea(min(nr,j-i),m+j*nr,0.);
 }
 return *this;
}

Matrix& Matrix::concat_right(const Matrix& A)
{
 chk_init(*this);
 chk_init(A);
#ifdef DEBUG
 if (nr!=A.nr)
     MEmessage(MEdim(nr,nc,A.nr,A.nc,
                 "Matrix::concat_right(const Matrix&) dimensions do not match",MTmatrix));
#endif
 if (mem_dim<nr*nc+A.nr*A.nc){
     Real *mnew;
     mem_dim=memarray.get(nr*nc+A.nr*A.nc,mnew);
     if (mem_dim<nr*nc+A.nr*A.nc){
         MEmessage(MEmem(nr*nc+A.nr*A.nc,
                     "Matrix::concat_right(const Matrix&) not enough memory",
                     MTmatrix));
     }
     mat_xey(nr*nc,mnew,m);
     memarray.free(m);
     m=mnew;
 }
 mat_xey(A.nr*A.nc,m+nr*nc,A.m);
 nc+=A.nc;
 chk_set_init(*this,1);
 return (*this);
}

Matrix& Matrix::concat_below(const Matrix& A)
{
 chk_init(*this);
 chk_init(A);
#ifdef DEBUG
 if (nc!=A.nc)
     MEmessage(MEdim(nr,nc,A.nr,A.nc,
                 "Matrix::concat_below(const Matrix&) dimensions do not match",MTmatrix));
#endif
 Integer i,j;
 Real* mp;
 Real* ap;
 Real* oldm=m;
 Real* op;
 int free_oldm=0;
 if (mem_dim<nr*nc+A.nr*A.nc){
     mem_dim=memarray.get(nr*nc+A.nr*A.nc,m);
     if (mem_dim<nr*nc+A.nr*A.nc){
         MEmessage(MEmem(nr*nc+A.nr*A.nc,
                     "Matrix::concat_right(const Matrix&) not enough memory",
                     MTmatrix));
     }
     free_oldm=1;
 }
 mp=m+nr*nc+A.nr*A.nc-1;
 ap=A.m+A.nr*A.nc-1;
 op=oldm+nr*nc-1;
 for(j=nc;--j>=0;){
     for(i=A.nr;--i>=0;)
         (*mp--)=(*ap--);
     for(i=nr;--i>=0;)
         (*mp--)=(*op--);
 }
 nr+=A.nr;
 if (free_oldm) memarray.free(oldm);
 chk_set_init(*this,1);
 return (*this);
}

Matrix& Matrix::rand(Integer inr,Integer inc)
{
 newsize(inr,inc);
 Real *mp=m;
 for(int i=inr*inc;--i>=0;)
     (*mp++)=mat_randgen.next();
 chk_set_init(*this,1);
 return *this;
}

Matrix& Matrix::inv(void)
{
 chk_init(*this);
 Integer i;
 for(i=nr*nc;--i>=0;) m[i]=1./m[i];
 return *this;
}

Matrix& Matrix::sqrt(void)
{
 chk_init(*this);
 Integer i;
 for(i=nr*nc;--i>=0;) m[i]=::sqrt(m[i]);
 return *this;
}

Matrix& Matrix::sign(Real tol)
{
 chk_init(*this);
 Integer i;
 Real *mp=m;
 for(i=nr*nc;--i>=0;) {
     if (*mp>tol) *mp++=1.;
     else if (*mp<-tol) *mp++=-1.;
     else *mp++=0.;
 }
 return *this;
}

Matrix& Matrix::floor(void)
{
 chk_init(*this);
 for(Integer i=nr*nc;--i>=0;) m[i]=::floor(m[i]);
 return *this;
}

Matrix& Matrix::ceil(void)
{
 chk_init(*this);
 for(Integer i=nr*nc;--i>=0;) m[i]=::ceil(m[i]);
 return *this;
}

Matrix& Matrix::rint(void)
{
 chk_init(*this);
 for(Integer i=nr*nc;--i>=0;) m[i]=::rint(m[i]);
 return *this;
}

Matrix& Matrix::abs(void)
{
 chk_init(*this);
 for(Integer i=0;i<nr*nc;i++) m[i]=my_abs(m[i]);
 return *this;
}


Matrix& Matrix::scale_rows(const Matrix& vec)
{
 chk_init(*this);
 chk_init(vec);
#ifdef DEBUG
 if (vec.dim()!=nr) MEmessage(MEdim(nr,nc,vec.dim(),1,
                    "Matrix::scale_rows(const Matrix& vec) vec.dim() is not number of  rows",
                    MTmatrix));
#endif
 const Real *vp=vec.m;
 Integer i;
 for(i=0;i<nr;i++,vp++){
     mat_xmultea(nc,m+i,nr,*vp);
 }
 return *this;
}

Matrix& Matrix::scale_cols(const Matrix& vec)
{
 chk_init(*this);
 chk_init(vec);
#ifdef DEBUG
 if (vec.dim()!=nc) MEmessage(MEdim(nr,nc,vec.dim(),1,
                    "Matrix::scale_cols(const Matrix& vec) vec.dim() is not number of  columns",
                    MTmatrix));
#endif
 const Real *vp=vec.m;
 Integer i;
 for(i=0;i<nc;i++,vp++){
     mat_xmultea(nr,m+i*nr,*vp);
 }
 return *this;
}

Indexmatrix Matrix::find(Real tol) const
{
 chk_init(*this);
 Indexmatrix ind(nr*nc,1);
 chk_set_init(ind,1);
 Integer i,k=0;
 Real *mp=m;
 for(i=0;i<nr*nc;i++){
     if (my_abs(*mp++)>tol) ind(k++)=i;
 }
 ind.nr=k;
 return ind;
}

Indexmatrix Matrix::find_number(Real num,Real tol) const
{
 chk_init(*this);
 Indexmatrix ind(nr*nc,1);
 chk_set_init(ind,1);
 Integer i,k=0;
 Real *mp=m;
 for(i=0;i<nr*nc;i++){
     if (my_abs(*mp++-num)<=tol) ind(k++)=i;
 }
 ind.nr=k;
 return ind;
}

Matrix& Matrix::delete_rows(const Indexmatrix& ind)
{
 chk_init(*this);
 chk_init(ind);
 if (ind.dim()==0) return *this;
 Indexmatrix sind=sortindex(ind);
 chk_range(ind(sind(0)),ind(sind.dim()-1),nr,nr);
 Integer j,k;
 Real* mp=m;
 for(j=0;(j<nc);j++){
     mat_xey(ind(sind(0)),mp,m+j*nr);
     mp+=ind(sind(0));
     for(k=1;k<ind.dim();k++){
#ifdef DEBUG
         if (ind(sind(k))==ind(sind(k-1))) MEmessage(MatrixError(ME_unspec,"Matrix::delete_rows(): a row is to be deleted twice",MTmatrix));
#endif
         mat_xey(ind(sind(k))-ind(sind(k-1))-1,mp,m+j*nr+ind(sind(k-1))+1);
         mp+=ind(sind(k))-ind(sind(k-1))-1;
     }
     mat_xey(nr-ind(sind(k-1))-1,mp,m+j*nr+ind(sind(k-1))+1);
     mp+=nr-ind(sind(k-1))-1;
 }
 nr-=ind.dim();
 return *this;
}


Matrix& Matrix::delete_cols(const Indexmatrix& ind)
{
 chk_init(*this);
 chk_init(ind);
 if (ind.dim()==0) return *this;
 Indexmatrix sind=sortindex(ind);
 chk_range(ind(sind(0)),ind(sind.dim()-1),nc,nc);
 Integer j,oldj,k;
 oldj=ind(sind(0));
 k=1;
 for(j=ind(sind(0))+1;(j<nc)&&(k<ind.dim());j++){

#ifdef DEBUG
     if (ind(sind(k))<j) MEmessage(MatrixError(ME_unspec,"Matrix::delete_cols(): a column is to be deleted twice",MTmatrix));
#endif

     if (j==ind(sind(k))){
         k++;
         continue;
     }

     mat_xey(nr,m+oldj*nr,m+j*nr);
     oldj++;
 }

 mat_xey(nr*(nc-j),m+oldj*nr,m+j*nr);
 nc-=ind.dim();
 return *this;
}


void Matrix::display(ostream& out,int precision,int width,
                        int screenwidth) const
{
 out<<"matrix "<<mtype<<" ("<<nr<<","<<nc<<")"<<endl;
 if ((nr==0)||(nc==0)) return;
 chk_init(*this);
 if (precision==0) precision=4;
 out.precision(precision);
 if (width==0) width=precision+6;
 if (screenwidth==0) screenwidth=80;
 Integer colnr=screenwidth/(width+1);
 Integer k,i,j;
 Integer maxk=nc/colnr+((nc%colnr)>0);
 Integer maxj;
 for(k=0;k<maxk;k++){
     out<<"columns "<<k*colnr<<" to "<<min(nc,(k+1)*colnr)-1<<endl;
     for(i=0;i<nr;i++){
         maxj=min((k+1)*colnr,nc);
         for(j=k*colnr;j<maxj;j++){
             out<<' ';out.width(width);out<<(*this)(i,j);
         }
         out<<endl;
     }     
 }
}

Matrix diag(const Matrix& A)
{
 chk_init(A);
 if (min(A.nr,A.nc)==1){ //make a diagonal matrix of this vector
     Integer k=max(A.nr,A.nc);
     Matrix B(k,k,0.);
     for(Integer i=0;i<k;i++)
         B(i,i)=A(i);
     return B;
 }
 return Matrix(min(A.nr,A.nc),1,A.m,A.nr+1);
}

Matrix sumrows(const Matrix& A)
{
 chk_init(A);
 Matrix v(1,A.nc,0.);
 for(Integer i=0;i<A.nr;i++)
     mat_xpey(A.nc,v.m,1,A.m+i,A.nr);
 chk_set_init(v,1);
 return v;
}

Matrix sumcols(const Matrix& A)
{
 chk_init(A);
 Matrix v(A.nr,1,0.);
 for(Integer i=0;i<A.nc;i++)
     mat_xpey(A.nr,v.m,A.m+i*A.nr);
 chk_set_init(v,1);
 return v;
}

Real sum(const Matrix& A)
{
 chk_init(A);
 Real s=0.;
 Integer i;
 Real *mp=A.m;
 for(i=A.nr*A.nc;--i>=0;){
     s+=(*mp++);
 }
 return s;
}

Matrix maxrows(const Matrix& A)
{
 chk_init(A);
 if (A.dim()==0) return Matrix(0,0,0.);
 Matrix v(1,A.nc);
 Real maxd;
 Integer i,j;
 for(j=0;j<A.nc;j++){
     maxd=A(0,j);
     for(i=1;i<A.nr;i++)
         maxd=max(maxd,A(i,j));
     v(j)=maxd;
 }
 chk_set_init(v,1);
 return v;
}

Matrix maxcols(const Matrix& A)
{
 chk_init(A);
 if (A.dim()==0) return Matrix(0,0,0.);
 Matrix v(A.nr,1);
 Real maxd;
 Integer i,j;
 for(i=0;i<A.nr;i++){
     maxd=A(i,0);
     for(j=1;j<A.nc;j++)
         maxd=max(maxd,A(i,j));
     v(i)=maxd;
 }
 chk_set_init(v,1);
 return v;
}

Real max(const Matrix& A,Integer *iindex,Integer *jindex)
{
 chk_init(A);
 if (A.dim()==0) return min_Real;
 Real maxd;
 if (iindex==0){
     Integer i=A.nr*A.nc-1;
     maxd=A(i);
     for(;--i>=0;){
         maxd=max(maxd,A(i));
     }
 }
 else{
     Integer i=A.nr*A.nc-1;
     maxd=A(i);
     Integer besti=i;
     for(;--i>=0;){
         if (A(i)>maxd){
             maxd=A(i);
             besti=i;
         }
     }
     if (jindex!=0){
         *jindex=besti/A.nr;
         *iindex=besti%A.nr;
     }
     else *iindex=besti;
 }
     
 return maxd;
}

Matrix minrows(const Matrix& A)
{
 chk_init(A);
 if (A.dim()==0) return Matrix(0,0,0.);
#ifdef DEBUG
 if ((A.nc==0)||(A.nr==0))
     MEmessage(MEdim(A.nc,A.nr,0,0,"minrows(const Matrix&) dimension zero",MTmatrix));
#endif
 Matrix v(1,A.nc);
 Real mind;
 Integer i,j;
 for(j=0;j<A.nc;j++){
     mind=A(0,j);
     for(i=1;i<A.nr;i++)
         mind=min(mind,A(i,j));
     v(j)=mind;
 }
 chk_set_init(v,1);
 return v;
}

Matrix mincols(const Matrix& A)
{
 chk_init(A);
 if (A.dim()==0) return Matrix(0,0,0.);
 Matrix v(A.nr,1);
 Real mind;
 Integer i,j;
 for(i=0;i<A.nr;i++){
     mind=A(i,0);
     for(j=1;j<A.nc;j++)
         mind=min(mind,A(i,j));
     v(i)=mind;
 }
 chk_set_init(v,1);
 return v;
}

Real min(const Matrix& A,Integer *iindex,Integer *jindex)
{
 chk_init(A);
 if (A.dim()==0) return max_Real;
 Real mind;
 if (iindex==0){
     Integer i=A.nr*A.nc-1;
     mind=A(i);
     for(;--i>=0;){
         mind=min(mind,A(i));
     }
 }
 else{
     Integer i=A.nr*A.nc-1;
     mind=A(i);
     Integer besti=i;
     for(;--i>=0;){
         if (A(i)<mind){
             mind=A(i);
             besti=i;
         }
     }
     if (jindex!=0){
         *jindex=besti/A.nr;
         *iindex=besti%A.nr;
     }
     else *iindex=besti;
 }
     
 return mind;
}

Indexmatrix sortindex(const Matrix& vec)
{
 Indexmatrix I(Range(0,vec.dim()-1));
 Integer *ip=I.get_store();
 const Real *vp=vec.get_store();
 heapsort(vec.dim(),ip,vp);
 return I;
}

void sortindex(const Matrix& vec,Indexmatrix& ind)
{
 ind.init(Range(0,vec.dim()-1));
 Integer *ip=ind.get_store();
 const Real *vp=vec.get_store();
 heapsort(vec.dim(),ip,vp);
}


Real trace(const Matrix& A)
{
 chk_init(A);
 Real sum=0.;
 Integer k=min(A.nr,A.nc);
 for(Integer i=0;i<k;i++) sum+=A(i,i);
 return sum;
}

Matrix abs(const Matrix& A)
{
 chk_init(A);
 Matrix B(A.nr,A.nc);
 for(Integer i=0;i<A.nr*A.nc;i++) B(i)=abs(A(i));
 chk_set_init(B,1);
 return B;
}

Matrix transpose(const Matrix& A)
{
 chk_init(A);
 if ((A.nr<=1)||(A.nc<=1)) return Matrix(A.nc,A.nr,A.m);
 Matrix B(A.nc,A.nr);
 Integer i;
 for(i=0;i<B.nc;i++){
     mat_xey(B.nr,B.m+i*B.nr,1,A.m+i,A.nr);
 }
 chk_set_init(B,1);
 return B;
}

ostream& operator<<(ostream& o,const Matrix &A)
{
 chk_init(A);
 o<<A.nr<<" "<<A.nc<<'\n';
 Integer i,j;
 for(i=0;i<A.nr;i++){
     for(j=0;j<A.nc;j++) o<<' '<<A(i,j);
     o<<'\n';
 }
 return o;
}

istream& operator>>(istream& in,Matrix &A)
{
 Real d;
 Integer nr,nc;
 in>>d;
 nr=Integer(d+.5);
 in>>d;
 nc=Integer(d+.5);
 if ((nr<0)||(nc<0))
     MEmessage(MEdim(nc,nr,0,0,"operator>>(istream&,Matrix&) dimension negative",MTmatrix));
 A.newsize(nr,nc);
 Integer i,j;
 for(i=0;i<nr;i++)
     for(j=0;j<nc;j++)
         in>>A(i,j);
 chk_set_init(A,1);
 return in;
}

// **************************************************************************
//                     Matrix specific Indexmatrix 
// **************************************************************************

Indexmatrix::Indexmatrix(const Matrix &A)
{
 chk_init(A);
 init_to_zero();
 newsize(A.nr,A.nc);
 register Integer i;
 register Integer *mp=m;
 register Real *matp=A.m;
 for(i=nr*nc;--i>=0;matp++){
     *mp++=Integer((*matp>0)? *matp+.5 : *matp-.5);
 }
 chk_set_init(*this,1);
}



/* ---------------------------------------------------------------------------
 *    Change log $Log: matrix.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:20  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/matrix.cc,v $
 --------------------------------------------------------------------------- */
