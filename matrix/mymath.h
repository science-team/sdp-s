/* -*-c++-*-
 * 
 *    Source     $RCSfile: mymath.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:54 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************


#ifndef __MYMATH_H__
#define __MYMATH_H__

inline double my_abs(double d)
{
	 return (d>=0)?d:-d;
}

/*
inline int my_abs(int d)
{
 return (d>=0)?d:-d;
}
*/

inline long my_abs(long d)
{
 return (d>=0)?d:-d;
}

inline double max(double a,double b)
{
 return (a>=b)?a:b;
}

inline double min(double a,double b)
{
 return (a<=b)?a:b;
}

inline int max(int a,int b)
{
 return (a>=b)?a:b;
}

inline int min(int a,int b)
{
 return (a<=b)?a:b;
}

inline long max(long a,long b)
{
 return (a>=b)?a:b;
}

inline long min(long a,long b)
{
 return (a<=b)?a:b;
}

inline void swap(double& a,double& b)
{
 double h=a; a=b; b=h;
}

inline void swap(long& a,long& b)
{
 long h=a; a=b; b=h;
}

inline void swap(int& a,int& b)
{
 int h=a; a=b; b=h;
}

inline int sqr(int a)
{
 return a*a;
}

inline long sqr(long a)
{
 return a*a;
}

inline double sqr(double a)
{
 return a*a;
}

inline int sign(int a)
{
 if (a>0) return 1;
 if (a<0) return -1;
 return 0;
}

inline long sign(long a)
{
 if (a>0) return 1;
 if (a<0) return -1;
 return 0;
}

inline double sign(double a)
{
 return (a>=0)?1.:-1.;
}

inline double sign(double a,double tol)
{
 if (a>tol) return 1.;
 if (a<-tol) return -1.;
 return 0.;
}

//---- functions for f2c translations

inline double d_sign(double a,double b)
{
 return (((b>=0)&&(a>=0))||((b<0)&&(a<0)))?a:-a;
}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: mymath.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/mymath.h,v $
 --------------------------------------------------------------------------- */
