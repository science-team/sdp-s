/* -*-c++-*-
 * 
 *    Source     $RCSfile: symmat.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:26 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: symmat.cc,v 1.1.1.1.2.1 2000/02/28 13:55:26 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "matop.h"
#include "symmat.h"

// **************************************************************************
//                                Constructors
// **************************************************************************

Symmatrix& Symmatrix::xeya(const Symmatrix& A,Real d)
{
 chk_init(A);
 newsize(A.nr);
 chk_set_init(*this,1);
 if (d==1.) { mat_xey((nr*(nr+1))/2,m,A.get_store()); return *this;}
 if (d==0.) { mat_xea((nr*(nr+1))/2,m,0.); return *this;}
 if (d==-1.) { mat_xemy((nr*(nr+1))/2,m,A.get_store()); return *this;}
 mat_xeya((nr*(nr+1))/2,m,A.get_store(),d);
 return *this;
}

Symmatrix& Symmatrix::xpeya(const Symmatrix& A,Real d)
{
 chk_add(*this,A);
 if (d==1.) { mat_xpey((nr*(nr+1))/2,m,A.get_store()); return *this;}
 if (d==0.) { return *this;}
 if (d==-1.) { mat_xmey((nr*(nr+1))/2,m,A.get_store()); return *this;}
 mat_xpeya((nr*(nr+1))/2,m,A.get_store(),d);
 return *this;
}


// returns C=beta*C+alpha* A*A^T, where A may be transposed
Symmatrix& rankadd(const Matrix& A,Symmatrix& C,Real alpha,Real beta,int trans)
{
 chk_init(A);
 if (trans){ //A^T*A
     if (beta==0.) {
         C.init(A.coldim(),0.);
     }
     else {
         chk_colseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     Integer n=A.rowdim();
     Real *mp=C.get_store();
     const Real *ap=A.get_store()-n;
     for(Integer i=nr;--i>=0;){ 
         const Real *aap=(ap+=n);
         (*mp++) +=alpha*mat_ip(n,ap,aap);
         for(Integer j=i;--j>=0;){
             (*mp++) +=alpha*mat_ip(n,ap,aap+=n);
         }
     }
     return C;
 }
 else { //A*A^T
     if (beta==0.) {
         C.init(A.rowdim(),0.);
     }
     else {
         chk_rowseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     Integer nc2=(nr*(nr+1))/2;
     Integer n=A.coldim();
     const Real *ap=A.get_store()-1;
     Real *mp=C.get_store();
     for(Integer i=n;--i>=0;){
         for(Integer j=nr; --j>=0; ){
             const Real *aap;
             Real d1=*(aap=++ap);
             Real d=d1*alpha;
             (*mp++)+=d*d1;
             for(Integer k=j;--k>=0;)
                 (*mp++)+=d*(*(++aap));
         }
         mp-=nc2;
     }
     return C;
 }
}
 
     
// returns C=beta*C+alpha* sym(A*B^T) [or sym(A^T*B)]
Symmatrix& rank2add(const Matrix& A, const Matrix& B, Symmatrix& C,
                              Real alpha,Real beta,int trans)
{
 chk_add(A,B);
 if (trans) { //A^T*B
     if (beta==0.) {
         C.init(A.coldim(),0.);
     }
     else {
         chk_colseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     Integer n=A.rowdim();
     Real *mp=C.get_store();
     const Real *abp=A.get_store()-n;
     const Real *bbp=B.get_store()-n;
     Real d=alpha/2.;
     for(Integer i=nr;--i>=0;){ 
         const Real *bp;
         const Real *ap;
         *mp++ += alpha*mat_ip(n,ap=(abp+=n),bp=(bbp+=n));
         for(Integer j=i;--j>=0;){
             (*mp++) +=d*(mat_ip(n,abp,bp+=n)+mat_ip(n,ap+=n,bbp));
         }       
     }
     return C;
 }
 else {     //A*B^T
     if (beta==0.) {
         C.init(A.rowdim(),0.);
     }
     else {
         chk_rowseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     Integer nc2=(nr*(nr+1))/2;
     Integer n=A.coldim();
     const Real *abp=A.get_store()-1;
     const Real *bbp=B.get_store()-1;
     Real *mp=C.get_store();
     for(Integer i=n;--i>=0;){
         for(Integer j=nr; --j>=0; ){
             const Real *ap;
             const Real *bp;
             Real ad=*(ap=++abp);
             Real bd=alpha**(bp=++bbp);
             (*mp++)+=ad*bd;
             ad*=alpha/2.;
             bd/=2.;
             for(Integer k=j;--k>=0;)
                 (*mp++)+=bd*(*(++ap))+ad*(*(++bp));
         }
         mp-=nc2;
     }
     return C;
 }
}     
     

Symmatrix& Symmatrix::xetriu_yza(const Matrix& A,const Matrix& B,Real d)
{
 chk_add(A,B);
 newsize(A.coldim());
 chk_set_init(*this,1);
 if (d==0.) { mat_xea((nr*(nr+1))/2,m,0.); return *this;}
 if (d==1.) {
     Real *mp=m;
     const Real *ap=A.get_store();
     Integer n=A.rowdim();
     const Real *bbp=B.get_store()-n;
     for(Integer i=nr;i>0;i--){ 
         const Real *bp=(bbp+=n);
         for(Integer j=i;--j>=0;){
             *mp++=mat_ip(n,ap,bp);
             bp+=n;
         }
         ap+=n;         
     }
     return *this;
 }
 if (d==-1.) {
     Real *mp=m;
     const Real *ap=A.get_store();
     Integer n=A.rowdim();
     const Real *bbp=B.get_store()-n;
     for(Integer i=nr;i>0;i--){ 
         const Real *bp=(bbp+=n);
         for(Integer j=i;--j>=0;){
             *mp++=-mat_ip(n,ap,bp);
             bp+=n;
         }
         ap+=n;         
     }
     return *this;
 }
 Real *mp=m;
 const Real *ap=A.get_store();
 Integer n=A.rowdim();
 const Real *bbp=B.get_store()-n;
 for(Integer i=nr;i>0;i--){ 
     const Real *bp=(bbp+=n);
     for(Integer j=i;--j>=0;){
         *mp++=d*mat_ip(n,ap,bp);
         bp+=n;
     }
     ap+=n;         
 }
 return *this;
}
         
Symmatrix& Symmatrix::xpetriu_yza(const Matrix& A,const Matrix& B,Real d)
{
 chk_add(A,B);
 chk_colseq(*this,A);
 if (d==0.) { return *this;}
 if (d==1.) {
     Real *mp=m;
     const Real *ap=A.get_store();
     Integer n=A.rowdim();
     for(Integer i=0;i<nr;i++){
         const Real *bp=B.get_store()+i*n;
         for(Integer j=i;j<nr;j++){
             (*mp++) += mat_ip(n,ap,bp);
             bp+=n;
         }
         ap+=n;
     }
     return *this;
 }
 if (d==-1.) {
     Real *mp=m;
     const Real *ap=A.get_store();
     Integer n=A.rowdim();
     for(Integer i=0;i<nr;i++){
         const Real *bp=B.get_store()+i*n;
         for(Integer j=i;j<nr;j++){
             (*mp++)-=mat_ip(n,ap,bp);
             bp+=n;
         }
         ap+=n;
     }
     return *this;
 }
 Real *mp=m;
 const Real *ap=A.get_store();
 Integer n=A.rowdim();
 for(Integer i=0;i<nr;i++){
     const Real *bp=B.get_store()+i*n;
     for(Integer j=i;j<nr;j++){
         (*mp++)+=d*mat_ip(n,ap,bp);
         bp+=n;
     }
     ap+=n;
 }
 return *this;
}

Matrix& genmult(const Symmatrix& A,const Matrix& B,Matrix& C,
                    Real alpha,Real beta,int btrans)
            //returns C=beta*C+alpha*A*B, where A and B may be transposed
{
 chk_init(A);
 chk_init(B);
 Integer nr,nm,nc;
 nr=A.nr;nm=A.nr;
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult(Symmatrix,Matrix,Matrix): dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult(Symmatrix,Matrix,Matrix): dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.nr)||(nc!=C.nc)) {
         MEmessage(MatrixError(ME_dim,"genmult(Symmatrix,Matrix,Matrix): dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (btrans){
     const Real *ap=A.get_store();
     for(Integer j=0;j<nm;j++){
         mat_xpeya(nc,C.get_store()+j,nr,B.get_store()+j*B.nc,1,alpha*(*ap++));
         for(Integer i=j+1;i<nr;i++){
             Real d=alpha*(*ap++);
             mat_xpeya(nc,C.get_store()+i,nr,B.get_store()+j*B.nc,1,d);
             mat_xpeya(nc,C.get_store()+j,nr,B.get_store()+i*B.nc,1,d);
         }
     }
 }
 else {
     const Real *ap=A.get_store();
     for(Integer j=0;j<nm;j++){
         mat_xpeya(nc,C.get_store()+j,nr,B.get_store()+j,B.nr,alpha*(*ap++));
         for(Integer i=j+1;i<nr;i++){
             Real d=alpha*(*ap++);
             mat_xpeya(nc,C.get_store()+i,nr,B.get_store()+j,B.nr,d);
             mat_xpeya(nc,C.get_store()+j,nr,B.get_store()+i,B.nr,d);
         }
     }
 }
 return C;
}

Matrix& genmult(const Matrix& A,const Symmatrix& B,Matrix& C,
                    Real alpha,Real beta,int atrans)
            //returns C=beta*C+alpha*A*B, where A and B may be transposed
{
 chk_init(A);
 chk_init(B);
 Integer nr,nm,nc;
 if (atrans){
     nr=A.nc;
     nm=A.nr;
 }
 else {
     nr=A.nr;
     nm=A.nc;
 }
 nc=B.nr;
#ifdef DEBUG
 if (nm!=B.nr) {
     MEmessage(MatrixError(ME_dim,"genmult(Symmatrix,Matrix,Matrix): dimensions don't match",MTsymmetric));;
     exit(1);
 }
#endif
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.nr)||(nc!=C.nc)) {
         MEmessage(MatrixError(ME_dim,"genmult(Symmatrix,Matrix,Matrix): dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (atrans){
     const Real *bp=B.get_store();
     for(Integer j=0;j<nc;j++){
         mat_xpeya(nr,C.get_store()+j*nr,1,A.get_store()+j,nm,alpha*(*bp++));
         for(Integer i=j+1;i<nm;i++){
             Real d=alpha*(*bp++);
             mat_xpeya(nr,C.get_store()+j*nr,1,A.get_store()+i,nm,d);
             mat_xpeya(nr,C.get_store()+i*nr,1,A.get_store()+j,nm,d);
         }
     }
 }
 else {
     const Real *bp=B.get_store();
     for(Integer j=0;j<nc;j++){
         mat_xpeya(nr,C.get_store()+j*nr,A.get_store()+j*nr,alpha*(*bp++));
         for(Integer i=j+1;i<nm;i++){
             Real d=alpha*(*bp++);
             mat_xpeya(nr,C.get_store()+j*nr,A.get_store()+i*nr,d);
             mat_xpeya(nr,C.get_store()+i*nr,A.get_store()+j*nr,d);
         }
     }
 }
 return C;
}

Symmatrix& Symmatrix::xeya(const Matrix &A,double d)
{
 chk_init(A);
#ifdef DEBUG
 if (A.nr!=A.nc)
     MEmessage(MEdim(A.nr,A.nc,0,0,"Symmatrix::Symmatrix(const Matrix& A) A not square",MTsymmetric));
 is_init=0;
#endif
 newsize(A.nr);
 Real *mp=m;
 Real f=d/2.;
 for(Integer i=0;i<nr;i++){
     Real* matcp=A.m+i*nr+i;
     Real* matrp=matcp+nr;
     (*mp++)=(*matcp++)*d;
     for(Integer j=i+1;j<nr;j++,matrp+=nr)
         (*mp++)=((*matcp++)+(*matrp))*f;
 }
 chk_set_init(*this,1);
 return *this;
}

Symmatrix& Symmatrix::xpeya(const Matrix &A,double d)
{
 chk_add(*this,A);
 Real *mp=m;
 Real f=d/2.;
 for(Integer i=0;i<nr;i++){
     Real* matcp=A.m+i*nr+i;
     Real* matrp=matcp+nr;
     (*mp++)+=(*matcp++)*d;
     for(Integer j=i+1;j<nr;j++,matrp+=nr)
         (*mp++)+=((*matcp++)+(*matrp))*f;
 }
 return *this;
}

Symmatrix& Symmatrix::xeya(const Indexmatrix &A,double d)
{
 chk_init(A);
#ifdef DEBUG
 if (A.nr!=A.nc)
     MEmessage(MEdim(A.nr,A.nc,0,0,"Symmatrix::Symmatrix(const Matrix& A) A not square",MTsymmetric));
 is_init=0;
#endif
 newsize(A.nr);
 Real *mp=m;
 Real f=d/2.;
 for(Integer i=0;i<nr;i++){
     Integer* matcp=A.m+i*nr+i;
     Integer* matrp=matcp+nr;
     (*mp++)=(*matcp++)*d;
     for(Integer j=i+1;j<nr;j++,matrp+=nr)
         (*mp++)=((*matcp++)+(*matrp))*f;
 }
 chk_set_init(*this,1);
 return *this;
}

Symmatrix& Symmatrix::xpeya(const Indexmatrix &A,double d)
{
 chk_add(*this,A);
 Real *mp=m;
 Real f=d/2.;
 for(Integer i=0;i<nr;i++){
     Integer* matcp=A.m+i*nr+i;
     Integer* matrp=matcp+nr;
     (*mp++)+=(*matcp++)*d;
     for(Integer j=i+1;j<nr;j++,matrp+=nr)
         (*mp++)+=((*matcp++)+(*matrp))*f;
 }
 return *this;
}

void Symmatrix::newsize(Integer inr)
{
#ifdef DEBUG
 if (inr<0)
     MEmessage(MEdim(inr,inr,0,0,"Symmatrix::newsize(Integer,Integer) dim<0",MTsymmetric));
 is_init=0;
#endif
 if (inr!=nr) {
     nr=inr;
     Integer n2=(nr*(nr+1))/2;
     if (n2>mem_dim){
         memarray.free(m); m=0;
         mem_dim=memarray.get(n2,m);
         if (mem_dim<n2)
             MEmessage(MEmem(n2,
                         "Symmatrix::Symmatrix(Integer,Integer,Real) not enough memory",MTsymmetric));
     }
 }
}

void Symmatrix::display(ostream& out,int precision,int width,
                        int screenwidth) const
{
 chk_init(*this);
 out<<"symmatrix("<<nr<<")"<<endl;
 if (nr==0) return;
 if (precision==0) precision=4;
 out.precision(precision);
 if (width==0) width=precision+6;
 if (screenwidth==0) screenwidth=80;
 Integer colnr=screenwidth/(width+1);
 Integer k,i,j;
 Integer maxk=nr/colnr+((nr%colnr)>0);
 Integer maxj;
 for(k=0;k<maxk;k++){
     out<<"columns "<<k*colnr<<" to "<<min(nr,(k+1)*colnr)-1<<endl;
     for(i=0;i<nr;i++){
         maxj=min((k+1)*colnr,nr);
         for(j=k*colnr;j<maxj;j++){
             out<<' ';out.width(width);out<<(*this)(i,j);
         }
         out<<endl;
     }     
 }
}

Matrix Symmatrix::col(Integer c) const
{
  chk_init(*this);
#ifdef DEBUG
 if ((c<0)||(c>=nr))
     MEmessage(MErange(nr,0,nr,c,"Symmatrix::col(Integer) index out of range",MTsymmetric));
#endif
 Matrix v(nr,1);
 Real *mp=m+c;
 Real *vp=v.m;
 Integer i=0;
 for(;i<c;i++){
     (*vp++)=*mp;
     mp+=nr-i-1;
 }
 for(;i<nr;i++) (*vp++)=*mp++;
 chk_set_init(v,1);
 return v;
}

Matrix Symmatrix::row(Integer r) const
{
  chk_init(*this);
#ifdef DEBUG
 if ((r<0)||(r>=nr))
     MEmessage(MErange(nr,r,nr,0,"Symmatrix::row(Integer) index out of range",MTsymmetric));
#endif
 Matrix v(1,nr);
 Real *mp=m+r;
 Real *vp=v.m;
 Integer i=0;
 for(;i<r;i++){
     (*vp++)=*mp;
     mp+=nr-i-1;
 }
 for(;i<nr;i++) (*vp++)=*mp++;
 chk_set_init(v,1);
 return v;
}

// *****************************************************************************
//                  Interaction with other classes
// *****************************************************************************


// ******************************   Sparsmat   ******************************


#ifdef WITH_SPARSEMAT
#include "sparsmat.h"

Symmatrix& rankadd(const Sparsemat& A,Symmatrix& C,
                    Real alpha,Real beta,int trans)
{
 chk_init(A);
 if (trans){ //A^T*A
     if (beta==0.) {
         C.init(A.coldim(),0.);
     }
     else {
         chk_colseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     const Integer *ip=A.get_rowindex().get_store()-1;
     const Real *vp=A.get_rowval().get_store()-1;
     for(Integer j=0;j<A.rowinfo.rowdim();j++){
         for(Integer i=A.rowinfo(j,1);--i>=0;){
             const Real *vvp;
             Real d1=*(vvp=++vp);
             Real d=d1*alpha;
             const Integer *iip;
             Integer ii=*(iip=++ip);
             Real *mp=C.get_store()+ii*nr-(ii*(ii+1))/2;
             *(mp+ii)+=d*d1;
             for(Integer k=i;--k>=0;){
                 *(mp+(*(++iip)))+=d*(*(++vvp));
             }
         }
     }
     return C;
 }
 else { //A*A^T
     if (beta==0.) {
         C.init(A.rowdim(),0.);
     }
     else {
         chk_rowseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer nr=C.rowdim();
     const Integer *ip=A.get_colindex().get_store()-1;
     const Real *vp=A.get_colval().get_store()-1;
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         for(Integer i=A.colinfo(j,1);--i>=0;){
             const Real *vvp;
             Real d1=*(vvp=++vp);
             Real d=d1*alpha;
             const Integer *iip;
             Integer ii=*(iip=++ip);
             Real *mp=C.get_store()+ii*nr-(ii*(ii+1))/2;
             *(mp+ii)+=d*d1;
             for(Integer k=i;--k>=0;){
                 *(mp+(*(++iip)))+=d*(*(++vvp));
             }
         }
     }
     return C;
 }
}
 

Symmatrix& rank2add(const Sparsemat& A,const Matrix& B,Symmatrix& C,
                    Real alpha,Real beta,int trans)
{
 chk_add(A,B);
 if (trans) { //A^TB
     if (beta==0.) {
         C.init(A.coldim(),0.);
     }
     else {
         chk_colseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer n=A.rowdim();
     Integer nr=C.rowdim();
     const Integer *ip=A.get_colindex().get_store();
     const Real *vp=A.get_colval().get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         for(Integer i=A.colinfo(j,1);--i>=0;){
             Real d=alpha/2.*(*vp++);
             Real *mp=C.get_store()+jj;
             const Real *bp=B.get_store()+*ip++-n;
             Integer k=0;
             for(;++k<=jj;){
                 *mp += d* *(bp+=n);
                 mp+=nr-k;
             }
             (*mp++) += 2.*d* *(bp+=n);
             for(;++k<=nr;){
                 (*mp++) += d* *(bp+=n);
             }                 
         }
     }
     return C;
 }
 else { // AB^T 
     if (beta==0.) {
         C.init(A.rowdim(),0.);
     }
     else {
         chk_rowseq(C,A);
         if (beta!=1.) C*=beta;
     }
     if (alpha==0.) return C;
     Integer n=A.rowdim();
     Integer nr=C.rowdim();
     const Integer *jp=A.get_rowindex().get_store();
     const Real *vp=A.get_rowval().get_store();
     for(Integer i=0;i<A.rowinfo.rowdim();i++){
         Integer ii=A.rowinfo(i,0);
         for(Integer j=A.rowinfo(i,1);--j>=0;){
             Real d=alpha/2.*(*vp++);
             Real *mp=C.get_store()+ii;
             const Real *bp=B.get_store()+(*jp++)*n;
             Integer k=0;
             for(;++k<=ii;){
                 *mp += d* *(bp++);
                 mp+=nr-k;
             }
             (*mp++) += 2.*d* *(bp++);
             for(;++k<=nr;){
                 (*mp++) += d* *(bp++);
             }                 
         }
     }
     return C;
 }
}     

Symmatrix& Symmatrix::xetriu_yza(const Sparsemat& A,const Matrix& B,Real d)
{
 chk_add(A,B);
 init(A.coldim(),0.);
 chk_set_init(*this,1);
 if (d==0.) { return *this;}
 if (d==1.) {
     Integer n=A.rowdim();
     const Integer *ip=A.get_colindex().get_store();
     const Real *vp=A.get_colval().get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         Real *mp=m+jj*nr-(jj*(jj-1))/2;
         const Real *bp=B.get_store()+jj*n;
         for(Integer i=A.colinfo(j,1);--i>=0;){
             mat_xpeya(nr-jj,mp,1,bp+*ip++,n,*vp++);
         }
     }
     return *this;
 }
 if (d==-1.) {
     Integer n=A.rowdim();
     const Integer *ip=A.get_colindex().get_store();
     const Real *vp=A.get_colval().get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         Real *mp=m+jj*nr-(jj*(jj-1))/2;
         const Real *bp=B.get_store()+jj*n;
         for(Integer i=A.colinfo(j,1);--i>=0;){
             mat_xpeya(nr-jj,mp,1,bp+*ip++,n,-*vp++);
         }
     }
     return *this;
 }
 Integer n=A.rowdim();
 const Integer *ip=A.get_colindex().get_store();
 const Real *vp=A.get_colval().get_store();
 for(Integer j=0;j<A.colinfo.rowdim();j++){
     Integer jj=A.colinfo(j,0);
     Real *mp=m+jj*nr-(jj*(jj-1))/2;
     const Real *bp=B.get_store()+jj*n;
     for(Integer i=A.colinfo(j,1);--i>=0;){
         mat_xpeya(nr-jj,mp,1,bp+*ip++,n,d*(*vp++));
     }
 }
 return *this;
}
         

// *this is the upper triangle of the matrix product A^T*B
Symmatrix& Symmatrix::xpetriu_yza(const Sparsemat& A,const Matrix& B,Real d)
{
 chk_add(A,B);
 chk_colseq(*this,A);
 if (d==0.) { return *this;}
 if (d==1.) {
     Integer n=A.rowdim();
     const Integer *ip=A.get_colindex().get_store();
     const Real *vp=A.get_colval().get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         Real *mp=m+jj*nr-(jj*(jj-1))/2;
         const Real *bp=B.get_store()+jj*n;
         for(Integer i=A.colinfo(j,1);--i>=0;){
             mat_xpeya(nr-jj,mp,1,bp+*ip++,n,*vp++);
         }
     }
     return *this;
 }
 if (d==-1.) {
     Integer n=A.rowdim();
     const Integer *ip=A.get_colindex().get_store();
     const Real *vp=A.get_colval().get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         Real *mp=m+jj*nr-(jj*(jj-1))/2;
         const Real *bp=B.get_store()+jj*n;
         for(Integer i=A.colinfo(j,1);--i>=0;){
             mat_xpeya(nr-jj,mp,1,bp+*ip++,n,-(*vp++));
         }
     }
     return *this;
 }
 Integer n=A.rowdim();
 const Integer *ip=A.get_colindex().get_store();
 const Real *vp=A.get_colval().get_store();
 for(Integer j=0;j<A.colinfo.rowdim();j++){
     Integer jj=A.colinfo(j,0);
     Real *mp=m+jj*nr-(jj*(jj-1))/2;
     const Real *bp=B.get_store()+jj*n;
     for(Integer i=A.colinfo(j,1);--i>=0;){
         mat_xpeya(nr-jj,mp,1,bp+*ip++,n,d*(*vp++));
     }
 }
 return *this;
}

Matrix& genmult(const Symmatrix& A,const Sparsemat& B,Matrix& C,
                    Real alpha,Real beta,int btrans)
            //returns C=beta*C+alpha*A*B, where A and B may be transposed
{
 chk_init(A);
 chk_init(B);
 Integer nr,nc;
 nr=A.nr;
#ifdef DEBUG
 Integer nm=A.nr;
#endif
 if (btrans) {
     nc=B.nr;
#ifdef DEBUG
     if (nm!=B.nc) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
 }
 else {
     nc=B.nc;
#ifdef DEBUG
     if (nm!=B.nr) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
 }
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.rowdim())||(nc!=C.coldim())) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (btrans){
     const Integer *bip=B.rowindex.get_store();
     const Real *bvp=B.rowval.get_store();
     for(Integer j=0;j<B.rowinfo.rowdim();j++){
         Integer jj=B.rowinfo(j,0);
         for(Integer i=0;i<B.rowinfo(j,1);i++){
             Real d=alpha*(*bvp++);
             Integer ii=*bip++;
             const Real* ap=A.get_store()+ii;
             Real* cp=C.get_store()+jj*nr;
             Integer k=0;
             for(;k<ii;){
                 *cp++ +=d*(*ap);
                 ap+=nr- ++k;
             }
             for(;k++<nr;){
                 *cp++ +=d*(*ap++);
             }
         }
     }
 }
 else {
     const Integer *bip=B.colindex.get_store();
     const Real *bvp=B.colval.get_store();
     for(Integer j=0;j<B.colinfo.rowdim();j++){
         Integer jj=B.colinfo(j,0);
         for(Integer i=0;i<B.colinfo(j,1);i++){
             Real d=alpha*(*bvp++);
             Integer ii=*bip++;
             const Real* ap=A.get_store()+ii;
             Real* cp=C.get_store()+jj*nr;
             Integer k=0;
             for(;k<ii;){
                 *cp++ +=d*(*ap);
                 ap+=nr- ++k;
             }
             for(;k++<nr;){
                 *cp++ +=d*(*ap++);
             }
         }
     }
 }
 return C;
}
         
Matrix& genmult(const Sparsemat& A,const Symmatrix& B,Matrix& C,
                    Real alpha,Real beta,int atrans)
            //returns C=beta*C+alpha*A*B, where A and B may be transposed
{
 chk_init(A);
 chk_init(B);
 Integer nr,nc;
#ifdef DEBUG
 Integer nm;
#endif
 if (atrans){
     nr=A.nc;
#ifdef DEBUG
     nm=A.nr;
#endif
 }
 else {
     nr=A.nr;
#ifdef DEBUG
     nm=A.nc;
#endif
 }
 nc=B.nr;
#ifdef DEBUG
 if (nm!=B.nr) {
     MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsymmetric));;
     exit(1);
 }
#endif
 if (beta!=0.){
     chk_init(C);
#ifdef DEBUG
     if ((nr!=C.rowdim())||(nc!=C.coldim())) {
         MEmessage(MatrixError(ME_dim,"genmult: dimensions don't match",MTsymmetric));;
         exit(1);
     }
#endif
     if (beta!=1.) C*=beta;
 }
 else {
     C.init(nr,nc,0.);
 }
 if (alpha==0.) return C;
 if (atrans){
     const Integer *aip=A.colindex.get_store();
     const Real *avp=A.colval.get_store();
     for(Integer j=0;j<A.colinfo.rowdim();j++){
         Integer jj=A.colinfo(j,0);
         for(Integer i=0;i<A.colinfo(j,1);i++){
             Real d=alpha*(*avp++);
             Integer ii=*aip++;
             const Real* bp=B.get_store()+ii;
             Real* cp=C.get_store()+jj;
             Integer k=0;
             for(;k<ii;){
                 *cp +=d*(*bp);
                 cp+=nr;
                 bp+=nr- ++k;
             }
             for(;k++<nr;){
                 *cp +=d*(*bp++);
                 cp+=nr;
             }
         }
     }
 }
 else {
     const Integer *aip=A.rowindex.get_store();
     const Real *avp=A.rowval.get_store();
     for(Integer j=0;j<A.rowinfo.rowdim();j++){
         Integer jj=A.rowinfo(j,0);
         for(Integer i=0;i<A.rowinfo(j,1);i++){
             Real d=alpha*(*avp++);
             Integer ii=*aip++;
             const Real* bp=B.get_store()+ii;
             Real* cp=C.get_store()+jj;
             Integer k=0;
             for(;k<ii;){
                 *cp +=d*(*bp);
                 cp+=nr;
                 bp+=nr- ++k;
             }
             for(;k++<nr;){
                 *cp +=d*(*bp++);
                 cp+=nr;
             }
         }
     }
 }
 return C;
}
         

#endif

// **************************************************************************
//                               friends
// **************************************************************************

void svec(const Symmatrix &A,Matrix& sv)
{
 chk_init(A);
 const Real sqrt2=sqrt(2.); //1.41421356237310;
 sv.newsize((A.nr*(A.nr+1))/2,1);
 Real *mp=A.m;
 Real *vp=sv.get_store();
 Integer i,j;
 for(i=A.nr;--i>=0;){
     (*vp++)=(*mp++);
     for(j=i;--j>=0;){
         (*vp++)=sqrt2*(*mp++);
     }
 }
 chk_set_init(sv,1);
}

void sveci(const Matrix& sv,Symmatrix &A)
{
 chk_init(sv);
 Integer dim=Integer(sqrt(Real(8*sv.dim()+1))-1+.1)/2;
#ifdef DEBUG
 if ((dim*(dim+1))/2!=sv.dim())
     MEmessage(MatrixError(ME_unspec,
               "sveci(): dimension of svec does not permit conversion",
               MTsymmetric));
#endif                            
 const Real sqrt2=1./sqrt(2.); //1./1.41421356237310;
 A.newsize(dim);
 const Real *mp=sv.get_store();
 Real *vp=A.m;
 Integer i,j;
 for(i=A.nr;--i>=0;){
     (*vp++)=(*mp++);
     for(j=i;--j>=0;){
         (*vp++)=sqrt2*(*mp++);
     }
 }
 chk_set_init(A,1);
}

void skron(const Symmatrix& A,const Symmatrix& B,Symmatrix& S)
{
 chk_add(A,B);
 const Real sqrt2=sqrt(2.); // 1.41421356237310;
 Integer n=A.rowdim(); 
 S.newsize((n*(n+1))/2);
 chk_set_init(S,1);
 Matrix ai(n,1);
 Matrix bi(n,1);
 Matrix aj(n,1);
 Matrix bj(n,1);
 Matrix sv((n*(n+1))/2,1);
 chk_set_init(sv,1);
 Integer h,i,j,k,l,m;
 Real* Sp=S.get_store();
 m=(n*(n+1))/2;
 for(i=0;i<n;i++){
     ai=A.col(i);
     bi=B.col(i);

     //row corresponding to diagonal
     h=0;
     for(l=i;l<n;l++){
         sv(h++)=ai(l)*bi(l);
         for(k=l+1;k<n;k++){
             sv(h++)=sqrt2*(ai(l)*bi(k)+ai(k)*bi(l))/2.;
         }
     }
     mat_xey(m,Sp,sv.get_store());
     Sp+=m;
     m--;
     
     for(j=i+1;j<n;j++){
         aj=A.col(j);
         bj=B.col(j);

         //row correpsonding to offdiagonal
         h=0;
         for(k=j;k<n;k++){
             sv(h++)=(ai(i)*bj(k)+ai(k)*bj(i)+aj(i)*bi(k)+aj(k)*bi(i))/2.;
         }
         for(l=i+1;l<n;l++){
             sv(h++)=sqrt2*(ai(l)*bj(l)+aj(l)*bi(l))/2.;
             for(k=l+1;k<n;k++){
                 sv(h++)=(ai(l)*bj(k)+ai(k)*bj(l)+aj(l)*bi(k)+aj(k)*bi(l))/2.;
             }
         }
         mat_xey(m,Sp,sv.get_store());
         Sp+=m;
         m--;
     }
 }
}

Matrix diag(const Symmatrix& A)
{
  chk_init(A);
  Matrix v(A.nr,1);
  for(Integer i=0;i<A.nr;i++)
    v(i)=A(i,i);
  chk_set_init(v,1);
  return v;
}

Symmatrix Diag(const Matrix& A)
{
  chk_init(A);
  Symmatrix S(A.dim(),0.);
  for(Integer i=0;i<A.dim();i++)
    S(i,i)=A(i);
  chk_set_init(S,1);
  return S;
}

Matrix sumrows(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"sumrows(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"sumrows(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(1,A.nr);
 Real sum;
 Integer i,j;
 for(j=0;j<A.nr;j++){
     sum=0.;
     for(i=0;i<A.nr;i++)
         sum+=A(i,j);
     v(j)=sum;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Matrix sumcols(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"sumcols(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"sumcols(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(A.nr,1);
 Real sum;
 Integer i,j;
 for(i=0;i<A.nr;i++){
     sum=0.;
     for(j=0;j<A.nr;j++)
         sum+=A(i,j);
     v(i)=sum;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Real sum(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"sum(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"sum(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 register Real s=0.;
 register Real s2=0;
 register Integer i,j;
 register const Real *ap=A.m;
 for(i=A.nr;--i>=0;){
     s+=(*ap++);
     s2=0.;
     for(j=i;--j>=0;){
         s2+=(*ap++);
     }
     s+=2.*s2;
 }
 return s;
}

Matrix maxrows(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"maxrows(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"maxrows(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(1,A.nr);
 Real maxd;
 Integer i,j;
 for(j=0;j<A.nr;j++){
     maxd=A(0,j);
     for(i=1;i<A.nr;i++)
         maxd=max(maxd,A(i,j));
     v(j)=maxd;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Matrix maxcols(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"maxcols(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"maxcols(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(A.nr,1);
 Real maxd;
 Integer i,j;
 for(i=0;i<A.nr;i++){
     maxd=A(i,0);
     for(j=1;j<A.nr;j++)
         maxd=max(maxd,A(i,j));
     v(i)=maxd;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Real max(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"max(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"max(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Integer i,j;
 Real maxd=A(0,0);
 for(i=0;i<A.nr;i++){
     maxd=max(maxd,A(i,i));
     for(j=i+1;j<A.nr;j++){
         maxd=max(maxd,A(i,j));
     }
 }
 return maxd;
}

Matrix minrows(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"minrows(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"minrows(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(1,A.nr);
 Real mind;
 Integer i,j;
 for(j=0;j<A.nr;j++){
     mind=A(0,j);
     for(i=1;i<A.nr;i++)
         mind=min(mind,A(i,j));
     v(j)=mind;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Matrix mincols(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"mincols(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"mincols(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Matrix v(A.nr,1);
 Real mind;
 Integer i,j;
 for(i=0;i<A.nr;i++){
     mind=A(i,0);
     for(j=1;j<A.nr;j++)
         mind=min(mind,A(i,j));
     v(i)=mind;
 }
#ifdef DEBUG
 v.set_init(1);
#endif 
 return v;
}

Real min(const Symmatrix& A)
{
#ifdef DEBUG
 if (A.nr==0)
     MEmessage(MEdim(A.nr,A.nr,0,0,"min(const Symmatrix&) dimension zero",MTsymmetric));
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"min(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Integer i,j;
 Real mind=A(0,0);
 for(i=0;i<A.nr;i++){
     mind=min(mind,A(i,i));
     for(j=i+1;j<A.nr;j++){
         mind=min(mind,A(i,j));
     }
 }
 return mind;
}

Real trace(const Symmatrix& A)
{
#ifdef DEBUG
 if (!A.is_init)
     MEmessage(MatrixError(ME_unspec,"trace(const Symmatrix& A) A is not initialized",MTsymmetric));
#endif
 Real sum=0.;
 for(Integer i=0;i<A.nr;i++) sum+=A(i,i);
 return sum;
}

Real ip(const Symmatrix& A, const Symmatrix& B)
{
#ifdef DEBUG
 if (B.nr!=A.nr)
     MEmessage(MEdim(A.nr,A.nr,B.nr,B.nr,"ip(const Symmatrix&,const Symmatrix&) wrong dimensions",MTsymmetric));
 if ((!A.is_init)||(!B.is_init))
     MEmessage(MatrixError(ME_unspec,"ip(const Symmatrix&,const Symmatrix&) not initialized",MTsymmetric));
#endif
 register Real sum=0.;
 register Real s2;
 register Integer i,j;
 register const Real *ap=A.m;
 register const Real *bp=B.m;
 for(i=A.nr;--i>=0;){
     sum+=(*ap++)*(*bp++);
     s2=0.;
     for(j=i;--j>=0;){
         s2+=(*ap++)*(*bp++);
     }
     sum+=2.*s2;
 }
 return sum;
}

Real ip(const Symmatrix& A, const Matrix& B)
{
  chk_add(A,B);
  Matrix C(A);
  return ip(C,B);
}

Real ip(const Matrix& A, const Symmatrix& B)
{
  chk_add(A,B);
  Matrix C(B);
  return ip(A,C);
}

Symmatrix abs(const Symmatrix& A)
{
  chk_init(A);
  Symmatrix B(A.nr);
  Real *ap=A.m;
  Real *bp=B.m;
  for(Integer i=(A.nr*(A.nr+1))/2;--i>=0;)
    (*bp++)=my_abs(*ap++);
  chk_set_init(B,1);
  return B;
}

ostream& operator<<(ostream& o,const Symmatrix &A)
{
 chk_init(A);
 o<<A.nr<<'\n';
 Integer i,j;
 for(i=0;i<A.nr;i++){
     for(j=i;j<A.nr;j++) o<<' '<<A(i,j);
     o<<'\n';
 }
 return o;
}

istream& operator>>(istream& in,Symmatrix &A)
{
 Real d;
 in>>d;
 Integer nr=Integer(d+.5);
 if (nr<0)
     MEmessage(MEdim(nr,nr,0,0,"operator>>(istream&,Symmatrix&) dimension negative",MTsymmetric));
 A.newsize(nr);
 for(Integer i=0;i<nr;i++)
     for(Integer j=i;j<nr;j++)
         in>>A(i,j);
 chk_set_init(A,1);
 return in;
}

// **************************************************************************
//                       Matrix:: Symmatrix specific implementations
// **************************************************************************

Matrix& Matrix::xeya(const Symmatrix& A,Real d)
{
  chk_init(A);
  newsize(A.nr,A.nr);
  const Real *matp=A.m;
  if (d==1.){
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)=(*matp++);
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
	(*mcp++)=(*mrp)=(*matp++);
      }
    }
  }
  else if (d==-1.){
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)=-(*matp++);
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
	(*mcp++)=(*mrp)=-(*matp++);
      }
    }
  }
  else {
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)=(*matp++)*d;
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
	(*mcp++)=(*mrp)=(*matp++)*d;
      }
    }
  }
  chk_set_init(*this,1);
  return *this;
}

Matrix& Matrix::xpeya(const Symmatrix& A,Real d)
{
  chk_add(*this,A);
  const Real *matp=A.m;
  if (d==1.){
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)+=(*matp++);
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
	(*mcp++)+=(*matp);
        (*mrp)+=(*matp++);
      }
    }
  }
  else if (d==-1.){
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)-=(*matp++);
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
	(*mcp++)-=(*matp);
        (*mrp)-=(*matp++);
      }
    }
  }
  else {
    for(Integer i=0;i<nr;i++){
      Real *mcp=m+i*nr+i;
      Real *mrp=mcp+nr;
      (*mcp++)+=(*matp++)*d;
      for(Integer j=i+1;j<nr;j++,mrp+=nr){
        Real f=d*(*matp++);
	(*mcp++)+=f;
        (*mrp)+=f;
      }
    }
  }
  chk_set_init(*this,1);
  return *this;
}



/* ---------------------------------------------------------------------------
 *    Change log $Log: symmat.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:26  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/symmat.cc,v $
 --------------------------------------------------------------------------- */
