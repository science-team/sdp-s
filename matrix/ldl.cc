/* -*-c++-*-
 * 
 *    Source     $RCSfile: ldl.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:26 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: ldl.cc,v 1.1.1.1.2.1 2000/02/28 13:55:26 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "matop.h"
#include "symmat.h"

int Symmatrix::LDLfactor(Real tol)
{
  chk_init(*this);
 register Real d;
 register Integer k,h,j,hn,i;
 Real *v;
 memarray.get(nr,v);
 register Real *vp;
 register Real *colj;
 register Real *mp;
 Integer failure=0;
 hn=nr;
 for(i=0;i<nr;i++){
     mp=m+i;
     vp=v;
     d=0.;
     for(j=0;j<i;j++,mp+=nr-j) {
         d+=(*mp)*((*vp++)=(*mp)*(*(mp-i+j)));
     }
     *vp=*mp-=d;
     if (*mp<tol) {
         failure=1;
         break;
     }
     /*
     h=nr-i;
     //for(j=i+1;j<nr;j++){
     for(j=i;++j<hn;){
         colj=m+j;
         vp=v;
         d=*(++mp);
         for(k=hn;--k>=h;){
             d-=(*colj)*(*vp++);
             colj+=k;
         }
         (*mp)=d/(*vp);
     }
     */
     h=nr-i+1;
     for(j=i;++j<hn;){
         colj=m+j-hn;
         vp=v;
         d=*(++mp);
         for(k=hn+1;--k>=h;){
             d-=(*(colj+=k))*(*vp++);
         }
         (*mp)=d/(*vp);
     }
 }
 memarray.free(v);
 return failure;
}

int Symmatrix::LDLsolve(Matrix& x) const
{
 chk_mult(*this,x);

 register Real f;
 register Integer i,j,h;
 Integer k;
 register Real *mp;
 register Real *rp;
 Real *xbase;

 for(k=0;k<x.coldim();k++){ //solve for and overwrite column k of x
     xbase=x.m+k*nr;
     //---- solve Lr=xbase
     for(i=0;i<nr;i++){
         f=xbase[i];
         rp=xbase;
         mp=m+i;
         h=nr-i;
         for(j=nr;--j>=h;) {
             f-=(*rp++)*(*mp);
             mp+=j;
         }
         *rp=f;
     }

     //---- solve Dr=r
     mp=m;
     rp=xbase;
     for(i=nr;i>0;i--) {
         (*rp++)/=*mp;
         mp+=i;
     }

     //---- solve L'r=r
     for(i=nr;--i>=0;){
         rp=xbase+i;
         f=(*rp++);
         mp=m+i*nr-(i*(i-1))/2+1;
         for(j=nr;--j>i;) f-=(*rp++)*(*mp++);
         xbase[i]=f;
     }
 }
 return 0;
}

int Symmatrix::LDLinverse(Symmatrix &S) const
{
 chk_init(*this);
 S.newsize(nr);
 Real *sp=S.m;
 register Real f;
 register const Real *mp;
 register Real* sp1;
 register Integer e,i,j,indi;
 for(e=0;e<nr;e++){
     sp=S.m+e*nr-((e-1)*e)/2-e;
     //---- solve Lr=v
     sp[e]=1.;
     indi=e*nr-((e-1)*e)/2+1;
     mat_xemy(nr-e-1,sp+e+1,m+indi);
     for(i=e+1;i<nr;i++){
         indi+=nr-i+1;
         mat_xpeya(nr-i-1,sp+i+1,m+indi,-sp[i]);
     }
 
     //---- solve Dr=r
     mp=m+(nr*(nr+1))/2-1;
     sp1=sp+nr;
     for(i=nr;--i>=e;) {
         (*(--sp1))/=(*mp);
         mp-=nr-i+1;
     }
 
     //---- solve L'r=r
     indi=(nr*(nr+1))/2-1;
     for(i=nr;--i>=e;){
         mp=m+indi-(nr-i);
         sp1=sp+i;
         f=*sp1;
         for(j=i;--j>=e;) {
             (*(--sp1))-=f*(*mp);
             mp-=nr-j;
         }
         indi-=nr-i+1;
     } 
 }
 chk_set_init(S,1);
 return 0;
}



/* ---------------------------------------------------------------------------
 *    Change log $Log: ldl.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:26  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/ldl.cc,v $
 --------------------------------------------------------------------------- */
