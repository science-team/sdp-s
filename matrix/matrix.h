/* -*-c++-*-
 * 
 *    Source     $RCSfile: matrix.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:21 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <cmath>
#include "indexmat.h"
#include "mymath.h"


// **************************************************************************
//                               Range
// **************************************************************************

class Realrange
{
public:
  Real from;
  Real to;
  Real step;
  Real tol;
  Realrange(Real f,Real t,Real s=1,Real tl=1e-8):
    from(f),to(t),step(s),tol(tl){}
  ~Realrange(){} 
};

// **************************************************************************
//                               Matrix
// **************************************************************************


class Matrix: protected Memarrayuser
{
  friend class Indexmatrix;
#ifdef WITH_SYMMATRIX
  friend class Symmatrix;
#endif
#ifdef WITH_SPARSEMAT
  friend class Sparsemat;
#endif
#ifdef WITH_SPARSESYM
  friend class Sparsesym;
#endif
  
private:

  Mtype mtype;   //name of this type to recognize special kinds of matrices
  Integer mem_dim;   //amount of memory currently allocated
  Integer nr,nc;     //number rows, number columns
  Real *m;     //stored columnwise (a11,a21,...,anr1,a22,a23,.....)
  
#ifdef DEBUG
  Integer is_init;      //flag whether memory is initialized
#endif
  
  inline void init_to_zero();   

public:

  //----------------------------------------------------
  //----  constructors, destructor, and initialization
  //----------------------------------------------------

  inline Matrix();                              
  inline Matrix(const Matrix&,Real d=1.);       
  inline Matrix(const Indexmatrix&,Real d=1.);            
  inline Matrix(const Realrange&);              
  inline Matrix(Integer nr,Integer nc);         
  inline Matrix(Integer nr,Integer nc,Real d);  
  inline Matrix(Integer nr,Integer nc,const Real* dp,Integer incr=1); 
  ~Matrix(){memarray.free(m);}
  
#ifdef DEBUG
  void set_init(Integer i){is_init=i;}
  int get_init() const {return is_init;}
#endif
  
  inline Matrix& init(const Matrix &A,Real d=1.);                   
  inline Matrix& init(const Indexmatrix&,Real d=1.);
  Matrix& init(const Realrange&);
  inline Matrix& init(Integer r,Integer c,Real d);                    
  inline Matrix& init(Integer r,Integer c,const Real *dp,Integer incr=1); 
    
  void newsize(Integer r,Integer c);  //resize matrix without initialization


  //----------------------------------------------------
  //----  size and type information
  //----------------------------------------------------

  void dim(Integer& r, Integer& c) const {r=nr; c=nc;}
  Integer dim() const {return nr*nc;}
  Integer rowdim() const {return nr;}
  Integer coldim() const {return nc;}
  
  Mtype get_mtype() const {return mtype;}

    
  //--------------------------------
  //----  Indexing and Submatrices
  //--------------------------------

  inline Real& operator()(Integer i,Integer j);      
  inline Real& operator()(Integer i);      //index to vector of stacked columns
  inline Real operator()(Integer i,Integer j) const; 
  inline Real operator()(Integer i) const; //index to vector of stacked columns
  Matrix operator()(const Indexmatrix& vecrow,const Indexmatrix& veccol) const;
      //extract submatrix, both indexmatrices are interpreted as vectors
      //repeated elements are allowed
  Matrix operator()(const Indexmatrix& A) const;
      //*this matrix is interpreted as vector, the resulting matrix has
      //has the same shape as A
  
  inline Real& operator[](Integer i);      //{return (*this)(i);}
  inline Real operator[](Integer i) const; //{return (*this)(i);}
  
  Matrix col(Integer i) const;          //returns column i as column vector
  Matrix row(Integer i) const;          //returns row i as row vector
  Matrix cols(const Indexmatrix &vec) const;  //returns cols as indexed by vec
  Matrix rows(const Indexmatrix &vec) const;  //returns rows as indexed by vec

  friend Matrix diag(const Matrix& A);      //=(A(1,1),A(2,2),...)^t

  Matrix& triu(Integer d=0);    // (*this)(i,j)=0 for i<j+d
  Matrix& tril(Integer d=0);    // (*this)(i,j)=0 for i>j+d
  friend inline Matrix triu(const Matrix& A,Integer i=0);
  friend inline Matrix tril(const Matrix& A,Integer i=0);
  
  Matrix& subassign(const Indexmatrix& vecrow,const Indexmatrix& veccol,
		    const Matrix& A);
     //(*this)(vecrow(i),veccol(j))=A(i,j) for all i,j
  Matrix& subassign(const Indexmatrix& vec,const Matrix& A);
     //(*this)(vec(i))=A(i);
     //*this, vec, and A may be rect matrices but will be used as vectors
  
  Matrix& delete_rows(const Indexmatrix& ind);
  Matrix& delete_cols(const Indexmatrix& ind);

  inline Matrix& reduce_length(Integer n);  
     //interpret *this as a column vector and cut at n 

  Matrix& concat_right(const Matrix& A);
  Matrix& concat_below(const Matrix& A);
  friend inline Matrix concat_right(const Matrix& A,const Matrix& B);
  friend inline Matrix concat_below(const Matrix& A,const Matrix& B);
  
  friend inline void swap(Matrix &A,Matrix &B);

  Real* get_store() {return m;}       //use cautiously, do not use delete!
  const Real* get_store() const {return m;}   //use cautiously

  
  //------------------------------
  //----  BLAS-like Routines
  //------------------------------

  Matrix& xeya(const Matrix& A,Real d=1.);   //*this=d*A
  Matrix& xpeya(const Matrix& A,Real d=1.);  //*this+=d*A;
  Matrix& xeya(const Indexmatrix& A,Real d=1.);   //*this=d*A
  Matrix& xpeya(const Indexmatrix& A,Real d=1.);  //*this+=d*A;
    
  friend Matrix& xbpeya(Matrix& x,const Matrix& y,Real alpha=1.,Real beta=0.,int ytrans=0);
  //returns x= alpha*y+beta*x, where y may be transposed (ytrans=1)
  //if beta==0. then x is initialized to the correct size
  
  friend Matrix& xeyapzb(Matrix& x,const Matrix& y,const Matrix& z,Real alpha=1.,Real beta=1.);
  //returns x= alpha*y+beta*z
  //x is initialized to the correct size
  
  friend Matrix& genmult(const Matrix& A,const Matrix& B,Matrix& C,
			 Real alpha=1.,Real beta=0.,int atrans=0,int btrans=0);
  //returns C=beta*C+alpha*A*B, where A and B may be transposed
  //C may neither be equal to A nor B
  //if beta==0 then C is initialized to the correct size
    

  //------------------------------
  //----  usual operators
  //------------------------------

  inline Matrix& operator=(const Matrix &A);
  inline Matrix& operator*=(const Matrix &s);
  inline Matrix& operator+=(const Matrix &v);
  inline Matrix& operator-=(const Matrix &v);
  inline Matrix& operator%=(const Matrix &A);   //Hadamard product
  inline Matrix operator-() const;
  
  inline Matrix& operator*=(Real d);
  inline Matrix& operator/=(Real d);
  inline Matrix& operator+=(Real d);
  inline Matrix& operator-=(Real d);
  
  Matrix& transpose();            //transposes itself
  
  friend inline Matrix operator*(const Matrix &A,const Matrix& B);
  friend inline Matrix operator+(const Matrix &A,const Matrix& B);
  friend inline Matrix operator-(const Matrix &A,const Matrix& B);
  friend inline Matrix operator%(const Matrix &A,const Matrix& B);
  friend inline Matrix operator*(const Matrix &A,Real d);
  friend inline Matrix operator*(Real d,const Matrix &A);
  friend inline Matrix operator/(const Matrix& A,Real d);
  friend inline Matrix operator+(const Matrix& A,Real d);
  friend inline Matrix operator+(Real d,const Matrix& A);
  friend inline Matrix operator-(const Matrix& A,Real d);
  friend inline Matrix operator-(Real d,const Matrix& A);

  friend Matrix transpose(const Matrix& A);

  //------------------------------------------
  //----  Connections to other Matrix Classes
  //------------------------------------------
  
#ifdef WITH_SYMMATRIX
  Matrix& xeya(const Symmatrix& A,Real d=1.);  //*this=A*d;
  Matrix& xpeya(const Symmatrix& A,Real d=1.); //*this+=A*d;
  friend Matrix& genmult(const Symmatrix& A,const Matrix& B,Matrix& C,
			 Real alpha,Real beta,int btrans);
  friend Matrix& genmult(const Matrix& A,const Symmatrix& B,Matrix& C,
			 Real alpha,Real beta,int atrans);
  inline Matrix(const Symmatrix& S,Real d=1.);       
  inline Matrix& init(const Symmatrix& S,Real d=1.);
  inline Matrix& operator=(const Symmatrix& S);
  inline Matrix& operator*=(const Symmatrix& S);
  inline Matrix& operator+=(const Symmatrix& S);
  inline Matrix& operator-=(const Symmatrix& S);
#endif

#ifdef WITH_SPARSESYM
  Matrix& xeya(const Sparsesym& A,Real d=1.);  //*this=A*d;
  Matrix& xpeya(const Sparsesym& A,Real d=1.); //*this+=A*d;
  friend Matrix& genmult(const Sparsesym& A,const Matrix& B,Matrix& C,
			 Real alpha,Real beta,int btrans);
  friend Matrix& genmult(const Matrix& A,const Sparsesym& B,Matrix& C,
			 Real alpha,Real beta,int atrans);
  inline Matrix(const Sparsesym&,Real d=1.);       
  inline Matrix& init(const Sparsesym&,Real d=1.);
  inline Matrix& operator=(const Sparsesym &);
  inline Matrix& operator*=(const Sparsesym& S);
  inline Matrix& operator+=(const Sparsesym& S);
  inline Matrix& operator-=(const Sparsesym& S);
#endif
    
#ifdef WITH_SPARSEMAT
  Matrix& xeya(const Sparsemat& A,Real d=1.);  //*this=A*d;
  Matrix& xpeya(const Sparsemat& A,Real d=1.); //*this+=A*d;
  friend Matrix& genmult(const Sparsemat& A,const Matrix& B,Matrix &C,
			 Real alpha,Real beta, int atrans,int btrans);
  friend Matrix& genmult(const Matrix& A,const Sparsemat& B,Matrix &C,
			 Real alpha,Real beta, int atrans,int btrans);
  inline Matrix(const Sparsemat& A, Real d=1.);       //*this=A*d;
  inline Matrix& init(const Sparsemat& A, Real d=1.); //*this=A*d;
  inline Matrix& operator=(const Sparsemat & A);      
  inline Matrix& operator*=(const Sparsemat &A);      
  inline Matrix& operator+=(const Sparsemat &A);      
  inline Matrix& operator-=(const Sparsemat &A);      
#endif


  //------------------------------
  //----  Elementwise Operations
  //------------------------------

  Matrix& rand(Integer nr,Integer nc);
  Matrix& inv(void);                        //reciprocal value componentwise
  Matrix& sqrt(void);
  Matrix& sign(Real tol=1e-12);
  Matrix& floor(void);
  Matrix& ceil(void);
  Matrix& rint(void);
  Matrix& abs(void);        

  friend inline Matrix rand(Integer rows,Integer cols);
  friend inline Matrix inv(const Matrix& A);
  friend inline Matrix sqrt(const Matrix& A);
  friend inline Matrix sign(const Matrix& A,Real tol=1e-12);
  friend inline Matrix floor(const Matrix& A);
  friend inline Matrix ceil(const Matrix& A);
  friend inline Matrix rint(const Matrix& A);
  friend Matrix abs(const Matrix& A);                 

  //----------------------------
  //----  Numerical Methods
  //----------------------------

  friend Real trace(const Matrix& A);               //=sum(diag(A))
  friend inline Real ip(const Matrix& A, const Matrix& B); //=trace(B^t*A)
  friend inline Real norm2(const Matrix& A);               //=sqrt(ip(A,A));

  Matrix& scale_rows(const Matrix& vec);    //A=diag(vec)*A
  Matrix& scale_cols(const Matrix& vec);    //A=A*diag(vec)
  
  friend Matrix sumrows(const Matrix& A);   //=(1 1 1 ... 1)*A
  friend Matrix sumcols(const Matrix& A);   //=A*(1 1 ... 1)^t
  friend Real sum(const Matrix& A);         //=(1 1 ... 1)*A*(1 1 ... 1)^t


  //----- triangular routines
  int triu_solve(Matrix& rhs,Real tol=1e-10);   //only elements i>=j are used
  int tril_solve(Matrix& rhs,Real tol=1e-10);   //only elements i<=j are used


  //----- Householder rotations
  friend Matrix house(const Matrix &x,Integer i=0,Integer j=0);
  friend int rowhouse(Matrix &A,const Matrix& v,Integer i=0,Integer j=0);
  friend int colhouse(Matrix &A,const Matrix& v,Integer i=0,Integer j=0);   


  //----- QR-Factorization
  int QR_factor();                            //factorization stored in *this
  int QR_factor(Matrix& Q);                   //afterwards *this is R
  inline int QR_factor(Matrix& Q,Matrix& R) const;    //*this is unchanged

  int QR_factor(Indexmatrix& piv);
  int QR_factor(Matrix& Q,Indexmatrix& piv);
  inline int QR_factor(Matrix& Q,Matrix& R,Indexmatrix& piv) const;
  //{R=*this;return R.QR_factor(Q,piv);}
  int Qt_times(Matrix& A,Integer r);
  int Q_times(Matrix& A,Integer r);
  int times_Q(Matrix& A,Integer r);
  int QR_solve(Matrix& rhs,Real tol=1e-10);   //*this is factored and the
  //solution replaces rhs

  friend inline int QR_factor(const Matrix& A,Matrix& Q,Matrix &R);
  friend inline int QR_factor(const Matrix& A,Matrix& Q,Matrix &R,Indexmatrix& piv);


  //----- Least squares  
  inline int ls(Matrix & rhs, Real tol=1e-10);
  //{ return this->QR_solve(rhs,tol);}
  //least squares solution by QR_solve,
  //*this is factored and the solution replaces rhs
  
#ifdef WITH_SYMMATRIX
  int nnls(Matrix& rhs,Matrix* dual=0,Real tol=1e-10);
  //nonnegative least squares, if dual!=0, the dual variables are
  //are stored there. (*this) remains unchanged
#endif
    

  //---------------------------------------------
  //----  Comparisons / Max / Min / sort / find
  //---------------------------------------------

  friend Matrix operator<(const Matrix &A,const Matrix &B);
  friend inline Matrix operator>(const Matrix &A,const Matrix &B);
  friend Matrix operator<=(const Matrix &A,const Matrix &B);
  friend inline Matrix operator>=(const Matrix &A,const Matrix &B);
  friend Matrix operator==(const Matrix &A,const Matrix &B);
  friend Matrix operator!=(const Matrix &A,const Matrix &B);
  friend Matrix operator<(const Matrix &A,Real d);
  friend Matrix operator>(const Matrix &A,Real d);
  friend Matrix operator<=(const Matrix &A,Real d);
  friend Matrix operator>=(const Matrix &A,Real d);
  friend Matrix operator==(const Matrix &A,Real d);
  friend Matrix operator!=(const Matrix &A,Real d);
  friend inline Matrix operator<(Real d,const Matrix &A);
  friend inline Matrix operator>(Real d,const Matrix &A);
  friend inline Matrix operator<=(Real d,const Matrix &A);
  friend inline Matrix operator>=(Real d,const Matrix &A);
  friend inline Matrix operator==(Real d,const Matrix &A);
  friend inline Matrix operator!=(Real d,const Matrix &A);
    
  friend Matrix minrows(const Matrix& A);   //min of each column (over the rows)
  friend Matrix mincols(const Matrix& A);   //min of each row (over the columns)
  friend Real min(const Matrix& A,Integer *iindex=0,Integer *jindex=0);
  friend Matrix maxrows(const Matrix& A);   //similar
  friend Matrix maxcols(const Matrix& A);   //similar
  friend Real max(const Matrix& A,Integer *iindex=0,Integer *jindex=0);
  
  friend Indexmatrix sortindex(const Matrix& vec);
  //first element of Indexmatrix points to smallest value of A
  friend void sortindex(const Matrix& vec,Indexmatrix &ind);
  //first element of Indexmatrix points to smallest value of A
  
  Indexmatrix find(Real tol=1e-10) const;   //finds nonzeros
  Indexmatrix find_number(Real num=0.,Real tol=1e-10) const; 
  friend inline Indexmatrix find(const Matrix& A,Real tol=1e-10);
  friend inline Indexmatrix find_number(const Matrix& A,Real num=0.,Real tol=1e-10);


  //--------------------------------
  //----  Input / Output
  //--------------------------------

  void display(ostream& out,
	       int precision=0,int width=0,int screenwidth=0) const;
  //for variables of value zero default values are used
  //precision=4,width=precision+6,screenwidth=80
  

  friend ostream& operator<<(ostream& o,const Matrix &v);
  friend istream& operator>>(istream& i,Matrix &v);
  
};


// **************************************************************************
//                   implementation of inline functions
// **************************************************************************

inline void Matrix::init_to_zero()
{
 mtype=MTmatrix;
 nr=nc=0;mem_dim=0;m=0;
 chk_set_init(*this,1);
}

inline Matrix& Matrix::init(Integer inr,Integer inc,Real d)
{
 newsize(inr,inc);
 mat_xea(nr*nc,m,d);
 chk_set_init(*this,1);
 return *this;
}

inline Matrix& Matrix::init(Integer inr,Integer inc,const Real* d,Integer incr)
{
 newsize(inr,inc);
 if (incr==1) mat_xey(nr*nc,m,d);
 else mat_xey(nr*nc,m,Integer(1),d,incr);
 chk_set_init(*this,1);
 return *this;
}

inline Matrix& Matrix::init(const Matrix& A,Real d)
{
 return xeya(A,d);
}

inline Matrix& Matrix::init(const Indexmatrix& A,Real d)
{
 return xeya(A,d);
}

inline Matrix::Matrix()
{
 init_to_zero();
}

inline Matrix::Matrix(const Matrix &A,Real d)
{
 init_to_zero();
 xeya(A,d);
}

inline Matrix::Matrix(const Indexmatrix &A,Real d)
{
 init_to_zero();
 xeya(A,d);
}

inline Matrix::Matrix(Integer inr,Integer inc)
{
 init_to_zero();
 newsize(inr,inc);
}

inline Matrix::Matrix(const Realrange& range)
{
 init_to_zero();
 init(range);
}

inline Matrix::Matrix(Integer inr,Integer inc,Real d)
{
 init_to_zero();
 init(inr,inc,d);
}

inline Matrix::Matrix(Integer inr,Integer inc,const Real *d,Integer incr)
{
 init_to_zero();
 init(inr,inc,d,incr);
}

inline Real& Matrix::operator()(Integer i,Integer j)
{
 chk_range(i,j,nr,nc);
 return m[j*nr+i];
}

inline Real& Matrix::operator()(Integer i) 
{
 chk_range(i,0,nr*nc,1);
 return m[i];
}

inline Real Matrix::operator()(Integer i,Integer j) const
{
 chk_range(i,j,nr,nc);
 return m[j*nr+i];
}

inline Real Matrix::operator()(Integer i) const
{
 chk_range(i,0,nr*nc,1);
 return m[i];
}

inline Real& Matrix::operator[](Integer i)
{return (*this)(i);}

inline Real Matrix::operator[](Integer i) const 
{return (*this)(i);}

inline Matrix& Matrix::reduce_length(Integer n) 
{ nr=min(nr*nc,max(Integer(0),n)); nc=1; return *this;}


inline Real ip(const Matrix& A, const Matrix& B)
{
 chk_add(A,B);
 return mat_ip(A.nc*A.nr,A.m,B.m);
}

inline Real norm2(const Matrix& A)
{
 chk_init(A);
 return ::sqrt(mat_ip(A.nc*A.nr,A.m,A.m));
}

inline Matrix& Matrix::operator=(const Matrix &A)
{ return xeya(A);}

inline Matrix& Matrix::operator*=(const Matrix &A)
{ Matrix C; return xeya(genmult(*this,A,C));}

inline Matrix& Matrix::operator+=(const Matrix &A)
{ return xpeya(A); }

inline Matrix& Matrix::operator-=(const Matrix &A)
{ return xpeya(A,-1.); }

Matrix& Matrix::operator%=(const Matrix &A)
{ chk_add(*this,A); mat_xhadey(nr*nc,m,A.m); return *this; }

inline Matrix Matrix::operator-() const
{ return Matrix(*this,-1.); }

inline Matrix& Matrix::operator*=(register Real d)
{ chk_init(*this); mat_xmultea(nr*nc,m,d); return *this; }

inline Matrix& Matrix::operator/=(register Real d)
{ chk_init(*this); mat_xmultea(nr*nc,m,1./d); return *this; }

inline Matrix& Matrix::operator+=(register Real d)
{ chk_init(*this); mat_xpea(nr*nc,m,d); return *this; }

inline Matrix& Matrix::operator-=(register Real d)
{ chk_init(*this); mat_xpea(nr*nc,m,-d); return *this; }


inline Matrix operator*(const Matrix &A,const Matrix &B) 
    {Matrix C; return genmult(A,B,C);}
inline Matrix operator+(const Matrix &A,const Matrix &B)
    {Matrix C; return xeyapzb(C,A,B,1.,1.);}
inline Matrix operator-(const Matrix &A,const Matrix &B)
    {Matrix C; return xeyapzb(C,A,B,1.,-1.);}
inline Matrix operator%(const Matrix &A,const Matrix &B) 
    {Matrix C(A); return C%=B;}
inline Matrix operator*(const Matrix &A,Real d)          
    {return Matrix(A,d);}
inline Matrix operator*(Real d,const Matrix &A)
    {return Matrix(A,d);}
inline Matrix operator/(const Matrix &A,Real d)
    {return Matrix(A,1./d);}
inline Matrix operator+(const Matrix &A,Real d)           
    {Matrix B(A); return B+=d;}
inline Matrix operator+(Real d,const Matrix &A)           
    {Matrix B(A); return B+=d;}
inline Matrix operator-(const Matrix &A,Real d)       
    {Matrix B(A); return B-=d;}
inline Matrix operator-(Real d,const Matrix &A)       
    {Matrix B(A,-1.); return B+=d;}

#ifdef WITH_SYMMATRIX
    inline Matrix::Matrix(const Symmatrix &A,Real d)
        {init_to_zero(); xeya(A,d);}
    inline Matrix& Matrix::init(const Symmatrix &A,Real d)
        { return xeya(A,d); }
    inline Matrix& Matrix::operator=(const Symmatrix &A)
        { return xeya(A); }
    inline Matrix& Matrix::operator*=(const Symmatrix &A)
        { Matrix C; return *this=genmult(*this,A,C);}
    inline Matrix& Matrix::operator+=(const Symmatrix &A)
        { return xpeya(A); }
    inline Matrix& Matrix::operator-=(const Symmatrix &A)
        { return xpeya(A,-1.); }
#endif

#ifdef WITH_SPARSEMAT
    inline Matrix::Matrix(const Sparsemat& A, Real d)        
       {init_to_zero(); xeya(A,d);}
    inline Matrix& Matrix::init(const Sparsemat& A, Real d)  
       {return xeya(A,d);}
    inline Matrix& Matrix::operator=(const Sparsemat & A)       
       {return xeya(A);}
    inline Matrix& Matrix::operator*=(const Sparsemat &A)
        { Matrix C; return *this=genmult(*this,A,C);}
    inline Matrix& Matrix::operator+=(const Sparsemat &A)       
       {return xpeya(A);}
    inline Matrix& Matrix::operator-=(const Sparsemat &A)       
       {return xpeya(A,-1.);}
#endif

#ifdef WITH_SPARSESYM
    inline Matrix::Matrix(const Sparsesym& A,Real d)
       {init_to_zero(); xeya(A); }
    inline Matrix& Matrix::init(const Sparsesym& A,Real d)
       {return xeya(A); }
    inline Matrix& Matrix::operator=(const Sparsesym& A)
       {return xeya(A); }
    inline Matrix& Matrix::operator*=(const Sparsesym& A)
        { Matrix C; return *this=genmult(*this,A,C);}
    inline Matrix& Matrix::operator+=(const Sparsesym& A)
       {return xpeya(A); }
    inline Matrix& Matrix::operator-=(const Sparsesym& A)
       {return xpeya(A,-1.); }
#endif

inline int Matrix::QR_factor(Matrix& Q,Matrix& R) const  //*this is unchanged
    {R=*this;return R.QR_factor(Q);}
inline int Matrix::QR_factor(Matrix& Q,Matrix& R,Indexmatrix& piv) const
    {R=*this;return R.QR_factor(Q,piv);}
inline int Matrix::ls(Matrix & rhs, Real tol)
    { return this->QR_solve(rhs,tol);}

inline int QR_factor(const Matrix& A,Matrix& Q,Matrix &R)
          {return A.QR_factor(Q,R);}
inline int QR_factor(const Matrix& A,Matrix& Q,Matrix &R,Indexmatrix& piv)
          {return A.QR_factor(Q,R,piv);}

inline Matrix triu(const Matrix& A,Integer i)
          {Matrix B(A); B.triu(i); return B;}
inline Matrix tril(const Matrix& A,Integer i)
          {Matrix B(A); B.tril(i); return B;}

inline Matrix concat_right(const Matrix& A,const Matrix& B)
          {Matrix C(A.dim()+B.dim(),1);C=A;C.concat_right(B);return C;}
inline Matrix concat_below(const Matrix& A,const Matrix& B)
          {Matrix C(A.dim()+B.dim(),1);C=A;C.concat_below(B);return C;}
    
inline void swap(Matrix &A,Matrix &B)
{ 
 Real *hm=A.m;A.m=B.m;B.m=hm;
 Integer hi=A.nr;A.nr=B.nr;B.nr=hi;
 hi=A.nc;A.nc=B.nc;B.nc=hi;
 hi=A.mem_dim;A.mem_dim=B.mem_dim;B.mem_dim=hi;
#ifdef DEBUG
 hi=A.is_init;A.is_init=B.is_init;B.is_init=hi;
#endif
}

inline Matrix rand(Integer rows,Integer cols)
          {Matrix A; return A.rand(rows,cols);}
inline Matrix inv(const Matrix& A)
          {Matrix B(A); return B.inv();}
inline Matrix sqrt(const Matrix& A)
          {Matrix B(A); return B.sqrt();}
inline Matrix sign(const Matrix& A,Real tol)
          {Matrix B(A); return B.sign(tol);}
inline Matrix floor(const Matrix& A)
          {Matrix B(A); return B.floor();}
inline Matrix ceil(const Matrix& A)
          {Matrix B(A); return B.floor();}
inline Matrix rint(const Matrix& A)
          {Matrix B(A); return B.floor();}

inline Matrix operator>(const Matrix &A,const Matrix &B)
{return B<A;}
inline Matrix operator>=(const Matrix &A,const Matrix &B)
{return B<=A;}
inline Matrix operator<(Real d,const Matrix &A)
{return A>d;}
inline Matrix operator>(Real d,const Matrix &A)
{return A<d;}
inline Matrix operator<=(Real d,const Matrix &A)
{return A>=d;}
inline Matrix operator>=(Real d,const Matrix &A)
{return A<=d;}
inline Matrix operator==(Real d,const Matrix &A)
{return A==d;}
inline Matrix operator!=(Real d,const Matrix &A)
{return A!=d;}

inline Indexmatrix find(const Matrix& A,Real tol)
          {return A.find(tol);}
inline Indexmatrix find_number(const Matrix& A,Real num,Real tol)
          {return A.find_number(num,tol);}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: matrix.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:21  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/matrix.h,v $
 --------------------------------------------------------------------------- */
