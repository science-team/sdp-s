/* -*-c++-*-
 * 
 *    Source     $RCSfile: symmat.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:26 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************


#ifndef __SYMMATRIX_H__
#define __SYMMATRIX_H__

#include "matrix.h"

// **************************************************************************
//                            class definition
// **************************************************************************

class Symmatrix: protected Memarrayuser
{
    
    friend class Matrix;
#ifdef WITH_SPARSESYM
    friend class Sparsesym;
#endif
#ifdef WITH_SPARSEMAT
    friend class Sparsemat;
#endif
    
private:

    Mtype mtype;   //name of this type to recognize special kinds of matrices
    Integer mem_dim;   //amount of memory currently allocated
    Integer nr;        //number rows=number columns
    Real *m;     //lower triangle stored columnwise (a11,a21,...,anr1,a22,.....)

#ifdef DEBUG
    Integer is_init;      //flag whether memory is initialized
#endif

    inline void init_to_zero();            // (inline)
    
    //eigenvalue routines translated from fortran by f2c (eigval.cc)
    Integer tred2(Integer nm, Integer n, Real *a, Real *d, Real *e, Real *z) const;
    Integer imtql2(Integer nm, Integer n, Real *d, Real *e, Real *z) const;

public:

  //----------------------------------------------------
  //----  constructors, destructor, and initialization
  //----------------------------------------------------
  inline Symmatrix(); 
  inline Symmatrix(const Symmatrix& A,double d=1.);
  inline Symmatrix(const Matrix&,double d=1.);
  inline Symmatrix(const Indexmatrix&,double d=1.);
  inline Symmatrix(Integer nr);          
  inline Symmatrix(Integer nr,Real d);   
  inline Symmatrix(Integer nr,Real* dp); 
  inline ~Symmatrix();                   
  
#ifdef DEBUG
  void set_init(int i){is_init=i;}
  int get_init() const {return is_init;}
#endif

  inline Symmatrix& init(const Symmatrix& A,double d=1.);
  inline Symmatrix& init(const Matrix&,double d=1.);
  inline Symmatrix& init(const Indexmatrix&,double d=1.);
  inline Symmatrix& init(Integer nr,Real d);
  inline Symmatrix& init(Integer nr,Real* dp);
    
  void newsize(Integer n);      //resize matrix without Initialization
  

  //----------------------------------------------------
  //----  size and type information
  //----------------------------------------------------

  void dim(Integer& r, Integer& c) const {r=nr; c=nr;}
  Integer dim() const {return nr;}
  Integer rowdim() const {return nr;}
  Integer coldim() const {return nr;}
  
  Mtype get_mtype() const {return mtype;}
    

  //--------------------------------
  //----  Indexing and Submatrices
  //--------------------------------

  inline Real& operator()(Integer i,Integer j);
  inline Real operator()(Integer i,Integer j) const;
  inline Real& operator()(Integer i);
  inline Real operator()(Integer i) const;
  //returns values for vector of columns stacked on each other 
  
  Matrix col(Integer i) const;        //returns column i as column vector
  Matrix row(Integer i) const;        //returns row i as row vector
  
  friend Matrix diag(const Symmatrix& A);    //=(A(1,1),A(2,2),...)^t
  friend Symmatrix Diag(const Matrix& A);    //vec(A) on the diagonal
 
  Real* get_store() {return m;}   //use cautiously, do not use delete!
  const Real* get_store() const {return m;}   //use cautiously


  //------------------------------
  //----  BLAS-like Routines
  //------------------------------

  Symmatrix& xeya(const Symmatrix& A,Real d=1.);   //*this=d*A
  Symmatrix& xpeya(const Symmatrix& A,Real d=1.);  //*this+=d*A;
  Symmatrix& xeya(const Matrix& A,Real d=1.);   //*this=d*A
  Symmatrix& xpeya(const Matrix& A,Real d=1.);  //*this+=d*A;
  Symmatrix& xeya(const Indexmatrix& A,Real d=1.);   //*this=d*A
  Symmatrix& xpeya(const Indexmatrix& A,Real d=1.);  //*this+=d*A;

  friend Symmatrix& rankadd(const Matrix& A,Symmatrix& C,
			    Real alpha=1.,Real beta=0.,int trans=0);
      // returns C=beta*C+alpha* A*A^T, where A may be transposed
  friend Symmatrix& rank2add(const Matrix& A, const Matrix& B, Symmatrix& C,
			     Real alpha=1.,Real beta=0.,int trans=0);
      // returns C=beta*C+alpha* sym(A*B^T) [or sym(A^T*B)]
    
  Symmatrix& xetriu_yza(const Matrix& A,const Matrix& B,Real d=1.);
      //*this is the upper triangle of the matrix product A^T*B
  Symmatrix& xpetriu_yza(const Matrix& A,const Matrix& B,Real d=1.);

  friend Matrix& genmult(const Symmatrix& A,const Matrix& B,Matrix& C,
			 Real alpha=1., Real beta=0., int btrans=0);
      //returns C=beta*C+alpha*A*B, where B may be transposed
      //if beta==0 then C is initialized to the correct size

  friend Matrix& genmult(const Matrix& A,const Symmatrix& B,Matrix& C,
			 Real alpha=1., Real beta=0., int atrans=0);
      //returns C=beta*C+alpha*A*B, where B may be transposed
      //if beta==0 then C is initialized to the correct size


  //------------------------------
  //----  usual operators
  //------------------------------

  inline Symmatrix& operator=(const Symmatrix &A); 
  inline Symmatrix& operator+=(const Symmatrix &A);
  inline Symmatrix& operator-=(const Symmatrix &A);
  inline Symmatrix& operator%=(const Symmatrix &A);  //Hadamard product
  inline Symmatrix operator-() const; 
  
  inline Symmatrix& operator*=(Real d);
  inline Symmatrix& operator/=(Real d);
  inline Symmatrix& operator+=(Real d);
  inline Symmatrix& operator-=(Real d);

  Symmatrix& transpose(){return *this;} 

  friend inline Matrix operator*(const Symmatrix &A,const Symmatrix &B);
  friend inline Symmatrix operator%(const Symmatrix &A,const Symmatrix &B);
  friend inline Symmatrix operator+(const Symmatrix &A,const Symmatrix &B);
  friend inline Symmatrix operator-(const Symmatrix &A,const Symmatrix &B);
  friend inline Matrix operator*(const Symmatrix &A,const Matrix &B); 
  friend inline Matrix operator*(const Matrix &A,const Symmatrix &B);
  friend inline Matrix operator+(const Symmatrix &A,const Matrix &B);
  friend inline Matrix operator+(const Matrix &A,const Symmatrix &B);
  friend inline Matrix operator-(const Symmatrix &A,const Matrix &B);
  friend inline Matrix operator-(const Matrix &A,const Symmatrix &B);

  friend inline Symmatrix operator*(const Symmatrix &A,Real d);
  friend inline Symmatrix operator*(Real d,const Symmatrix &A);
  friend inline Symmatrix operator/(const Symmatrix &A,Real d);
  friend inline Symmatrix operator+(const Symmatrix &A,Real d);
  friend inline Symmatrix operator+(Real d,const Symmatrix &A);
  friend inline Symmatrix operator-(const Symmatrix &A,Real d);
  friend inline Symmatrix operator-(Real d,const Symmatrix &A);

  friend inline Symmatrix transpose(const Symmatrix& A);


  //------------------------------------------
  //----  Connections to other Matrix Classes
  //------------------------------------------
  
#ifdef WITH_SPARSEMAT
    friend Matrix& genmult(const Symmatrix& A,const Sparsemat& B,Matrix& C,
                           Real alpha=1.,Real beta=0., int btrans=0);
            //returns C=beta*C+alpha*A*B, where B may be transposed
            //if beta==0 then C is initialized to the correct size

    friend Matrix& genmult(const Sparsemat& A,const Symmatrix& B,Matrix& C,
                           Real alpha=1.,Real beta=0., int atrans=0);
            //returns C=beta*C+alpha*A*B, where B may be transposed
            //if beta==0 then C is initialized to the correct size

    friend Symmatrix& rankadd(const Sparsemat& A,Symmatrix& C,
                              Real alpha=1.,Real beta=0.,int trans=0);
    // returns C=beta*C+alpha* A*A^T, where A may be transposed
    
    friend Symmatrix& rank2add(const Sparsemat& A,const Matrix& B,Symmatrix& C,
                    Real alpha=1.,Real beta=0.,int trans=0);
    // returns C=beta*C+alpha* sym(A*B^T) [or sym(A^T*B)]

    Symmatrix& xetriu_yza(const Sparsemat& A,const Matrix& B,Real d=1.);
    //*this is the upper triangle of the matrix product A^T*B
    //Symmatrix& xetriu_yza(const Matrix& A,const Sparsemat& B,Real d=1.);
    Symmatrix& xpetriu_yza(const Sparsemat& A,const Matrix& B,Real d=1.);
    //Symmatrix& xpetriu_yza(const Matrix& A,const Sparsemat& B,Real d=1.);
#endif

#ifdef WITH_SPARSESYM
    Symmatrix& xeya(const Sparsesym& A,Real d=1.);   //*this=d*A
    Symmatrix& xpeya(const Sparsesym& A,Real d=1.);  //*this+=d*A;
    inline Symmatrix(const Sparsesym& A,Real d=1.); 
    inline Symmatrix& init(const Sparsesym& A,Real d=1.);
    inline Symmatrix& operator=(const Sparsesym& A);
    inline Symmatrix& operator+=(const Sparsesym& A);
    inline Symmatrix& operator-=(const Sparsesym& A);
#endif
    
  //------------------------------
  //----  Elementwise Operations
  //------------------------------

  friend Symmatrix abs(const Symmatrix& A);                 

  //----------------------------
  //----  Numerical Methods
  //----------------------------

  friend Real trace(const Symmatrix& A);                  //=sum(diag(A))
  friend Real ip(const Symmatrix& A, const Symmatrix& B); //=trace(B^t*A)
  friend Real ip(const Matrix& A, const Symmatrix& B);    //=trace(B^t*A)
  friend Real ip(const Symmatrix& A, const Matrix& B);    //=trace(B^t*A)
  friend inline Real norm2(const Symmatrix& A); //{return sqrt(ip(A,A));}

  friend Matrix sumrows(const Symmatrix& A); //=(1 1 1 ... 1)*A
  friend Matrix sumcols(const Symmatrix& A); //=A*(1 1 ... 1)^t
  friend Real sum(const Symmatrix& A);       //=(1 1 ... 1)*A*(1 1 ... 1)^t
  
  //---- svec and skron
  friend void svec(const Symmatrix& A,Matrix& sv); //stores svec in sv 
  friend inline Matrix svec(const Symmatrix& A);
  //=(A(11),sqrt(2)A(12),...A(22)..)^t
  friend void sveci(const Matrix& sv,Symmatrix& A);
  //invers operation to svec()
  
  friend inline Symmatrix skron(const Symmatrix& A,const Symmatrix& B);
  friend void skron(const Symmatrix& A,const Symmatrix& B,Symmatrix& S);

  //----- LDL Factorization (for positive definite matrices, no pivots so far!)
  int LDLfactor(Real tol=1e-10);      //stores factorization in *this
      //returns row+1 in which pivot is smaller than tol or 0 if pos. definite 
  int LDLsolve(Matrix& x) const;      //call LDLfactor before using LDLsolve!
  int LDLinverse(Symmatrix& S) const; //call LDLfactor before using LDLinverse!
    
  //----- Cholesky Factorization with pivoting
  int Chol_factor(Indexmatrix &piv, Real tol=1e-10); //stores fact. in *this
  int Chol_solve(Matrix& x,const Indexmatrix& piv) const; //call _factor before
  int Chol_inverse(Symmatrix& S,const Indexmatrix & piv) const; // --- " ---

  //----- Eigenvalue decomposition
  Integer eig(Matrix& P,Matrix& d) const;
  //if return value ==0 then P contains eigenvectors as columns
  //and d is a column vector containing the eigenvalues.

  //---------------------------------------------
  //----  Comparisons / Max / Min / sort / find
  //---------------------------------------------

  friend Matrix minrows(const Symmatrix& A); //minimum of each column (over rows)
  friend Matrix mincols(const Symmatrix& A); //minimum of each row (over colums)
  friend Real min(const Symmatrix& A);       //minimum of matrix
  friend Matrix maxrows(const Symmatrix& A); //similar
  friend Matrix maxcols(const Symmatrix& A); //similar
  friend Real max(const Symmatrix& A);       //similar


  //--------------------------------
  //----  Input / Output
  //--------------------------------


  void display(ostream& out,
	       int precision=0,int width=0,int screenwidth=0) const;
  //for variables of value zero default values are used
  //precision=4,width=precision+6,screenwidth=80

  friend ostream& operator<<(ostream& o,const Symmatrix &A);
  friend istream& operator>>(istream& i,Symmatrix &A);

};

Symmatrix Diag(const Matrix& A);


// **************************************************************************
//                    implemenation of inline functions
// **************************************************************************

inline void Symmatrix::init_to_zero()
{
 mtype=MTsymmetric;
 nr=0;mem_dim=0;m=0;
#ifdef DEBUG
 is_init=1;
#endif
} 

inline Symmatrix& Symmatrix::init(const Symmatrix& A,double d) 
{ return xeya(A,d); }

inline Symmatrix& Symmatrix::init(const Matrix& A,double d) 
{ return xeya(A,d); }

inline Symmatrix& Symmatrix::init(const Indexmatrix& A,double d) 
{ return xeya(A,d); }

inline Symmatrix& Symmatrix::init(Integer inr,Real d)
{
 newsize(inr);
 mat_xea((nr*(nr+1))/2,m,d);
 chk_set_init(*this,1);
 return *this;
}

inline Symmatrix& Symmatrix::init(Integer inr,Real* pd)
{
 newsize(inr);
 mat_xey((nr*(nr+1))/2,m,pd);
 chk_set_init(*this,1);
 return *this;
}

inline Symmatrix::Symmatrix() 
{ init_to_zero(); }
    
inline Symmatrix::Symmatrix(const Symmatrix& A,double d)
{init_to_zero(); xeya(A,d);}     

inline Symmatrix::Symmatrix(const Matrix &M,double d)
{ init_to_zero(); xeya(M,d); }

inline Symmatrix::Symmatrix(const Indexmatrix &M,double d)
{ init_to_zero(); xeya(M,d); }

inline Symmatrix::Symmatrix(Integer i)
{ init_to_zero(); newsize(i); }

inline Symmatrix::Symmatrix(Integer i,Real d)
{ init_to_zero(); init(i,d); }

inline Symmatrix::Symmatrix(Integer i,Real *pd)
{ init_to_zero(); init(i,pd); }

inline Symmatrix::~Symmatrix()
{memarray.free(m);}

inline Real& Symmatrix::operator()(Integer i,Integer j) 
{
 chk_range(i,j,nr,nr);
 if (j>=i) return m[((i*((nr<<1)-(i+1)))>>1)+j];
 return m[((j*((nr<<1)-(j+1)))>>1)+i];
}

inline Real Symmatrix::operator()(Integer i,Integer j) const
{
 chk_range(i,j,nr,nr);
 if (j>=i) return m[((i*((nr<<1)-(i+1)))>>1)+j];
 return m[((j*((nr<<1)-(j+1)))>>1)+i];
}

inline Real& Symmatrix::operator()(Integer i)
{
 chk_range(i,0,nr*nr,1);
 Integer j=i/nr;
 i=i%nr;
 if (j>=i) return m[((i*((nr<<1)-(i+1)))>>1)+j];
 return m[((j*((nr<<1)-(j+1)))>>1)+i];
}

inline Real Symmatrix::operator()(Integer i) const
{
 chk_range(i,0,nr*nr,1);
 Integer j=i/nr;
 i=i%nr;
 if (j>=i) return m[((i*((nr<<1)-(i+1)))>>1)+j];
 return m[((j*((nr<<1)-(j+1)))>>1)+i];
}

inline Symmatrix& Symmatrix::operator=(const Symmatrix &A)   
    {return xeya(A);}
inline Symmatrix& Symmatrix::operator+=(const Symmatrix &A) 
    {return xpeya(A);}
inline Symmatrix& Symmatrix::operator-=(const Symmatrix &A) 
    {return xpeya(A,-1.);}
inline Symmatrix& Symmatrix::operator%=(const Symmatrix &A)
    { chk_add(*this,A); mat_xhadey((nr*(nr+1))/2,m,A.m); return *this; } 
inline Symmatrix  Symmatrix::operator-() const    
    {return Symmatrix(*this,-1.);}

inline Symmatrix& Symmatrix::operator*=(Real d)
    {chk_init(*this); mat_xmultea(nr*(nr+1)/2,m,d); return *this;}
inline Symmatrix& Symmatrix::operator/=(Real d)
    {chk_init(*this); mat_xmultea(nr*(nr+1)/2,m,1./d); return *this;}
inline Symmatrix& Symmatrix::operator+=(Real d)
    {chk_init(*this); mat_xpea(nr*(nr+1)/2,m,d); return *this;}
inline Symmatrix& Symmatrix::operator-=(Real d)
    {chk_init(*this); mat_xpea(nr*(nr+1)/2,m,-d); return *this;}

inline Matrix operator*(const Symmatrix &A,const Symmatrix &B)
    {Matrix C;return genmult(Matrix(A),B,C);}
inline Symmatrix operator%(const Symmatrix &A,const Symmatrix &B) 
    {Symmatrix C(A);C%=B;return C;}
inline Symmatrix operator+(const Symmatrix &A,const Symmatrix &B) 
    {Symmatrix C(A);return C.xpeya(B);}
inline Symmatrix operator-(const Symmatrix &A,const Symmatrix &B)
    {Symmatrix C(A);return C.xpeya(B,-1.);}
inline Matrix operator*(const Symmatrix &A,const Matrix &B) 
    {Matrix C; return genmult(A,B,C);}
inline Matrix operator*(const Matrix &A,const Symmatrix &B)
    {Matrix C; return genmult(A,B,C);}
inline Matrix operator+(const Symmatrix &A,const Matrix &B)
    {Matrix C(A);return C.xpeya(B);}
inline Matrix operator+(const Matrix &A,const Symmatrix &B)
    {Matrix C(A);return C.xpeya(B);}
inline Matrix operator-(const Symmatrix &A,const Matrix &B)
    {Matrix C(A);return C.xpeya(B,-1);}
inline Matrix operator-(const Matrix &A,const Symmatrix &B)
    {Matrix C(A);return C.xpeya(B,-1);}

inline Symmatrix operator*(const Symmatrix& A,Real d)
    { return Symmatrix(A,d);}
inline Symmatrix operator*(Real d,const Symmatrix &A)
    { return Symmatrix(A,d);}
inline Symmatrix operator/(const Symmatrix& A,Real d)
    { return Symmatrix(A,1./d);}
inline Symmatrix operator+(const Symmatrix& A,Real d)        
    { Symmatrix B(A); return B+=d;}
inline Symmatrix operator+(Real d,const Symmatrix &A)
    {Symmatrix B(A); return B+=d;}
inline Symmatrix operator-(const Symmatrix& A,Real d)        
    { Symmatrix B(A); return B-=d;}
inline Symmatrix operator-(Real d,const Symmatrix &A)    
    { Symmatrix B(A,-1.);return B+=d;}

#ifdef WITH_SPARSESYM
inline Symmatrix::Symmatrix(const Sparsesym& A,Real d)
    { init_to_zero(); xeya(A,d);}
inline Symmatrix& Symmatrix::init(const Sparsesym& A,Real d)          
    { return xeya(A,d);}
inline Symmatrix& Symmatrix::operator=(const Sparsesym& A)                
    { return xeya(A); }
inline Symmatrix& Symmatrix::operator+=(const Sparsesym& A)               
    { return xpeya(A);}
inline Symmatrix& Symmatrix::operator-=(const Sparsesym& A)           
    { return xpeya(A,-1.);}
#endif
    

inline Matrix svec(const Symmatrix& A) //=(A(11),sqrt(2)A(12),...A(22)..)^t
    {Matrix sv;svec(A,sv);return sv;}
inline Symmatrix skron(const Symmatrix& A,const Symmatrix& B)
    {Symmatrix S;skron(A,B,S);return S;}

inline Real norm2(const Symmatrix& A)
    {return sqrt(ip(A,A));}
inline Symmatrix transpose(const Symmatrix& A)
    {return Symmatrix(A);}
#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: symmat.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:26  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:55  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/symmat.h,v $
 --------------------------------------------------------------------------- */
