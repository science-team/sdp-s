/* -*-c++-*-
 * 
 *    Source     $RCSfile: sparssym.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:25 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SPARSSYM_H__
#define __SPARSSYM_H__

#include "symmat.h"
#include "sparsmat.h"

class Sparsesym: protected Memarrayuser
{
    friend class Indexmatrix;
    friend class Matrix;
    friend class Symmatrix;
    friend class Sparsemat;
    
private:

    Mtype mtype;   //name of this type to recognize special kinds of matrices
    Integer nr;     //number rows, number columns

    //column by column representation of lower triangle
    Indexmatrix colinfo;  //k by 4 matrix, gives for nonzero columns
                          //index j of column, nr of nonzero elements with i>j,
                          //first element in colindex/colval and suppind.
                          //A special column is introduced for the diagonal.
                          //if diagonal elements exist, they are stored
                          //as the first column and the index is in this case <0
    Indexmatrix colindex; //gives the rowindex of the element at position i 
                          //(minus column index j for offdiagonal elements) 
                          //(sorted increasingly in each column)
    Matrix colval;        //gives the value    of the element at position i
    Indexmatrix suppind;  //index of an element with respect to the 
                          //principal submatrix spanned by the entire support
                          //(upper and lower triangle)
    Indexmatrix suppcol;  //the index of the support column in the original matrix
        
    Real tol;          //>0, if abs(number)<tol, then number is taken to be zero

#ifdef DEBUG
    Integer is_init;      //flag whether memory is initialized
#endif

    inline void init_to_zero();            // (inline), initialization to zero

    void update_support();     //removes zeros and updates suppind and suppcol

public:

  //----------------------------------------------------
  //----  constructors, destructor, and initialization
  //----------------------------------------------------

  inline Sparsesym();     
  inline Sparsesym(const Sparsesym&,Real d=1.); 
  inline Sparsesym(const Matrix&,Real d=1.);         
  inline Sparsesym(const Indexmatrix&,Real d=1.);    
  inline Sparsesym(Integer nr);          //zero-matrix of order nr
  inline Sparsesym(Integer nr,Integer nz,
		   const Integer *ini,const Integer *inj,const Real* va);
  inline Sparsesym(Integer nr,Integer nz,
		   const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va);
      //in the last two routines only one of the two corresponding 
      //offdiagonal elements should be given. If an element appears
      //more than once, the values are summed up.

#ifdef DEBUG
  void set_init(Integer i){is_init=i;}
  int get_init() const {return is_init;} 
#endif 

  inline Sparsesym& init(const Sparsesym&,Real d=1.);   
  inline Sparsesym& init(const Matrix&,Real d=1.);       
  inline Sparsesym& init(const Indexmatrix&,Real d=1.);  
  inline Sparsesym& init(Integer nr);     //zero-matrix of order nr
  Sparsesym& init(Integer nr,Integer nz,
		  const Integer *ini,const Integer *inj,const Real* va);
  Sparsesym& init(Integer nr,Integer nz,
		  const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va);
  Sparsesym& init_support(const Sparsesym& A,Real d=0.);
      //initialize to same support as A but with constant value d

  void set_tol(Real t){tol=t;}


  //----------------------------------------------------
  //----  size and type information
  //----------------------------------------------------

  void dim(Integer& r, Integer& c) const {r=nr; c=nr;}
  Integer dim() const {return nr*nr;}
  Integer rowdim() const {return nr;}
  Integer coldim() const {return nr;}
  Integer nonzeros() const {return colval.dim();}
  
  Mtype get_mtype() const {return mtype;}

    
  //--------------------------------
  //----  Indexing and Submatrices
  //--------------------------------

  const Indexmatrix& get_colinfo() const {return colinfo;}
  const Indexmatrix& get_colindex() const {return colindex;}
  const Matrix& get_colval() const {return colval;}
  const Indexmatrix& get_suppind() const {return suppind;}
  const Indexmatrix& get_suppcol() const {return suppcol;}

  void get_edge_rep(Indexmatrix& I, Indexmatrix& J, Matrix& val) const;
  int contains_support(const Sparsesym& A) const;

  int check_support(Integer i,Integer j) const;
      //returns 0 if (i,j) is not in the support, 1 otherwise
    
  Real operator()(Integer i,Integer j) const;
  Real operator()(Integer i) const;    //vector of stacked columns

  Real operator[](Integer i) const;    //vector of stacked columns

  friend Matrix diag(const Sparsesym& A);      //=(A(1,1),A(2,2),...)^t
  friend Sparsesym sparseDiag(const Matrix& A,Real tol=SPARSE_ZERO_TOL);

  friend void swap(Sparsesym& A, Sparsesym& B);
  

  //------------------------------
  //----  BLAS-like Routines
  //------------------------------

  Sparsesym& xeya(const Sparsesym& A,Real d=1.);
  //returns (*this)=A*d;
  Sparsesym& xeya(const Matrix& A,Real d=1.);
  //returns (*this)=A*d;
  Sparsesym& xeya(const Indexmatrix& A,Real d=1.);
  //returns (*this)=A*d;

  friend Sparsesym& xeyapzb(Sparsesym& x,const Sparsesym& y,const Sparsesym& z,Real alpha=1.,Real beta=1.);
  //returns x= alpha*y+beta*z
  //x is initialized to the correct size
  
  friend Sparsesym& support_rankadd(const Matrix& A,Sparsesym& C,
				    Real alpha=1.,Real beta=0.,int trans=0);
      // C=beta*C+alpha*AA^T (or A^TA) only on the current support of C

  friend Matrix& genmult(const Sparsesym& A,const Matrix& B,Matrix &C,
			 Real alpha=1.,Real beta=0., int btrans=0);
      //C=beta*C+alpha A B

  friend Matrix& genmult(const Matrix& A,const Sparsesym& B,Matrix &C,
			 Real alpha=1.,Real beta=0., int atrans=0);
      //C=beta*C+alpha A B
  

  //------------------------------
  //----  usual operators
  //------------------------------

  inline Sparsesym& operator=(const Sparsesym &A);
  inline Sparsesym& operator+=(const Sparsesym &v);
  inline Sparsesym& operator-=(const Sparsesym &v);
  inline Sparsesym operator-() const;
 
  inline Sparsesym& operator*=(Real d);
  inline Sparsesym& operator/=(Real d);
  
  friend inline Sparsesym operator+(const Sparsesym &A,const Sparsesym& B);
  friend inline Sparsesym operator-(const Sparsesym &A,const Sparsesym& B);
  friend inline Sparsesym operator*(const Sparsesym& A,Real d);
  friend inline Sparsesym operator*(Real d,const Sparsesym &A);
  friend inline Sparsesym operator/(const Sparsesym& A,Real d);
  
  inline Sparsesym& operator=(const Matrix &A);
  inline Sparsesym& operator=(const Indexmatrix &A);
  
  friend inline Matrix operator*(const Matrix& A,const Sparsesym& B);
  friend inline Matrix operator*(const Sparsesym& A,const Matrix& B);
  friend inline Matrix operator+(const Matrix& A,const Sparsesym& B);
  friend inline Matrix operator+(const Sparsesym& A,const Matrix& B);
  friend inline Matrix operator-(const Matrix& A,const Sparsesym& B);
  friend inline Matrix operator-(const Sparsesym& A,const Matrix& B);
  
  Sparsesym& transpose(){return *this;}            //transposes itself
  friend inline Sparsesym transpose(const Sparsesym& A);


  //------------------------------------------
  //----  Connections to other Matrix Classes
  //------------------------------------------
  
#ifdef WITH_SYMMATRIX
  Sparsesym& xeya(const Symmatrix& S,Real d=1.);
  inline Sparsesym(const Symmatrix&,Real d=1.);       
  inline Sparsesym& init(const Symmatrix&,Real d=1.); 
  inline Sparsesym& operator=(const Symmatrix &);
  inline Symmatrix operator+(const Symmatrix &A) const;
  inline Symmatrix operator-(const Symmatrix &A) const;   
  friend inline Symmatrix operator+(const Sparsesym &A,const Symmatrix &B);
  friend inline Symmatrix operator+(const Symmatrix &A,const Sparsesym &B);
  friend inline Symmatrix operator-(const Sparsesym& A,const Symmatrix &B);
  friend inline Symmatrix operator-(const Symmatrix &A,const Sparsesym &B);
  friend Real ip(const Symmatrix& A, const Sparsesym& B); //=trace(B^t*A)
  friend inline Real ip(const Sparsesym& A, const Symmatrix& B);
#endif

#ifdef WITH_SPARSEMAT
  Sparsesym& xeya(const Sparsemat& S,Real d=1.);
  friend Matrix& genmult(const Sparsesym& A,const Sparsemat& B,Matrix &C,
			 Real alpha=1.,Real beta=0.,int btrans=0);
  friend Matrix& genmult(const Sparsemat& A,const Sparsesym& B,Matrix &C,
			 Real alpha=1.,Real beta=0.,int atrans=0);
  inline Sparsesym(const Sparsemat&,Real d=1.);
  inline Sparsesym& init(const Sparsemat&,Real d=1.);    
  inline Sparsesym& operator=(const Sparsemat &);
  Sparsemat sparsemult(const Matrix& A) const;
  friend inline Matrix operator*(const Sparsesym& A,const Sparsemat& B);
  friend inline Matrix operator*(const Sparsemat& A,const Sparsesym& B);
#endif



  //------------------------------
  //----  Elementwise Operations
  //------------------------------

  friend Sparsesym abs(const Sparsesym& A);                 


  //----------------------------
  //----  Numerical Methods
  //----------------------------

  friend Real trace(const Sparsesym& A);               //=sum(diag(A))
  friend Real ip(const Sparsesym& A, const Sparsesym& B); //=trace(B^t*A)
  friend Real ip(const Matrix& A, const Sparsesym& B); //=trace(B^t*A)
  friend inline Real ip(const Sparsesym& A, const Matrix& B); 
  friend Real norm2(const Sparsesym& A); //=sqrt(ip(A,A))

  friend Matrix sumrows(const Sparsesym& A);   //=(1 1 1 ... 1)*A
  friend Matrix sumcols(const Sparsesym& A)    //=A*(1 1 1 ... 1)^t
        {Matrix s(sumrows(A)); return s.transpose();}
  friend Real sum(const Sparsesym& A);         //=(1 1 ... 1)*A*(1 1 ... 1)^t


  //---------------------------------------------
  //----  Comparisons / Max / Min / sort / find
  //---------------------------------------------

  friend int equal(const Sparsesym& A, const Sparsesym& B);
      //returns 1 if both matrices are identical, 0 otherwise

  //--------------------------------
  //----  Input / Output
  //--------------------------------

  void display(ostream& out,
	       int precision=0,int width=0,int screenwidth=0) const;
           //for variables of value zero default values are used
           //precision=4,width=precision+6,screenwidth=80

    friend ostream& operator<<(ostream& o,const Sparsesym &v);
    friend istream& operator>>(istream& i,Sparsesym &v);

};

Matrix diag(const Sparsesym& A);
Sparsesym sparseDiag(const Matrix& A,Real tol);
Real ip(const Sparsesym& A,const Sparsesym& B); //ajout 13/12/2007

// **************************************************************************
//                   implementation of inline functions
// **************************************************************************

inline void Sparsesym::init_to_zero()
{
 mtype=MTsparse;
 nr=0;
 tol=SPARSE_ZERO_TOL;
#ifdef DEBUG
 is_init=1;
#endif
}

inline Sparsesym& Sparsesym::init(const Sparsesym& A,Real d)
{ return xeya(A,d); }

inline Sparsesym& Sparsesym::init(const Matrix& A,Real d)
{ return xeya(A,d);}

inline Sparsesym& Sparsesym::init(const Indexmatrix& A,Real d)
{ return xeya(A,d);}

inline Sparsesym& Sparsesym::init(Integer r)
{
nr=r;
colinfo.init(0,0,Integer(0)); colindex.init(0,0,Integer(0)); colval.init(0,0,0.);
chk_set_init(*this,1);
return *this;
}

inline Sparsesym::Sparsesym()
{ init_to_zero(); chk_set_init(*this,1);}

inline Sparsesym::Sparsesym(const Sparsesym& A,Real d)
{ init_to_zero(); xeya(A,d);}
inline Sparsesym::Sparsesym(const Matrix& A,Real d)
{ init_to_zero(); xeya(A,d);}
inline Sparsesym::Sparsesym(const Indexmatrix& A,Real d)
{ init_to_zero(); xeya(A,d);}

inline Sparsesym::Sparsesym(Integer r)
{ init_to_zero(); init(r); }

inline Sparsesym::Sparsesym(Integer in_nr,Integer nz,
       const Integer *ini,const Integer *inj,const Real* va)
{ init_to_zero(); init(in_nr,nz,ini,inj,va);}

inline Sparsesym::Sparsesym(Integer in_nr,Integer nz,
              const Indexmatrix& ini,const Indexmatrix& inj, const Matrix& va)
{ init_to_zero(); init(in_nr,nz,ini,inj,va);}

inline Real Sparsesym::operator()(Integer i) const
{ return (*this)(i%nr,i/nr); }
inline Real Sparsesym::operator[](Integer i) const
{ return (*this)(i%nr,i/nr); }

inline Sparsesym& Sparsesym::operator=(const Sparsesym &A)
{ return xeya(A);} 
inline Sparsesym& Sparsesym::operator+=(const Sparsesym &A)
{ Sparsesym B; xeyapzb(B,*this,A); swap(*this,B); return *this;}
inline Sparsesym& Sparsesym::operator-=(const Sparsesym &A)
{ Sparsesym B; xeyapzb(B,*this,A,1.,-1.); swap(*this,B); return *this;}
inline Sparsesym& Sparsesym::operator*=(Real d)
{chk_init(*this); colval*=d; return *this;}
inline Sparsesym& Sparsesym::operator/=(Real d)
{chk_init(*this);colval/=d;return *this;}
inline Sparsesym Sparsesym::operator-() const
{ return Sparsesym(*this,-1.); }

    
inline Sparsesym& Sparsesym::operator=(const Matrix &A)
{ return xeya(A);}
inline Sparsesym& Sparsesym::operator=(const Indexmatrix &A)
{ return xeya(A);}

inline Sparsesym operator+(const Sparsesym& A,const Sparsesym& B) 
{Sparsesym C; return xeyapzb(C,A,B);}
inline Sparsesym operator-(const Sparsesym& A,const Sparsesym& B) 
{Sparsesym C; return xeyapzb(C,A,B,1.,-1.);}

inline Sparsesym operator*(const Sparsesym &A,Real d) 
    { return Sparsesym(A,d);}
inline Sparsesym operator*(Real d,const Sparsesym &A)
    { return Sparsesym(A,d);}
inline Sparsesym operator/(const Sparsesym &A,Real d)
    { return Sparsesym(A,1/d);}

inline Matrix operator*(const Sparsesym& A,const Matrix& B)
{ Matrix C; return genmult(A,B,C); }
inline Matrix operator*(const Matrix& A,const Sparsesym& B)
{ Matrix C; return genmult(A,B,C); }
inline Matrix operator+(const Matrix& A,const Sparsesym& B)
{ Matrix C(A); return C.xpeya(B);}
inline Matrix operator+(const Sparsesym& A,const Matrix& B)
{ Matrix C(B); return C.xpeya(A);}
inline Matrix operator-(const Matrix& A,const Sparsesym& B)
{ Matrix C(A); return C.xpeya(B,-1.);}
inline Matrix operator-(const Sparsesym& A,const Matrix& B)
{ Matrix C(B,-1.); return C.xpeya(A);}

inline Real ip(const Sparsesym& A, const Matrix& B)
    {return ip(B,A);}
inline Sparsesym transpose(const Sparsesym& A)
    {return Sparsesym(A);}

#ifdef WITH_SYMMATRIX
inline Sparsesym::Sparsesym(const Symmatrix& A,Real d)
    { init_to_zero(); xeya(A,d);}
inline Sparsesym& Sparsesym::init(const Symmatrix& A,Real d)
    { return xeya(A,d);}
inline Sparsesym& Sparsesym::operator=(const Symmatrix& A)
    { return xeya(A);}
inline Symmatrix Sparsesym::operator+(const Symmatrix &A) const
    {Symmatrix B(A); B+=*this; return B;}
inline Symmatrix Sparsesym::operator-(const Symmatrix &A) const
    {Symmatrix B(A); B-=*this; return B;}
inline Symmatrix operator+(const Sparsesym &A,const Symmatrix &B)
    {Symmatrix C(B);return C.xpeya(A);}
inline Symmatrix operator+(const Symmatrix &A,const Sparsesym &B)
    {Symmatrix C(A);return C.xpeya(B);}
inline Symmatrix operator-(const Sparsesym& A,const Symmatrix &B)
    {Symmatrix C(B,-1.);return C.xpeya(A);}
inline Symmatrix operator-(const Symmatrix &A,const Sparsesym &B)
    {Symmatrix C(A);return C.xpeya(B,-1);}
inline Real ip(const Sparsesym& A, const Symmatrix& B)
    { return ip(B,A);}    
#endif

#ifdef WITH_SPARSEMAT
inline Sparsesym::Sparsesym(const Sparsemat& A,Real d)
    { init_to_zero(); xeya(A,d);}
inline Sparsesym& Sparsesym::init(const Sparsemat& A,Real d)
    { return xeya(A,d);}
inline Sparsesym& Sparsesym::operator=(const Sparsemat& A)
    { return xeya(A);}
inline Matrix operator*(const Sparsesym& A,const Sparsemat& B)
    { Matrix C; return genmult(A,B,C);} 
inline Matrix operator*(const Sparsemat& A,const Sparsesym& B)
    { Matrix C; return genmult(A,B,C);} 
#endif


#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: sparssym.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:25  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/sparssym.h,v $
 --------------------------------------------------------------------------- */
