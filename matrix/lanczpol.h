/* -*-c++-*-
 * 
 *    Source     $RCSfile: lanczpol.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:20 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __LANCZPOL_H__
#define __LANCZPOL_H__

#include "matrix.h"
#include "lanczos.h"
#include "clock.h"

class Lanczpol:public Lanczos,protected Memarrayuser
{
private:
    int     ierr;        //error return code of dnlaso
    Integer maxop;       //upper bound on calls to op
    Integer maxiter;     //upper bound on lanczos iterations
    Integer maxguessiter;//upper bound on lanczos iterations to guess interval
    Integer guessmult;   //number of matrix vector mults to guess interval
    Integer choicencheb; //user's choice for number of block Chebychev iterations
                         //<0 -> automatic determination
    Integer nchebit;     //number of block Chebychev iterations
                         //within one iteration
    Integer choicenbmult;//user's choice for number of block multiplications
                         //<0 -> automatic determination, min 6
    Integer nblockmult;  //number of blockmultiplications in one iteration
    Integer nlanczvecs;  //number of columns of X carrying "useful" infromation 
    Integer retlanvecs;

    //--- global variables to allow reastarting
    Integer neigfound;  //number of eigenvalues known
                        //these are in the first neigfound columns of X
    Integer blocksz;    //working block of lanczos vectors
                        //X(:,neigfound:neigfound+blocksz-1)
    Integer iter;       //iteration counter
    Integer nmult;      //number of single vector multiplications with matrix

    Real errc;   //error accumulation
    Real eps;    //relative precision
    Real mcheps; //machine precision is computed in constructor
    Real maxval; //approximation of largest eigenvalue
    Real minval; //approximation of smallest eigenvalue
    Real polval; //The Chebychef polynomial will have this value at maxval
    
    Matrix X;
    Matrix C;
    Matrix d;
    Matrix e;
    Matrix u;
    Matrix v;
    Matrix w;
    Matrix minvec;      
    
    Lanczosmatrix* bigmatrix;
    
    int stop_above;    //1 if algorithm stops after upper bound is exceeded
    Real upper_bound;

    int do_out;        //flag whether iteration information should be displayed
    ostream* myout;    //everything is output to *myout, default: myout=&cout;
    Integer ncalls;    //counts number of calls to Lanczpol
  Integer mymaxj;

  Clock myclock;
  Microseconds time_mult;      //for each restart time spent in lanczosmult
  Microseconds time_mult_sum;  //sum over all restarts
  Microseconds time_iter;
  Microseconds time_sum;

    void error(const char *s){
        (*myout)<<"Lanczos Error: "<<s<<endl;
    }

    int guess_extremes(Integer nproposed);
    
    int dhua(Integer nreig,Integer nproposed, Integer maxmult);
    
    int bklanc(Integer neigfound,Integer blocksz,Integer s,
               const Matrix &d,Matrix &C,Matrix &X,
               Matrix &e,Matrix &u,Matrix &v);

    int bklanccheb(Integer neigfound,Integer blocksz,Integer s,
                    Matrix &C,Matrix &X,Matrix &e,Matrix &v);

    int pch(Integer q,Integer neigfound,Integer nreig,Integer nconv,
            Integer &blocksz,Integer &s,Integer iter,Integer sbs,const Matrix& d);

    int err(Integer neigfound,Integer blocksz,const Matrix& X,Matrix &e);

    int cnvtst(Integer neigfound,Integer blocksz,Real& errc,Real eps,
               const Matrix& d,const Matrix &e,Integer &nconv);
    
    int eigen(Integer neigfound,Integer blocksz,Integer sbsz,Matrix& C,
                   Matrix& d,Matrix& u,Matrix& v,Real& af);

    int sectn(Matrix& X,Integer neigfound,Integer blocksz,
              Matrix& C,Matrix& d,Matrix& u,Matrix& v,
              Real& af);

    int rotate_extremes(Integer neigfound,Integer sbs,Matrix& d,
                        const Matrix& C,Matrix &X,Matrix& v);
    
    int rotate(Integer neigfound,Integer sbs,Integer l,
               const Matrix& C,Matrix &X,Matrix& v);

    int random(Matrix& X,Integer j);

    Integer orthog(Integer offset,Integer blocksz,Matrix &X,Matrix& B);

    int blockcheby(Integer col_offset,const Matrix& X,Matrix& v);

    Real scalarcheby(Real xval);


    int tred2(Integer n,const Matrix& C,Matrix& u,Matrix& v,Matrix& Z);

    int tql2(Integer n,Matrix &u,Matrix &v, Matrix& Z);

public:
    Lanczpol();
    ~Lanczpol();

    void set_do_out(int o){do_out=o;}
    void set_myout(ostream& o){myout=&o;}

    int get_err() const{return ierr;}
    Integer get_iter() const{return iter;}
    Integer get_nmult() const{return nmult;}
    int get_lanczosvecs(Matrix& val,Matrix& vecs) const;
        //if compute was already run, the first neigfound+blocksz
        //values of d and columns of X are returned

    void set_mineig(Real ie){minval=ie;}
    void set_maxmult(Integer mop){maxop=mop;}
    void set_maxiter(Integer mi){maxiter=mi;}
    void set_relprec(Real relprec){eps=relprec;}  //relative precision
    void set_nchebit(Integer cheb){choicencheb=cheb;} //Chebychev iterations
    void set_nblockmult(Integer nb){choicenbmult=nb;}

    void enable_stop_above(Real ub){stop_above=1; upper_bound=ub;}
    void disable_stop_above(){stop_above=0;}
    
    void set_retlanvecs(Integer nl){retlanvecs=nl;} 
    //return at most this number of Lanczosvectors in get_lanczosvecs

    Real get_relprec(void){return eps;} //relative precision

    int compute(Lanczosmatrix* bigmat,Matrix& eigval,Matrix& eigvec,
                Integer nreig,Integer in_blocksz=0,Integer maxcol=0);

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: lanczpol.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:20  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:54  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/matrix/lanczpol.h,v $
 --------------------------------------------------------------------------- */
