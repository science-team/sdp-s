/* -*-c++-*-
 * 
 *    Source     $RCSfile: bigmat.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:34 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: bigmat.cc,v 1.1.1.1.2.1 2000/02/28 13:55:34 bzfhelmb Exp $"

#include <cstdlib>
#include "mymath.h"
#include "bigmat.h"
#include "sparssym.h"
#include "problem.h"

void Bigmatrix::error(const char *s) const
{
 cout<<"Error in Bigmatrix: "<<s<<endl;
 exit(1);
}

int Bigmatrix::provide_mat(Integer k)
{
 if (memmcp<k){
     const Coeffmat** hp;
     
     //Integer newmem=memarray.get(2*k*Integer(sizeof(Coeffmat *)),(char *)hp);
     //if (hp==0){newmem=memarray.get(k*Integer(sizeof(Coeffmat *)),(char *)hp);}
     //newmem/=Integer(sizeof(Coeffmat *));

     Integer newmem=max(2*memmcp,k);
     hp=new const Coeffmat*[newmem];
     if (hp==0) hp=new const Coeffmat*[newmem=k];
     if (hp==0) return 1;
     Integer i;
     for(i=0;i<n;i++) hp[i]=mcp[i];
     for(;i<newmem;i++) hp[i]=0;
     delete[] mcp;
     mcp=hp;
     memmcp=newmem;    
 }
 if (memmcv<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memmcv,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 2;
     Integer i;
     for(i=0;i<n;i++) hp[i]=mcv[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(mcv);
     mcv=hp;
     memmcv=newmem;    
 }
 return 0;
}

int Bigmatrix::provide_sparse(Integer k)
{
 if (mem_provide_init0(memarray,k,meminzp,inzp)) return 1;
 if (mem_provide_init0(memarray,k,mempmemjp,pmemjp)) return 1;
 if (mem_provide_init0(memarray,k,memjp,jp)) return 1;
 if (mem_provide_init0(memarray,k,mempmemvp,pmemvp)) return 1;
 if (mem_provide_init0(memarray,k,memvalp,valp)) return 1;
 if (k>nrowhash){
     nrowhash=max(k,2*nrowhash);
     delete[] rowhash;
     rowhash=new Indexhash[nrowhash];
     if (rowhash==0) return 1;
 }
 return 0;
}

int Bigmatrix::provide_column(Integer i,Integer k)
{
 if (mem_provide(memarray,k,inzp[i],pmemjp[i],jp[i])) return 1;
 if (mem_provide(memarray,k,inzp[i],pmemvp[i],valp[i])) return 1;
 return 0;
}

int Bigmatrix::add_ij(Integer addi,Integer addj,Real valij)
{
 if (addi>addj) swap(addi,addj);
 // try to find row j element in column i
 Integer *jpi=jp[addi];
 Integer j=inzp[addi];
 while((--j>=0)&&((*jpi++)!=addj));


 // if present add value and return
 if (j>=0){
     j=inzp[addi]-j-1;
     valp[addi][j]+=valij;
     return 0;
 }

 // provide enough memory and append new entry
 if (provide_column(addi,inzp[addi]+1)) return 1;
 jp[addi][inzp[addi]]=addj;
 valp[addi][inzp[addi]]=valij;
 inzp[addi]++;
 return 0;
}

Bigmatrix::Bigmatrix()
{
 dim=n=0;
 meminzp=0; inzp=0;
 mempmemjp=0; pmemjp=0;
 memjp=0; jp=0;
 mempmemvp=0; pmemvp=0;
 memvalp=0; valp=0;
 memmcp=0; mcp=0;
 memmcv=0; mcv=0;
 nrowhash=0; rowhash=0;
 
 problem=0;yp=0;
}

Bigmatrix::Bigmatrix(Problem& problemin,const Matrix& yin)
{
 dim=n=0;
 meminzp=0; inzp=0;
 mempmemjp=0; pmemjp=0;
 memjp=0; jp=0;
 mempmemvp=0; pmemvp=0;
 memvalp=0; valp=0;
 memmcp=0; mcp=0;
 memmcv=0; mcv=0;
 nrowhash=0; rowhash=0;
 use_dense=0;
 
 problem=0;yp=0;
 if (init(problemin,yin)) error("Bigmatrix::Bigmatrix(const Sparsesym&,...): init failed");
}

Bigmatrix::~Bigmatrix()
{
 delete[] mcp;
 delete[] rowhash;
 memarray.free(mcv);

 Integer i;
 if (jp){
     for(i=0;i<memjp;i++){
         memarray.free(jp[i]);
     }
 }
 if (valp){
     for(i=0;i<memvalp;i++) {
         memarray.free(valp[i]);
     }
 }
 memarray.free(inzp);
 memarray.free(pmemvp);
 memarray.free(jp);
 memarray.free(pmemvp);
 memarray.free(valp);
}

void Bigmatrix::clear()
{
 Integer i;
 if (inzp){
     for(i=0;i<dim;i++){
         inzp[i]=0;
     }
 }
 dim=n=nmult=0;
}

int Bigmatrix::init(Problem& problemin,const Matrix& yin, int dense,int with_C)
{
 if (yin.dim()!=problemin.nr_constraints()) error("Bigmatrix::init(...): yin.dim()!=problem.nr_constraints()");
 clear();

 //--- check for the dense case;
 if (dense) {
     dim=problemin.mat_dim();
     use_dense=1;
     if (with_C) problemin.get_C()->make_symmatrix(symrep);
     else symrep.init(dim,0.);
     for(Integer i=0;i<problemin.nr_constraints();i++){
         if (yin(i)==0.) continue;
         problemin(i)->addmeto(symrep,-yin(i));
     }
     return 0;
 }
 
 use_dense=0;

 //--- collect indices and values of sparse matrices, store non-sparse info

 Integer tmpdim=problemin.mat_dim();
 if (provide_sparse(tmpdim)) {
     cout<<"*** Bigmatrix::init(Problem&,const Matrix&): provide_sparse(";
     cout<<tmpdim<<" failed)"<<endl;
     return 1;
 }
 
 dim=tmpdim;         //order of the matrix
 di.init(dim,1,0.);  //will hold all diagonal entries
 n=0;                //counter for non-sparse constraints

 //--- cost matrix
 if (with_C){
   if (problemin.get_C()->sparse(collecti,collectj,collectval)){
     Integer* cip=collecti.get_store();
     Integer* cjp=collectj.get_store();
     Real* cvp=collectval.get_store();
     for(Integer k=collecti.dim();--k>=0;){
       if (*cip==*cjp){
	 di(*cip++)+=*cvp++;
	 cjp++;
       }
       else if (*cip<*cjp){
	 (rowhash[*cip++])[*cjp++]+=*cvp++;
       }
       else {
	 (rowhash[*cjp++])[*cip++]+=*cvp++;
       }
     }
   }
   else {
     provide_mat(n+1);
     mcp[n]=problemin.get_C();
     mcv[n]=1.;
     n++;
   }
 }

 //--- coefficient matrices
 for(Integer i=0;i<problemin.nr_constraints();i++){
     if (yin(i)==0.) continue;
     if (problemin(i)->sparse(collecti,collectj,collectval,-yin(i))){
         Integer* cip=collecti.get_store();
         Integer* cjp=collectj.get_store();
         Real* cvp=collectval.get_store();
         for(Integer k=collecti.dim();--k>=0;){
             if (*cip==*cjp){
                 di(*cip++)+=*cvp++;
                 cjp++;
             }
             else if (*cip<*cjp){
                 (rowhash[*cip++])[*cjp++]+=*cvp++;
             }
             else {
                 (rowhash[*cjp++])[*cip++]+=*cvp++;
             }
         }
     }
     else {
         provide_mat(n+1);
         mcp[n]=problemin(i);
         mcv[n]=-yin(i);
         n++;
     }
 }

 //--- copy values in rowhash to rows
 nz=0;
 {for(Integer i=0;i<dim;i++){
     Integer sz=rowhash[i].size();
     nz+=sz;
     provide_column(i,max(sz,Integer(1)));     
     Indexhash::iterator it;
     Indexhash::iterator del_it;
     Integer del=0;
     Integer *indexp=jp[i];
     Integer *nzp=&(inzp[i]);
     Real *vp=valp[i];
     for (it = rowhash[i].begin(); it != rowhash[i].end();){
          Real d=it->second;
          if (my_abs(d)<=1e-20){
              del_it=it;
              del++;
              it++;
              rowhash[i].erase(del_it);
              continue;
          }
          if (my_abs(d)>1e-20){
              *indexp++=it->first-i;
              *vp++=d;
              (*nzp)++;
          }
          it->second=0.;
          it++;
     }
     if (sz!=*nzp+del){
         cout<<"*** ERROR in Bigmatrix::init(): hash.erase() caused troubles!";
     }
     rowhash[i].resize(rowhash[i].size());
 }}

 if (nz>=dim*(dim+1)/4) {
     make_symmatrix(symrep);
     use_dense=1;
 }

 //--- for testing purposes
 problem=&problemin;
 yp=&yin;
 
 return 0;
}

Integer Bigmatrix::lanczosdim() const
{
 return dim;
}


Integer Bigmatrix::lanczosflops() const
{
  if (use_dense) return dim*dim;
  Integer flopcnt=4*nz;
  for(Integer i=0;i<n;i++){
     flopcnt+=mcp[i]->prodvec_flops();
  }
  return flopcnt;
}


int Bigmatrix::lanczosmult(const Matrix& A,Matrix& B) const
{
  if (use_dense) {
    genmult(symrep,A,B);
    nmult+=A.coldim();
    return 0;
  }
 
 register Integer i;
 register Integer j;
 register Integer nc=A.coldim();
  nmult+=nc;
 if (nc>=2){
     //--- compute A transposed and initialize Bt=di*At for diagonal elements di
     At.newsize(nc,dim);
     Bt.newsize(nc,dim);
     register const Real *ap;
     register const Real *ap2;
     register Real *abp;
     register Real *bp;
     for(i=0;i<nc;i++){
         abp=At.get_store()+i-nc;
         bp=Bt.get_store()+i-nc;
         ap=A.get_store()+dim*i;
         ap2=di.get_store();
         for(j=dim;--j>=0;){
             *(bp+=nc)=(*(abp+=nc)=(*ap++))*(*ap2++);
         }
     }
     chk_set_init(At,1);
     chk_set_init(Bt,1);
     
     //--- compute "Bt=bigm*At" for sparse offdiagonal part

     register Real d;
     register Real *bp2;
     abp=At.get_store()+dim*nc;
     register Real *bbp=Bt.get_store()+dim*nc;
     for(i=dim;--i>=0;){ //for each column of the Sparsesym Matrix
         abp-=nc;
         bbp-=nc;
         j=inzp[i];
         register Integer *hjp=jp[i]+j;
         register Real *mp=valp[i]+j;
         for(;--j>=0;){
             // offdiagonal entries only
             d=*(--mp);
             ap=abp+(*(--hjp))*nc;
             bp=bbp;
             ap2=abp;
             bp2=bbp+(*hjp)*nc;
             for(register Integer k=nc;--k>=0;) {
                 (*bp++)+=d*(*ap++);
                 (*bp2++)+=d*(*ap2++);
             }
         }
     }

     //--- store transposed Bt in B
     B.newsize(dim,nc);
     for(i=0;i<nc;i++){
         ap=Bt.get_store()+i-nc;
         bp=B.get_store()+dim*i;
         for(j=dim;--j>=0;){
             (*bp++)=*(ap+=nc);
         }
     }
     chk_set_init(B,1);
 }
 
 else { //only one column: matrix times vector
     //--- compute "B=bigm*A" for sparse part
     //diagonal part
     B.newsize(dim,1);
     register const Real *abp=A.get_store();
     register Real *bbp=B.get_store();
     register const Real *mp=di.get_store();
     for(i=dim;--i>=0;){
         (*bbp++)=(*abp++)*(*mp++);
     }
     chk_set_init(B,1);

     //offdiagonal part
     abp=A.get_store()+dim;
     bbp=B.get_store()+dim;
     for(i=dim;--i>=0;){ //for each column of the Sparsesym Matrix
         j=inzp[i];
         --abp;
         --bbp;
         register Integer *hjp=jp[i]+j;
         mp=valp[i]+j;
         register Real d;
         register Integer h;
         for(;--j>=0;){
             *(bbp)+=(d=*(--mp))*(*(abp+(h=*(--hjp))));
             *(bbp+h)+=d*(*(abp));
         }
     }
 }     

 //--- add non-sparse constraints
 for(i=0;i<n;i++){
     mcp[i]->addprodto(B,A,mcv[i]);
 }

 return 0;
}

int Bigmatrix::make_symmatrix(Symmatrix& S) const
{
 if (use_dense) {S=symrep; return 0;}
 S.init(dim,0.);
 Integer i,j;
 for(i=0;i<dim;i++){
     S(i,i)+=di(i);
     for(j=0;j<inzp[i];j++){
         S(i,jp[i][j]+i)+=valp[i][j];
     }
 }
 for(i=0;i<n;i++) mcp[i]->addmeto(S,mcv[i]);
 return 0;
}

ostream& operator<<(ostream& out,const Bigmatrix& M)
{
 Integer lnz=0,i,j;
 if (M.n==0){
     for(i=0;i<M.dim;i++){
         if (M.di(i)!=0.) lnz++;
         lnz+=M.inzp[i];
     } 
     out<<M.dim<<" "<<lnz<<"\n";out.precision(20);
     out.setf(ios::scientific|ios::left);
     for(i=0;i<M.dim;i++){
         if (M.di(i)!=0.)
             out<<' '<<i<<' '<<i<<' '<<M.di(i)<<'\n';
         for(j=0;j<M.inzp[i];j++){
             out<<' '<<i<<' '<<M.jp[i][j]+i<<' '<<M.valp[i][j]<<'\n';
         }
     }
 }
 else {
     static Symmatrix S;
     M.make_symmatrix(S);
     Real tol=1e-20;
     for(i=0;i<M.dim;i++){
         for(j=i;j<M.dim;j++){
             if (my_abs(S(i,j))>=tol) lnz++;
         }
     }
     out<<M.dim<<" "<<lnz<<"\n";out.precision(20);
     out.setf(ios::scientific|ios::left);
     for(i=0;i<M.dim;i++){
         for(j=i;j<M.dim;j++){
             if (my_abs(S(i,j))<tol) continue;
             out<<' '<<i<<' '<<j<<' '<<S(i,j)<<'\n';
         }
     }
 }
 return out;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: bigmat.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:34  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/bigmat.cc,v $
 --------------------------------------------------------------------------- */
