/* -*-c++-*-
 * 
 *    Source     $RCSfile: cmranksd.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __CMRANKSD_H__
#define __CMRANKSD_H__

//defines a base class for constraint matrices.
//the idea is to help exploiting special structures for
//the computation of tr(AiXAjZi) without having to know
//the special structure of both, Ai and Aj. 

#include "coeffmat.h"
#include "cmrankdd.h"

class CMlowranksd: public Coeffmat
{
private:
    //Symmatrix=A*B^T+B*A^T
    Sparsemat A;
    Matrix B;
public:
    CMlowranksd(const Sparsemat& Ain,const Matrix& Bin,Integer k=0)
    {A=Ain;B=Bin;CM_type=CM_lowranksd;userkey=k;}
    virtual ~CMlowranksd(){}

    //virtual CM_type get_type() const {return CM_type;} //no need to overload
    //virtual Integer get_userkey() const {return userkey;}
    //virtual void set_userkey(Integer k) const {userkey=k;}
    
    virtual Integer dim() const  { return A.rowdim(); }
    //returns the order of the represented symmetric matrix
    
    virtual Real operator()(Integer i,Integer j) const 
    { return ::ip(B.row(j),A.row(i))+ ::ip(B.row(i),A.row(j));}

    virtual void make_symmatrix(Symmatrix& S) const
    { rank2add(A,B,S,2.);}
    //return dense symmetric constraint matrix

    virtual Real norm(void) const
    { Matrix C,D; genmult(A,B,C,1.,0.,1); genmult(C,C,D);
      Real d=2.*trace(D); genmult(A,A,C,1.,0.,1); genmult(B,B,D,1.,0.,1);
      return sqrt(2.*::ip(C,D)+d);}
   //compute Frobenius norm of matrix

    virtual Coeffmat* subspace(const Matrix& P) const
    {Matrix C,D; genmult(P,A,C,1.,0.,1); genmult(P,B,D,1.,0.,1);
     return new CMlowrankdd(C,D,userkey); }
    //delivers a new object on the heap corresponding
    //to the matrix P^TAP, the caller is responsible for deleting the object
    
    virtual void multiply(Real d)
    { A*=d; }
    //multiply constraint permanentely by d
    //this is to allow scaling or sign changes in the constraints

    virtual Real ip(const Symmatrix& S) const
    { Matrix C; return 2.*::ip(genmult(S,A,C),B); }
    //=ip(*this,S)=trace(*this*S) trace inner product
    
    virtual Real gramip(const Matrix& P) const
    { Matrix C,D; genmult(P,A,C,1.,0.,1); genmult(P,B,D,1.,0.,1);
      return 2.*::ip(C,D); }
    //=ip(*this,PP^T)=trace P^T(*this)P

    virtual void addmeto(Symmatrix& S,Real d=1.) const
    { rank2add(A,B,S,2.*d,1.); }
    //S+=d*(*this);

    virtual void addprodto(Matrix& D,const Matrix&C ,Real d=1.) const
    {Matrix E; genmult(A,genmult(B,C,E,1.,0.,1),D,d,1.);
     genmult(B,genmult(A,C,E,1.,0.,1),D,d,1.);}
    //B+=d*(*this)*C

    virtual void addprodto(Matrix& D,const Sparsemat&C ,Real d=1.) const
    {Matrix E; genmult(A,genmult(B,C,E,1.,0.,1),D,d,1.);
     genmult(B,genmult(A,C,E,1.,0.,1),D,d,1.);}
    //B+=d*(*this)*C

    virtual Integer prodvec_flops() const 
    { return 4*A.nonzeros()+4*B.rowdim()*B.coldim(); }
    //return estimate of number of flops to compute addprodto for a vector

    virtual int dense() const
    {return 0;}
    //returns 1 if its structure as bad as its dense symmetric representation,
    //otherwise 1
    
    virtual int sparse() const
    { return 0;}
    //returns 0 if not sparse otherwise 1
    
    virtual int sparse(Indexmatrix& I,Indexmatrix& J,Matrix& val,Real d=1.)const
    {return 0;}
    //returns 0 if not sparse. If it is spars it returns 1 and
    //the nonzero structure in I,J and val, where val is multiplied by d.
    //Only the upper triangle (including diagonal) is delivered
    
    virtual int support_in(const Sparsesym& S) const
    {return 0;}
    //returns 0 if the support of the costraint matrix is not contained in the
    //support of the sparse symmetric matrix A, 1 if it is contained.

    virtual Real ip(const Sparsesym& S) const
    {return 2.*::ip(S*A,B);}
    //returns the inner product of the constraint matrix with A
    
    virtual void project(Symmatrix& S,const Matrix& P) const
    {Matrix C,D; genmult(P,A,C,1.,0.,1); genmult(P,B,D,1.,0.,1);
     rank2add(C,D,S,2.);}
    // S=P^t*(*this)*P

    virtual ostream& display(ostream& o) const 
    {o<<"CMlowranksd\n";A.display(o);B.display(o);return o;}
    //display constraint information
    
    virtual ostream& out(ostream& o) const
    {return o<<"LOWRANK_SPARSE_DENSE\n"<<A<<B;}
    //put entire contents onto outstream with the class type in the beginning so
    //that the derived class can be recognized.

    virtual istream& in(istream& i)
    {return i>>A>>B;}
    //counterpart to out, does not read the class type, though.
    //This is assumed to have been read in order to generate the correct class

    CMlowranksd(istream& is,Integer k=0)
    {CM_type=CM_lowranksd;userkey=k;in(is);}

};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: cmranksd.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/cmranksd.h,v $
 --------------------------------------------------------------------------- */
