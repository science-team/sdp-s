/* -*-c++-*-
 * 
 *    Source     $RCSfile: sdpqp.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:43 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: sdpqp.cc,v 1.1.1.1.2.1 2000/02/28 13:55:43 bzfhelmb Exp $"

#include "mymath.h"
#include "sdpqp.h"
#include <fstream>

int Sdpqp::error(const char* str) const 
{
 cerr<<"\n*** ERROR in Sdpqp: "<<str<<endl;
 return 1;
}

void Sdpqp::set_defaults()
{
 termeps=1e-7;
 maxiter=-1;
 init_size(0);
 out=0;
}

void Sdpqp::init_size(Integer maxdim)
{
 Integer d=maxdim;
 Integer d2=(d*(d+1))/2;
 Q.newsize(d2);Q.init(0,0.);
 C.newsize(d2,1);C.init(0,0,0.);
 a=0.;
 dcoeff=0.;
 svecI.newsize(d2,1); svecI.init(0,0,0.);
 X.newsize(d); X.init(0,0.);
 Z.newsize(d); Z.init(0,0.);
 dX.newsize(d); dX.init(0,0.);
 dZ.newsize(d); dZ.init(0,0.);
 dX2.newsize(d); dX2.init(0,0.);
 dZ2.newsize(d); dZ2.init(0,0.);
 Xi.newsize(d); Xi.init(0,0.);
 FZ.newsize(d2,1); FZ.init(0,0,0.);
 FZmuI.newsize(d2,1); FZmuI.init(0,0,0.);
 GiI.newsize(d2,1); GiI.init(0,0,0.);
 bigtmp.newsize(d2); bigtmp.init(0,0.);
 tmpsvec.newsize(d2,1); tmpsvec.init(0,0,0.);
 symtmp.newsize(d); symtmp.init(0,0.);
}

// *************************************************************************
//                             init_variables
// *************************************************************************

// computes a strictly feasible primal-dual starting point. 

void Sdpqp::init_variables()
{
 dim=Integer(sqrt(8.*Q.rowdim()+1)/2.);
 svecI=svec(Diag(Matrix(dim,1,1.)));
 X=Diag(Matrix(dim,1,a/Real(dim)));
 svec(X,tmpsvec);
 tmpsvec=Q*tmpsvec;
 tmpsvec+=C;
 sveci(tmpsvec,Z);
 u=max(sumrows(abs(Z)))+.1;
 u=-u;
 Z-=Diag(Matrix(dim,1,u));
 
 //intialize step sizes
 alphap=1.;
 alphad=1.;
 mu=1e20;

 /*
 cout<<"\n starting point:";
 cout<<"  st_FZ="<<norm2(Q*svec(X)+C-u*svecI-svec(Z));
 cout<<"  st_FP="<<my_abs(a-trace(X))<<endl;
 */
}

// *************************************************************************
//                             pd_line_search
// *************************************************************************

//performs line search for A+stepsize*dA with 0<=stepsize<=upper_bound.
//stepsize should be set to an initial good guess at input.
//tries to find maximal stepsize for which A+stepsize*dA is still positive
//parameters not changed: A,dA,upper_bound;
//parameters changed: stepsize
//globals changed: ldlcnt
//returns 1 if A+stepsize*dA is pos def for the whole interval else 0

int Sdpqp::pd_line_search(Symmatrix& S,Symmatrix& dS,Real& stepsize,Real upper_bound)
{
 int posdef;
 stepsize=max(stepsize,1e-6);
 symtmp.init(dS,stepsize); symtmp+=S;
 posdef=!symtmp.LDLfactor(tol);
 if(!posdef){
     do{
         stepsize*=.8;
         if (stepsize<1e-10) {stepsize=0;break;}
         symtmp.init(dS,stepsize); symtmp+=S;
     }while(symtmp.LDLfactor(tol));
 }
 else if(stepsize<upper_bound){
     do{
         stepsize=min(upper_bound,stepsize/.8);
         symtmp.init(dS,stepsize); symtmp+=S;
     }while((posdef=!symtmp.LDLfactor(tol))&&
            (stepsize<upper_bound-1e-10));
     if(!posdef) stepsize*=.8;
 }
 return posdef;
}

// *************************************************************************
//                             choose_mu
// *************************************************************************

// select the barrier parameter in dependence on previous step size

void Sdpqp::choose_mu()
{
 Real alpha;
 Real d=mu;
 if (mu==1e20) alpha=0.;
 else alpha=min(alphap,alphad);
 mu=ip(Z,X)/dim;
 if (alpha>.2) mu*=.5-.45*alpha*alpha;
 mu=min(mu,d);
 tol=min(1e-6*mu,1e-3/max(X));
}

// *************************************************************************
//                             search direction
// *************************************************************************

int Sdpqp::search_direction()
{
 //compute Xi
 bigtmp=X;
 if (bigtmp.LDLfactor(tol)){
     ofstream fout("sdpqpXIfail.dat");
     fout<<Matrix(X);
     fout.close();
     return error("search_direction(): LDLfactor for XI failed");
 }
 bigtmp.LDLinverse(Xi);

 //compute FZ
 svec(Z,tmpsvec);
 FZ.init(svecI,-u);
 FZ-=tmpsvec;
 svec(X,tmpsvec);
 FZ+=Q*tmpsvec;
 FZ+=C;
 
 //compute FZmuI
 svec(Xi,FZmuI);
 FZmuI*=mu;
 svec(Z,tmpsvec);
 FZmuI-=tmpsvec;
 FZmuI-=FZ;

 //compute FP
 FP=a-trace(X);

 //compute the Symmetric Kronnecker product and add other terms
 skron(Z,Xi,bigtmp);
 bigtmp+=Q;
 if (bigtmp.LDLfactor(0.01*tol))
     return error("search_direction: bigtmp.factor failed");

 //compute right hand side and solve for du, dX, and dZ
 tmpsvec=svecI;
 bigtmp.LDLsolve(tmpsvec);
 du=(-FP-ip(tmpsvec,FZmuI))/ip(svecI,tmpsvec);
 tmpsvec.init(svecI,du);
 tmpsvec+=FZmuI;
 bigtmp.LDLsolve(tmpsvec);
 sveci(tmpsvec,dX);
 sveci(Q*tmpsvec-du*svecI+FZ,dZ);

 /*
 cout<<"  FdZ="<<norm2(FZ+Q*svec(dX)-du*svecI-svec(dZ));
 cout<<"  Fds="<<my_abs(FP-trace(dX))<<flush;
 */
 return 0;
}

// *************************************************************************
//                             pred_corr
// *************************************************************************

int Sdpqp::pred_corr()
{
 //compute Xi
 bigtmp=X;
 if (bigtmp.LDLfactor(tol)){
     ofstream fout("sdpqpXIfail.dat");
     fout<<Matrix(X);
     fout.close();
     return error("search_direction(): LDLfactor for XI failed");
 }
 bigtmp.LDLinverse(Xi);

 //compute FZ
 svec(Z,tmpsvec);
 FZ.init(svecI,-u);
 FZ-=tmpsvec;
 svec(X,tmpsvec);
 FZ+=Q*tmpsvec;
 FZ+=C;
 
 //compute FZmuI
 svec(Z,FZmuI);
 FZmuI*=-1.;
 FZmuI-=FZ;

 //compute FP
 FP=a-trace(X);

 //compute the Symmetric Kronnecker product and add other terms
 skron(Z,Xi,bigtmp);
 bigtmp+=Q;
 if (bigtmp.LDLfactor(0.01*tol))
     return error("search_direction: bigtmp.factor failed");

 //compute right hand side and solve for predictor du, dX, and dZ
 GiI=svecI;
 bigtmp.LDLsolve(GiI);
 du=(-FP-ip(GiI,FZmuI))/ip(svecI,GiI);
 tmpsvec.init(svecI,du);
 tmpsvec+=FZmuI;
 bigtmp.LDLsolve(tmpsvec);
 sveci(tmpsvec,dX);
 sveci(Q*tmpsvec-du*svecI+FZ,dZ);
 //dZ=-Z-Symmatrix(Xi*(dX*Z));


 //cout<<"  FdZ1="<<norm2(FZ+Q*svec(dX)-du*svecI-svec(dZ));
 //cout<<"  Fds1="<<my_abs(FP-trace(dX))<<flush;

 //compute right hand side and solve for corrector
 svec(Xi,tmpsvec);
 tmpsvec*=mu;
 tmpsvec-=svec(Symmatrix(Xi*(dX*dZ)));
 du2=(-ip(GiI,tmpsvec))/ip(svecI,GiI);
 tmpsvec+=du2*svecI;
 bigtmp.LDLsolve(tmpsvec);
 sveci(tmpsvec,dX2);
 sveci(Q*tmpsvec-du2*svecI,dZ2);
 //svec(Xi,tmpsvec);
 //tmpsvec*=mu;
 //tmpsvec-=svec(Symmatrix(Xi*(dX*dZ)));
 //tmpsvec-=svec(Symmatrix(Xi*(dX2*Z)));
 //sveci(tmpsvec,dZ2);
 
 //cout<<" FdZ2="<<norm2(FZ+Q*svec(dX+dX2)-(du+du2)*svecI-svec(dZ+dZ2));
 //cout<<" Fds2="<<my_abs(FP-trace(dX+dX2))<<flush;

 dX+=dX2;
 du+=du2;
 dZ+=dZ2;
 
 return 0;
}

// *************************************************************************
//                        line_search_and_update
// *************************************************************************

void Sdpqp::line_search_and_update()
{
  //----- find primal steplength alphap
 alphap=min(1.,alphap*2.);
 int posdef=pd_line_search(X,dX,alphap,1.);
 if (alphap<1.) alphap*=.95;

 //----- find dual steplength alphad
 alphad=min(1.,alphad*2.);
 posdef=pd_line_search(Z,dZ,alphad,1.);
 if (alphad<1.) alphad*=.95;

 //----- update
 Real alpha=min(alphap,alphad);

 dX*=alpha; X += dX;  
 dZ*=alpha; Z += dZ;  
 du*=alpha; u += du;

}

// *************************************************************************
//                             iterate
// *************************************************************************

// loop till convergence to optimal solution

int Sdpqp::iterate()
{
 if (out){
     *out<<"    mi="<<maxiter;
     *out<<" te=";out->precision(3);*out<<termeps;
     *out<<" lb=";out->precision(8);out->width(9);*out<<lowerbound;
     *out<<" ub=";out->precision(8);out->width(9);*out<<upperbound;out->precision(2);*out<<endl;
 }
 svec(X,tmpsvec);
 dualval=-(primalval=ip(tmpsvec,Q*tmpsvec)/2);
 primalval+=ip(C,tmpsvec)+dcoeff;
 if (upperbound>=1e20) upperbound=primalval;
 dualval+=a*u+dcoeff;
 
 while(((maxiter<0)||(iter<maxiter))&&
       (primalval-lowerbound >1e-10*(my_abs(primalval)+1.))&&
       (upperbound-dualval>1e-10*(my_abs(dualval)+1.))&&
       (primalval-dualval>min(termeps*(primalval-lowerbound),.5*(upperbound-dualval)))
       ){
                                                        
     if (out){
         out->precision(2);
         *out<<"    ";out->width(2);*out<<iter<<":"<<" gapXZ=";out->width(7);*out<<ip(X,Z);
         *out<<" gappd=";out->width(7);*out<<primalval-dualval;
         *out<<" mu=";out->width(7);*out<<mu;
         *out<<" pv=";out->precision(8);out->width(10);*out<<primalval;
         *out<<" dv=";out->precision(8);out->width(10);*out<<dualval;
	 out->precision(2);
         *out<<" FZ=";out->width(7);*out<<norm2(Q*svec(X)+C-u*svecI-svec(Z));
         *out<<" FP=";out->width(7);*out<<my_abs(a-trace(X));
         *out<<" ap=";out->width(4);*out<<alphap<<" ad=";out->width(4);*out<<alphad;
         *out<<" epspl=";out->width(7);*out<<termeps*(primalval-lowerbound);
         *out<<" epsp=";out->width(7);*out<<eps_Real*(my_abs(primalval)+1.);
         *out<<" epsud=";out->width(7);*out<<.5*(upperbound-dualval)<<endl;
	 
     }     
     
     iter++;

     choose_mu();

     if (pred_corr()) {
         status=2;
         return status;
     }

     line_search_and_update();
     
     svec(X,tmpsvec);
     dualval=-(primalval=ip(tmpsvec,Q*tmpsvec)/2);
     primalval+=ip(C,tmpsvec)+dcoeff;
     dualval+=a*u+dcoeff;
 
 }

 if (out){
     out->precision(2);
     *out<<"\n    ";out->width(2);*out<<iter<<":"<<" gap=";out->width(7);*out<<ip(X,Z);
     *out<<" mu=";out->width(7);*out<<mu;
     *out<<" pv=";out->precision(8);out->width(10);*out<<primalval;
     *out<<" dv=";out->precision(8);out->width(10);*out<<dualval;out->precision(2);
     *out<<" au=";out->precision(8);out->width(10);*out<<a*u;out->precision(2);
     *out<<" FZ=";out->width(7);*out<<norm2(Q*svec(X)+C-u*svecI-svec(Z));
     *out<<" FP=";out->width(7);*out<<my_abs(a-trace(X));
     *out<<" ap=";out->width(4);*out<<alphap<<" ad=";out->width(4);*out<<alphad<<endl;
 }
 
 if ((maxiter<0)||(iter<maxiter)){
     status=0;
 }
 else {
     status=1;
 }

 return  status;
}

// *************************************************************************
//                             solve
// *************************************************************************

// call init_variables and loop till convergence to optimal solution

int Sdpqp::solve(const Symmatrix& Qin,const Matrix& Cin,Real ain,Real dco)
{
  //out=&cout;
 Q=Qin;
 C=Cin;
 a=ain;
 dcoeff=dco;
 init_variables();

 //(*out)<<Qin<<"\n";
 //(*out)<<Cin<<"\n";
 //(*out)<<ain<<"\n";
 //(*out)<<dco<<"\n";

 iter=0;

 status=iterate();

 return status;
}

// *************************************************************************
//                             update
// *************************************************************************

// check wether a problem has been solved already, if not return with error.
// Else compute a good starting point using the old solution
// and solve the new problem.

int Sdpqp::update(const Matrix& dC,Real dco)
{
 if (a<=0.) return -1;

 dcoeff+=dco;
 
 Real factor;
 factor=max(0.9,sqrt(1.-norm2(dC)/norm2(C)));
 factor=min(factor,.99999999);   //.99999
 if (out){
   *out<<" factor=";out->precision(10);*out<<factor<<flush;
 }
 
 svec(X,tmpsvec);
 Real oldtrace=trace(X);
 Real olddiff=a-oldtrace;

 // push X a little to the center
 X*=factor; X+=Diag(Matrix(dim,1,((1-factor)*oldtrace+olddiff)/Real(dim)));

 //compute the change on the dual side caused by change of X and dC
 tmpsvec*=factor-1.;
 tmpsvec+=(((1-factor)*oldtrace+olddiff)/Real(dim))*svecI;
 tmpsvec=Q*tmpsvec;
 tmpsvec+=dC;

 //make this change positive definite by means of a change in u
 sveci(tmpsvec,dZ);
 du=max(sumrows(abs(dZ)))+(1-factor)*.1;
 u-=du;
 dZ+=Diag(Matrix(dim,1,du));
 Z+=dZ;

 C+=dC;

 //*out<<"\n   starting point: factor="<<factor;
 //*out<<"  st_FZ="<<norm2(Q*svec(X)+C-u*svecI-svec(Z));
 //*out<<"  st_FP="<<my_abs(a-trace(X))<<" was "<<olddiff<<endl;

 alphap=1.;
 alphad=1.;
 mu=1e20;

 
 iter=0;
 
 status=iterate();

 return status;
}

// *************************************************************************
//                             compare
// *************************************************************************

void Sdpqp::compare(const Symmatrix& Qin,const Matrix& Cin,Real ain,Real dco)
{
 cout<<"\n   |Q|="<<norm2(Q-Qin)<<" |C|="<<norm2(C-Cin);
 cout<<" |a|="<<my_abs(a-ain)<<" |d|="<<my_abs(dcoeff-dco)<<endl;
}

// *************************************************************************
//                             save
// *************************************************************************

// write all variables to out in order to enable resuming interrupted sessions

ostream& Sdpqp::save(ostream& out) const
{
 out.precision(20);
 out<<dim<<"\n";    
 out<<Q<<C<<a<<"\n"<<dcoeff<<"\n"<<svecI;
 out<<X<<Z<<u<<"\n";
 out<<primalval<<"\n";
 out<<dualval<<"\n";
 out<<alphap<<"\n"; 
 out<<alphad<<"\n"; 
 out<<mu<<"\n";     
 out<<tol<<"\n";    
 out<<termeps<<"\n";
 out<<maxiter<<"\n";
 out<<lowerbound<<"\n";
 out<<upperbound<<"\n";
 out<<iter<<"\n";
 out<<status<<"\n";
 return out;
}

// *************************************************************************
//                             restore
// *************************************************************************

// read all variables from "in" in order to enable resuming interrupted sessions

istream& Sdpqp::restore(istream& in)
{
 in>>dim;    
 in>>Q>>C>>a>>dcoeff>>svecI;
 in>>X>>Z>>u;
 in>>primalval;
 in>>dualval;
 in>>alphap;  
 in>>alphad;  
 in>>mu;       
 in>>tol;       
 in>>termeps;   
 in>>maxiter;   
 in>>lowerbound;
 in>>upperbound;
 in>>iter;
 in>>status;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: sdpqp.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:43  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/sdpqp.cc,v $
 --------------------------------------------------------------------------- */
