/* -*-c++-*-
 * 
 *    Source     $RCSfile: myinctau.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __MYINCTAU_H__
#define __MYINCTAU_H__

#include "spectral.h"

class Myinctau: public SBchoosetau
{
    Integer itau;
    Real tau;
    Real epstau;        //helps in choosing tau
    Real taumin;
    Real taumax;
    Real oldmodelval;
    
    int tauchanged;    //1 if last choose_* call modified tau

    Matrix olddy;       //normalized vector of last serious step direction
    Matrix ip_history;  //stores ip(olddy,dy)  
    Integer iphist_size,
            iphist_i;   
    Matrix cutval_history;  //stores value of cutting plane model
    Integer cuthist_size,
        cuthist_i;   

    Real mR;            //parameter for reduction criterion in serious

    ostream* out;
    
public:
    Myinctau(Real mRin=.5,Integer iphist_size=5,Integer cuthist_size=5);
    ~Myinctau(){}
    
    SBchoose_val init(const Matrix& subgrad);
    //compute first tau and set some parameters
    
    SBchoose_val prob_changed(const Matrix& subgrad);
    //called if variables were added or deleted
    
    virtual void set_taumin(Real tm)
    //<=0 means no bound 
    { taumin=tm; if ((taumin>0)&&(taumax>0)&&(taumax<taumin)) taumax=taumin;}

    virtual void set_taumax(Real tm)
    //<=0 means no bound
    { taumax=tm; if ((taumin>0)&&(taumax>0)&&(taumin>taumax)) taumin=taumax;}

    Real get_tau() const;
    //returns current value of tau
    
    int tau_changed() const;
    //returns 1 if last call of choose_ modified current value of tau, else 0
    
    SBchoose_val choose_serious(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem);
    //determine next tau after a serious step
    
    SBchoose_val choose_nullstep(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem);
    //determine next tau after a null step
    
    virtual void set_out(ostream* inout){out=inout;}

    virtual ostream& save(ostream& out) const
     { 
        out<<itau<<"\n"<<tau<<"\n"<<epstau<<"\n"<<taumin<<"\n";
        out<<oldmodelval<<"\n"<<tauchanged<<"\n"<<olddy;
        out<<ip_history<<iphist_size<<"\n"<<iphist_i<<"\n";
        out<<cutval_history<<cuthist_size<<"\n"<<cuthist_i<<"\n"<<mR<<"\n";
        return out;
     }
    virtual istream& restore(istream& in)
     { 
        in>>itau>>tau>>epstau>>taumin;
        in>>oldmodelval>>tauchanged>>olddy;
        in>>ip_history>>iphist_size>>iphist_i;
        in>>cutval_history>>cuthist_size>>cuthist_i>>mR;
        return in;
     }
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: myinctau.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/myinctau.h,v $
 --------------------------------------------------------------------------- */
