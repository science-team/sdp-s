/* -*-c++-*-
 * 
 *    Source     $RCSfile: cmgramde.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:50 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __CMGRAMDE_H__
#define __CMGRAMDE_H__

//defines a base class for constraint matrices.
//the idea is to help exploiting special structures for
//the computation of tr(AiXAjZi) without having to know
//the special structure of both, Ai and Aj. 

#include "coeffmat.h"

class CMgramdense: public Coeffmat
{
private:
    Matrix A;  //Symmatrix=A*A^T
    bool positive;
public:
    CMgramdense(const Matrix& Ain,bool pos=true,Integer k=0)
    {A=Ain;positive=pos;CM_type=CM_gramdense;userkey=k;}
    virtual ~CMgramdense(){}

    //virtual CM_type get_type() const {return CM_type;} //no need to overload
    //virtual Integer get_userkey() const {return userkey;}
    //virtual void set_userkey(Integer k) const {userkey=k;}
    
    virtual Integer dim() const  { return A.rowdim(); }
    //returns the order of the represented symmetric matrix
    
    virtual Real operator()(Integer i,Integer j) const 
    {return mat_ip(A.coldim(),A.get_store()+i,A.rowdim(),A.get_store()+j,A.rowdim());}
    //returns the value of the matrix element (i,j)
    
    virtual void make_symmatrix(Symmatrix& S) const
    {if (positive) { rankadd(A,S);} else { rankadd(A,S,-1.);}}
    //return dense symmetric constraint matrix

    virtual Real norm(void) const
    {Symmatrix B; return norm2(rankadd(A,B,1.,0.,1));}
    //compute Frobenius norm of matrix

    virtual Coeffmat* subspace(const Matrix& P) const
    { Matrix B; genmult(P,A,B,1.,0.,1); return new CMgramdense(B,positive,userkey); }
    //delivers a new object on the heap corresponding
    //to the matrix P^TAP, the caller is responsible for deleting the object
    
    virtual void multiply(Real d)
    { if (d<0.) {A*=sqrt(-d); positive=!positive;} else {A*=sqrt(d);} }
    //multiply constraint permanentely by d
    //this is to allow scaling or sign changes in the constraints

    virtual Real ip(const Symmatrix& S) const
    {Matrix B; if (positive) return ::ip(genmult(S,A,B),A);
     else return -::ip(genmult(S,A,B),A); }
    //=ip(*this,S)=trace(*this*S) trace inner product
    
    virtual Real gramip(const Matrix& P) const
    {Matrix B; genmult(P,A,B,1.,0.,1);
    if (positive) return ::ip(B,B); else return -::ip(B,B);}
    //=ip(*this,PP^T)=trace P^T(*this)P

    virtual void addmeto(Symmatrix& S,Real d=1.) const
    {if (positive) rankadd(A,S,d,1.); else rankadd(A,S,-d,1.);}
    //S+=d*(*this);

    virtual void addprodto(Matrix& B,const Matrix&C ,Real d=1.) const
    {Matrix D;
     if (positive) genmult(A,genmult(A,C,D,1.,0.,1),B,d,1.);
     else genmult(A,genmult(A,C,D,1.,0.,1),B,-d,1.);}
    //B+=d*(*this)*C

    virtual void addprodto(Matrix& B,const Sparsemat&C ,Real d=1.) const
    {Matrix D;
     if (positive) genmult(A,genmult(A,C,D,1.,0.,1),B,d,1.);
     else genmult(A,genmult(A,C,D,1.,0.,1),B,-d,1.);}
    //B+=d*(*this)*C

    virtual Integer prodvec_flops() const 
    { return 4*A.rowdim()*A.coldim(); }
    //return estimate of number of flops to compute addprodto for a vector

    virtual int dense() const
    {return 0;}
    //returns 1 if its structure as bad as its dense symmetric representation,
    //otherwise 1
    
    virtual int sparse() const
    { return 0;}
    //returns 0 if not sparse otherwise 1
    
    virtual int sparse(Indexmatrix& I,Indexmatrix& J,Matrix& val,Real d=1.)const
    {return 0;}
    //returns 0 if not sparse. If it is spars it returns 1 and
    //the nonzero structure in I,J and val, where val is multiplied by d.
    //Only the upper triangle (including diagonal) is delivered
    
    virtual int support_in(const Sparsesym& S) const
    {return 0;}
    //returns 0 if the support of the costraint matrix is not contained in the
    //support of the sparse symmetric matrix A, 1 if it is contained.

    virtual Real ip(const Sparsesym& S) const
    {if (positive) return ::ip(A,S*A); else return -::ip(A,S*A);}
    //returns the inner product of the constraint matrix with A
    
    virtual void project(Symmatrix& S,const Matrix& P) const
    {Matrix B; genmult(P,A,B,1.,0.,1);
     if (positive) rankadd(B,S); else rankadd(B,S,-1.);
    }
    // S=P^t*A*P

    virtual ostream& display(ostream& o) const 
    {o<<"CMgramdense\n"; A.display(o); return o;}
    //display constraint information
    
    virtual ostream& out(ostream& o) const
    {return o<<"GRAM_DENSE\n"<<positive<<"\n"<<A<<endl;}
    //put entire contents onto outstream with the class type in the beginning so
    //that the derived class can be recognized.

    virtual istream& in(istream& i)
    {return i>>positive>>A;}
    //counterpart to out, does not read the class type, though.
    //This is assumed to have been read in order to generate the correct class

    CMgramdense(istream& is,Integer k=0)
    {CM_type=CM_gramdense;userkey=k;in(is);}

    //--- specific routines
    const Matrix& get_A() const {return A;}
    bool get_positive() const {return positive;}
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: cmgramde.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/cmgramde.h,v $
 --------------------------------------------------------------------------- */
