/* -*-c++-*-
 * 
 *    Source     $RCSfile: coeffmat.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:49 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __COEFFMAT_H__
#define __COEFFMAT_H__

//defines a base class for constraint matrices.
//the idea is to help exploiting special structures for
//the computation of tr(AiXAjZi) without having to know
//the special structure of both, Ai and Aj. 

#include "memarray.h"
#include "symmat.h"
#include "sparssym.h"

#define GEQ  '>'
#define LEQ  '<'
#define EQU  '='

enum Coeffmattype {
                  CM_unspec=0,    //any user defined constraint may use this
                  CM_symdense=1,
                  CM_symsparse=2,
                  CM_lowrankdd=3,
                  CM_lowranksd=4,
                  CM_lowrankss=5,
                  CM_gramdense=6,
                  CM_gramsparse=7,
                  CM_singleton=8
                 };

class Coeffmat: protected Memarrayuser
{
protected:
    Coeffmattype CM_type; //in order to enable type identification
    Integer userkey;      //for the user to specify additional information
 public:
    Coeffmat(){CM_type=CM_unspec;userkey=0;}
    virtual ~Coeffmat(){}

    virtual Coeffmattype get_type() const {return CM_type;}
    virtual Integer get_userkey() const {return userkey;}
    virtual void set_userkey(Integer k) {userkey=k;}

    virtual Integer dim() const =0;
    //returns the order of the represented symmetric matrix
    
    virtual Real operator()(Integer i,Integer j) const =0;
    //returns the value of the matrix element (i,j)
    
    virtual void make_symmatrix(Symmatrix& S) const =0;
    //return dense symmetric constraint matrix

    virtual Real norm(void) const =0;
    //compute Frobenius norm of matrix

    virtual Coeffmat* subspace(const Matrix& P) const =0;
    //delivers a new object on the heap corresponding
    //to the matrix P^TAP, the caller is responsible for deleting the object
    
    virtual void multiply(Real d) =0;
    //multiply constraint permanentely by d
    //this is to allow scaling or sign changes in the constraints

    virtual Real ip(const Symmatrix& S) const =0;
    //=ip(*this,S)=trace(*this*S) trace inner product
    
    virtual Real gramip(const Matrix& P) const =0;
    //=ip(*this,PP^T)=trace P^T(*this)P

    virtual void addmeto(Symmatrix& S,Real d=1.) const =0;
    //S+=d*(*this);

    virtual void addprodto(Matrix& A,const Matrix& B,Real d=1.) const =0;
    //A+=d*(*this)*B

    virtual void addprodto(Matrix& A,const Sparsemat& B,Real d=1.) const =0;
    //A+=d*(*this)*B

    virtual Integer prodvec_flops() const =0;
    //return estimate of number of flops to compute addprodto for a vector

    virtual int dense() const =0;
    //returns 1 if its structure as bad as its dense symmetric representation,
    //otherwise 1
    
    virtual int sparse() const =0;
    //returns 0 if not sparse, otherwise 1
    
    virtual int sparse(Indexmatrix& I,Indexmatrix& J,Matrix& val,Real d=1.)const=0;
    //returns 0 if not sparse. If it is sparse it returns 1 and
    //the nonzero structure in I,J and val, where val is multiplied by d.
    //Only the upper triangle (including diagonal) is delivered

    virtual int support_in(const Sparsesym& A) const =0;
    //returns 0 if the support of the costraint matrix is not contained in the
    //support of the sparse symmetric matrix A, 1 if it is contained.

    virtual Real ip(const Sparsesym& A) const =0;
    //returns the inner product of the constraint matrix with A
    
    virtual void project(Symmatrix& S,const Matrix& P) const=0;
    // S=P^t*A*P
    
    virtual ostream& display(ostream& o) const =0;
    //display constraint information

    virtual ostream& out(ostream& o) const =0;
    //put entire contents onto outstream with the class type in the beginning so
    //that the derived class can be recognized.
    
    virtual istream& in(istream& i) =0;
    //counterpart to out, does not read the class type, though.
    //This is assumed to have been read in order to generate the correct class

};

Coeffmat* coeffmat_read(istream& in);
//reads the next Coeffmat from in into an object on the heap and
//returns a pointer to it. The caller has to destruct the object.

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: coeffmat.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/coeffmat.h,v $
 --------------------------------------------------------------------------- */
