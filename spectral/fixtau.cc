/* -*-c++-*-
 * 
 *    Source     $RCSfile: fixtau.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: fixtau.cc,v 1.1.1.1 1999/12/15 13:47:52 bzfhelmb Exp $"

#include "fixtau.h"
#include "mymath.h"

SBchoose_val Fixtau::init(const Matrix& subgrad){return SBchoose_ok;}
//compute first tau and set some parameters

SBchoose_val Fixtau::prob_changed(const Matrix& subgrad){return SBchoose_ok;}
//called if variables were added or deleted

void Fixtau::set_tau(Real tauval) {tau=max(1e-10,tauval);}

Real Fixtau::get_tau() const {return tau;}
//returns current value of tau

int Fixtau::tau_changed() const {return 0;}
//returns 1 if last call of choose_ modified current value of tau, else 0

SBchoose_val Fixtau::choose_serious(Real newval,Real oldval,Real modelval,
                                    const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                                    const Matrix& last_subgrad, Real norm2subg,
                                    SpectralBundleProblem *problem)
{return SBchoose_ok;}
//determine next tau after a serious step

SBchoose_val Fixtau::choose_nullstep(Real newval,Real oldval,Real modelval,
                                     const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                                     const Matrix& last_subgrad, Real norm2subg,
                                     SpectralBundleProblem *problem)
{return SBchoose_ok;}
//determine next tau after a null step

void Fixtau::set_out(ostream* inout){out=inout;}

ostream& Fixtau::save(ostream& out) const
{
 out<<tau<<"\n";
 return out;
}

istream& Fixtau::restore(istream& in)
{
 in>>tau;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: fixtau.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/fixtau.cc,v $
 --------------------------------------------------------------------------- */
