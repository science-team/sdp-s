/* -*-c++-*-
 * 
 *    Source     $RCSfile: bmaxvecs.h,v $ 
 *    Version    $Revision: 1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:38 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __BMAXVECS_H__
#define __BMAXVECS_H__

#include "spectral.h"

class SBupdate_maxvecs: public SBupdate_bundle
{
/*   protected: */
/*     Integer maxkeepvecs; //max number of vectors kept in bundle from prev iteration */
/*     Integer minkeepvecs; //min number of vectors kept in bundle from prev iteration */
/*     Integer maxaddvecs;  //max number of new vectors added to old bundle */
/*     Integer rankdefcnt;  //counts number of rank deficiencies  */
/*                          //(if an eigenvector is already contained in the bundle) */
/*     Real aggregtol;      //aggregate primalvector if primalvalue smaller than that */
/*     Matrix tmpmat,tmpvec; */
/*     ostream *out; */
 
 public:
  SBupdate_maxvecs(){}
  //SBupdate_bundle(){maxkeepvecs=10;minkeepvecs=5;maxaddvecs=5;rankdefcnt=0;out=0;}
  ~SBupdate_maxvecs(){}
  //virtual void set_maxkeepvecs(Integer i){maxkeepvecs=i;}
  //virtual Integer get_maxkeepvecs() const{return maxkeepvecs;}
  //virtual void set_minkeepvecs(Integer i){minkeepvecs=i;}
  //virtual Integer get_minkeepvecs() const{return minkeepvecs;}
  //virtual void set_maxaddvecs(Integer i){maxaddvecs=i;}
  //virtual Integer get_maxaddvecs() const{return maxaddvecs;}
  //virtual void set_aggregtol(Real a){aggregtol=a;}
  //virtual Real get_aggregtol() const{return aggregtol;}
  //virtual void reset_rankdefcnt(){rankdefcnt=0;}
  //virtual Integer get_rankdefcnt() const{return rankdefcnt;}
  void update_bundle(SpectralBundle* sb,
		     SpectralBundleProblem *sbp,
		     Matrix& bundlevecs,
		     const Matrix& eigvecs,const Matrix& eigvals,
		     Matrix& primalvecs, Matrix& primaleigs,
                     Matrix& rayleighval, Real& s);
  //virtual void set_out(ostream* o){out=o;}
  //reduces primalvecs and primaleigs accordingly
  
  //virtual ostream& save(ostream& out) const;
  //virtual istream& restore(istream& in);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: bmaxvecs.h,v $
 *    Change log Revision 1.1.2.1  2000/02/28 13:55:38  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/Attic/bmaxvecs.h,v $
 --------------------------------------------------------------------------- */
