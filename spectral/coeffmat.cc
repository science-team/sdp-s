/* -*-c++-*-
 * 
 *    Source     $RCSfile: coeffmat.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: coeffmat.cc,v 1.1.1.1 1999/12/15 13:47:51 bzfhelmb Exp $"

#include <cstring>
#include <cstdlib>
#include "coeffmat.h"
#include "cmsymden.h"
#include "cmsymspa.h"
#include "cmgramde.h"
#include "cmgramsp.h"
#include "cmrankdd.h"
#include "cmranksd.h"
#include "cmrankss.h"
#include "cmsingle.h"


Coeffmat* coeffmat_read(istream& in)
{
 char name[80];
 in>>ws;
 if (!in) {
     cerr<<"*** ERROR: coeffmat_read(): input stream broken"<<endl;
     return 0;
 }
 if (!in.get(name,30)) {
     cerr<<"*** ERROR: coeffmat_read(): failed in reading name of constraint"<<endl;
     return 0;
 }
 Coeffmat* p=0;
 if (!strcmp(name,"SYMMETRIC_DENSE")) p=new CMsymdense(in);
 else if (!strcmp(name,"SYMMETRIC_SPARSE")) p=new CMsymsparse(in);
 else if (!strcmp(name,"GRAM_DENSE")) p=new CMgramdense(in);
 else if (!strcmp(name,"GRAM_SPARSE")) p=new CMgramsparse(in);
 else if (!strcmp(name,"LOWRANK_DENSE_DENSE")) p=new CMlowrankdd(in);
 else if (!strcmp(name,"LOWRANK_SPARSE_DENSE")) p=new CMlowranksd(in);
 else if (!strcmp(name,"LOWRANK_SPARSE_SPARSE")) p=new CMlowrankss(in);
 else if (!strcmp(name,"SINGLETON")) p=new CMsingleton(in);
 else {
     cerr<<"*** ERROR: coeffmat_read(): unknown constraint name :"<<name<<endl;
     in.clear(ios::failbit);
     return 0;
 }
 if (p==0) {
     cerr<<"*** ERROR: coeffmat_read():";
     cerr<<" failed in reading a constraint of type "<<name<<endl;
     in.clear(ios::failbit);
     return 0;
 }
 return p;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: coeffmat.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/coeffmat.cc,v $
 --------------------------------------------------------------------------- */
