/* -*-c++-*-
 * 
 *    Source     $RCSfile: spectral.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:44 $ 
 *    Author     Christoph Helmberg 
 *    Modified in 2002 by G.Delaporte, S.Jouteau, F.Roupin
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: spectral.cc,v 1.1.1.1.2.1 2000/02/28 13:55:44 bzfhelmb Exp $"

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include "spectral.h"
#include "problem1.h"
#include "mytau.h"
#include "coeffmat.h"
#include "mymath.h"
#include "clock.h"

// *****************************************************************************
//                               update_bundle
// *****************************************************************************

// First the bundlevectors are set to the vectors generated from the
// primal optimal solution (primalvecs). Vectors whose contribution
// is relatively small will be aggregated to keep the bundle small.
// If too many vectors are important, then the least important ones
// are aggregated to ensure that at most maxkeepvecs vectors remain.
// Afterwards at most maxaddvecs vectors obtained by the last
// function evaluation are added to the bundle to improve the model.
// Finally the vectors of the bundle are orthognoalized.
// input: eigvecs, primalvecs,primaleigs, s
// output: bundlevecs

void SBupdate_bundle::update_bundle(SpectralBundle* sb,
				    SpectralBundleProblem *problem,
                                    Matrix& bundlevecs,
          const Matrix& eigvecs, const Matrix& eigvals,
          Matrix& primalvecs, Matrix& primaleigs,Matrix& rayleighval, 
          Real& s)
{
 maxaddvecs=min(maxaddvecs,max(primalvecs.rowdim()/4,Integer(1)));
 maxkeepvecs=min(maxkeepvecs,primalvecs.rowdim()-maxaddvecs);
 Real aggregval=aggregtol*max(primaleigs);
 if (out){
     (*out)<<" ";
     (*out).setf(ios::showpoint);
 }
 Indexmatrix ind;
 Integer i;
 if ((primalvecs.coldim()>maxkeepvecs)||(min(primaleigs)<aggregval)){
         
     //fill ind with column indices that are to be aggregated
     ind=sortindex(primaleigs);
     i=max(Integer(0),primalvecs.coldim()-maxkeepvecs);
     while((i<ind.dim()-minkeepvecs)&&(primaleigs(ind(i))<aggregval)) i++;
     if (i<ind.dim()){
         ind.delete_rows(Range(i,ind.dim()-1));
     }
         
     //aggregate
     if (out) {(*out)<<" aggrdim="<<ind.dim();out->precision(4);}
     if (ind.dim()>0){
         Real su;
         if (ind.dim()<primalvecs.coldim()){  //only a subset of the columns
	   su=sum(primaleigs(ind));
	   if (problem->subgrad_is_available()&&(s>0.)){
             if (out) (*out)<<"  add subgrad ("<<(s+su)/problem->lmaxmult()<<") "<<flush;
             problem->update_subgrad(primalvecs.cols(ind),primaleigs(ind),
				     su/(su+s));
	   }
	   else {
             if (out) (*out)<<" init subgrad ("<<su/problem->lmaxmult()<<") "<<flush;
             problem->init_subgrad(primalvecs.cols(ind),primaleigs(ind));
	   }
	   primalvecs.delete_cols(ind);
	   primaleigs.delete_rows(ind);
	   rayleighval.delete_rows(ind);
	 }
         else { //aggregate all columns
	   su=sum(primaleigs);
	   if (problem->subgrad_is_available()&&(s>0.)){
             if (out) (*out)<<"  add subgrad ("<<(s+su)/problem->lmaxmult()<<") "<<flush;
             problem->update_subgrad(primalvecs,primaleigs,su/(su+s));
	   }
	   else {
             if (out) (*out)<<" init subgrad ("<<su/problem->lmaxmult()<<") "<<flush;
             problem->init_subgrad(primalvecs,primaleigs);
	   }
	   primalvecs.init(0,0,0.);
	   primaleigs.init(0,0,0.);
           rayleighval.init(0,0,0.);
	 }
         s+=su;
     }
 }
 else if(s==0.){
problem->clear_subgrad_available();
 }
 bundlevecs=primalvecs;
     
 //--- enlarge bundle with new information
 int nr_new=min(maxaddvecs,eigvecs.coldim());

 if (out) {
   if (bundlevecs.coldim()>0){
     tmpvec=eigvecs.col(0);
     tmpvec.transpose();
     tmpvec*=bundlevecs;
     tmpvec.transpose();
     (*out)<<" res="<<norm2(bundlevecs*tmpvec-eigvecs.col(0))<<endl;
   }
   else (*out)<<" res="<<1.<<endl;
   (*out)<<" ";
   for(i=0;i<nr_new;i++){
     out->precision(6);(*out)<<" "<<eigvals(i);
   }
   (*out)<<endl;
 }

 if (bundlevecs.coldim()==0){ //only new vectors
   bundlevecs.init(eigvecs.cols(Range(0,nr_new-1)));
   return;
 }

 //old and new vectors
 bundlevecs.concat_right(eigvecs.cols(Range(0,nr_new-1)));
 
 //--- orthogonalize bundlevecs
//     {
//         Integer i,k;
//         Integer n=bundlevecs.rowdim();
//         for(k=0;k<bundlevecs.coldim();k++){
//             Real *xk=bundlevecs.get_store()+k*n;
//             Real t,t1;
//             do{
//                 //compute projection of vector k on vector i and subtract this 
//                 t=0.;
//                 for(i=0;i<k;i++){
//                     Real *xi=bundlevecs.get_store()+i*n;
//                     t1=mat_ip(n,xi,xk);
//                     t+=t1*t1;
//                     mat_xpeya(n,xk,xi,-t1);
//                 }
//                 t1=mat_ip(n,xk,xk);
//                 t+=t1;
//             }while(t1<t/100.);
//             t1=sqrt(t1);
//             if (t1>1e-20) {
//                 //vector is not in the span, normalize it
//                 mat_xmultea(n,xk,1./t1);
//             }
//             else {
//                 //vector is in the span of previous vectors, delete it
//                 bundlevecs.delete_cols(Indexmatrix(1,1,k));
//                 k--;  //correct the value of k
//             }
//         }
//         primaleigs.init(bundlevecs.coldim(),1,1.);
//     }
     

 //if the old bundlevecs may be changed this is more stable
 tmpmat=bundlevecs;
 Indexmatrix piv;
 Integer r=tmpmat.QR_factor(piv);
 if (r<bundlevecs.coldim()) {
     if (out) {
         (*out)<<"\nWARNING: bundle update linearly dependent:";
         (*out)<<" cos="<<bundlevecs.coldim()<<" rank="<<r<<endl;
     }
     rankdefcnt++;
 }
 
 bundlevecs.init(tmpmat.rowdim(),r,0.);
 for(i=0;i<r;i++) bundlevecs(i,i)=1.;
 tmpmat.Q_times(bundlevecs,r);
}

// *****************************************************************************
//                             SBupdate_bundle::save
// *****************************************************************************

ostream& SBupdate_bundle::save(ostream& out) const
{
 out.precision(20);
 out<<maxkeepvecs<<"\n";
 out<<minkeepvecs<<"\n";
 out<<maxaddvecs<<"\n";
 out<<rankdefcnt<<"\n";
 out<<aggregtol<<"\n";
 return out;
}

// *****************************************************************************
//                             SBupdate_bundle::restore
// *****************************************************************************

istream& SBupdate_bundle::restore(istream& in)
{
 if (!(in>>maxkeepvecs)){
     cerr<<"*** ERROR:  SBupdate_bundle::restore(): failed in reading maxkeepvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>minkeepvecs)){
     cerr<<"*** ERROR:  SBupdate_bundle::restore(): failed in reading minkeepvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>maxaddvecs)){
     cerr<<"*** ERROR:  SBupdate_bundle::restore(): failed in reading maxaddvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>rankdefcnt)){
     cerr<<"*** ERROR:  SBupdate_bundle::restore(): failed in reading rankdefcnt"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>aggregtol)){
     cerr<<"*** ERROR:  SBupdate_bundle::restore(): failed in reading aggregtol"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 return in;
}

// *****************************************************************************
//                                 set_defaults
// *****************************************************************************

// usually called on construction

void SpectralBundle::set_defaults()
{
 clockp=&myclock;
 modeleps=.6;
 qpeps=.001;
 mL=0.1;   //was .1
 mN=0.1;
 mult_stepsize=1.;
 do_scaling=0;
 intpoint_maxiter=50;
 max_updates=-1;
 use_linval=1;

 if (terminator) delete terminator;
 terminator=new SBterminator(1e-5);
 if (choose_tau) delete choose_tau;
 choose_tau=new Mytau(.5,5,5);
 if (update_bundle) delete update_bundle;
 update_bundle=new SBupdate_bundle;
 
 init_size(0,0);

 innerit=0;
 suminnerit=0;
 cntobjeval=0;
 recomp=0;
 sumrecomp=0;
 qsdpfails=0;
 sumqsdpfails=0;
 augvalfails=0;
 sumaugvalfails=0;
 maxrank=0;
 clockp=0;
 
 time_coeff=Microseconds(long(0));
 time_solve=Microseconds(long(0));
 time_update=Microseconds(long(0));
 time_resolve=Microseconds(long(0));
 time_augmod=Microseconds(long(0));
 time_eval=Microseconds(long(0));
 time_bunupd=Microseconds(long(0));
 time_choosetau=Microseconds(long(0));
 time_last_augmod=Microseconds(long(0));
 time_last_eval=Microseconds(long(0));
 time_last_bunupd=Microseconds(long(0));
 time_last_choosetau=Microseconds(long(0));
 time_qsp=Microseconds(long(0));
 time_subg=Microseconds(long(0));
 time_lagupd=Microseconds(long(0));
 time_model=Microseconds(long(0));
 cntsolves=0;
 cntresolves=0;
 cntitersolves=0;
 cntiterresolves=0;
 calls=0;
 bens=0;
 sens=0;
 dens=0;
 shallowcut=0;
 sensvkappasum=0.;
 lower_bound=min_Real;

 remove_cuts=0;
}

// *****************************************************************************
//                                 init_size
// *****************************************************************************

// try to reserve enough memory so that no reallocation will occur
// n is the order of the matrix
// m is the number of constraints

void SpectralBundle::init_size(Integer n,Integer m)
{
 memkeepvecs=update_bundle->get_maxkeepvecs()+
             update_bundle->get_maxaddvecs();
 
 Integer r=memkeepvecs;
 Integer r2=(r*(r+1))/2;
 sdpqp.init_size(r);
 aggregqp.init_size(r);

 // 30 is the blocksize used in the eigenvalue computation 
 eigval.newsize(r,1);    eigval.init(0,0,0.);
 eigvecs.newsize(n,r);    eigvecs.init(0,0,0.);
 y.newsize(m,1); y.init(0,0,0.);
 newy.newsize(m,1); newy.init(0,0,0.);
 last_subgrad.newsize(m,1); last_subgrad.init(0,0,0.);
 tmpvec.newsize(m,1); tmpvec.init(0,0,0.);
 tmpmat.newsize(n,r); tmpmat.init(0,0,0.);
 bundlevecs.newsize(n,r); bundlevecs.init(0,0,0.);
 primaleigs.newsize(r,1); primaleigs.init(0,0,0.);
 primalvecs.newsize(n,r); primalvecs.init(0,0,0.);
 X.newsize(r); X.init(0,0.);
 Z.newsize(r); Z.init(0,0.);
 Q.newsize(r2); Q.init(0,0.);
 C.newsize(r2,1); C.init(0,0,0.);
 Q12.newsize(r2,1); Q12.init(0,0,0.);
 update_index.newsize(m,1); update_index.init(0,0,Integer(0));
 update_value.newsize(m,1); update_value.init(0,0,0.);
}


// *****************************************************************************
//                               quadratic_model
// *****************************************************************************

// compute cost coefficients for the quadratic model and solve from scratch.
// most data are already available in problem.
// called from: solve_model
// input: bundlevecs, tau, qpeps, and do_scaling.
// output: (X,s) and (Z,t) (primal and dual solution of the quad. SDP)
// returnvalue: status of the quad. SDP solution
//              (0 ok, 1 maxit exceeded, 2 some error)

#define NO_METHOD 0
#define NO_AGGREGATE 1
#define WITH_AGGREGATE 2

int SpectralBundle::quadratic_model(
        Symmatrix &X,Symmatrix &Z,Real& s,Real& t, int& last_method_used,
        const Matrix& bundlevecs, Real tau,
        int do_scaling)
{
 Microseconds secs;
 oldaugval=augval;
 if (!problem->subgrad_is_available()){  //without aggregate constraint
     if (clockp) {secs=clockp->time();}
     problem->compute_sdpqp_coeff(Q,C,a,dcoeff,bundlevecs,tau,do_scaling);
     tmpvec=problem->get_rhs();
     if (clockp) {
         time_coeff+=clockp->time()-secs;
         if (out) {
             (*out)<<"  ";print_time((*out),clockp->time()-secs);
         }
         secs=clockp->time();
     }
     sdpqp.set_out(0);
     sdpqp.set_termeps(qpeps);
     sdpqp.set_termbounds(-oldval,-oldaugval);
     sdpqp.set_maxiter(intpoint_maxiter);
     if (sdpqp.solve(Q,C,a,dcoeff)){
         cerr<<"\n**** ERROR in sdpqp.solve: "<<sdpqp.get_status()<<endl;
         if (out) (*out)<<"\n**** ERROR in sdpqp.solve: "<<sdpqp.get_status()<<endl;
     }
     last_method_used=NO_AGGREGATE;
     augval=-sdpqp.get_primalval();
     sdpqp.get_X(X);
     sdpqp.get_Z(Z);
     s=0.; t=1.;

     cntsolves++;
     cntitersolves+=sdpqp.get_iter();
     if (clockp) {
         time_solve+=clockp->time()-secs;
         if (out) {
             (*out)<<" ";print_time((*out),clockp->time()-secs);(*out)<<flush;
         }
     }
     if (out){
         (*out)<<" sdp="<<sdpqp.get_iter();
         (*out).setf(ios::scientific,ios::floatfield);
         out->precision(2);(*out)<<" gap="<<sdpqp.get_gap()<<endl;
#if __GNUC__ > 3 || \
	 	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
	 (*out).setf(ios_base::fmtflags(0),ios::floatfield|ios::showpoint);
#else
	 (*out).setf(0,ios::floatfield|ios::showpoint);
#endif
     }
     return sdpqp.get_status();
 }

 else { //with aggregate constraint
     if (clockp) {secs=clockp->time();}
     problem->compute_aggregqp_coeff(Q,Q12,q2,C,c2,a,dcoeff,bundlevecs,tau,do_scaling);
     tmpvec=problem->get_rhs();
     if (clockp) {
         time_coeff+=clockp->time()-secs;
         if (out){
             (*out)<<"  ";print_time((*out),clockp->time()-secs);
         }
         secs=clockp->time();
     }
     aggregqp.set_out(0);
     aggregqp.set_termeps(qpeps);
     aggregqp.set_termbounds(-oldval,-oldaugval);
     aggregqp.set_maxiter(intpoint_maxiter);
     if (aggregqp.solve(Q,Q12,q2,C,c2,a,dcoeff)){
         cerr<<"\n*** ERROR in aggregqp.solve: "<<aggregqp.get_status()<<endl;
         if (out) (*out)<<"\n*** ERROR in aggregqp.solve: "<<aggregqp.get_status()<<endl;
     }
     last_method_used=WITH_AGGREGATE;
     augval=-aggregqp.get_primalval();
     aggregqp.get_X(X);
     aggregqp.get_Z(Z);
     s=aggregqp.get_s();
     t=aggregqp.get_t();

     cntsolves++;
     cntitersolves+=aggregqp.get_iter();
     if (clockp) {
         time_solve+=clockp->time()-secs;
         if (out) {
             (*out)<<" ";print_time((*out),clockp->time()-secs);(*out)<<flush;
         }
     }
     if (out){
         (*out)<<" asdp="<<aggregqp.get_iter();
         (*out).setf(ios::scientific,ios::floatfield);
         (*out)<<" gap=";out->precision(2);(*out)<<aggregqp.get_gap()<<flush;
#if __GNUC__ > 3 || \
	 	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
	 (*out).setf(ios_base::fmtflags(0),ios::floatfield|ios::showpoint);
#else
	 (*out).setf(0,ios::floatfield|ios::showpoint);
#endif
         (*out)<<" alp=";out->precision(4);(*out)<<s/problem->lmaxmult();
         (*out)<<endl;
     }
     return aggregqp.get_status();
 }
}

// *****************************************************************************
//                               quadratic_restart
// *****************************************************************************

// update changed linear cost coefficients and solve by restarting from old sol.
// most data are already available in problem and solvers.
// called from: solve_model
// input: update_index, update_value, bundlevecs, tau, do_scaling
// output: (X,s) and (Z,t) (primal and dual solution of the quad. SDP)
// returnvalue: status of the quad. SDP solution
//              (0 ok, 1 maxit exceeded, 2 some error)

int SpectralBundle::quadratic_restart(
    Symmatrix &X,Symmatrix &Z,Real& s,Real& t, int& last_method_used,
    const Indexmatrix& update_index, const Matrix& update_value,
    const Matrix& bundlevecs, Real tau, int do_scaling)
{
 Microseconds secs;
 oldaugval=augval;
 if (!problem->subgrad_is_available()){  //without aggregate constraint
     if (last_method_used!=NO_AGGREGATE)
         return quadratic_model(X,Z,s,t,last_method_used,
                                bundlevecs,tau,do_scaling);
     if (clockp) {secs=clockp->time();}
     problem->update_sdpqp_coeff(C,dcoeff,bundlevecs,tau,update_index,update_value,do_scaling);
     if (clockp) {
         time_update+=clockp->time()-secs;
         if (out){
             (*out)<<"  ";print_time((*out),clockp->time()-secs);
         }
         secs=clockp->time();
     }
     sdpqp.set_termeps(qpeps);
     sdpqp.set_termbounds(-oldval,-oldaugval);
     sdpqp.set_maxiter(intpoint_maxiter);
     if (sdpqp.update(C,dcoeff)){
         cerr<<"\n**** ERROR in sdpqp.update: "<<sdpqp.get_status()<<endl;
         if (out) (*out)<<"\n**** ERROR in sdpqp.update: "<<sdpqp.get_status()<<endl;
     }
     augval=-sdpqp.get_primalval();
     sdpqp.get_X(X);
     sdpqp.get_Z(Z);
     s=0.; t=1.;

     cntresolves++;
     cntiterresolves+=sdpqp.get_iter();
     if (clockp) {
         time_resolve+=clockp->time()-secs;
         if (out) {
             (*out)<<" ";print_time((*out),clockp->time()-secs);(*out)<<flush;
         }
     }
     if (out) {
         (*out)<<" sdp="<<sdpqp.get_iter();
         (*out).setf(ios::scientific,ios::floatfield);
         (*out)<<" gap=";out->precision(2);(*out)<<sdpqp.get_gap()<<endl;
#if __GNUC__ > 3 || \
	 	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
	 (*out).setf(ios_base::fmtflags(0),ios::floatfield|ios::showpoint);
#else
	 (*out).setf(0, ios::floatfield|ios::showpoint);
#endif
     }
     return sdpqp.get_status();
 }
 else { //with aggregate constraint
     if (last_method_used!=WITH_AGGREGATE)
         return quadratic_model(X,Z,s,t,last_method_used,
                                bundlevecs,tau,do_scaling);
     if (clockp) {secs=clockp->time();}
     problem->update_aggregqp_coeff(C,c2,dcoeff,bundlevecs,tau,update_index,update_value,do_scaling);
     if (clockp) {
         time_update+=clockp->time()-secs;
         if (out){
             (*out)<<"  ";print_time((*out),clockp->time()-secs);
         }
         secs=clockp->time();
     }
     aggregqp.set_termeps(qpeps);
     aggregqp.set_termbounds(-oldval,-oldaugval);
     aggregqp.set_maxiter(intpoint_maxiter);
     if (aggregqp.update(C,c2,dcoeff)){
         cerr<<"\n**** ERROR in aggregqp.update: "<<aggregqp.get_status()<<endl;
         if (out) (*out)<<"\n**** ERROR in aggregqp.update: "<<aggregqp.get_status()<<endl;
     }
     augval=-aggregqp.get_primalval();
     aggregqp.get_X(X);
     aggregqp.get_Z(Z);
     s=aggregqp.get_s();
     t=aggregqp.get_t();

     cntresolves++;
     cntiterresolves+=aggregqp.get_iter();
     if (clockp) {
         time_resolve+=clockp->time()-secs;
         if (out){
             (*out)<<" ";print_time((*out),clockp->time()-secs);(*out)<<flush;
         }
     }
     if (out){
         (*out)<<" asdp="<<aggregqp.get_iter();
         (*out).setf(ios::scientific,ios::floatfield);
         (*out)<<" gap=";out->precision(2);(*out)<<aggregqp.get_gap()<<flush;
#if __GNUC__ > 3 || \
	 	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
	 (*out).setf(ios_base::fmtflags(0),ios::floatfield|ios::showpoint);
#else
	 (*out).setf(0, ios::floatfield|ios::showpoint);
#endif
         (*out)<<" alp=";out->precision(4);(*out)<<s/problem->lmaxmult();
         (*out)<<endl;
     }
     return aggregqp.get_status();
 }
}

// *****************************************************************************
//                              update_multipliers
// *****************************************************************************

// The Lagrange multipliers of sign constrained y-variables are updated
// and newy is computed such that it is feasible and
// such that complementarity to the Lagrange multipliers holds.
// Only non-zero changes of the lagrange multipliers are stored.
// input: last_subgrad (the old multipliers are stored in problem)
// output: newy, last_subgrad,
//         (new multipliers in problem) update_index, update_value

void SpectralBundle::update_multipliers(
    Matrix& newy, Matrix& last_subgrad,
    Indexmatrix& update_index, Matrix& update_value)
{
 update_index.newsize(problem->nr_constraints(),1); chk_set_init(update_index,1);
 update_value.newsize(problem->nr_constraints(),1); chk_set_init(update_value,1);
 Integer nrupd=0;
 nr_ineq=0;
 nr_lmult_act=0;
 nr_add=0;
 nr_del=0;
 Integer i;
 newy.init(last_subgrad,-1./tau);
 if (do_scaling) {
     problem->get_scale(tmpvec);
     tmpvec.inv();
     newy%=tmpvec;
 }
 newy+=y;
 for(i=0;i<problem->nr_constraints();i++){
     if (problem->sign(i)==LEQ){
         nr_ineq++;
         Real old_lmult=problem->lmult(i);
         if (old_lmult>0.) nr_lmult_act++;
         if (do_scaling) problem->lmult(i)=max(0.,-tau*problem->scale(i)*newy(i));
         else problem->lmult(i)=max(0.,-tau*newy(i));
         if (problem->lmult(i)>0.){
             newy(i)=0.;
             if (old_lmult<=0.) nr_add++;
         }
         else if (old_lmult>0.) nr_del++;
	 last_subgrad(i)-=problem->lmult(i);
	 update_value(nrupd)=problem->lmult(i)-old_lmult;
	 if (update_value(nrupd)!=0.){
	     update_index(nrupd)=i;
	     nrupd++;
	 }
     }
     else if (problem->sign(i)==GEQ){
         nr_ineq++;
         Real old_lmult=problem->lmult(i);
         if (old_lmult<0.) nr_lmult_act++;
         if (do_scaling) problem->lmult(i)=min(0.,-tau*problem->scale(i)*newy(i));
         else problem->lmult(i)=min(0.,-tau*newy(i));
         if (problem->lmult(i)<0.){
             newy(i)=0.;
             if (old_lmult>=0.) nr_add++;
         }
         else if (old_lmult<0.) nr_del++;
	 last_subgrad(i)-=problem->lmult(i);
	 update_value(nrupd)=problem->lmult(i)-old_lmult;
	 if (update_value(nrupd)!=0.){
	     update_index(nrupd)=i;
	     nrupd++;
	 }
     }
 }
 
 update_index.reduce_length(nrupd);
 update_value.reduce_length(nrupd);

 norm2subg=norm2(last_subgrad);
 if (do_scaling){
     tmpvec=newy-y;
     tmpvec%=tmpvec;
     problem->get_scale(tmpmat);
     tmpvec%=tmpmat;
     norm2dy=tau/2*sum(tmpvec);
 }
 else norm2dy=tau/2*sqr(norm2(newy-y));


 if (out){
     out->precision(4);(*out)<<"  n2(s)="<<norm2subg<<flush;
     out->precision(8);(*out)<<" n2dy="<<norm2dy<<endl;
     
     (*out)<<"  nr_ineq="<<nr_ineq<<" changed="<<nrupd;
     (*out)<<" nact="<<nr_lmult_act;
     (*out)<<" nadd="<<nr_add<<" ndel="<<nr_del;
 }
}

// *****************************************************************************
//                               delete_cuts
// *****************************************************************************

// deletes removable inequalities (generic cuts) with positive
// lagrange multipliers and updates y, newy, last_subgrad
// is sufficiently close to a feasible solution. The Lagrange multipliers
// are stored directly in the problem.
// input: primalvecs, primaleigs
// output: newy, last_subgrad, y
// returnvalue: number of cuts deleted

Integer SpectralBundle::delete_cuts(
    Matrix& newy, Matrix& last_subgrad, Matrix& y,
    const Matrix& primalvecs,const Matrix& primaleigs)
{
 if (problem->nr_constraints()-problem->get_perm()<=0) return 0;
 Indexmatrix rmind(y.dim()-problem->get_perm(),Integer(1),Integer(0));
 Integer i,nz=0;
 Real lmult_average=0.;
 for(i=problem->get_perm();i<problem->nr_constraints();i++){
     lmult_average+=my_abs(problem->lmult(i));
 }
 lmult_average/=(problem->nr_constraints()-problem->get_perm());
 Real threshold= lmult_average/4.;
 for(i=problem->get_perm();i<problem->nr_constraints();i++){
     if ((problem->get_del_stat(i)!=DEL_STAT_YES)||
         (my_abs(problem->lmult(i))<threshold)
         ) continue; 
     rmind(nz)=i;
     nz++;
 }
 if (nz>50) {
     rmind=rmind(Range(0,nz-1));
     problem->remove(rmind);
     newy.delete_rows(rmind);
     y.delete_rows(rmind);
     problem->opA(primalvecs,primaleigs,tmpvec);
     if (s>0.){
         problem->get_subgrad(last_subgrad);
         last_subgrad*=s;
         tmpvec+=last_subgrad;
     }
     problem->get_rhs(last_subgrad);
     last_subgrad-=tmpvec;
     last_subgrad-=problem->get_lmult();
     choose_tau->prob_changed(last_subgrad);
     norm2subg=norm2(last_subgrad);
     augval=-1e20;
 }
 return nz;
}

// *****************************************************************************
//                                 solve_model
// *****************************************************************************

// loops over model optimization and lagrange updates till model optimizer
// is sufficiently close to a feasible solution. The Lagrange multipliers
// are stored directly in the problem.
// input: y, bundlevecs, tau, oldval, do_scaling
// output: newy, y, linval, primalvecs, primaleigs, s,
//         last_subgrad

int SpectralBundle::solve_model(
    Matrix& newy, Real &linval,
    Matrix& primalvecs,Matrix& primaleigs, Real& s,
    Matrix& last_subgrad,
    Matrix& y,const Matrix& bundlevecs, Real tau, Real oldval,
    int do_scaling,int resume)
{
 Microseconds aug_secs;
 if (clockp) {aug_secs=clockp->time();}
     
 if (!resume){
     time_last_augmod=0;
     last_method_used=NO_METHOD;  //needed for restarting with different solvers
     updatecnt=0;
     retcode=0;

     //--- after a serious step or change of u try to make a good guess for lmult
     if ((innerit==1)||(choose_tau->tau_changed())){
         Integer i;
         for(i=0;i<problem->nr_constraints();i++){
             if (problem->sign(i)==LEQ){
                 Real old_lmult=problem->lmult(i);                  
                 Real hat_lmult;
		 if (do_scaling) hat_lmult=tau*problem->scale(i)*y(i)-last_subgrad(i)-old_lmult;
		 else hat_lmult=tau*y(i)-last_subgrad(i)-old_lmult;
                 problem->lmult(i)=mult_stepsize*max(0.,-hat_lmult)
                     +(1.-mult_stepsize)*old_lmult;
             }
             else if (problem->sign(i)==GEQ){
                 Real old_lmult=problem->lmult(i);
                 Real hat_lmult;
                 if(do_scaling) hat_lmult=tau*problem->scale(i)*y(i)-last_subgrad(i)-old_lmult;
                 else hat_lmult=tau*y(i)-last_subgrad(i)-old_lmult;
                 problem->lmult(i)=mult_stepsize*min(0.,-hat_lmult)
                     +(1.-mult_stepsize)*old_lmult;
             }
         }
     }
     
     problem_changed=0;  //will be set to 1 if problem is modified
     qsdpfails=0;            //is increased whenever the quadratic semidefinite program
                             //could not be solved to sufficient precision
 }
 else {
     //resume assumes continuing with a changed termination criterion
     //therefore we continue by checking termination criterion of solve_model_loop
     terminate=terminator->check_termination(this);
     if ((terminate)||(modeleps<0) || 
	 (!(augval>oldaugval+eps_Real*my_abs(oldaugval))) ||
         (cutval-linval<=modeleps*(oldval-linval))){ 
       if (clockp) {
	 time_last_augmod+=clockp->time()-aug_secs;
       }
       return retcode;
     }
 }

 //--- iterate till model is sufficiently precise
 do {
     
     updatecnt++;
     if (out){
         (*out)<<"  upd"<<updatecnt<<":";
     }

     //--- solve the quadratic model or update it
     Microseconds secs;
     if (clockp) {secs=clockp->time();}
     int errcode;
     if ((updatecnt==1)||(problem_changed)){ 
         errcode=quadratic_model(X,Z,s,t,last_method_used,bundlevecs,
                                 tau,do_scaling);
         problem_changed=0;
     }
     else
         errcode=quadratic_restart(X,Z,s,t,last_method_used,
                                   update_index,update_value,
                                   bundlevecs,tau,do_scaling);
     retcode=retcode || errcode;
     if (clockp) {time_qsp+=clockp->time()-secs;}
     if (errcode){
         cerr<<"*** WARNING SpectralBundle::solve_model(): solving QSDP failed "<<errcode<<endl;
         qsdpfails++;
         sumqsdpfails++;
     }
         
     //--- eigenvalue factorization of primal solution
     if (clockp) {secs=clockp->time();}
     Symmatrix Xtmp(X,-1.);
     Xtmp.eig(tmpmat,primaleigs);
     primaleigs*=-1.;
     genmult(bundlevecs,tmpmat,primalvecs); //primalvecs=bundlevecs*tmpat
         
     //--- compute last_subgrad
     problem->opA(primalvecs,primaleigs,tmpvec);
     if (s>0.){
         problem->get_subgrad(last_subgrad);
         last_subgrad*=s;
         tmpvec+=last_subgrad;
     }
     problem->get_rhs(last_subgrad);
     last_subgrad-=tmpvec;
     
     //--- compute <C,W^+>, this is constant for the rest
     Real model_const=problem->ip_C(primalvecs,primaleigs);
     if (s>0.) {
         model_const+=s*problem->get_subgrad_Cvalue();
     }
     if (clockp) {time_subg+=clockp->time()-secs;}

     //--- save phi(W^+,hat eta) only for output
     Real semiaugval=augval;

     //--- compute newy, update the multipliers and last_subgrad
     if (clockp) {secs=clockp->time();}
     update_multipliers(newy,last_subgrad,update_index,update_value);

     //--- after having added new cuts eliminate removable ineqs with
     //--- positive mutlipliers
     if (remove_cuts && update_index.dim()&&(updatecnt>=0)){
         problem_changed=delete_cuts(newy,last_subgrad,y,primalvecs,primaleigs);
     }
     if (clockp) {time_lagupd+=clockp->time()-secs;}

     //--- compute (the lower bound on) the model value
     if (clockp) {secs=clockp->time();}
     linval=ip(newy,last_subgrad)+model_const;

     //--- compute phi(W,eta) 
     augval=linval+norm2dy;

     //--- if there were changes in newy compute the true model value
     if (update_index.dim()){
         cutval=problem->model_compute(newy,primalvecs,rayleighval);
     }
     else {
         cutval=linval;
         rayleighval.init(primaleigs.rowdim(),1,
                          (linval-problem->objval(0,newy))/problem->lmaxmult());
     }
     if (out){
       (*out)<<" eigmod="<<(cutval-problem->objval(0,newy))/problem->lmaxmult();
     }
     if (clockp) {time_model+=clockp->time()-secs;}

     //--- output some information on current iteration
     if (out){
         out->precision(10);(*out)<<"\n ";
         if (update_index.dim()) {
             (*out)<<" L+1/2="<<semiaugval;
         }
         (*out)<<" L+1="<<augval;out->precision(3);
         out->precision(8);(*out)<<" flin="<<linval;
         if (update_index.dim()) {
             out->precision(8);(*out)<<" fhat="<<cutval;
             (*out)<<" (";out->precision(2);
             (*out)<<(cutval-linval)/max(oldval-linval,1e-16)<<")";
         }
         out->precision(6);(*out)<<endl;
     }

     //--- check for termination
     if (use_linval){
         modelval=linval;
     }
     else {
         modelval=cutval;
     }
     if (clockp) {
       secs=clockp->time();
       time_last_augmod+=secs-aug_secs; //necessary for correct resume
       aug_secs=secs;
     }
     terminate=terminator->check_termination(this);

     if (terminate) break;     

 } while ( (modeleps>0) &&
	   (augval>oldaugval+eps_Real*my_abs(oldaugval)) &&
           (cutval-linval>modeleps*(oldval-linval)) &&  //modelprecision
           ((max_updates<0)||(updatecnt<max_updates))
         );

 if (clockp) {
   time_last_augmod+=clockp->time()-aug_secs;
 }
 return retcode;
}



// *****************************************************************************
//                              constraints_changed
// *****************************************************************************

//this routine must be called before excuting inner_loop if the
//constraints to prob have been changed

int SpectralBundle::constraints_changed(SpectralBundleProblem& prob,int rem_cuts)
{
  if (primaleigs.dim()>0) {
    problem->opA(primalvecs,primaleigs,tmpvec);
  }
  else {
    tmpvec.init(problem->nr_constraints(),1,0.);
  }

  if (s>0.){
    problem->get_subgrad(last_subgrad);
    last_subgrad*=s;
    tmpvec+=last_subgrad;
  }
  problem->get_rhs(last_subgrad);
  last_subgrad-=tmpvec;
  last_subgrad-=problem->get_lmult();
  norm2subg=norm2(last_subgrad);
  newval=problem->objval();
  linval=newval-sqr(norm2subg)/choose_tau->get_tau();
  choose_tau->prob_changed(last_subgrad);
  return 0;
}

// *****************************************************************************
//                                 inner_loop
// *****************************************************************************

// performs null steps till a serious step is encountered

int SpectralBundle::inner_loop(SpectralBundleProblem& prob, int resume)
{
 problem=&prob;
 if (resume){
     terminate=0;
 }
 else {
     calls++;
     problem=&prob;
     problem->get_y(y);
     problem->get_eigval(eigval);
     problem->get_eigvecs(eigvecs);
     Integer n,m;
     eigvecs.dim(n,m);

     //initialize bundle if necessary
     if (bundlevecs.dim()==0){
         bundlevecs=eigvecs.cols(Range(0,min(min(Integer(3),eigvecs.coldim()),
                                             update_bundle->get_maxaddvecs())-1));
         primaleigs=Matrix(bundlevecs.coldim(),1,problem->lmaxmult()/Real(bundlevecs.coldim()));
         problem->opA(eigvecs.col(0),Matrix(1,1,problem->lmaxmult()),tmpvec);
         problem->get_rhs(last_subgrad);
         last_subgrad-=tmpvec;
         last_subgrad-=problem->get_lmult();
         norm2subg=norm2(last_subgrad);
         choose_tau->init(last_subgrad);
         choose_tau->set_out(out);
         if (do_scaling){
             tmpvec.init(problem->nr_constraints(),1,1.);
             problem->set_scale(tmpvec);
         }
         newval=problem->objval();
         linval=newval-sqr(norm2subg)/choose_tau->get_tau();
     }

     oldval=newval=problem->objval();
     augval=-1e20;
     innerit=0;
     recomp=0;
     terminate=0;
     augvalfails=0;
 }

 //--- compute null steps till a serious step is achieved
 do{
     if (!resume){
         innerit++;
         suminnerit++;
         
         tau=choose_tau->get_tau();
         
         maxrank=max(maxrank,bundlevecs.coldim());   //only needed for output
         if (out){
             (*out)<<" ir"<<innerit;
             if (clockp) {(*out)<<"("<<*clockp<<")";}
             (*out)<<": u=";out->precision(4);(*out)<<tau;
             (*out)<<" bundledim="<<bundlevecs.coldim()<<endl;
         }
	 lastaugval=augval;
     }

     //--- solve model and update the lagrange multipliers iteratively
     //--- till the model is sufficiently accurate
     solve_model(newy,linval,primalvecs,primaleigs,s,
                 last_subgrad,
                 y,bundlevecs,tau,oldval,do_scaling,resume);
     if (!(augval>lastaugval+eps_Real*my_abs(lastaugval))){
       augvalfails++;
       sumaugvalfails++;
     }
     if (clockp) {
       time_augmod+=time_last_augmod; //time_last_augmod set in solve_model
     }
     resume=0;

     if (terminate) {  //termination checked in solve_model
         break;
     }

     //---- determine nullstep_bound and descent_bound
     Real nullstep_bound;
     Real descent_bound;
     nullstep_bound=oldval-mN*(oldval-modelval);
     descent_bound=oldval-mL*(oldval-modelval);
     
     //--- evaluate function at the candidate newy
     Microseconds secs;
     if (clockp) {secs=clockp->time();}
     Real ip_rhs_newy=problem->objval(0.,newy);
     Real absoldeig=my_abs(eigval(0));
     Real eigval_bound=(nullstep_bound-ip_rhs_newy)/
         problem->lmaxmult();
     Real relprec=min(1e-3,.1*(oldval-nullstep_bound)/problem->lmaxmult()/
                      (absoldeig+1.));
     Integer nreig=0;
     eigvecs=bundlevecs;
     int status=problem->obj_compute(newy,eigval,eigvecs,
                                     eigval_bound,relprec,nreig);
     cntobjeval++;
     if (status) {
         if (out) (*out)<<"WARNING: problem->obj_compute(..) returned: "<<status<<endl;
     }

     //--- handle the precision problem for eigmax and newval
     newval=problem->lmaxmult()*eigval(0)+ip_rhs_newy;
     Real vkappa;
     vkappa=(oldval-newval)/(oldval-modelval);
     if ((nreig>0)&&(newval<=descent_bound)){
         Real guessval=problem->lmaxmult()*
             (eigval(0)+relprec*(my_abs(eigval(0))+1.))+
             ip_rhs_newy;
         if(guessval>descent_bound){
             if ((vkappa>.5)||(oldval-newval>.8*(oldval-cutval))) {
                 if (out){
                     (*out)<<"WARNING: relative precision enforces null step,";
                     out->precision(3);(*out)<<" vkappa="<<vkappa;
                     (*out)<<" cutratio="<<(oldval-newval)/(oldval-cutval)<<endl;
                 }
                 dens++;
             }
             else if (3.*vkappa>4.*mN) {
                 sens++;
                 sensvkappasum+=vkappa;
             }
             else bens++;
             nreig=0;
         }
         else {
             newval=guessval;
             eigval(0)+=relprec*(my_abs(eigval(0))+1.);
         }
     }
     else {
         if (oldval-newval>.8*(oldval-cutval)){
             if (out){
                 (*out)<<"  shallow cut (";out->precision(3);
                 (*out)<<(oldval-newval>.8*(oldval-cutval))<<")";
             }
             shallowcut++;
         }
     }
     if (clockp) {time_last_eval=clockp->time()-secs;time_eval+=time_last_eval;}

     if (out){
         (*out)<<"  nreval="<<cntobjeval;
         out->precision(8);(*out)<<" ov="<<oldval;
         out->precision(8);(*out)<<" nv="<<newval;
         if (use_linval) {
             out->precision(8);(*out)<<" flin=";
         }
         else {
             out->precision(8);(*out)<<" fhat=";
         }
         (*out)<<modelval;
         out->precision(2);(*out)<<" vkappa="<<vkappa;
         out->precision(4);(*out)<<endl;
     }
     
     //--- reduce and aggregate bundle
     if (clockp) {secs=clockp->time();}
     update_bundle->update_bundle(this,problem,bundlevecs,eigvecs,eigval,primalvecs,primaleigs,rayleighval,s);
     if (clockp) {
       time_last_bunupd=clockp->time()-secs;
       time_bunupd+=time_last_bunupd;
     }

     //--- decide on serious or null step
     if ((nreig>0)&&(newval<=descent_bound)){
         //descent step (serious step)
         if (clockp) {secs=clockp->time();}
         choose_tau->choose_serious(newval,oldval,modelval,y,newy,eigvecs,
                                    last_subgrad,norm2subg,problem);
         if (clockp) {
	   time_last_choosetau=clockp->time()-secs;
	   time_choosetau+=time_last_choosetau;
	 }
         y=newy;
         problem->set_y(y,eigval,eigvecs);
         break;
     }
      
     if (clockp) {secs=clockp->time();}
     SBchoose_val sbcv=choose_tau->choose_nullstep(newval,oldval,modelval,
                           y,newy,eigvecs,last_subgrad,norm2subg,problem);
     if (clockp) {
       time_last_choosetau=clockp->time()-secs;
       time_choosetau+=time_last_choosetau;
     }
     if (sbcv==SBchoose_lapprox){
         if (out) (*out)<<" WARNING: lapprox<0! recompute oldval ..."<<endl;
         recomp++;
         sumrecomp++;
         tmpvec=eigval;
         tmpmat=eigvecs;
         nreig=1;
         int status=problem->obj_compute(y,tmpvec,tmpmat,
                                         1e40,.001*relprec,nreig);
         cntobjeval++;
         if (status) {
             cerr<<"WARNING: problem->obj_compute(..) returned: "<<status<<endl;
         }
         Real tryval=problem->objval(tmpvec(0),y);
         if (tryval>oldval) {
             oldval=tryval;
             problem->set_y(y,tmpvec,tmpmat);    
             if (out) (*out)<<" recompute yields higher oldval="<<oldval<<endl;         
         }
         else {
             if (out) (*out)<<" recompute yields no better value ("<<tryval<<")"<<endl;
         }
     }

 }while(1);

 
 if (!terminate && do_scaling){
     tmpvec.init(y.dim(),1,1.);
     for(Integer i=0;i<tmpvec.dim();i++) {
         if (my_abs(y(i))>1.) tmpvec(i)=1./sqr(y(i));
     }
     problem->set_scale(tmpvec);
 }     

 return 0;
} 


// *****************************************************************************
//                                 print_line_summary
// *****************************************************************************

// outputs most important data within one line

ostream& SpectralBundle::print_line_summary(ostream& out) const
{
  extern int flag_min_max;
  extern ofstream result;
  extern char* filename_result;
  float Dual, Primal;
    
 out.setf(ios::showpoint);
 out<<get_clock();
 Primal = (flag_min_max *get_modelval());
 out<<" Primal : ";out.precision(8);out.width(9);out<<Primal;
 out.width(8);
 out<<" Bound :";out.width(9);
 Dual = (flag_min_max * get_problem()->objval());
 out<<"("<< Dual <<")";
 out.width(6);
 if (get_lower_bound()>min_Real)
 {
   out<<">";out<<get_lower_bound();
 }
 out<<endl;

 if (filename_result)
 {
       result.setf(ios::showpoint);
       result<<"\n"<<get_clock();
       result<<" Primal : ";result.precision(8);result.width(9);result<<Primal;
       result.width(8);
       result<<" Bound :";result.width(9);
       result<<"("<< Dual <<")";
       result.width(6);
       if (get_lower_bound()>min_Real)
       {
	 result<<">";result<<get_lower_bound();
       }
//       result<<endl;  	            
 }	 
 
 return out;
}

// *****************************************************************************
//                                 print_full_summary
// *****************************************************************************

// calls print_line_summary and displays several additional items and statistics

ostream& SpectralBundle::print_full_summary(ostream& out) const
{
 print_line_summary(out);
 out.precision(4);
 out<<"  eigvaltime=";
 print_time(out,get_problem()->get_eigvaltime());
 out<<" reltime="<<Real(get_problem()->get_eigvaltime())/Real(get_clock().time())*100.;
 out<<" maxPrank="<<get_maxrank();
 out<<endl;
 out<<"  qspsolves  :";
 out<<" ";out.width(4);out<<get_cntsolves();
 out<<" ";out.width(5);out<<get_cntitersolves();
 out<<" ";print_time(out,get_time_coeff());
 out<<" ";print_time(out,get_time_solve());
 out<<endl;
 out<<"  qspresolves:";
 out<<" ";out.width(4);out<<get_cntresolves();
 out<<" ";out.width(5);out<<get_cntiterresolves();
 out<<" ";print_time(out,get_time_update());
 out<<" ";print_time(out,get_time_resolve());
 out<<endl;
 out<<"  augmod=";
 print_time(out,time_augmod);
 out<<"  eval=";
 print_time(out,time_eval);
 out<<"  update=";
 print_time(out,time_bunupd);
 out<<"  choose=";
 print_time(out,time_choosetau);
 out<<endl;
#ifdef DETAILED_SUMMARY
 out<<"  qsp   =";
 print_time(out,time_qsp);
 out<<"  subg=";
 print_time(out,time_subg);
 out<<"  lagupd=";
 print_time(out,time_lagupd);
 out<<"  model =";
 print_time(out,time_model);
 out<<endl;
 out<<"  matrixtime=";
 print_time(out,get_problem()->get_matrixtime());
 out<<"  subgtime=";
 print_time(out,get_problem()->get_subgtime());
 out<<endl;
 out<<"  enforced null steps: benign="<<get_bens();
 out<<"  significant="<<get_sens();
 out<<" (";out.precision(2);
 out<<get_sensvkappasum()/max(1.,Real(get_sens()));
 out<<")";
 out<<endl;
 out<<"  null steps with shallow cut: "<<get_shallowcut();
 out<<endl;
#endif
 terminator->print_status(out);
 if (get_sumrecomp()) {
     out<<"\n WARNING: descent step values were recomputed ";
     out<<get_sumrecomp()<<" time(s)"<<endl;
 }
 if (get_dens()){
     out<<"WARNING: relative precision enforced null step ";
     out<<get_dens()<<" time(s)"<<endl;
 }
 if (update_bundle->get_rankdefcnt()){
     out<<"\n WARNING: rank deficiency in bundle updates encountered: ";
     out<<update_bundle->get_rankdefcnt()<<" time(s)"<<endl;
 }
 if (get_sumqsdpfails()) {
     out<<"\n WARNING: Solving the quadratic subproblem failed  ";
     out<<get_sumqsdpfails()<<" time(s)"<<endl;
 }
 if (get_sumaugvalfails()) {
     out<<"\n WARNING: The augmented model failed to be increased  ";
     out<<get_sumaugvalfails()<<" time(s)"<<endl;
 }
 return out;
}

// *****************************************************************************
//                                  save
// *****************************************************************************

// writes all variables to out that are needed for resuming an interrupted session

ostream& SpectralBundle::save(ostream& out) const
{
 out.precision(20);
 sdpqp.save(out);
 aggregqp.save(out);

 out.precision(20);
 out<<tau<<"\n";
 out<<eigval<<eigvecs<<y<<newy<<bundlevecs<<primalvecs<<primaleigs;
 out<<rayleighval<<last_subgrad;
 out<<X<<Z;
 out<<s<<"\n"<<t<<"\n";
 out<<norm2dy<<"\n"<<norm2subg<<"\n";
 out<<modeleps<<"\n"<<qpeps<<"\n"<<memkeepvecs<<"\n"<<mL<<"\n"<<mN<<"\n";
 out<<mult_stepsize<<"\n"<<intpoint_maxiter<<"\n"<<do_scaling<<"\n";
 out<<remove_cuts<<"\n"<<use_linval<<"\n";  
 out<<last_method_used<<"\n"<<max_updates<<"\n"<<updatecnt<<"\n"<<retcode<<"\n"<<problem_changed<<"\n";
 out<<oldval<<"\n"<<newval<<"\n"<<linval<<"\n"<<cutval<<"\n"<<modelval<<"\n";
 out<<augval<<"\n"<<oldaugval<<"\n"<<lastaugval<<"\n";
 out<<innerit<<"\n"<<suminnerit<<"\n"<<cntobjeval<<"\n"<<recomp<<"\n"<<sumrecomp<<"\n";
 out<<qsdpfails<<"\n"<<sumqsdpfails<<"\n";
 out<<augvalfails<<"\n"<<sumaugvalfails<<"\n"<<maxrank<<"\n";
 out<<time_coeff<<"\n"<<time_solve<<"\n"<<time_update<<"\n"<<time_resolve<<"\n";
 out<<time_augmod<<"\n"<<time_eval<<"\n"<<time_bunupd<<"\n"<<time_choosetau<<"\n";
 out<<time_last_augmod<<"\n"<<time_last_eval<<"\n"<<time_last_bunupd<<"\n"<<time_last_choosetau<<"\n";
 out<<time_qsp<<"\n"<<time_subg<<"\n"<<time_lagupd<<"\n"<<time_model<<"\n";
 out<<cntsolves<<"\n"<<cntresolves<<"\n";
 out<<cntitersolves<<"\n"<<cntiterresolves<<"\n"<<calls<<"\n";
 out<<bens<<"\n"<<sens<<"\n"<<dens<<"\n"<<shallowcut<<"\n"<<sensvkappasum<<"\n";
 out<<nr_ineq<<"\n"<<nr_lmult_act<<"\n"<<nr_add<<"\n"<<nr_del<<"\n"<<lower_bound<<"\n";
 return out;
}

// *****************************************************************************
//                                  restore
// *****************************************************************************

// reads all variables from in that are needed for resuming an interrupted session

istream& SpectralBundle::restore(istream& in)
{
 sdpqp.restore(in);
 aggregqp.restore(in);
          
 in>>tau;
 in>>eigval>>eigvecs>>y>>newy>>bundlevecs>>primalvecs>>primaleigs;
 in>>rayleighval>>last_subgrad;
 in>>X>>Z;
 in>>s>>t;
 in>>norm2dy>>norm2subg;
 in>>modeleps>>qpeps>>memkeepvecs>>mL>>mN;
 in>>mult_stepsize>>intpoint_maxiter>>do_scaling;
 in>>remove_cuts>>use_linval;  
 in>>last_method_used>>max_updates>>updatecnt>>retcode>>problem_changed;
 in>>oldval>>newval>>linval>>cutval>>modelval;
 in>>augval>>oldaugval>>lastaugval;
 in>>innerit>>suminnerit>>cntobjeval>>recomp>>sumrecomp;
 in>>qsdpfails>>sumqsdpfails>>augvalfails>>sumaugvalfails>>maxrank;
 in>>time_coeff>>time_solve>>time_update>>time_resolve;
 in>>time_augmod>>time_eval>>time_bunupd>>time_choosetau;
 in>>time_last_augmod>>time_last_eval>>time_last_bunupd>>time_last_choosetau;
 in>>time_qsp>>time_subg>>time_lagupd>>time_model;
 in>>cntsolves>>cntresolves;
 in>>cntitersolves>>cntiterresolves>>calls;
 in>>bens>>sens>>dens>>shallowcut>>sensvkappasum;
 in>>nr_ineq>>nr_lmult_act>>nr_add>>nr_del>>lower_bound;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: spectral.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:44  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/spectral.cc,v $
 --------------------------------------------------------------------------- */
