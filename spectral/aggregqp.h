/* -*-c++-*-
 * 
 *    Source     $RCSfile: aggregqp.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:34 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __AGGREGQP_H__
#define __AGGREGQP_H__

#include "memarray.h"
#include "symmat.h"

//   For given Q1, Q12, q2, together >=0, C1, c2, and a>0 solve
//   min    max  <1/2 Q1 X,X>+s<Q12,X>+ 1/2 q2 s^2+<C1,X>+<c2,s>+<a-s-<I,X>,u>+d
//  X,s>=0   u

class Aggregqp
{
 private:
    //problem description
    Integer dim;       //size of X
    Symmatrix Q1;
    Matrix Q12;
    Real q2;
    Matrix C1;
    Real c2;
    Real a;
    Real dcoeff;
    Matrix svecI;      //svec of the identity

    //Variables
    Symmatrix X;     //primal matrix variable
    Symmatrix Z;     //dual matrix slack
    Real s;          //Variable for aggregate constraint
    Real t;          //dual slack for aggregate variable
    Real u;          //dual to tr(X)=n

    Real primalval;
    Real dualval;

    //delta-Variables
    Symmatrix dX;     //primal matrix variable
    Symmatrix dZ;     //dual matrix slack
    Real ds;
    Real dt;
    Real du;
    //second set for predictor corrector
    Symmatrix dX2;     //primal matrix variable
    Symmatrix dZ2;     //dual matrix slack
    Real ds2;
    Real dt2;
    Real du2;
   

    //"constants"
    Symmatrix Xi;    //inverse of X
    Matrix FZ;       //Z-infeasibility
    Real   Ft;       //t-infeasibility
    Real   FP;       //primal trace-infeasibility
    Matrix FZmuI;

    //step-sizes and barrier parameter
    Real alphap;     //primal step-size
    Real alphad;     //dual step-size
    Real mu;
    Real tol;        //tolerance for distance to boundary depending on mu 

    //temporary variables
    Symmatrix bigtmp; 
    Matrix tmpsvec;
    Symmatrix symtmp;

    //termination parameters
    Real termeps;     //relative precision termination criterion
    Integer maxiter;     //stop after this number of iterations
    Real lowerbound;
    Real upperbound;

    //meta-variables
    Integer iter;
    int status;       //is also returned by solve
                      //0 ... terminated correctly
                      //1 ... maximum number of iterations exceeded
                      //2 ... some error occured

    //output variables
    ostream* out;

    int error(const char *s) const;

    void init_variables();
    int pd_line_search(Symmatrix& A,Symmatrix& dA,Real& stepsize,Real upper_bound);
    void choose_mu();
    int search_direction();
    int pred_corr();
    void line_search_and_update();
    int iterate();
   
 public:
    Aggregqp(){set_defaults();}
    ~Aggregqp(){}

    void set_defaults();
    void init_size(Integer maxdim);

    void set_termbounds(Real lb,Real ub){lowerbound=lb;upperbound=ub;}
    void set_termeps(Real te){termeps=te;}
    void set_maxiter(Integer mi){maxiter=mi;}
    void set_out(ostream* o){out=o;}

    void get_X(Symmatrix &Xout) const {Xout=X;}
    void get_Z(Symmatrix &Zout) const {Zout=Z;}
    Real get_s() const {return s;}
    Real get_t() const {return t;}
    Real get_u() const {return u;}
    Real get_gap() const {return ip(X,Z)+s*t;}

    const Symmatrix& get_Q1() const {return Q1;}
    const Matrix& get_Q12() const {return Q12;}
    Real get_q2() const {return q2;}
    const Matrix& get_C1() const {return C1;}
    Real get_c2() const {return c2;}
    Real get_a() const {return a;}
    Real get_dcoeff() const {return dcoeff;}
  

    Integer get_iter() const {return iter;}
    Integer get_status() const {return status;}
    Real get_termeps() const {return termeps;}
    Integer get_maxiter() const {return maxiter;}

    Real get_primalval()const {return primalval;}
    Real get_dualval()const {return dualval;}

    int solve(const Symmatrix& Q1,const Matrix& Q12,Real q2,
              const Matrix& C1,Real c2,Real a,Real dco);

    int update(const Matrix& dC1,Real dc2,Real dco);    

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: aggregqp.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:34  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/aggregqp.h,v $
 --------------------------------------------------------------------------- */
