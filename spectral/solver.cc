/* -*-c++-*-
 * 
 *    Source     $RCSfile: solver.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 *    Modifed in 2002 by G.Delaporte, S. Jouteau, F.Roupin
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: solver.cc,v 1.1.1.1 1999/12/15 13:47:52 bzfhelmb Exp $"

#include "solver.h"
#include <cstdio>
#include <fstream>
#include "clock.h"

extern char* filename_result;
ofstream result;	
Clock* CLOCK;

int Solver::solve(Problem* problem,SpectralBundle* bundle,
                  Clock* myclock,SBparams* params,int resume)
{
  extern int flag_min_max;
  extern char *master_file;

 Integer n,m;
 Matrix eigval,eigvec,y;
 float Primal, Dual;

 if (filename_result != NULL)
 {
	 result.open(filename_result);
	 result<<"\n\nSDP_S version 0.0.1, Copyright (C) 2002 G.Delaporte, S.Jouteau, F.Roupin\n";
	 result<<"SDP_S comes with ABSOLUTELY NO WARRANTY\n\n";
	 result<<"Problem file : "<<master_file<<"\n\n";
         result.setf(ios::showpoint);
         n=problem->mat_dim();
         m=problem->nr_constraints();
         result<<"Dim = "<<n;
         result<<"  nr_constraints = "<<m<<endl;

 }
 
 if (!resume)
 {
     //-- initialization
     n=problem->mat_dim();
     m=problem->nr_constraints();
     bundle->init_size(n,m);
     if (out)
     {
       (*out)<<"Dimension = "<<n;
       (*out)<<" Number of constraints = "<<m<<endl;
     }
  
     
     //---- compute maximal eigenvalues and eigenvectors of first point
     problem->starting_point();
     
     if (out)
     {
         (*out).setf(ios::showpoint);
	 CLOCK = myclock;
         (*out)<<"\n"<<*CLOCK;
         (*out)<<" Primal : ";out->precision(8);out->width(9);(*out)<<"0";
         Primal = 0;   	 
         out->precision(8);
         out->width(8);
         (*out)<<" Bound :";out->width(9);
	 Dual = (flag_min_max * problem->objval());
	 (*out)<<"("<< Dual <<")";
	 out->precision(6);
	 if (bundle->get_lower_bound()>min_Real)
	 {
	   (*out)<<">";
	   (*out)<<Dual;
	 }
	 (*out)<<endl;	     
      }
     
     if (filename_result)
     {    
       result.setf(ios::showpoint);
       result<<"\n"<<*CLOCK;
       result<<" Primal : ";result.precision(8);result.width(9);result<<"0";
       result<<" Bound :";result.width(9);
       result<<"("<< Dual <<")";
       result.precision(6);
       if (bundle->get_lower_bound()>min_Real)
       {
	 result<<">";
	 result<<Dual;
       }            
     }   
 }    
 //---- LOOP

 do{

     //---- call bundle algorithm
     bundle->inner_loop(*problem,resume);
     resume=0;
     
     //--- output
     if (out) bundle->print_line_summary((*out));

 } while(
     (!bundle->get_terminate())
     &&
     ((params->maxiter<0)||(bundle->get_calls()<params->maxiter))
     &&
     (myclock->time()<bundle->get_terminator()->get_timelimit())
    );

   if(filename_result) result.close();
 
 return 0;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: solver.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/sb/solver.cc,v $
 --------------------------------------------------------------------------- */
