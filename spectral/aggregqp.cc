/* -*-c++-*-
 * 
 *    Source     $RCSfile: aggregqp.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:33 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: aggregqp.cc,v 1.1.1.1.2.1 2000/02/28 13:55:33 bzfhelmb Exp $"

#include "mymath.h"
#include "aggregqp.h"
#include <fstream>

int Aggregqp::error(const char* str) const 
{
 cerr<<"\n*** ERROR in Aggregqp: "<<str<<endl;
 return 1;
}

void Aggregqp::set_defaults()
{
 termeps=1e-7;
 maxiter=-1;
 init_size(0);
}

void Aggregqp::init_size(Integer maxdim)
{
 Integer d=maxdim;
 Integer d2=(d*(d+1))/2;
 Q1.newsize(d2); Q1.init(0,0.);
 Q12.newsize(d2,1); Q12.init(0,0,0.);
 q2=0.;
 C1.newsize(d2,1); C1.init(0,0,0.);
 c2=0.;
 a=0.;
 svecI.newsize(d2,1); svecI.init(0,0,0.);
 X.newsize(d); X.init(0,0.);
 Z.newsize(d); Z.init(0,0.);
 dX.newsize(d); dX.init(0,0.);
 dZ.newsize(d); dZ.init(0,0.);
 dX2.newsize(d); dX2.init(0,0.);
 dZ2.newsize(d); dZ2.init(0,0.);
 Xi.newsize(d); Xi.init(0,0.);
 FZ.newsize(d2,1); FZ.init(0,0,0.);
 FZmuI.newsize(d2,1); FZmuI.init(0,0,0.);
 bigtmp.newsize(d2); bigtmp.init(0,0.);
 tmpsvec.newsize(d2,1); tmpsvec.init(0,0,0.);
 symtmp.newsize(d); symtmp.init(0,0.);
}

// *************************************************************************
//                             init_variables
// *************************************************************************

// computes a strictly feasible primal-dual starting point. 

void Aggregqp::init_variables()
{
 dim=Integer(sqrt(8.*Q1.rowdim()+1)/2.);
 svecI=svec(Diag(Matrix(dim,1,1.)));
 X=Diag(Matrix(dim,1,a/Real(dim+1)));
 s=a/Real(dim+1);
 svec(X,tmpsvec);
 t=ip(Q12,tmpsvec)+q2*s+c2;
 tmpsvec=Q1*tmpsvec;
 tmpsvec+=C1;
 tmpsvec+=s*Q12;
 sveci(tmpsvec,Z);
 u=max(max(sumrows(abs(Z))),max(0.,-t))+.1;
 u=-u;
 Z-=Diag(Matrix(dim,1,u));
 t-=u;    
 
 //intialize step sizes
 alphap=1.;
 alphad=1.;
 mu=1e20;

 /*
 cout<<"\n starting point:";
 cout<<"  st_FZ="<<norm2(Q1*svec(X)+s*Q12+C1-u*svecI-svec(Z));
 cout<<"  st_Ft="<<my_abs(q2*s+ip(Q12,svec(X))+c2-u-t);
 cout<<"  st_FP="<<my_abs(a-s-trace(X))<<flush;
 cout<<"  n2(X)="<<norm2(X)<<" n2(Z)="<<norm2(Z);
 */
}

// *************************************************************************
//                             pd_line_search
// *************************************************************************

//performs line search for A+stepsize*dA with 0<=stepsize<=upper_bound.
//stepsize should be set to an initial good guess at input.
//tries to find maximal stepsize for which A+stepsize*dA is still positive
//parameters not changed: A,dA,upper_bound;
//parameters changed: stepsize
//globals changed: ldlcnt
//returns 1 if A+stepsize*dA is pos def for the whole interval else 0

int Aggregqp::pd_line_search(Symmatrix& S,Symmatrix& dS,Real& stepsize,Real upper_bound)
{
 int posdef;
 stepsize=max(stepsize,1e-6);
 symtmp.init(dS,stepsize); symtmp+=S;
 posdef=!symtmp.LDLfactor(tol);
 if(!posdef){
     do{
         stepsize*=.8;
         if (stepsize<1e-10) {stepsize=0;break;}
         symtmp.init(dS,stepsize); symtmp+=S;
     }while(symtmp.LDLfactor(tol));
 }
 else if(stepsize<upper_bound){
     do{
         stepsize=min(upper_bound,stepsize/.8);
         symtmp.init(dS,stepsize); symtmp+=S;
     }while((posdef=!symtmp.LDLfactor(tol))&&
            (stepsize<upper_bound-1e-10));
     if(!posdef) stepsize*=.8;
 }
 return posdef;
}

// *************************************************************************
//                             choose_mu
// *************************************************************************

// select the barrier parameter in dependence on previous step size

void Aggregqp::choose_mu()
{
 Real alpha;
 Real d=mu;
 if (mu==1e20) alpha=0.;
 else alpha=min(alphap,alphad);
 mu=(ip(Z,X)+s*t)/(dim+1);
 if (alpha>.2) mu*=.5-.45*alpha*alpha;
 mu=min(mu,d);
 tol=min(1e-6*mu,1e-3/max(X));
}

// *************************************************************************
//                             search direction
// *************************************************************************

int Aggregqp::search_direction()
{
 //compute Xi
 bigtmp=X;
 if (bigtmp.LDLfactor(tol)){
     ofstream fout("aggXIfail.dat");
     fout<<Matrix(X);
     fout.close();
     return error("search_direction(): LDLfactor for XI failed");
 }
 bigtmp.LDLinverse(Xi);

 //compute FZ
 svec(Z,tmpsvec);
 FZ.init(svecI,-u);
 FZ-=tmpsvec;
 FZ+=Q12*s;
 svec(X,tmpsvec);
 FZ+=Q1*tmpsvec;
 FZ+=C1;
 
 //compute Ft
 Ft=ip(Q12,tmpsvec);
 Ft+=q2*s;
 Ft+=c2;
 Ft-=u;
 Ft-=t;
 
 //compute FZmuI
 svec(Xi,FZmuI);
 FZmuI*=mu;
 svec(Z,tmpsvec);
 FZmuI-=tmpsvec;

 //compute FP
 FP=a-s-trace(X);

 //compute the Symmetric Kronnecker product and add other terms
 skron(Z,Xi,bigtmp);
 bigtmp+=Q1;
 Integer i,j,k,l;
 Real d=t/s+q2;
 Integer svecdim=(dim*(dim+1))/2;
 k=0;
 for(i=0;i<dim;i++){
     for(j=0;j<svecdim;j++) bigtmp(k,j)-=Q12(j);
     bigtmp(k,k)-=Q12(k);
     l=k;
     for(j=i;j<dim;j++){
         bigtmp(k,l)+=d;
         l+=dim-j;
     }
     k+=dim-i;
 }
 if (bigtmp.LDLfactor(0.01*tol))
     return error("search_direction(): bigtmp.factor failed");

 //compute right hand side and solve for dX
 tmpsvec=FZmuI;
 tmpsvec-=FZ;
 tmpsvec-=FP*Q12;
 tmpsvec-=(mu/s-t-(t/s+q2)*FP)*svecI;
 bigtmp.LDLsolve(tmpsvec);

 //compute remaining directions
 sveci(tmpsvec,dX);
 ds=FP-trace(dX);
 dt=(mu-t*ds)/s-t;
 du=q2*ds+ip(Q12,tmpsvec)-dt+Ft;
 tmpsvec=Q1*tmpsvec;
 tmpsvec+=ds*Q12;
 tmpsvec-=du*svecI;
 tmpsvec+=FZ;
 sveci(tmpsvec,dZ);

 /*
 cout<<"  FdZ="<<norm2(FZ+Q1*svec(dX)+ds*Q12-du*svecI-svec(dZ));
 cout<<"  Fdt="<<my_abs(Ft+q2*ds+ip(svec(dX),Q12)-du-dt);
 cout<<"  Fds="<<my_abs(FP-trace(dX)-ds)<<flush;
 */
 
 return 0;
}

// *************************************************************************
//                             pred_corr
// *************************************************************************

int Aggregqp::pred_corr()
{
 //compute Xi
 bigtmp=X;
 if (bigtmp.LDLfactor(tol)){
     ofstream fout("aggXIfail.dat");
     fout<<Matrix(X);
     fout.close();
     return error("pred_corr(): LDLfactor for XI failed");
 }
 bigtmp.LDLinverse(Xi);
 //cout<<"  Xi="<<norm2(X*Xi-diag(Matrix(X.rowdim(),Integer(1),1.)));

 //compute FZ
 svec(Z,tmpsvec);
 FZ.init(svecI,-u);
 FZ-=tmpsvec;
 FZ+=Q12*s;
 svec(X,tmpsvec);
 FZ+=Q1*tmpsvec;
 FZ+=C1;
 
 //compute Ft
 Ft=ip(Q12,tmpsvec);
 Ft+=q2*s;
 Ft+=c2;
 Ft-=u;
 Ft-=t;
 
 //compute FZmuI
 svec(Z,FZmuI);
 FZmuI*=-1;

 //compute FP
 FP=a-s-trace(X);

 //compute the Symmetric Kronnecker product and add other terms
 skron(Z,Xi,bigtmp);
 bigtmp+=Q1;
 Integer i,j,k,l;
 Real d=t/s+q2;
 Integer svecdim=(dim*(dim+1))/2;
 k=0;
 for(i=0;i<dim;i++){
     for(j=0;j<svecdim;j++) bigtmp(k,j)-=Q12(j);
     bigtmp(k,k)-=Q12(k);
     l=k;
     for(j=i;j<dim;j++){
         bigtmp(k,l)+=d;
         l+=dim-j;
     }
     k+=dim-i;
 }
 if (bigtmp.LDLfactor(0.01*tol))
     return error("pred_corr(): bigtmp.factor failed");

 //compute right hand side and solve for predictor dX
 tmpsvec=FZmuI;
 tmpsvec-=FZ;
 tmpsvec-=FP*Q12;
 tmpsvec-=(-t-(t/s+q2)*FP)*svecI;
 bigtmp.LDLsolve(tmpsvec);

 //compute remaining directions
 sveci(tmpsvec,dX);
 ds=FP-trace(dX);
 dt=(-t*ds)/s-t;
 du=q2*ds+ip(Q12,tmpsvec)-dt+Ft;
 tmpsvec=Q1*tmpsvec;
 tmpsvec+=ds*Q12;
 tmpsvec-=du*svecI;
 tmpsvec+=FZ;
 sveci(tmpsvec,dZ);

 /*
 cout<<"  FdZ1="<<norm2(FZ+Q1*svec(dX)+ds*Q12-du*svecI-svec(dZ));
 cout<<"  Fdt1="<<my_abs(Ft+q2*ds+ip(svec(dX),Q12)-du-dt);
 cout<<"  Fds1="<<my_abs(FP-trace(dX)-ds);
 cout<<"  Fcom="<<norm2(Symmatrix(X*dZ+dX*Z+X*Z))<<flush;
 cout<<"  Fco2="<<norm2(Symmatrix(dZ+Xi*dX*Z+Z))<<flush;
 cout<<"  Fcst="<<my_abs(s*dt+ds*t+s*t)<<flush;
 */
 
 //compute right hand side and solve for corrector dX
 svec(Xi,tmpsvec);
 tmpsvec*=mu;
 tmpsvec-=svec(Symmatrix(Xi*(dX*dZ)));
 tmpsvec-=(mu/s-dt*ds/s)*svecI;
 bigtmp.LDLsolve(tmpsvec);

 //compute remaining directions
 sveci(tmpsvec,dX2);
 ds2=-trace(dX2);
 dt2=(mu-t*ds2)/s-dt*ds/s;
 du2=q2*ds2+ip(Q12,tmpsvec)-dt2;
// tmpsvec=Q1*tmpsvec;
// tmpsvec+=ds2*Q12;
// tmpsvec-=du2*svecI;
// sveci(tmpsvec,dZ2);
 svec(Xi,tmpsvec);
 tmpsvec*=mu;
 tmpsvec-=svec(Symmatrix(Xi*(dX*dZ)));
 tmpsvec-=svec(Symmatrix(Xi*(dX2*Z)));
 sveci(tmpsvec,dZ2);
 

 /*
 cout<<"  FdZ2="<<norm2(FZ+Q1*svec(dX+dX2)+(ds+ds2)*Q12-(du+du2)*svecI-svec(dZ+dZ2));
 cout<<"  Fdt2="<<my_abs(Ft+q2*(ds+ds2)+ip(svec(dX+dX2),Q12)-(du+du2)-(dt+dt2));
 cout<<"  Fds2="<<my_abs(FP-trace(dX+dX2)-(ds+ds2));
 cout<<"  Fcom="<<norm2(Symmatrix(X*dZ2+dX2*Z-mu*Diag(Matrix(X.rowdim(),Integer(1),1.))+dX*dZ))<<flush;
 cout<<"  Fco2="<<norm2(Symmatrix(dZ2+Xi*dX2*Z-mu*Xi+Xi*dX*dZ))<<flush;
 cout<<"  Fcst="<<my_abs(s*dt2+ds2*t-mu+ds*dt)<<flush;
 */
 
 dX+=dX2;
 ds+=ds2;
 du+=du2;
 dt+=dt2;
 dZ+=dZ2;

 return 0;
}

// *************************************************************************
//                        line_search_and_update
// *************************************************************************

void Aggregqp::line_search_and_update()
{
  //----- find primal steplength alphap
 Real salphap=1.;
 if (ds<-tol) salphap=min(salphap,-s/ds*.9);
 
 alphap=min(salphap,alphap*2.);
 int posdef=pd_line_search(X,dX,alphap,salphap);
 if (alphap<1.) alphap*=.95;

 //----- find dual steplength alphad
 Real talphad=1.;
 if (dt<-tol) talphad=min(talphad,-t/dt*.9);
 
 alphad=min(talphad,alphad*2.);
 posdef=pd_line_search(Z,dZ,alphad,talphad);
 if (alphad<1.) alphad*=.95;

 //----- update
 //cout<<" ap="<<setw(2)<<alphap<<" ad="<<setw(2)<<alphad;
 Real alpha=min(alphap,alphad);

 dX*=alpha; X += dX;  
 du*=alpha; u += du;  
 dZ*=alpha; Z += dZ;  
 ds*=alpha; s += ds;
 dt*=alpha; t += dt;

}

// *************************************************************************
//                             iterate
// *************************************************************************

// loop till convergence to optimal solution

int Aggregqp::iterate()
{
 if (out){
     *out<<"    mi="<<maxiter;
     *out<<" te=";out->precision(3);*out<<termeps;
     *out<<" lb=";out->precision(8);out->width(9);*out<<lowerbound;
     *out<<" ub=";out->precision(8);out->width(9);*out<<upperbound;
     out->precision(2);*out<<endl;
 }
 
 svec(X,tmpsvec);
 dualval=-(primalval=ip(tmpsvec,Q1*tmpsvec)/2+s*ip(Q12,tmpsvec)+q2*s*s/2);
 primalval+=ip(C1,tmpsvec)+s*c2+dcoeff;
 if (upperbound>=1e20) upperbound=primalval;
 dualval+=a*u+dcoeff;
 
 //while(((ip(X,Z)+s*t)>termeps*max(max(max(X),max(Z)),max(s,t)))&&
 while(((maxiter<0)||(iter<maxiter))&&
       (primalval-lowerbound >1e-10*(my_abs(primalval)+1.))&&
       (upperbound-dualval>1e-10*(my_abs(dualval)+1.))&&
       (primalval-dualval>min(termeps*(primalval-lowerbound),.5*(upperbound-dualval)))
       ){
   
     if (out){
         out->precision(2);
         *out<<"    ";out->width(2);*out<<iter<<":"<<" gap=";out->width(7);*out<<ip(X,Z)+s*t<<flush;
         *out<<" mu=";out->width(7);*out<<mu<<flush;
         *out<<" pv=";out->precision(8);out->width(10);*out<<primalval;
         *out<<" dv=";out->precision(8);out->width(10);*out<<dualval;out->precision(2);
         *out<<" FZ=";out->width(7);*out<<norm2(Q1*svec(X)+s*Q12+C1-u*svecI-svec(Z));
         *out<<" Ft=";out->width(7);*out<<my_abs(q2*s+ip(Q12,svec(X))+c2-u-t);
         *out<<" FP=";out->width(7);*out<<my_abs(a-s-trace(X))<<flush;
         *out<<" ap=";out->width(4);*out<<alphap<<" ad=";out->width(4);*out<<alphad<<endl;
     }
     
     
     iter++;
     
     //cout<<"\n    ";cout->width(2);cout<<iter<<":"<<" gap=";cout->width(9);cout<<ip(X,Z)+s*t<<flush;

     choose_mu();
     //cout<<" mu=";cout->width(9);cout<<mu<<flush;
     
     if (pred_corr()){
         status=2;
         return status;
     }
     
     line_search_and_update();

     svec(X,tmpsvec);
     dualval=-(primalval=ip(tmpsvec,Q1*tmpsvec)/2+s*ip(Q12,tmpsvec)+q2*s*s/2);
     primalval+=ip(C1,tmpsvec)+s*c2+dcoeff;
     dualval+=a*u+dcoeff;

 }

 if (out){
     out->precision(2);
     *out<<"\n    ";out->width(2);*out<<iter<<":"<<" gap=";out->width(7);*out<<ip(X,Z)+s*t<<flush;
     *out<<" mu=";out->width(7);*out<<mu<<flush;
     *out<<" pv=";out->precision(8);out->width(10);*out<<primalval;
     *out<<" dv=";out->precision(8);out->width(10);*out<<dualval;out->precision(2);
     *out<<" au=";out->precision(8);out->width(10);*out<<a*u;out->precision(2);
     *out<<" FZ=";out->width(7);*out<<norm2(Q1*svec(X)+s*Q12+C1-u*svecI-svec(Z));
     *out<<" Ft=";out->width(7);*out<<my_abs(q2*s+ip(Q12,svec(X))+c2-u-t);
     *out<<" FP=";out->width(7);*out<<my_abs(a-s-trace(X))<<flush;
     *out<<" ap=";out->width(4);*out<<alphap<<" ad=";out->width(4);*out<<alphad<<endl;
 }

 if ((maxiter<0)||(iter<maxiter)){
     status=0;
 }
 else {
     status=1;
 }

 return status;
}
 
// *************************************************************************
//                             solve
// *************************************************************************

// call init_variables and loop till convergence to optimal solution

int Aggregqp::solve(const Symmatrix& Q1in,const Matrix& Q12in,Real q2in,
              const Matrix& C1in,Real c2in,Real ain,Real dco)
{
 //out=&cout;
 Q1=Q1in;
 Q12=Q12in;
 q2=q2in;
 C1=C1in;
 c2=c2in;
 a=ain;
 dcoeff=dco;
 
 //(*out)<<Q1in<<"\n";
 //(*out)<<Q12in<<"\n";
 //(*out)<<q2in<<"\n";
 //(*out)<<C1in<<"\n";
 //(*out)<<c2in<<"\n";
 //(*out)<<ain<<"\n";
 //(*out)<<dco<<"\n";

 init_variables();

 iter=0;

 status=iterate();
 
 return status;
}
 
// *************************************************************************
//                             update
// *************************************************************************

// check wether a problem has been solved already, if not return with error.
// Else compute a good starting point using the old solution
// and solve the new problem.

int Aggregqp::update(const Matrix& dC1,Real dc2,Real dd)
{
 if (a<=0.) return -1;

 dcoeff+=dd;
 
 Real factor;
 factor=max(0.9,sqrt(1.-sqrt(ip(dC1,dC1)+sqr(dc2))/sqrt(ip(C1,C1)+sqr(c2))));
 factor=min(factor,.99999999);  //.99999
 if (out) {
     *out<<" factor=";out->precision(10);*out<<factor<<flush;
 }
 svec(X,tmpsvec);
 ds=s;
 Real oldtrace=trace(X)+s;
 Real olddiff=a-oldtrace;

 // push X and s a little to the center
 X*=factor; X+=Diag(Matrix(dim,1,((1-factor)*oldtrace+olddiff)/Real(dim+1)));
 s*=factor; s+=((1-factor)*oldtrace+olddiff)/Real(dim+1);

 //compute the change on the dual side caused by change of X/s and dC1/dc2
 tmpsvec*=factor-1.;
 tmpsvec+=(((1-factor)*oldtrace+olddiff)/Real(dim+1))*svecI;
 ds*=factor-1.;
 ds+=(((1-factor)*oldtrace+olddiff)/Real(dim+1));
 dt=q2*ds+ip(Q12,tmpsvec)+dc2;
 tmpsvec=Q1*tmpsvec;
 tmpsvec+=ds*Q12;
 tmpsvec+=dC1;

 //make this change positive definite by means of a change in u
 sveci(tmpsvec,dZ);
 du=max(max(sumrows(abs(dZ))),max(0.,-dt))+(1-factor)*.1;
 u-=du;
 dZ+=Diag(Matrix(dim,1,du));  //dZ is now positive definite
 dt+=du;                      //dt is now positive
 Z+=dZ;
 t+=dt;

 C1+=dC1;
 c2+=dc2;

 //out=&cout;
 //*out<<"\n   starting point: factor="<<factor;
 //*out<<"  st_FZ="<<norm2(Q1*svec(X)+s*Q12+C1-u*svecI-svec(Z));
 //*out<<"  st_Ft="<<my_abs(q2*s+ip(Q12,svec(X))+c2-u-t);
 //*out<<"  st_FP="<<my_abs(a-s-trace(X))<<endl;

 alphap=1.;
 alphad=1.;
 mu=1e20;

 
 iter=0;
 
 status=iterate();

 return status;
}

// *************************************************************************
//                             save
// *************************************************************************

// write all variables to out in order to enable resuming interrupted sessions

ostream& Aggregqp::save(ostream& out) const
{
 out.precision(20);
 out<<dim<<"\n";    
 out<<Q1<<Q12<<q2<<"\n"<<C1<<c2<<"\n"<<a<<"\n"<<dcoeff<<"\n"<<svecI;
 out<<X<<Z<<s<<"\n"<<t<<"\n"<<u<<"\n";
 out<<primalval<<"\n";
 out<<dualval<<"\n";
 out<<alphap<<"\n"; 
 out<<alphad<<"\n"; 
 out<<mu<<"\n";     
 out<<tol<<"\n";    
 out<<termeps<<"\n";
 out<<maxiter<<"\n";
 out<<lowerbound<<"\n";
 out<<upperbound<<"\n";
 out<<iter<<"\n";
 out<<status<<"\n";
 return out;
}

// *************************************************************************
//                             restore
// *************************************************************************

// read all variables from "in" in order to enable resuming interrupted sessions

istream& Aggregqp::restore(istream& in)
{
 in>>dim;    
 in>>Q1>>Q12>>q2>>C1>>c2>>a>>dcoeff>>svecI;
 in>>X>>Z>>s>>t>>u;
 in>>primalval;
 in>>dualval;
 in>>alphap;  
 in>>alphad;  
 in>>mu;       
 in>>tol;       
 in>>termeps;   
 in>>maxiter;   
 in>>lowerbound;
 in>>upperbound;
 in>>iter;
 in>>status;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: aggregqp.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:33  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/aggregqp.cc,v $
 --------------------------------------------------------------------------- */
