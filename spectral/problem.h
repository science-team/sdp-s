/* -*-c++-*-
 * 
 *    Source     $RCSfile: problem.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __PROBLEM_H__
#define __PROBLEM_H__

// organizes all currently active constraints and corresponding variables.
// constraints added to the pool will be deleted from the heap by the pool.

#include "coeffmat.h"
#include "sparssym.h"
#include "lanczos.h"
#include "spectral.h"
#include "clock.h"

class Problem: public SpectralBundleProblem
{
private:
public:
    virtual Integer nr_constraints() const =0;
    virtual Integer mat_dim() const =0;
    
    virtual int provide(Integer k) =0;
        //tries to provide enough storage to store k constraints,
        //return 0 on success and 1 on failure.

    virtual void clear()=0;
        //deletes all inequalities but keeps the storage
    
    virtual Coeffmat* operator()(Integer i) const =0;
    virtual char sign(Integer i) const =0;
    virtual Real& rhs(Integer i) const =0;

    virtual void set_C(const Coeffmat* Cin)=0; //object will be deleted by Problem
    virtual const Coeffmat* get_C()=0;               
        
    virtual void get_y(Matrix& y) const=0;
    virtual Matrix get_y(void) const=0;
    virtual void get_eigval(Matrix& eigv) const=0;
    virtual Matrix get_eigval(void) const=0;
    virtual void get_eigvecs(Matrix& eigv) const=0;
    virtual Matrix get_eigvecs(void) const=0;
    virtual void set_y(const Matrix& y,const Matrix& eigval,const Matrix& eigvec)=0;
    
    virtual void get_rhs(Matrix& rhs) const=0;
    virtual Matrix get_rhs(void) const =0;
    virtual Real& scale(Integer i) const =0;
    virtual void get_scale(Matrix& s) const =0;
    virtual Matrix get_scale(void) const =0;
    virtual void set_scale(const Matrix& s) =0;
    virtual Real& lmult(Integer i) const =0;
    virtual void get_lmult(Matrix& s) const =0;
    virtual Matrix get_lmult(void) const =0;
    virtual void set_lmult(const Matrix& s) =0;
    virtual char get_del_stat(Integer i) const =0;
    virtual void set_del_stat(Integer i,char ds) =0;

    virtual void set_out(ostream* out) =0;

    virtual void get_subgrad(Matrix& s) const =0;
    virtual Matrix get_subgrad(void) const =0;
    virtual void set_store_symsubg(Integer i) =0;
    virtual const Symmatrix& get_symsubg(void) const =0;
    virtual void set_store_sparsesubg(Integer i,const Sparsesym& sps) =0;
    virtual const Sparsesym& get_sparsesubg(void) const =0;
    virtual Real get_subgrad_Cvalue() const =0;
    virtual int subgrad_is_available() const =0;
    virtual void clear_subgrad_available() =0;
    virtual void init_subgrad(const Matrix& P,const Matrix& d) =0;
    virtual void update_subgrad(const Matrix& P,const Matrix& d,Real alpha) =0;
    
    virtual Real lmaxmult() const =0;
    virtual Real ip_C(const Matrix& P,const Matrix& di) =0;
    virtual Real gramip_C(const Matrix& P) =0;
    virtual void gram_opA(const Matrix& P,Matrix& vec) =0;
    virtual Matrix opA(const Matrix& P,const Matrix& di) =0;
    virtual void opA(const Matrix& P,const Matrix& di,Matrix& vec) =0;

    virtual void compute_sdpqp_coeff(Symmatrix& Q, Matrix& C, Real& a, Real& d,
                             const Matrix& P, Real regparam,
                             int do_scaling=0) =0; 

    virtual void compute_aggregqp_coeff(Symmatrix& Q1, Matrix& Q12, Real& q2,
                                Matrix& C1, Real& c2, Real& a, Real& d,
                                const Matrix& P, Real regparam,
                                int do_scaling=0) =0; 

    virtual void update_sdpqp_coeff(Matrix& dC, Real& dd,
                            const Matrix& P, Real regparam,
                            const Indexmatrix& ind,const Matrix& val,
                            int do_scaling=0) =0;

    virtual void update_aggregqp_coeff(Matrix& dC1, Real& dc2, Real& dd,
                               const Matrix& P, Real regparam,
                               const Indexmatrix& ind,const Matrix& val,
                               int do_scaling=0) =0;

    virtual Matrix rhs(void) const =0;
    virtual Real objval() const =0;
    virtual Real objval(Real maxeigval,const Matrix& yvec) =0;
    virtual Real model_compute(const Matrix& y,const Matrix& primalvecs,
                               Matrix& rayleighval) =0;
    virtual int obj_compute(const Matrix &yvec,Matrix& eigval,Matrix& eigvecs,Real nullstep_bound,Real relprec,Integer& nreig) =0;
    //input: yvec gives the point of evaluation,
    //       null_step_bound gives the value above which a nullstep will be made
    //       any vector beating this bound may be returned, otherwise
    //       at least one precise eigenvector to the maximal eigenvalue
    //       has to be returned
    //       eigvecs may contain suggested starting vectors
    // output: eigval and eigvecs are returned in nonincreasing order,
    //       the nreig first eigenvectors have converged 

    virtual int starting_point(void) =0;
    virtual void set_use_startheur(int ush)=0;
    
    virtual void set_exacteigs(Integer ee) =0;
    virtual void set_lanczos(Lanczos* lanp) =0;

    virtual int set_perm(Integer i) =0;
    virtual Integer get_perm() const =0;

    virtual int add(Coeffmat* m,char s,Real rhs,Real scaleval=1.,char del_stat=DEL_STAT_NO)=0; //add a constraint at the end
    virtual int remove(const Indexmatrix& ind)=0;   //delete all constraints indexed

    virtual int constraints_out(ostream& o) const =0;
    virtual int constraints_in(istream& i) =0;
    virtual int read_problem(istream& i,int do_scaling=0) =0;
    virtual int write_problem(ostream& o) const =0;
    virtual int read_y(istream& in) =0;
    virtual int write_y(ostream& out) const =0;
    
    virtual Microseconds get_eigvaltime() const =0;
    virtual Microseconds get_matrixtime() const =0;
    virtual Microseconds get_subgtime() const =0;

    virtual ostream& save(ostream& out) const =0;
    virtual istream& restore(istream& in) =0;
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: problem.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/problem.h,v $
 --------------------------------------------------------------------------- */
