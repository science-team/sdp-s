/* -*-c++-*-
 * 
 *    Source     $RCSfile: solver.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SOLVER_H__
#define __SOLVER_H__

#include "basesolv.h"

class Solver:public BaseSolver
{
ostream *out;

public:
    Solver(){out=0;}
    virtual ~Solver(){}

    int solve(Problem* problem,SpectralBundle* bundle,
              Clock* myclock,SBparams* params,int resume=0);

    void set_out(ostream* o){out=o;}

    virtual int read(istream& in) {return 0;} //nothing to read
    
    ostream& save(ostream& out) const {return out;}
    istream& restore(istream& in) {return in;}

};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: solver.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/sb/solver.h,v $
 --------------------------------------------------------------------------- */
