/* -*-c++-*-
 * 
 *    Source     $RCSfile: problem1.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:40 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __PROBLEM1_H__
#define __PROBLEM1_H__

// organizes all currently active constraints and corresponding variables.
// constraints added to the pool will be deleted from the heap by the pool.

#include "problem.h"
#include "bigmat.h"
#include "lanczos.h"
#include "clock.h"


class Problem1: public Problem, protected Memarrayuser
{
private:
    Integer mempool;     //amount of storage currently available in pool
    Integer memsign;
    Integer memrhs;
    Integer memy;
    Integer memscale;
    Integer memsubgrad;
    Integer memlmult;
    Integer memdelstat;

    Integer dim;        //size of cost matrix
    Integer n;          //number of constraints in the pool
    Integer p;          //number of permanent constraints, in pos 0..perm-1
                        //it is not possible to sort or delete these inequalities

    
    Real lmax_multiplier;
    const Coeffmat* C;        //cost matrix
    Matrix eigval;
    Matrix eigvecs;

    Integer store_symsubg;
    Symmatrix symsubg;
    Integer store_sparsesubg;
    Sparsesym sparsesubg;

    Lanczos* lanczos;   //will be set during init
    Integer exacteigs;  //min number of extremal eigenvalues to be computed
                        //if 0 use inexact computation of extremal eig 
    int use_startheur;  //if 1 a heuristic is applied in starting_point to improve 
                        //the starting point if the eigenvalues are well separated

    Bigmatrix bigm;
    Integer bigm_init;  //flag is true, if bigm was intialized with the following vector
    Matrix last_bigm_y;
    int dense_constraints; //flag is true if dense constraints exist
    
    Symmatrix bigS;      //in case Lanczos is ineffizient use Eispack on this
    
    Coeffmat** pool;     //will keep the pointers to the matrix representations
    char*      signp;    //the sign of the inequality/equality
    Real*      rhsp;     //right hand side value
    Real*      yp;       //multipliers for the constraints, this gurantees,
                         //that they remain in the right order in case of deletions
    Real*      scalep;   //contains scaling value
    Real*      subgradp; //contains the aggregated subgradient
    Real*      lmultp;   //contains the lagrange multipliers to inequality constr.
    char*      del_statp;//flags on whether inequality may be deleted

    int subgrad_available; //flag whether a valid subgradient is stored
    Real subgrad_Cvalue;   // =ip(C,X)

    mutable Matrix tmpmat;
    mutable Matrix tmpvec;
    mutable Symmatrix tmpsym;
    mutable Indexmatrix tmpind;

    Clock myclock;
    Microseconds eigvaltime;  //accumulated time spent in eigenvalue computation
    Microseconds matrixtime;  //accumulated time spent in constructing matrix
    Microseconds subgtime;    //accumulated time spent in computing subgradient
    Integer nr_eval;          //counts calls to obj_compute

    ostream *out;

    void error(const char* s) const;
    
public:
    Problem1();
    ~Problem1();

    Integer nr_constraints() const {return n;}
    Integer mat_dim() const{return dim;}
    
    int provide(Integer k);
        //tries to provide enough storage to store k elements,
        //return 0 on success and 1 on failure.

    void clear();
        //deletes all inequalities but keeps the storage
    
    Coeffmat* operator()(Integer i) const {return ((i>=n)||(i<0))?0:pool[i];}
    char sign(Integer i) const {return signp[i];}
    Real& rhs(Integer i) const {return rhsp[i];}

    void set_C(const Coeffmat* Cin){C=Cin;}
    const Coeffmat* get_C(){return C;}
        
    virtual void get_y(Matrix& y) const {y.init(n,1,yp);}
    virtual Matrix get_y(void) const {return Matrix(n,1,yp);}
    virtual void get_eigval(Matrix& eigv) const {eigv=eigval;}
    virtual Matrix get_eigval(void) const {return Matrix(eigval);}
    virtual void get_eigvecs(Matrix& eigv) const {eigv=eigvecs;}
    virtual Matrix get_eigvecs(void) const {return Matrix(eigvecs);}
    virtual void set_y(const Matrix& y,const Matrix& eigval,const Matrix& eigvec);
    
    void get_rhs(Matrix& rhs) const {rhs.init(n,1,rhsp);}
    Matrix get_rhs(void) const {return Matrix(n,1,rhsp);}
    virtual Real& scale(Integer i) const {return scalep[i];}
    void get_scale(Matrix& s) const {s.init(n,1,scalep);}
    Matrix get_scale(void) const {return Matrix(n,1,scalep);}
    void set_scale(const Matrix& s);
    Real& lmult(Integer i) const {return lmultp[i];}
    void get_lmult(Matrix& s) const {s.init(n,1,lmultp);}
    Matrix get_lmult(void) const {return Matrix(n,1,lmultp);}
    void set_lmult(const Matrix& s);
    char get_del_stat(Integer i) const {return del_statp[i];}
    void set_del_stat(Integer i,char ds) {del_statp[i]=ds;}

    virtual void set_out(ostream* o){out=o;}

    void get_subgrad(Matrix& s) const {s.init(n,1,subgradp);}
    Matrix get_subgrad(void) const {return Matrix(n,1,subgradp);}
    virtual void set_store_symsubg(Integer i) {store_symsubg=i;}
    virtual const Symmatrix& get_symsubg(void) const {return symsubg;}
    virtual void set_store_sparsesubg(Integer i,const Sparsesym& sps)
        {store_sparsesubg=i;sparsesubg.init_support(sps);}
    virtual const Sparsesym& get_sparsesubg(void) const {return sparsesubg;}
    Real get_subgrad_Cvalue() const {return subgrad_Cvalue;}
    int subgrad_is_available() const {return subgrad_available;}
    void clear_subgrad_available() {subgrad_available=0;}
    void init_subgrad(const Matrix& P,const Matrix& d);
    void update_subgrad(const Matrix& P,const Matrix& d,Real alpha);
    
    Real lmaxmult() const {return lmax_multiplier;}
    Real gramip_C(const Matrix& P);
    Real ip_C(const Matrix& P,const Matrix& di);
    void gram_opA(const Matrix& P,Matrix& vec);
    Matrix opA(const Matrix& P,const Matrix& di);
    void opA(const Matrix& P,const Matrix& di,Matrix& vec);

    void compute_sdpqp_coeff(Symmatrix& Q, Matrix& C, Real& a, Real& d,
                             const Matrix& P, Real regparam,
                             int do_scaling=0); 

    void compute_aggregqp_coeff(Symmatrix& Q1, Matrix& Q12, Real& q2,
                                Matrix& C1, Real& c2, Real& a, Real& d,
                                const Matrix& P, Real regparam,
                                int do_scaling=0); 

    void update_sdpqp_coeff(Matrix& dC, Real& dd,
                            const Matrix& P, Real regparam,
                            const Indexmatrix& ind,const Matrix& val,
                            int do_scaling=0);

    void update_aggregqp_coeff(Matrix& dC1, Real& dc2, Real& dd,
                               const Matrix& P, Real regparam,
                               const Indexmatrix& ind,const Matrix& val,
                               int do_scaling=0);

    Matrix rhs(void) const {return Matrix(n,1,rhsp);}
    Real objval() const;
    Real objval(Real maxeigval,const Matrix& yvec);
    Real model_compute(const Matrix& y,const Matrix& primalvecs,
                       Matrix& rayleighval);
    int obj_compute(const Matrix &yvec,Matrix& eigval,Matrix& eigvecs,Real nullstep_bound,Real relprec,Integer& nreig);

    int starting_point(void);
    void set_use_startheur(int ush){use_startheur=ush;}
    
    int set_perm(Integer i){if ((i<0)||(i>n)) return 1; p=i; return 0;}
    Integer get_perm() const {return p;}


    int add(Coeffmat* m,char s,Real rhs,Real scaleval=1.,char del_stat=DEL_STAT_NO); //add a constraint at the end
    int remove(const Indexmatrix& ind);   //delete all constraints indexed

    int constraints_out(ostream& o) const;
    int constraints_in(istream& i);
    int read_problem(istream& i,int do_scaling=0);
    int write_problem(ostream& os) const;
    int read_y(istream& in);
    int write_y(ostream& out) const;

    ostream& save(ostream& out) const;
    istream& restore(istream& in);

    Microseconds get_eigvaltime() const {return eigvaltime;}
    virtual Microseconds get_matrixtime() const {return matrixtime;}
    virtual Microseconds get_subgtime() const {return subgtime;}

    void set_exacteigs(Integer ee){exacteigs=ee;}
    void set_lanczos(Lanczos* lanp){delete lanczos; lanczos=lanp;}
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: problem1.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:40  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/problem1.h,v $
 --------------------------------------------------------------------------- */
