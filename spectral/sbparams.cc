/* -*-c++-*-
 * 
 *    Source     $RCSfile: sbparams.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:41 $ 
 *    Author     Christoph Helmberg
 *    Modified in 2002 by G.Delaporte, S.Jouteau, F.Roupin 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: sbparams.cc,v 1.1.1.1.2.1 2000/02/28 13:55:41 bzfhelmb Exp $"

#include <cstdio>
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
//#include <strstream>
#include <sstream>
#include "sbparams.h"
#include "sparssym.h"
#include "lanczpol.h"
#include "mytau.h"
#include "myinctau.h"
#include "myktau.h"
#include "kiwieltau.h"
#include "kiwielntau.h"
#include "skiwieltau.h"
#include "skiwielntau.h"
#include "fixtau.h"
#include "spectral.h"
#include "updheur.h"
#include "bmaxvecs.h"
#include "expterm.h"
#include "basesolv.h"

#define VERSION_STRING "SBmethod version 1.1.1, Copyright (C) 2000 Christoph Helmberg"

extern int NB_VAR;
extern int SILENCE_MODE;
extern int KILL;
extern int PRINT_DEBUG;
extern ofstream result;
extern char * filename_result;

// *************************************************************************
//                             SBparams
// *************************************************************************

SBparams::SBparams()
{
     termeps=1e-5;
     timelimit=-1;
     maxiter=-1;
     maxkeepvecs=45;
     maxaddvecs=10;
     minkeepvecs=30;
     modeleps=.6;
     aggregtol=.01;
     use_startheur=1;
     initial_scaling=0;
     do_scaling=0;
     mL=0.1;
     mN=0.1;
     mult_stepsize=1.;
     mR=0.5;
     ct_method=4; //Kiwiel with level updates
     fixu=0;
     fixuval=1.;
     umin=-1.;
     umax=-1.;
     terminator=0; 
     exacteigs=0;
     use_cutval=0;
     bundle_update=0;
     lanczossteps=-1;
     chebits=-1;
#ifdef WITH_SEPARATION     
     first_sep=5;
     last_sep=-1;
     step_sep=1;
     sep_mult=3.;
     sepviol=0.01;
     first_sep_relprec=0.1;
#endif
     infile=0;
     instring=0;
     spfile=0;
     yfile=0;
     eigfile=0;
     etafile=0;
     Xfile=0;
	  Xvect=0;
     sXfile=0;
     Pfile=0;
     sysfile=0;
     spsfile=0;
     probfile=0;
     outprob=0;
     outbundle=0;
     savefile=0;
     resume=0;

     reading_parameter_file=0;
}
    
// *************************************************************************
//                               ~SBparams
// *************************************************************************

SBparams::~SBparams()
    {
     delete[] infile;
//     delete[] instring;
     delete[] spfile;
     delete[] yfile;
     delete[] eigfile;
     delete[] Xfile;
//     delete[] Xvect;
     delete[] sXfile;
     delete[] Pfile;
     delete[] sysfile;
     delete[] spsfile;
     delete[] probfile;
     delete[] savefile;
}
    
// *************************************************************************
//                               save
// *************************************************************************

ostream& SBparams::save(ostream& out) const
    {
     out<<termeps<<"\n";
     out<<timelimit<<"\n";
     out<<maxkeepvecs<<"\n";
     out<<maxaddvecs<<"\n";
     out<<minkeepvecs<<"\n";
     out<<maxiter<<"\n";
     out<<modeleps<<"\n";
     out<<aggregtol<<"\n";
     out<<initial_scaling<<"\n";
     out<<use_startheur<<"\n";
     out<<do_scaling<<"\n";
     out<<mL<<"\n";
     out<<mN<<"\n";
     out<<mult_stepsize<<"\n";
     out<<mR<<"\n";
     out<<ct_method<<"\n";
     out<<fixu<<"\n";
     out<<fixuval<<"\n";
     out<<umin<<"\n";
     out<<umax<<"\n";
     out<<terminator<<"\n";
     out<<exacteigs<<"\n";
     out<<use_cutval<<"\n";
     out<<bundle_update<<"\n";
     out<<lanczossteps<<"\n";
     out<<chebits<<"\n";

#ifdef WITH_SEPARATION     
     out<<first_sep<<"\n";
     out<<last_sep<<"\n";
     out<<step_sep<<"\n";
     out<<sep_mult<<"\n";
     out<<sepviol<<"\n";
     out<<first_sep_relprec<<"\n";
#endif
     
     if (infile) {
         out<<strlen(infile)+1<<"\n"<<infile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (spfile) {
         out<<strlen(spfile)+1<<"\n"<<spfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (yfile) {
         out<<strlen(yfile)+1<<"\n"<<yfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (eigfile) {
         out<<strlen(eigfile)+1<<"\n"<<eigfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (etafile) {
         out<<strlen(etafile)+1<<"\n"<<etafile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (Xfile) {
         out<<strlen(Xfile)+1<<"\n"<<Xfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (sXfile) {
         out<<strlen(sXfile)+1<<"\n"<<sXfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (Pfile) {
         out<<strlen(Pfile)+1<<"\n"<<Pfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (sysfile) {
         out<<strlen(sysfile)+1<<"\n"<<sysfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (spsfile) {
         out<<strlen(spsfile)+1<<"\n"<<spsfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (probfile) {
         out<<strlen(probfile)+1<<"\n"<<probfile<<"\n";
     }
     else {
         out<<"0\n";
     }
     if (outprob) out<<"1\n"; else out<<"0\n";
     if (outbundle) out<<"1\n"; else out<<"0\n";
     if (savefile) {
         out<<strlen(savefile)+1<<"\n"<<savefile<<"\n";
     }
     else {
         out<<"0\n";
     }
     return out;
}
    
// *************************************************************************
//                               restore
// *************************************************************************

istream& SBparams::restore(istream& in)
    {
     in>>termeps;
     in>>timelimit;
     in>>maxkeepvecs;
     in>>maxaddvecs;
     in>>minkeepvecs;
     in>>maxiter;
     in>>modeleps;
     in>>aggregtol;
     in>>initial_scaling;
     in>>use_startheur;
     in>>do_scaling;
     in>>mL;
     in>>mN;
     in>>mult_stepsize;
     in>>mR;
     in>>ct_method;
     in>>fixu;
     in>>fixuval;
     in>>umin;
     in>>umax;
     in>>terminator;
     in>>exacteigs;
     in>>use_cutval;
     in>>bundle_update;
     in>>lanczossteps;
     in>>chebits;

#ifdef WITH_SEPARATION          
     in>>first_sep;
     in>>last_sep;
     in>>step_sep;
     in>>sep_mult;
     in>>sepviol;
     in>>first_sep_relprec;
#endif
          
     Integer n;
     in>>n;
     delete[] infile; infile=0;
     if (n) {
         infile=new char[n];
         in>>infile;
     }
     in>>n;
     delete[] spfile; spfile=0;
     if (n) {
         spfile=new char[n];
         in>>spfile;
     }
     in>>n;
     delete[] yfile; yfile=0;
     if (n) {
         yfile=new char[n];
         in>>yfile;
     }
     in>>n;
     delete[] eigfile; eigfile=0;
     if (n) {
         eigfile=new char[n];
         in>>eigfile;
     }
     in>>n;
     delete[] etafile; etafile=0;
     if (n) {
         etafile=new char[n];
         in>>etafile;
     }
     in>>n;
     delete[] Xfile; Xfile=0;
     if (n) {
         Xfile=new char[n];
         in>>Xfile;
     }
     in>>n;
     delete[] sXfile; sXfile=0;
     if (n) {
         sXfile=new char[n];
         in>>Xfile;
     }
     in>>n;
     delete[] Pfile; Pfile=0;
     if (n) {
         Pfile=new char[n];
         in>>Pfile;
     }
     in>>n;
     delete[] sysfile; sysfile=0;
     if (n) {
         sysfile=new char[n];
         in>>sysfile;
     }
     in>>n;
     delete[] spsfile; spsfile=0;
     if (n) {
         spsfile=new char[n];
         in>>spsfile;
     }
     in>>n;
     delete[] probfile; probfile=0;
     if (n) {
         probfile=new char[n];
         in>>probfile;
     }
     in>>n;
     if (n) {
         outprob=&cout;
     }
     else outprob=0;
     in>>n;
     if (n) {
         outbundle=&cout;
     }
     else outbundle=0;
     in>>n;
     delete[] savefile; savefile=0;
     if (n) {
         savefile=new char[n];
         in>>savefile;
     }
     return in;
    }
    
// *************************************************************************
//                               usage
// *************************************************************************

void SBparams::usage()
{
  cout<<"usage: sb [options]         default input for problem description: stdin\n";
  cout<<"\n options:  [-te Real] [-tt Integer] [-td Integer]\n";
  cout<<"     [-bu Integer] [-bk Integer] [-ba Integer] [-bm Integer] [-bt Real]\n";
  cout<<"     [-ln Integer] [-lc Integer]\n";
#ifdef WITH_SEPARATION     
  cout<<"     [-sf Integer] [-sl Integer] [-st Integer] [-sm Real] [-sv Real] [-sr Real]\n";
#endif
  cout<<"     [-sh 0/1] [-si 0/1] [-sc 0/1] \n";
  cout<<"     [-k Real] [-ki Real] [-km Real] [-ke Real] [-e Integer] [-c 0/1]\n"; 
  cout<<"     [-u Integer] [-uk Real] [-uf Real] [-umin Real] [-umax Real]\n";
  cout<<"     [-f filename] [-fp filename] [-fs filename]\n";
  cout<<"     [-oy filename] [-ol filename] [-om filename] [-oP filename] \n";
  cout<<"     [-oas filename] [-oad filename] [-oXs filename] [-oXd filename]\n";
  cout<<"     [-oprob filename]\n";
  cout<<"     [-op 0/1] [-ob 0/1] [-ot Integer]\n";
  cout<<"     [-rf filename]\n";
  cout<<"   (for repeated or contradicting options the last specification is used)\n\n";
  cout<<" termination paramters:\n";
  cout<<"   -te ... relative precision (termination epsilon) [default=1e-5]\n";
  cout<<"   -tt ... timelimit in seconds [default=-1 for no limit]\n";
  cout<<"   -td ... maximum number of descent steps [default=-1 for no limit]\n";
  cout<<" bundle parameters:\n";
  cout<<"   -bu ... method for bundle update [default heuristic=0]\n";
  cout<<"   -bk ... maximum number of vectors kept [default=45]\n";
  cout<<"   -ba ... maximum number of vectors added [default=10]\n";
  cout<<"   -bm ... maximum for minimum number of vectors kept [default=30]\n";
  cout<<"   -bt ... relative aggregation threshold [default=.01]\n";
  cout<<" Lanczos eigenvalue computation:\n";
  cout<<"   -ln ... number of Lanczos steps per Lanczos restart [default=-1]\n";
  cout<<"   -lc ... degree of Chebychev polynomial in matrix multiplication [default=-1]\n";
  cout<<" starting point and scaling heuristics:\n";
  cout<<"   -sh ... use the starting point heuristic [default=0 no]\n"; 
  cout<<"   -si ... scale constraint matrices to norm one on input [default=0 no]\n";
  cout<<"   -sc ... use a scaling heuristic during computation [default=0 no]\n";
  cout<<" step acceptance/rejection parameters:\n"; 
  cout<<"   -k  ... factor for descent steps [0.1]\n";
  cout<<"   -ki ... factor for inexact null steps [0.1]\n"; 
  cout<<"   -km ... model precision (for inequalities only) [default=.6]\n";
  cout<<"   -ke ... convex combination factor for guessing new multipliers [1.]\n";
  cout<<"   -e  ... compute 'number' exact eigenvalue(s) for null steps [default=0 no]\n";
  cout<<"   -c  ... use cutval instead of linval for acceptance criteria [default=0 no]\n";
  cout<<" weight updating:\n";
  cout<<"   -u  ... choice of u: 0 Kiwiel/1 Helmberg/4 HelmbergKiwiel [4]\n";
  cout<<"   -uk ... factor for criterion for decreasing u [0.5]\n";
  cout<<"   -uf ... fix value of u for entire algorithm\n";
  cout<<"   -umin.. min value of u for entire algorithm, <=0 for default bound\n";
  cout<<"   -umax.. max value of u for entire algorithm, <=0 for default bound\n";
#ifdef WITH_SEPARATION     
  cout<<" separation parameters:\n";
  cout<<"   -sf ... first separation after this number of iterations [5]\n";
  cout<<"   -sl ... last separation after this number of iterations [-1]\n";
  cout<<"   -st ... repeat separation each ... iterations [1]\n";
  cout<<"   -sm ... factor times n determines maximum number of ineqs separated [3]*n\n";
  cout<<"   -sv ... separate inequalities violated by more than this value [0.01]\n";
  cout<<"   -sr ... start separation only if relative precision is at least [0.01]\n";
#endif
  cout<<" input files:\n";
  cout<<"   -f  ... file for problem description\n";
  cout<<"   -fp ... parameter file (format: list of command line options)\n";
  cout<<"   -fs ... starting point file (feasible!), format: m 1 y11 y21 ... ym1\n";
  cout<<" output files:\n";
  cout<<"   -oy ... store best y at termination in filename\n";
  cout<<"   -ol ... store computed Lanczosvalues/vectors of this y in filename\n";
  cout<<"   -om ... store last Lagrange multipliers for y in filename\n";
  cout<<"   -oXs... store sparse (pattern of cost matrix) approx of primal X in filename\n";
  cout<<"   -oXd... store dense approximation of primal X in filename\n";
  cout<<"   -oP ... store eigenvalues/vectors of last QSDP solution in filename\n";
  cout<<"   -oas... store sparse aggregate matrix of last solution in filename\n";
  cout<<"   -oad... store aggregate matrix of last solution in filename\n";
  cout<<"   -oprob..write problem description to file\n";
  cout<<" detail of log output:\n";
  cout<<"   -ob ... write log info for bundle method [default=0 no]\n";
  cout<<"   -op ... write log info for problem specific routines [default=0 no]\n";
  cout<<"   -ot  ... termination: output at 0 final precision/1 precision levels [0]\n";
  cout<<" resume file name:\n";
  cout<<"   -rf ... name of the 'save'-file for resuming in case of interruption\n";
  cout<<" or \n";
  cout<<"   sb -resume filename\n";
  cout<<"       ... resumes an old process that has been terminated prematurely\n";
  cout<<"           filename has to refer to the corresponding 'save'-file\n\n";
  cout<<"    SIGTERM  (15)   save resume info, output summary and option files, exit\n";
  cout<<"    SIGUSR1  (10)   output summary and option files, exit\n"; 
  cout<<"    SIGUSR2  (12)   output summary and option files and continue\n";
  cout<<"    SIGKILL  (9)    kill without any further output\n"<<endl;
}
    
// *************************************************************************
//                               message
// *************************************************************************
    
int SBparams::message(const char *s,Integer retcode)
{
  cout<<"input error: "<<s<<endl;
  usage();
  return retcode;
}

// *************************************************************************
//                               input
// *************************************************************************
    
int SBparams::input(Integer argc,char **argv,BaseSolver* basesolver)
{
 
     
  int i=1;
  while(i<argc){
    if ((strcmp(argv[i],"-h")==0)||(strcmp(argv[i],"--help")==0)){
      i++;
      usage();
      return 1;
    }
    if ((strcmp(argv[i],"-v")==0)||(strcmp(argv[i],"--version")==0)){
      i++;
      cout<<VERSION_STRING<<endl;
      return 1;
    }
    if (strcmp(argv[i],"-te")==0){
      i++;
      if (i>=argc) return message("-te lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&termeps);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-tt")==0){
      i++;
      if (i>=argc) return message("-tt lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&timelimit);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-td")==0){
      i++;
      if (i>=argc) return message("-td lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&maxiter);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-bu")==0){
      i++;
      if (i>=argc) return message("-bu lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&bundle_update);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-bk")==0){
      i++;
      if (i>=argc) return message("-bk lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&maxkeepvecs);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ba")==0){
      i++;
      if (i>=argc) return message("-ba lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&maxaddvecs);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-bm")==0){
      i++;
      if (i>=argc) return message("-bm lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&minkeepvecs);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-bt")==0){
      i++;
      if (i>=argc) return message("-bt lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&aggregtol);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ln")==0){
      i++;
      if (i>=argc) return message("-ln lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&lanczossteps);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-lc")==0){
      i++;
      if (i>=argc) return message("-lc lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&chebits);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sh")==0){
      i++;
      if (i>=argc) return message("-sh lacks argument [0/1]",1);
      sscanf(argv[i],"%ld",&use_startheur);
      if ((use_startheur!=0)&&(use_startheur!=1)){
	return message("argument for -sh is not 0 or 1",1);
      }
      i++;
      continue;
    }
    if (strcmp(argv[i],"-si")==0){
      i++;
      if (i>=argc) return message("-si lacks argument [0/1]",1);
      sscanf(argv[i],"%ld",&initial_scaling);
      if ((initial_scaling!=0)&&(initial_scaling!=1)){
	return message("argument for -si is not 0 or 1",1);
      }
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sc")==0){
      i++;
      if (i>=argc) return message("-sc lacks argument [0/1]",1);
      sscanf(argv[i],"%ld",&do_scaling);
      if ((do_scaling!=0)&&(do_scaling!=1)){
	return message("argument for -si is not 0 or 1",1);
      }
      i++;
      continue;
    }
    if (strcmp(argv[i],"-k")==0){
      i++;
      if (i>=argc) return message("-k lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&mL);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ki")==0){
      i++;
      if (i>=argc) return message("-ki lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&mN);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-km")==0){
      i++;
      if (i>=argc) return message("-km lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&modeleps);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ke")==0){
      i++;
      if (i>=argc) return message("-ke lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&mult_stepsize);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-e")==0){
      i++;
      if (i>=argc) return message("-e lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&exacteigs);
      if (exacteigs<0){
	return message("argument for -e must be nonnegative",1);
      }
      i++;
      continue;
    }
    if (strcmp(argv[i],"-c")==0){
      i++;
      if (i>=argc) return message("-c lacks argument [0/1]",1);
      sscanf(argv[i],"%ld",&use_cutval);
      if ((use_cutval!=0)&&(use_cutval!=1)){
	return message("argument for -c is not 0 or 1",1);
      }
      i++;
      continue;
    }
    if (strcmp(argv[i],"-u")==0){
      i++;
      fixu=0;
      if (i>=argc) return message("-u lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&ct_method);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-uk")==0){
      i++;
      if (i>=argc) return message("-uk lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&mR);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-uf")==0){
      fixu=1;
      i++;
      if (i>=argc) return message("-uf lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&fixuval);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-umin")==0){
      i++;
      if (i>=argc) return message("-umin lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&umin);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-umax")==0){
      i++;
      if (i>=argc) return message("-umax lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&umax);
      i++;
      continue;
    }
#ifdef WITH_SEPARATION
    if (strcmp(argv[i],"-sf")==0){
      i++;
      if (i>=argc) return message("-sf lacks argument [Real]",1);
      sscanf(argv[i],"%ld",&first_sep);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sl")==0){
      i++;
      if (i>=argc) return message("-sl lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&last_sep);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-st")==0){
      i++;
      if (i>=argc) return message("-st lacks argument [Integer]",1);
      sscanf(argv[i],"%ld",&step_sep);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sm")==0){
      i++;
      if (i>=argc) return message("-sm lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&sep_mult);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sv")==0){
      i++;
      if (i>=argc) return message("-sv lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&sepviol);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-sr")==0){
      i++;
      if (i>=argc) return message("-sr lacks argument [Real]",1);
      sscanf(argv[i],"%lf",&first_sep_relprec);
      i++;
      continue;
    }
#endif
    if (strcmp(argv[i],"-f")==0){
      i++;
      if (i>=argc) return message("-f",1);
      infile=new char[strlen(argv[i])+1];
      strcpy(infile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-f2")==0){
      i++;
      if (i>=argc) return message("-f2",1);
      instring=new char[strlen(argv[i])+1];
      strcpy(instring,argv[i]);
      instring[strlen(argv[i])] = 0;
      i++;
      continue;
    }
    if (strcmp(argv[i],"-fp")==0){
      i++;
      if (i>=argc) return message("-fp",1);
      if (++reading_parameter_file>10) {
	--reading_parameter_file;
	return message("-fp called recursively more than 10 times",1);
      }
      ifstream parin(argv[i]);
      if (!parin) {
	cout<<"cannot open parameter file "<<argv[i]<<endl;
	--reading_parameter_file;
	return 1;
      }
      char inputstring[400];
      Integer parcnt=1;
      char* parv[100];
      parv[0]=argv[i]; //store filename in first position
      parin>>ws;
      while ((parin)&&(!parin.eof())&&(parcnt<100)) {
	parin>>inputstring>>ws;
	parv[parcnt]=new char[strlen(inputstring)+1];
	strcpy(parv[parcnt],inputstring);
	parcnt++;
      }
      if ((parin)&&(!parin.eof())&&(parcnt>=100)){
	cout<<"too many arguments in parameter file "<<argv[i]<<endl;
	for(Integer j=1;j<parcnt;j++) delete[] parv[j];
	--reading_parameter_file;
	parin.close();
	return message("-fp",1);
      }
      parin.close();
      int errstat=0;
      if ((errstat=input(parcnt,parv,basesolver))){
	cout<<"input error while in parameter file "<<argv[i]<<endl;
	for(Integer j=1;j<parcnt;j++) delete[] parv[j];
	--reading_parameter_file;
	return errstat;
      }
      for(Integer j=1;j<parcnt;j++) delete[] parv[j];
      --reading_parameter_file;
      i++;
      continue;
    }
    if (strcmp(argv[i],"-fs")==0){
      i++;
      if (i>=argc) return message("-fs",1);
      spfile=new char[strlen(argv[i])+1];
      strcpy(spfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oy")==0){
      i++;
      if (i>=argc) return message("-oy",1);
      yfile=new char[strlen(argv[i])+1];
      strcpy(yfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ol")==0){
      i++;
      if (i>=argc) return message("-oe",1);
      eigfile=new char[strlen(argv[i])+1];
      strcpy(eigfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-om")==0){
      i++;
      if (i>=argc) return message("-om",1);
      etafile=new char[strlen(argv[i])+1];
      strcpy(etafile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oXs")==0){
      i++;
      if (i>=argc) return message("-oXs",1);
      sXfile=new char[strlen(argv[i])+1];
      strcpy(sXfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oXd")==0){
      i++;
      if (i>=argc) return message("-oXd",1);
      Xfile=new char[strlen(argv[i])+1];
      strcpy(Xfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oXd2")==0){
      i++;
      Xvect=(Real *)0X01;
      continue;
    }
    if (strcmp(argv[i],"-oP")==0){
      i++;
      if (i>=argc) return message("-oP",1);
      Pfile=new char[strlen(argv[i])+1];
      strcpy(Pfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oas")==0){
      i++;
      if (i>=argc) return message("-oas",1);
      spsfile=new char[strlen(argv[i])+1];
      strcpy(spsfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oad")==0){
      i++;
      if (i>=argc) return message("-oad",1);
      sysfile=new char[strlen(argv[i])+1];
      strcpy(sysfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-oprob")==0){
      i++;
      if (i>=argc) return message("-oprob",1);
      probfile=new char[strlen(argv[i])+1];
      strcpy(probfile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ob")==0){
      i++;
      int dummy;
      if (i>=argc) return message("-op lacks argument [0/1]",1);
      sscanf(argv[i],"%d",&dummy);
      if ((dummy!=0)&&(dummy!=1)){
	return message("argument for -op is not 0 or 1",1);
      }
      if (dummy) outbundle=&cout;
      else outbundle=0;
      i++;
      continue;
    }
    if (strcmp(argv[i],"-op")==0){
      i++;
      int dummy;
      if (i>=argc) return message("-op lacks argument [0/1]",1);
      sscanf(argv[i],"%d",&dummy);
      if ((dummy!=0)&&(dummy!=1)){
	return message("argument for -op is not 0 or 1",1);
      }
      if (dummy) outprob=&cout;
      else outprob=0;
      i++;
      continue;
    }
    if (strcmp(argv[i],"-ot")==0){
      i++;
      if (i>=argc) return message("-ot",1);
      sscanf(argv[i],"%ld",&terminator);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-rf")==0){
      i++;
      if (i>=argc) return message("-rf",1);
      savefile=new char[strlen(argv[i])+1];
      strcpy(savefile,argv[i]);
      i++;
      continue;
    }
    if (strcmp(argv[i],"-resume")==0){
      i++;
      if (i>=argc) return message("-resume",1);
      resume=new char[strlen(argv[i])+1];
      strcpy(resume,argv[i]);
      if (argc>3) return message("",1);
      i++;
      break;
    }
    int oldi=i;
    if (basesolver->read_next_param(argc,argv,i)){
      return message(argv[oldi],1);
    }
    if (i>oldi) continue;
    return message(argv[i],1);
  }
  
  return 0;
}
    
// *************************************************************************
//                               initialize
// *************************************************************************
        
int SBparams::initialize(Problem* problem,SpectralBundle* bundle,Clock* myclock,
                         BaseSolver* basesolver)
{

  cout<<VERSION_STRING<<endl;
  cout<<"SBmethod comes with ABSOLUTELY NO WARRANTY"<<endl;
  cout << endl << endl;
 if(PRINT_DEBUG) cout<<"\nParameter settings:"<<endl;
 if (termeps<=0){
   if(PRINT_DEBUG) cout<<"WARNING: termeps= "<<termeps<<"<=0; using 1e-5 instead"<<endl;
   termeps=1e-5;
 }
 if(PRINT_DEBUG) cout<<"-te : termeps   = "<<termeps<<endl;
 if (timelimit>0){
   if(PRINT_DEBUG) cout<<"-tt : timelimit = "<<timelimit<<endl;
 }
 else {
   if(PRINT_DEBUG) cout<<"-tt : timelimit = "<<timelimit<<" (no timelimit)"<<endl;
 }
 if (maxiter>0) {
   if(PRINT_DEBUG) cout<<"-td : maxiter   = "<<maxiter <<endl;
 }
 else {
   if(PRINT_DEBUG) cout<<"-td : maxiter   = "<<maxiter<<" (no limit on descent steps)"<<endl;
 }
 if(PRINT_DEBUG) cout<<"-bu : bundleupd = "<<bundle_update<<" -> ";
 SBupdate_bundle* ub;
 switch(bundle_update){
 case 0: ub=new SBupdate_heuristic;
     if(PRINT_DEBUG) cout<<"heuristic choice of bundle size, uses dual cost elimination"<<endl;
     ub->set_out(outbundle);
     break;
 case 1: ub=new SBupdate_bundle;
     if(PRINT_DEBUG) cout<<"standard dual cost elimination to n_K"<<endl;
     ub->set_out(outbundle);
     break;
 case 2: ub=new SBupdate_maxvecs;
     if(PRINT_DEBUG) cout<<"sort bundle and new vectors by rayleighval"<<endl;
     ub->set_out(outbundle);
     break;
 default: if(PRINT_DEBUG) cout<<" no bundle update method -bu "<<bundle_update<<endl;
     return message("",1);
 }
 bundle->set_update_bundle(ub);   
 if (maxkeepvecs<0){
   if(PRINT_DEBUG) cout<<"WARNING: maxkeepv= "<<maxkeepvecs<<"<0; using 0 instead"<<endl;
   maxkeepvecs=0;
 } 
 if(PRINT_DEBUG) cout<<"-bk : maxkeepv  = "<<maxkeepvecs<<endl;
 ub->set_maxkeepvecs(maxkeepvecs);
 if (maxaddvecs<1){
   if(PRINT_DEBUG) cout<<"WARNING: maxaddv= "<<maxaddvecs<<"<0; using 0 instead"<<endl;
   maxaddvecs=0;
 } 
 if(PRINT_DEBUG) cout<<"-ba : maxaddv   = "<<maxaddvecs<<endl;
 ub->set_maxaddvecs(maxaddvecs);
 if (minkeepvecs<0){
   if(PRINT_DEBUG) cout<<"WARNING: minkeepv= "<<minkeepvecs<<"<0; using 0 instead"<<endl;
   minkeepvecs=0;
 } 
 if (minkeepvecs>maxkeepvecs){
   if(PRINT_DEBUG) cout<<"WARNING: minkeepv= "<<minkeepvecs<<"> maxkeepvecs; using "<<maxkeepvecs<<" instead"<<endl;
   minkeepvecs=maxkeepvecs;
 } 
 if(PRINT_DEBUG) cout<<"-bk : minkeepv  = "<<minkeepvecs<<endl;
 ub->set_minkeepvecs(minkeepvecs);
 if ((aggregtol<0)||(aggregtol>1.)){
   if(PRINT_DEBUG) cout<<"WARNING: aggregtol= "<<aggregtol<<" not in [0,1]; using 0.01 instead"<<endl;
   aggregtol=0.01;
 }
 if(PRINT_DEBUG) cout<<"-bt : aggregtol = "<<aggregtol<<endl;
 ub->set_aggregtol(aggregtol);
 if(PRINT_DEBUG) cout<<"-ln : lanczosst = "<<lanczossteps<<endl;
 if(PRINT_DEBUG) cout<<"-lc : chebits   = "<<chebits<<endl;
 Lanczos* lanczos=new Lanczpol;
 lanczos->set_nblockmult(lanczossteps);
 lanczos->set_nchebit(chebits);
 lanczos->set_retlanvecs(maxaddvecs);
 problem->set_lanczos(lanczos);
 if(PRINT_DEBUG) cout<<"-sh : usestarth = "<<use_startheur<<endl;
 problem->set_use_startheur(use_startheur);
 if(PRINT_DEBUG) cout<<"-si : initialsc = "<<initial_scaling<<endl;
 if(PRINT_DEBUG) cout<<"-sc : do_scaling= "<<do_scaling<<endl;
 
 if ((mL<=0)||(mL>=1.)){
   if(PRINT_DEBUG) cout<<"WARNING: kappa= "<<mL<<" not in (0,1); using 0.1 instead"<<endl;
   mL=0.1;
 }
 if(PRINT_DEBUG) cout<<"-k  : kappa     = "<<mL<<endl;
 if ((mN<mL)||(mN>=1)){
   if(PRINT_DEBUG) cout<<"WARNING: bar kappa= "<<mN<<" not in [kappa,1); using kappa instead"<<endl;
   mN=mL;
 }
 if(PRINT_DEBUG) cout<<"-ki : bar kappa = "<<mN<<endl;
 if (modeleps<=0){
   if(PRINT_DEBUG) cout<<"WARNING: kappa_M= "<<modeleps<<"<0; using "<<1-mL<<" instead"<<endl;
   modeleps=1.-mL;
 }
 if(PRINT_DEBUG) cout<<"-km : kappa_M   = "<<modeleps<<endl;
 if (modeleps>1.-mL){
   if(PRINT_DEBUG) cout<<"WARNING: kappa_M= "<<modeleps<<">1-kappa may cause null steps due to model imprecision"<<endl;
 }
 if ((mult_stepsize<0)||(mult_stepsize>1.)){
   if(PRINT_DEBUG) cout<<"WARNING: bar kappa_eta= "<<mult_stepsize<<" not in [0,1]; using 1. instead"<<endl;
   mult_stepsize=1.;
 }
 if(PRINT_DEBUG) cout<<"-ke : kappa_eta = "<<mult_stepsize<<endl;
 if(PRINT_DEBUG) cout<<"-e  : exacteigs = "<<exacteigs<<endl;
 if(PRINT_DEBUG) cout<<"-c  : usecutval = "<<use_cutval<<endl;
 if(PRINT_DEBUG) cout<<"-u  : u method  = "<<ct_method<<" -> ";
 SBchoosetau* sbct;
 if (fixu){
   if (fixuval<1e-10) { 
     if(PRINT_DEBUG) cout<<" fixuval= "<<fixuval<<"<1e-10, using 1. instead";
     fixuval=1.;
   }
   sbct=new Fixtau(fixuval);
   if(PRINT_DEBUG) cout<<"-uf : fixed u   = "<<fixuval<<endl;
 }
 else {
     switch(ct_method){
     case 0: sbct=new Kiwieltau(mR);
         sbct->set_out(outbundle);
         if(PRINT_DEBUG) cout<<"Kiwiel90"<<endl;
         break;
     case 1: sbct=new Mytau(mR,5,5);
         if(PRINT_DEBUG) cout<<"Helmberg starting with ||g||/sqrt(m)"<<endl;
         sbct->set_out(outbundle);
         break;
     case 2: sbct=new Myktau(mR,5,5);
         if(PRINT_DEBUG) cout<<"Helmberg starting with ||g||"<<endl;
         sbct->set_out(outbundle);
         break;
     case 3: sbct=new SmallKiwieltau(mR);
         if(PRINT_DEBUG) cout<<"Helmberg starting with ||g||/sqrt(m)"<<endl;
         sbct->set_out(outbundle);
         break;
     case 4: sbct=new Kiwielntau(mR);
         if(PRINT_DEBUG) cout<<"Kiwiel90 with  bar f_k^* "<<endl;
         sbct->set_out(outbundle);
     break;
     case 5: sbct=new SKiwielntau(mR);
         if(PRINT_DEBUG) cout<<"Kiwiel90 with  bar f_k^* starting with ||g||/sqrt(m)"<<endl;
         sbct->set_out(outbundle);
     break;
     case 6: sbct=new Myinctau(mR,5,5);
         if(PRINT_DEBUG) cout<<"Helmberg starting with ||g||/sqrt(m) and *2 after 100 null steps"<<endl;
         sbct->set_out(outbundle);
     break;
     default: if(PRINT_DEBUG) cout<<" no method -u "<<ct_method<<" for choosing u"<<endl;
         return message("",1);
     }
 }

 if ((mR<mL)||(mR>=1)){
   if(PRINT_DEBUG) cout<<"WARNING: kappa_R= "<<mR<<" not in [kappa,1); using (kappa+1)/2 instead"<<endl;
   mR=(mL+1.)/2.;
 } 
 if(PRINT_DEBUG) cout<<"-uk : kappa_R   = "<<mR<<endl;
 if ((umin>0)&&(umax>0)&&(umin>umax)){
   if(PRINT_DEBUG) cout<<"WARNING: umin= "<<umin<<">"<<umax<<" =umax; using default -1 for both"<<endl;
   umin=-1.;
   umax=-1.;
 }
 if(PRINT_DEBUG) cout<<"-umin: umin     = "<<umin<<endl;
 sbct->set_taumin(umin);
 if(PRINT_DEBUG) cout<<"-umax: umax     = "<<umax<<endl;
 sbct->set_taumax(umax);
#ifdef WITH_SEPARATION     
 if(PRINT_DEBUG) cout<<"-sf : first_sep = "<<first_sep<<endl;
 if(PRINT_DEBUG) cout<<"-sl : last_sep  = "<<last_sep<<endl;
 if(PRINT_DEBUG) cout<<"-st : step_sep  = "<<step_sep<<endl;
 if(PRINT_DEBUG) cout<<"-sm : sep_mult  = "<<sep_mult<<endl;
 if(PRINT_DEBUG) cout<<"-sv : sepviol   = "<<sepviol<<endl;
 if(PRINT_DEBUG) cout<<"-sr : fs_relprec= "<<first_sep_relprec<<endl;
#endif

 if(PRINT_DEBUG) cout<<"-f  : prob-file = ";
 if (infile) if(PRINT_DEBUG) cout<<infile<<endl;
 else if(PRINT_DEBUG) cout<<" stdin"<<endl;
 if(PRINT_DEBUG) cout<<"-fs : start-y   = ";
 if (spfile) if(PRINT_DEBUG) cout<<spfile<<endl;
 else if(PRINT_DEBUG) cout<<" 0-vector"<<endl;
 if(PRINT_DEBUG) cout<<"-ot : terminat  = "<<terminator<<" -> ";
 SBterminator* sbt;
 Microseconds tl;
 if (timelimit<0) tl.set_infinity(true);
 else tl=Microseconds(timelimit);
 switch(terminator){
 case 0: sbt=new SBterminator(termeps,myclock,tl);
     if(PRINT_DEBUG) cout<<"Kiwiel90"<<endl;
     break;
 case 1: sbt=new Expterminator(termeps,myclock,tl);
     if(PRINT_DEBUG) cout<<"Kiwiel90 with intermediate precision output"<<endl;
     break;
 default: if(PRINT_DEBUG) cout<<" no terminator -t "<<terminator<<endl;
     return message("",1);
 }
      
 if (resume==0){
     istream *in;
     ifstream fin;
     if (infile){
         fin.open(infile);
         if (!fin) return message("cannot open input file",2);
         in=&fin;
     }
     else in=&cin;
     if (basesolver->read(*in)){
         cerr<<"    Failure in reading the solver data, terminating"<<endl;
         return 1;
     }
     if (problem->read_problem(*in,initial_scaling)){
         cerr<<"    Failure in reading the problem data, terminating"<<endl;
         return 1;
     }
     if (infile){
         fin.close();
     }
     if (spfile){
         fin.open(spfile);
         if (!fin) return message("cannot open starting point file",2);
         if (problem->read_y(fin)){
             cerr<<"  Failed in reading the starting point from "<<spfile<<endl;
             return 1;
         }
         fin.close();
     }
     if ((sysfile)||(Xfile)){
         problem->set_store_symsubg(1);
     }
     if ((spsfile)||(sXfile)){
         if (problem->get_C()->sparse()){
             Indexmatrix I,J;
             Matrix val;
             problem->get_C()->sparse(I,J,val);
             Integer dim=problem->get_C()->dim();
             Integer nz=I.dim();
             problem->set_store_sparsesubg(1,Sparsesym(dim,nz,I,J,val));
         }
         else {
	   problem->set_store_symsubg(1);
	   if (sysfile==0){
	     sysfile=spsfile;
	     spsfile=0;
	   }
	   else {
             delete[] spsfile;
             spsfile=0;
	   }
	   if (Xfile==0){
	     Xfile=sXfile;
             sXfile=0;
	   }
	   else {
	     delete[] sXfile;
	     sXfile=0;
	   }
         }
     }
 }
 if (exacteigs>=0) problem->set_exacteigs(exacteigs);

 bundle->set_modeleps(modeleps);
 //bundle->set_timelimit(tl);
 bundle->set_do_scaling(do_scaling);
 //bundle->set_maxiter(maxiter);
 bundle->set_mL(mL);
 bundle->set_mN(mN);
 bundle->set_use_linval(Integer(use_cutval==0));
 bundle->set_mult_stepsize(mult_stepsize);
 bundle->set_choose_tau(sbct);
 bundle->set_terminator(sbt);

 problem->set_out(outprob);
 bundle->set_out(outbundle);
 bundle->set_clock(*myclock);
 myclock->start();

 return 0;
}


// *************************************************************************
//                               output
// *************************************************************************
        
int SBparams::output(const Problem* problem,const SpectralBundle* bundle,const Clock* myclock)
{
  int i;
  int dim;



// Save X
     
    Matrix tmpmat(bundle->get_primalvecs());
    Matrix tmpvec(bundle->get_primaleigs());
    tmpvec.sqrt();
    tmpmat.scale_cols(tmpvec);
    
    Symmatrix sg(problem->get_symsubg(),bundle->get_s());
    rankadd(tmpmat,sg,1.,1.);
    dim = sg.dim();
    Xvect = (Real *)malloc(dim*sizeof(Real));
    for(i=0;i<dim;i++) Xvect[i] = sg(i,NB_VAR);
 
  //--- output
  ofstream tuptuo;
  cout<<">>>"; bundle->print_full_summary(tuptuo);

  if(!SILENCE_MODE)
    {
      
      myclock->elapsed_time(cout);cout<<"\n"<<endl;
      
      if (yfile){
	Matrix y;
	problem->get_y(y);
	ofstream out;
	out.open(yfile);
	out.precision(20);
	out<<y;
	out.close();
      }
	
      if (etafile){
	Matrix lmult;
	ofstream out;
	out.open(etafile);
	out.precision(20);
	out<<problem->get_lmult();
	out.close();
      }
	
      if (eigfile){
	Matrix eigval,eigvec;
	ofstream out;
	out.open(eigfile);
	problem->get_eigval(eigval);
	out.precision(20);
	out<<eigval;
	problem->get_eigval(eigvec);
	out<<eigvec;
	out.close();
      }
	
      if (Xfile && strcmp(Xfile,"_")!=0){
	ofstream out;
	out.open(Xfile);
	out.precision(20);
	Matrix tmpmat(bundle->get_primalvecs());
	Matrix tmpvec(bundle->get_primaleigs());
	tmpvec.sqrt();
	tmpmat.scale_cols(tmpvec);
	
	Symmatrix sg(problem->get_symsubg(),bundle->get_s());
	rankadd(tmpmat,sg,1.,1.);
	out<<sg;
	out.close();
      }
	
      if (sXfile){
	ofstream out;
	out.open(sXfile);
	Matrix tmpmat(bundle->get_primalvecs());
	Matrix tmpvec(bundle->get_primaleigs());
	tmpvec.sqrt();
	tmpmat.scale_cols(tmpvec);
	     
	Sparsesym sg(problem->get_sparsesubg(),bundle->get_s());
	support_rankadd(tmpmat,sg,1.,1.);
	out<<sg;
	out.close();
      }
	 
      if (Pfile){
	ofstream out;
	out.open(Pfile);
	out.precision(20);
	out<<bundle->get_primaleigs();
	out<<bundle->get_primalvecs();
	out.close();
      }
	 
      if (sysfile){
	ofstream out;
	out.open(sysfile);
	out.precision(20);
	out<<problem->get_symsubg();
	out<<bundle->get_s()<<"\n";
	out.close();
      }
	 
	 
      if (spsfile){
	ofstream out;
	out.open(spsfile);
	out.precision(20);
	out<<problem->get_sparsesubg();
	out<<bundle->get_s()<<"\n";
	out.close();
      }
	 
      if (probfile){
	ofstream out;
	out.open(probfile);
	out.precision(20);
	problem->write_problem(out);
	out.close();
      }
      if(Xvect && KILL)
       {
	cout << "Vector x:" << endl;
	cout.precision(6);
	cout.width(6);
	for(i=0;i<dim-2;i++)
	{
	  cout << Xvect[i] << " ";
	  if(!((i+1)%5)) cout << endl;
	}  
	 if (filename_result)
	   {
	     result << endl;
	     result << "Vector x:" << endl;
 	     result.precision(6);
	     result.width(6);
	     for(i=0;i<dim-2;i++)
	     {
  	        result << Xvect[i] << " ";
	        if(!((i+1)%5)) result << endl;
	     }	  
     	     result << endl; 
          }    
	cout << endl;
       }      
  }
 
  return 0;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: sbparams.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:41  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/sbparams.cc,v $
 --------------------------------------------------------------------------- */
