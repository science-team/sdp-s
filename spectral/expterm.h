/* -*-c++-*-
 * 
 *    Source     $RCSfile: expterm.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:50 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __EXPTERM_H__
#define __EXPTERM_H__

#include "spectral.h"

class Expterminator: public SBterminator
{
private:
    Real next_eps;
    int next_cnt;

    virtual int check_termination(SpectralBundle* sb);
public:
    Expterminator(Real teps):SBterminator(teps)
    {next_eps=0.1;next_cnt=0;}
    Expterminator(Real teps,const Clock* cp,Microseconds tl):
    SBterminator(teps,cp,tl){next_eps=0.01;next_cnt=0;}
    virtual ~Expterminator(){}

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: expterm.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/expterm.h,v $
 --------------------------------------------------------------------------- */
