/* -*-c++-*-
 * 
 *    Source     $RCSfile: sdpqp.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:43 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SDPQP_H__
#define __SDPQP_H__

#include "memarray.h"
#include "symmat.h"

//   for given Q>=0, C (symmetric matrix in svec form) and a>0 solve
//
//   min    max  <1/2 Q X,X>+<sveci(C),X>+<a-<I,X>,u> + d
//   X>=0    u


class Sdpqp
{
 private:
    //problem description
    Integer dim;       //size of X, original size, number of constraints
    Symmatrix Q;      
    Matrix C;
    Real a;
    Real dcoeff;
    Matrix svecI;      //svec of the identity

    //Variables
    Symmatrix X;     //primal matrix variable
    Symmatrix Z;     //dual matrix slack
    Real u;          //dual to tr(X)=n

    Real primalval;
    Real dualval;
    
    //delta-Variables
    Symmatrix dX;     //primal matrix variable
    Symmatrix dZ;     //dual matrix slack
    Real du;
    //second set for predictor corrector
    Symmatrix dX2;
    Symmatrix dZ2;
    Real du2;

    //"constants"
    Symmatrix Xi;    //inverse of X
    Matrix FZ;       //Z-infeasibility
    Real   FP;       //primal trace-infeasibility
    Matrix FZmuI;
    Matrix GiI;

    //step-sizes and barrier parameter
    Real alphap;     //primal step-size
    Real alphad;     //dual step-size
    Real mu;         //barrier paramter
    Real tol;        //tolerance for distance to boundary depending on mu     

    //temporary variables
    Symmatrix bigtmp; 
    Matrix tmpsvec;
    Symmatrix symtmp;

    //termination parameters
    Real termeps;     //relative precision termination criterion
    Integer maxiter;     //stop after this number of iterations
    Real lowerbound;
    Real upperbound;

    //meta-variables
    Integer iter;
    int status;       //is also returned by solve
                      //0 ... terminated correctly
                      //1 ... maximum number of iterations exceeded
                      //2 ... some error occured

    //output variables
    ostream* out;

    int error(const char *s) const;

    void init_variables();
    int pd_line_search(Symmatrix& A,Symmatrix& dA,Real& stepsize,Real upper_bound);
    void choose_mu();
    int search_direction();
    int pred_corr();
    void line_search_and_update();
    int iterate();
   

 public:
    Sdpqp(){set_defaults();}
    ~Sdpqp(){}

    void set_defaults();
    void init_size(Integer maxdim);
    
    void set_termbounds(Real lb,Real ub){lowerbound=lb;upperbound=ub;}
    void set_termeps(Real te){termeps=te;}
    void set_maxiter(Integer mi){maxiter=mi;}
    void set_out(ostream* o){out=o;}

    void get_X(Symmatrix &Xout) const {Xout=X;}
    void get_Z(Symmatrix &Zout) const {Zout=Z;}
    Real get_u() const {return u;}
    Real get_gap() const {return ip(X,Z);}
    
    const Symmatrix& get_Q() const {return Q;}
    const Matrix& get_C() const {return C;}
    Real get_a() const {return a;}
    Real get_dcoeff() const {return dcoeff;}
  
    Integer get_iter() const {return iter;}
    Integer get_status() const {return status;}
    Real get_termeps() const {return termeps;}
    Integer get_maxiter() const {return maxiter;}

    Real get_primalval()const {return primalval;}
    Real get_dualval()const {return dualval;}

    int solve(const Symmatrix& Q,const Matrix& C,Real a,Real dco);
    // returns status
    
    int update(const Matrix& dC,Real dco);
    // solves the system for C+=dC and returns status
    
    void compare(const Symmatrix& Q,const Matrix& C,Real a,Real dco);

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: sdpqp.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:43  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/sdpqp.h,v $
 --------------------------------------------------------------------------- */
