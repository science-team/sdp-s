/* -*-c++-*-
 * 
 *    Source     $RCSfile: ineqheap.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: ineqheap.cc,v 1.1.1.1 1999/12/15 13:47:52 bzfhelmb Exp $"

#include <cstdlib>
#include <cmath>
#include "mymath.h"
#include "heapsort.h"
#include "ineqheap.h"
#include "coeffmat.h"

void Violation::error(const char *s)
{
 cout<<"Violation ERROR: "<<s<<endl;
 exit(1);
}
    
Real Violation::eval(const Coeffmat& M,char sense,Real rhs)
{ //computes slack, negative if violated
 if (Xp==0) error("Xp is not initialized");
 Real scale=M.norm();
 if (sense==GEQ) return (M.ip(*Xp)-rhs)/scale;
 return (rhs-M.ip(*Xp))/scale;
}

void Backtrack::error(const char *s)
{
 cout<<"Backtrack ERROR: "<<s<<endl;
 exit(1);
}
    
Real Backtrack::eval(const Coeffmat& M,char sense,Real rhs)
{ 
 if ((Xp==0)||(DXp==0)) error("Xp or DXp is not initialized");
 Real val=rhs-M.ip(*Xp);
 if (((sense==GEQ)&&(val<=0.))
     ||((sense==LEQ)&&(val>=0.)))  return 1.;
 return val/M.ip(*DXp);
}

void Ineqheap::error(const char *s)
{
 cout<<"Ineqheap ERROR: "<<s<<endl;
 exit(1);
}

void Ineqheap::first_init()
{
 matrpmem=0;
 sensemem=0;
 indmem=0; 
 valmem=0; 
 size=0;
 matrp=0;
 minviol=0;
 sense=0;
 ind=0; 
 val=0;
 cnt=0;
 evalp=0;
} 

Ineqheap::~Ineqheap()
{
 delete[] matrp;
 delete[] sense;
 delete[] ind;
 delete[] val;
}

int Ineqheap::init(Integer insize,Eval* inevalp,Real inminviol,int delfl)
{
 delflag=delfl;
 minviol=min(0.,inminviol);
 size=max(insize,Integer(0));
 if (matrpmem<size){
     delete[] matrp;
     matrp=new Coeffmat*[size];
     if (matrp==0) error("not enough mem allocating matrp");
     matrpmem=size;
 }
 rhsvec.newsize(size,1);
 if (sensemem<size){
     delete[] sense;
     sense=new char[size];
     if (sense==0) error("not enough mem allocating sense");
     sensemem=size;
 }
 if (indmem<size){
     delete[] ind;
     ind=new Integer[size];
     if (ind==0) error("not enough mem allocating ind");
     indmem=size;
 }
 if (valmem<size){
     delete[] val;
     val=new Real[size];
     if (val==0) error("not enough mem allocating val");
     valmem=size;
 }
 evalp=inevalp;
 if (evalp==0) return 0;

 //--- initialize arrays and variables
 cnt=0;
 Integer i;
 for(i=0;i<size;i++){
     matrp[i]=0;
     sense[i]=0;
     val[i]=Real(minviol+size-i);    //this way smaller indices drop out first
     ind[i]=i;
 }
 build_heap(size,ind,val);
 return 0;
}

Real Ineqheap::eval(const Coeffmat& M,char sen,Real rhs)
{
 if (evalp==0) error("eval called with evalp not initialized");
 return evalp->eval(M,sen,rhs);
}

int Ineqheap::check(const Coeffmat& M,char sen,Real rhs,Real* result)
{
 if (evalp==0) error("check called with evalp not initialized");
 Real res=evalp->eval(M,sen,rhs);
 if (result!=0) *result=res;
 if (size==0) return 0;
 return (res<min(minviol,val[*ind]));
}
 
int Ineqheap::add(Coeffmat* M,char sen,Real rhs,Real* result)
{
 if (evalp==0) error("add called with evalp not initialized");

 //--- check wether the constraint is better than the currently worst in the heap
 Real res=evalp->eval(*M,sen,rhs);
 if (result!=0) *result=res;
 if ((size==0)||(res>=min(minviol,val[*ind]))) {if (delflag) delete M; return 0;}

 //--- in some cases it might be wise to check whether the inequality is
 //    not already in the heap! So far this is not implemented.

 //--- eliminate currently worst, replace by new, and rebuild heap
 if (delflag) delete matrp[*ind];
 Real nrm=M->norm();
 M->multiply(1./nrm);
 matrp[*ind]=M;
 rhsvec(*ind)=rhs/nrm;
 sense[*ind]=sen;
 val[*ind]=res;
 heapify(0,size,ind,val);
 cnt++;
 //cout<<"\nADDED "<<cnt<<": rhs="<<rhs<<" sense="<<sen;
 //cout<<" val="<<res<<" minv="<<min(minviol,val[*ind])<<endl;
 //M->display(cout);
 return 1;
}
 
int Ineqheap::add(Coeffmat* M,char sen,Real rhs,Real cheat)
{
 //--- check wether the constraint is better than the currently worst in the heap
 Real res=cheat;
 if ((size==0)||(res>=min(minviol,val[*ind]))) {if (delflag) delete M; return 0;}

 //--- in some cases it might be wise to check whether the inequality is
 //    not already in the heap! So far this is not implemented.

 //--- eliminate currently worst, replace by new, and rebuild heap
 if (delflag) delete matrp[*ind];
 Real nrm=M->norm();
 M->multiply(1./nrm);
 matrp[*ind]=M;
 rhsvec(*ind)=rhs/nrm;
 sense[*ind]=sen;
 val[*ind]=res;
 heapify(0,size,ind,val);
 cnt++;
 //cout<<"\nADDED "<<cnt<<": rhs="<<rhs<<" sense="<<sen;
 //cout<<" val="<<res<<" minv="<<min(minviol,val[*ind])<<endl;
 //M->display(cout);
 return 1;
}
 
Real Ineqheap::checkval()
{
 if (size==0) return 1.;
 return min(minviol,val[*ind]);
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: ineqheap.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/ineqheap.cc,v $
 --------------------------------------------------------------------------- */
