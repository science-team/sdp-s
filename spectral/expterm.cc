/* -*-c++-*-
 * 
 *    Source     $RCSfile: expterm.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:49 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: expterm.cc,v 1.1.1.1 1999/12/15 13:47:49 bzfhelmb Exp $"

#include "expterm.h"
#include "mymath.h"

int Expterminator::check_termination(SpectralBundle* sb)
{
 if (save_function) (*save_function)(sb);
 if ((sb->get_suminnerit()<10)&&(sb->get_norm2subg()>0.1)) return 0;

 Real rel_prec=(sb->get_oldval()-sb->get_modelval())/(my_abs(sb->get_oldval())+1.);
 while (next_eps>=rel_prec){
      cout<<"::: ";cout.precision(6);cout<<next_eps<<"\n";
      sb->print_full_summary(cout);          
      next_cnt++;
      if (next_cnt%2) {
          next_eps/=2.;
      }
      else{
          next_eps/=5.;
      }
 }

 terminated=(sb->get_oldval()-sb->get_modelval()<=
             termeps*(my_abs(sb->get_oldval())+1.));
 if (clockp)
     terminated|=2*(clockp->time()>=timelimit);
 if (recomplimit>=0)
     terminated|=4*(recomplimit<=sb->get_sumrecomp());
 if (qsdpfailslimit>=0)
     terminated|=8*(qsdpfailslimit<=sb->get_sumqsdpfails());
 if (augvalfailslimit>=0)
   terminated|=16*(augvalfailslimit<=sb->get_sumaugvalfails());
 return terminated;
}

ostream& Expterminator::save(ostream& out) const
{
 out.precision(20);
 out<<termeps<<"\n";
 out<<timelimit<<"\n";
 out<<recomplimit<<"\n";
 out<<qsdpfailslimit<<"\n";
 out<<augvalfailslimit<<"\n";
 out<<terminated<<"\n";
 out<<next_eps<<"\n";
 out<<next_cnt<<"\n";
 return out;
}

istream& Expterminator::restore(istream& in)
{
 in>>termeps;
 in>>timelimit;
 in>>recomplimit;
 in>>qsdpfailslimit;
 in>>augvalfailslimit;
 in>>terminated;
 in>>next_eps;
 in>>next_cnt;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: expterm.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/expterm.cc,v $
 --------------------------------------------------------------------------- */
