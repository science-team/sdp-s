/* -*-c++-*-
 * 
 *    Source     $RCSfile: bmaxvecs.cc,v $ 
 *    Version    $Revision: 1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:36 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: bmaxvecs.cc,v 1.1.2.1 2000/02/28 13:55:36 bzfhelmb Exp $"

#include <cmath>
#include "bmaxvecs.h"
#include "mymath.h"
#include "heapsort.h"

// *****************************************************************************
//                               update_bundle
// *****************************************************************************

// First the bundlevectors are set to the vectors generated from the
// primal optimal solution (primalvecs). Vectors whose contribution
// is relatively small will be aggregated to keep the bundle small.
// If too many vectors are important, then the least important ones
// are aggregated to ensure that at most maxkeepvecs vectors remain.
// Afterwards at most maxaddvecs vectors obtained by the last
// function evaluation are added to the bundle to improve the model.
// Finally the vectors of the bundle are orthognoalized.
// input: eigvecs, primalvecs,primaleigs, s
// output: bundlevecs

void SBupdate_maxvecs::update_bundle(SpectralBundle* sb,
				     SpectralBundleProblem *problem,
          Matrix& bundlevecs, const Matrix& eigvecs, const Matrix& eigvals,
	  Matrix& primalvecs, Matrix& primaleigs,Matrix& rayleighval, 
          Real& s)
{
 maxaddvecs=min(maxaddvecs,max(primalvecs.rowdim()/4,Integer(1)));
 maxkeepvecs=min(maxkeepvecs,primalvecs.rowdim()-maxaddvecs);
 if (out){
     (*out)<<" ";
     (*out).setf(ios::showpoint);
 }
 //sort bundlevecs first by rayhleighvalue
 Indexmatrix indold=sortindex(rayleighval);
 Integer nold=indold.dim();
 if (nold>0) { //sort identical rayleigh values by primaleigs
   Integer lasti=0;
   Real lastval=rayleighval(indold(lasti));
   Integer i;
   for(i=1;i<nold;i++){
     if (lastval==rayleighval(indold(i))) continue;
     if (lasti<i-1) {
       //resort from lasti to i-1
       Integer *indp=indold.get_store()+lasti;
       heapsort(i-lasti,indp,primaleigs.get_store());
     }
     lasti=i;
     lastval=rayleighval(indold(i));
   }
   if (lasti<i-1) {
     //resort from lasti to i-1
     Integer *indp=indold.get_store()+lasti;
     heapsort(i-lasti,indp,primaleigs.get_store());
   } 
 }     
 Indexmatrix indnew=sortindex(eigvals);
 Integer nnew=indnew.dim();
 Integer inew=1;
 Integer iold=0;
 while (iold+inew<maxkeepvecs+maxaddvecs){
   //if new and old vectors are both available
   if ((iold<nold-1)&&(inew<nnew-1)&&(inew<maxaddvecs)){
     //choose the one with larger rayleighvalue
     Real valold=rayleighval(indold(nold-1-iold));
     Real valnew=eigvals(indnew(nnew-1-inew));
     if (valold>valnew){
       //but stop if the larger bundle vector is of little influence
       if (primaleigs(indold(nold-1-iold))<aggregtol) break;
       iold++;
       continue;
     }
     else {
       inew++;
       continue;
     }
   }
   //if only bundle vectors are available
   if (iold<nold-1) {
     //stop if the current one is of little influence
     if (primaleigs(indold(nold-1-iold))<aggregtol) break;
     iold++;  //otherwise add the current old vector
     continue;
   }
   //if only new vectors are available
   if ((inew<nnew-1)&&(inew<maxaddvecs)){
     inew++;   //add the current new vector
     continue;
   }
   break;
 }
     
 if (iold<nold){//some vectors have to be aggregated
   if (iold>0) {//if some vectors remain, remove then from the scratch list
     indold.delete_rows(Range(nold-iold,nold-1));
   }
         
   //aggregate
   if (out) {(*out)<<" aggrdim="<<indold.dim();out->precision(4);}
   if (indold.dim()>0){
     Real su=sum(primaleigs(indold));
     if (problem->subgrad_is_available()&&(s>0.)){
       if (out) (*out)<<"  add subgrad ("<<(s+su)/problem->lmaxmult()<<") "<<flush;
       problem->update_subgrad(primalvecs.cols(indold),primaleigs(indold),
			       su/(su+s));
     }
     else {
       if (out) (*out)<<" init subgrad ("<<su/problem->lmaxmult()<<") "<<flush;
       problem->init_subgrad(primalvecs.cols(indold),primaleigs(indold));
     }
     primalvecs.delete_cols(indold);
     primaleigs.delete_rows(indold);
     rayleighval.delete_rows(indold);
     s+=su;
   }
 }
 else if(s==0.){
     problem->clear_subgrad_available();
 }
 bundlevecs=primalvecs;
     
 //--- enlarge bundle with new information
 if (bundlevecs.coldim()>0){
     tmpvec=eigvecs.col(0);
     tmpvec.transpose();
     tmpvec*=bundlevecs;
     tmpvec.transpose();
     if (out) (*out)<<" res="<<norm2(bundlevecs*tmpvec-eigvecs.col(0))<<endl;
 }
 else if (out) (*out)<<" res="<<norm2(eigvecs.col(0))<<endl;

 if (bundlevecs.coldim()>0)
     bundlevecs.concat_right(eigvecs.cols(indnew(Range(nnew-inew,nnew-1))));
 else
     bundlevecs.init(eigvecs.cols(indnew(Range(nnew-inew,nnew-1))));
 
 if (out) {
     (*out)<<" ";
     for(Integer i=0;i<inew;i++){
         out->precision(6);(*out)<<" "<<eigvals(indnew(nnew-1-i));
     }
     (*out)<<endl;
 }
 
 //--- orthogonalize bundlevecs
//     {
//         Integer i,k;
//         Integer n=bundlevecs.rowdim();
//         for(k=0;k<bundlevecs.coldim();k++){
//             Real *xk=bundlevecs.get_store()+k*n;
//             Real t,t1;
//             do{
//                 //compute projection of vector k on vector i and subtract this 
//                 t=0.;
//                 for(i=0;i<k;i++){
//                     Real *xi=bundlevecs.get_store()+i*n;
//                     t1=mat_ip(n,xi,xk);
//                     t+=t1*t1;
//                     mat_xpeya(n,xk,xi,-t1);
//                 }
//                 t1=mat_ip(n,xk,xk);
//                 t+=t1;
//             }while(t1<t/100.);
//             t1=sqrt(t1);
//             if (t1>1e-20) {
//                 //vector is not in the span, normalize it
//                 mat_xmultea(n,xk,1./t1);
//             }
//             else {
//                 //vector is in the span of previous vectors, delete it
//                 bundlevecs.delete_cols(Indexmatrix(1,1,k));
//                 k--;  //correct the value of k
//             }
//         }
//         primaleigs.init(bundlevecs.coldim(),1,1.);
//     }
     

 //if the old bundlevecs may be changed this is more stable
 tmpmat=bundlevecs;
 Indexmatrix piv;
 Integer r=tmpmat.QR_factor(piv);
 if (r<bundlevecs.coldim()) {
     if (out) {
         (*out)<<"\nWARNING: bundle update linearly dependent:";
         (*out)<<" cos="<<bundlevecs.coldim()<<" rank="<<r<<endl;
     }
     rankdefcnt++;
 }
 
 bundlevecs.init(tmpmat.rowdim(),r,0.);
 for(Integer i=0;i<r;i++) bundlevecs(i,i)=1.;
 tmpmat.Q_times(bundlevecs,r);
}


/* ---------------------------------------------------------------------------
 *    Change log $Log: bmaxvecs.cc,v $
 *    Change log Revision 1.1.2.1  2000/02/28 13:55:36  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/Attic/bmaxvecs.cc,v $
 --------------------------------------------------------------------------- */
