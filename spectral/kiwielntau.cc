/* -*-c++-*-
 * 
 *    Source     $RCSfile: kiwielntau.cc,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:49 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: kiwielntau.cc,v 1.1.1.1 1999/12/15 13:47:49 bzfhelmb Exp $"

#include "kiwielntau.h"
#include "mymath.h"

Kiwielntau::Kiwielntau(Real mRin)
{
 modelmax=-1e40;
 taumin=taumax=tau=-1.;
 out=0;
 mR=mRin;
}

SBchoose_val Kiwielntau::init(const Matrix& subgrad)
{
 modelmax=-1e40;
 tau=norm2(subgrad);
 if (taumin<=0) taumin=1e-10*tau;
 else tau=max(taumin,tau);
 if (taumax>0) tau=min(tau,taumax);  
 epstau=1e30;
 itau=0;
 tauchanged=1;
 return SBchoose_ok;
}

SBchoose_val Kiwielntau::prob_changed(const Matrix& subgrad)
{
 modelmax=-1e40;
 tau=norm2(subgrad);
 if (taumin<=0) taumin=1e-10*tau;
 else tau=max(taumin,tau);
 if (taumax>0) tau=min(tau,taumax);  
 epstau=1e30;
 itau=0;
 tauchanged=1;
 return SBchoose_ok;
 //epstau=1e30;
 //itau=0;
 //return SBchoose_ok;
}

Real Kiwielntau::get_tau() const
{return tau;}
    
int Kiwielntau::tau_changed() const
{
 return tauchanged;
}


SBchoose_val Kiwielntau::choose_serious(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)
{
 if (tau<0) return SBchoose_notinit;

 modelmax=max(modelmax,modelval);
 Real oldtau=tau;
 Real tauint=2*tau*(1.-(oldval-newval)/(oldval-modelval));
 if (out) (*out)<<"  serious step, i_u="<<itau<<flush;
 if (((oldval-newval)>mR*(oldval-modelval))&&(itau>0)){
     if (out) (*out)<<" uint="<<tauint<<flush;
     tau=tauint;
 }
 else if (itau>3){ //there were four, now is the 5th consecutive serious 
     if (out) (*out)<<" i_u>3 u/2 "<<flush;
     tau/=2.;
 }
 else if (newval<modelmax){
     if (out) (*out)<<" nv<linmax u/2 "<<flush;
     tau/=2.;
 }
 tau=max(oldtau/10.,tau);
 if (taumin>0) tau=max(tau,taumin);
 if (out) (*out)<<" unew="<<tau<<endl;
 epstau=max(epstau,2*(oldval-modelval));
 itau=max(itau+1,Integer(1));
 if (tau<oldtau) {
     tauchanged=1;
     itau=1;
     modelmax=-1e40;
 }
 else {
     tauchanged=0;
 }
 return SBchoose_ok;
}


SBchoose_val Kiwielntau::choose_nullstep(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)
{
 if (tau<0) return SBchoose_notinit;
 
 Real oldtau=tau;
 Real tauint=2*tau*(1.-(oldval-newval)/(oldval-modelval));

 epstau=min(epstau,norm2subg+oldval-modelval-ip(y-newy,last_subgrad));
 if (out) (*out)<<"  null step, i_u="<<itau<<" eps_v="<<epstau<<flush;

 Matrix tmpvec;
 problem->get_rhs(tmpvec);
 tmpvec-=problem->opA(eigvecs.col(0),Matrix(1,1,problem->lmaxmult()));
 Real lin_approx=oldval-newval-ip(y-newy,tmpvec);

 if (out) (*out)<<" lapprox="<<lin_approx<<flush;
 if (lin_approx<-1e-12*(my_abs(oldval)+1.)){
         if (out) {
             (*out)<<"\n\n*** ERROR in Kiwielntau::choose_nullstep():";
             (*out)<<" lin_approx negative"<<lin_approx<<endl;
         }
         return SBchoose_lapprox;
 }
 
 if ((lin_approx>max(epstau,10.*(oldval-modelval)))&&(itau<-3)){
     if (out) (*out)<<" uint="<<tauint<<flush;
     tau=tauint;
 }
 tau=min(tau,10.*oldtau);
 if (taumax>0) tau=min(tau,taumax);
 if (out) (*out)<<" unew="<<tau<<endl;
 itau=min(itau-1,Integer(-1));
 if (tau>oldtau) {
     itau=-1;
     tauchanged=1;
 }
 else {
     tauchanged=0;
 }
 return SBchoose_ok;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: kiwielntau.cc,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/kiwielntau.cc,v $
 --------------------------------------------------------------------------- */
