/* -*-c++-*-
 * 
 *    Source     $RCSfile: spectral.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:45 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SPECTRAL_H__
#define __SPECTRAL_H__

#include "symmat.h"
#include "clock.h"
#include "sdpqp.h"
#include "aggregqp.h"

#define DEL_STAT_NO  'n'
#define DEL_STAT_YES 'y'
#define DEL_STAT_NOT_NOW 'w'

extern int TERMINATION_OK;

class SpectralBundleProblem
{

public:
    virtual Integer nr_constraints() const =0;
    virtual Integer mat_dim() const =0;
    
    virtual void get_y(Matrix& y) const =0;
    virtual Matrix get_y(void) const =0;
    virtual void get_eigval(Matrix& eigval) const =0;
    virtual Matrix get_eigval(void) const =0;
    virtual void get_eigvecs(Matrix& eigvecs) const =0;
    virtual Matrix get_eigvecs(void) const =0;
    virtual void set_y(const Matrix& y,const Matrix& eigval,const Matrix& eigvec)=0;
    
    virtual char sign(Integer i) const =0;
    virtual void get_rhs(Matrix& rhs) const =0;
    virtual Matrix get_rhs(void) const =0;
    virtual Real& scale(Integer i) const =0;
    virtual void get_scale(Matrix& s) const=0;
    virtual void set_scale(const Matrix& s)=0;
    virtual Real& lmult(Integer i) const =0;
    virtual void get_lmult(Matrix& s) const =0;
    virtual Matrix get_lmult(void) const =0;
    virtual void set_lmult(const Matrix& s) =0;
    virtual char get_del_stat(Integer i) const =0;
    virtual void set_del_stat(Integer i,char) =0;
    
    virtual void get_subgrad(Matrix& s) const=0;
    virtual Matrix get_subgrad(void) const=0;
    virtual Real get_subgrad_Cvalue() const=0;
    virtual int subgrad_is_available() const =0;
    virtual void clear_subgrad_available() =0;
    virtual void init_subgrad(const Matrix& P,const Matrix& d) =0;
    virtual void update_subgrad(const Matrix& P,const Matrix& d,Real alpha) =0;

    virtual Integer get_perm() const =0;
    virtual int remove(const Indexmatrix& ind)=0; 

    virtual Real lmaxmult() const =0;
    virtual Real ip_C(const Matrix& P,const Matrix& di) =0;
    virtual Matrix opA(const Matrix& P,const Matrix& di) =0;
    virtual void opA(const Matrix& P,const Matrix& di,Matrix& vec) =0;

    virtual void compute_sdpqp_coeff(Symmatrix& Q, Matrix& C, Real& a, Real& d,
                                     const Matrix& P, Real regparam,
                                     int do_scaling=0)=0; 

    virtual void compute_aggregqp_coeff(Symmatrix& Q1, Matrix& Q12, Real& q2,
                                        Matrix& C1, Real& c2, Real& a, Real& d,
                                        const Matrix& P, Real regparam,
                                        int do_scaling=0)=0; 

    virtual void update_sdpqp_coeff(Matrix& dC, Real& dd,
                                    const Matrix& P, Real regparam,
                                    const Indexmatrix& ind,const Matrix& val,
                                    int do_scaling=0)=0;

    virtual void update_aggregqp_coeff(Matrix& dC1, Real& dc2, Real& dd,
                                       const Matrix& P, Real regparam,
                                       const Indexmatrix& ind,const Matrix& val,
                                       int do_scaling=0)=0;

    virtual Real objval() const =0;
    virtual Real objval(Real maxeigval,const Matrix& yvec) =0;
    virtual Real model_compute(const Matrix& y,const Matrix& primalvecs,
                               Matrix& rayleighval) =0;
    virtual int obj_compute(const Matrix &yvec,Matrix& eigval,Matrix& eigvecs,Real nullstep_bound,Real relprec,Integer& nreig) =0;
    virtual Microseconds get_eigvaltime() const =0;
    virtual Microseconds get_matrixtime() const =0;
    virtual Microseconds get_subgtime() const =0;

    virtual ostream& save(ostream& out) const =0;
    virtual istream& restore(istream& in) =0;
};


enum SBchoose_val { SBchoose_ok,
                    SBchoose_notinit,
                    SBchoose_nosubgrad,   //subgrad has 0 or wrong dimension
                    SBchoose_lapprox      //negative value of lapprox indicates
                                          //function evaluation error
};

class SBchoosetau
{
public:
    SBchoosetau(){}
    virtual ~SBchoosetau(){}
    
    virtual SBchoose_val init(const Matrix& subgrad)=0;
    //compute first tau and set some parameters
    
    virtual SBchoose_val prob_changed(const Matrix& subgrad)=0;
    //called if variables were added or deleted
 
    virtual void set_taumin(Real tm) =0;
    //<=0 means no bound 

    virtual void set_taumax(Real tm) =0;
    //<=0 means no bound
   
    virtual Real get_tau() const =0;
    //returns current value of tau
    
    virtual int tau_changed() const =0;
    //returns 1 if last call of choose_ modified current value of tau, else 0
    
    virtual SBchoose_val choose_serious(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)=0;
    //determine next tau after a serious step
    
    virtual SBchoose_val choose_nullstep(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)=0;
    //determine next tau after a null step

    virtual void set_out(ostream* out)=0;

    virtual ostream& save(ostream& out) const =0;
    virtual istream& restore(istream& in) =0;
};

class SpectralBundle;
class SBterminator;

class SBupdate_bundle
{
  protected:
    Integer maxkeepvecs; //max number of vectors kept in bundle from prev iteration
    Integer minkeepvecs; //min number of vectors kept in bundle from prev iteration
    Integer maxaddvecs;  //max number of new vectors added to old bundle
    Integer rankdefcnt;  //counts number of rank deficiencies 
                         //(if an eigenvector is already contained in the bundle)
    Real aggregtol;      //aggregate primalvector if primalvalue smaller than that
    Matrix tmpmat,tmpvec;
    ostream *out;
 
  public:
     SBupdate_bundle(){maxkeepvecs=10;minkeepvecs=5;maxaddvecs=5;aggregtol=0.01;rankdefcnt=0;out=0;}
     virtual ~SBupdate_bundle(){}
     virtual void set_maxkeepvecs(Integer i){maxkeepvecs=i;}
     virtual Integer get_maxkeepvecs() const{return maxkeepvecs;}
     virtual void set_minkeepvecs(Integer i){minkeepvecs=i;}
     virtual Integer get_minkeepvecs() const{return minkeepvecs;}
     virtual void set_maxaddvecs(Integer i){maxaddvecs=i;}
     virtual Integer get_maxaddvecs() const{return maxaddvecs;}
     virtual void set_aggregtol(Real a){aggregtol=a;}
     virtual Real get_aggregtol() const{return aggregtol;}
     virtual void reset_rankdefcnt(){rankdefcnt=0;}
     virtual Integer get_rankdefcnt() const{return rankdefcnt;}
     virtual void update_bundle(SpectralBundle* sb,SpectralBundleProblem *sbp,
                                Matrix& bundlevecs,
                       const Matrix& eigvecs,const Matrix& eigvals,
                       Matrix& primalvecs, Matrix& primaleigs,
                       Matrix& rayleighval, Real& s);
    virtual void set_out(ostream* o){out=o;}
    //reduces primalvecs and primaleigs accordingly

    virtual ostream& save(ostream& out) const;
    virtual istream& restore(istream& in);
};

class SpectralBundle
{
private:
    Sdpqp sdpqp;
    Aggregqp aggregqp;
    SpectralBundleProblem *problem;

    SBterminator *terminator;   //pointer to termination criterion routine
    int terminate;  //1 if termination criterion is satisfied, 0 else

    SBchoosetau *choose_tau;    //pointer to tau-choosing routine
    Real tau;           //Regularity multiplier

    SBupdate_bundle *update_bundle; //pointer to bundle updating routine
    
    Matrix eigval,                //eigenvalues
        eigvecs,                  //eigenvectors
        y,                        //variables
        newy,                     //candidate
        bundlevecs,               //storage for bundle vectors
        primalvecs,primaleigs,    //spectral representation of quad SDP solution
        rayleighval,              //Rayleigh-values of primalvecs for new matrix
        last_subgrad,             //last subgradient 
        tmpmat,
        tmpvec;
    Symmatrix X,Z;
    Real s,t;

    Real norm2dy;   //possibly scaled by diagonal M: tau/2*<newy-y,M (newy-y)>
    Real norm2subg;


    //coefficients for sdpqp
    Symmatrix Q;
    Matrix C;
    Real a;
    Real dcoeff;

    //coeffcient for aggregqp (on top of sdpqp)
    Matrix Q12;
    Real q2;
    Real c2;

    //for updating these coefficients
    Matrix dC;
    Real dc2;

    //for storing updates to lagrange multipliers
    Indexmatrix update_index;
    Matrix update_value;

    //parameters
    Real modeleps;       //fixed parameter for the precision of the model
    Real qpeps;          //fixed parameter for the precision of the qp-subproblem
    Integer memkeepvecs; //max number of vectors for which memory has been reserved
    Real mL;             //acceptance factor
    Real mN;             //nullstep factor for inexact evaluation
    Real mult_stepsize;  //for guessing eta after a change of u or serious step
    Integer intpoint_maxiter; //maximum number of iterations in quad SDP-code
    int do_scaling;
    int remove_cuts;     //flag: delete cuts with large lagrange multipliers during null steps
    int use_linval;      //or use cutval for acceptance crit. default=1

    //variables in solve_model needed for resuming interrupted optimizations
    int last_method_used;  //needed for restarting with different solvers
  Integer max_updates;
    Integer updatecnt;
    int retcode;
    int problem_changed;
    
    //objective values
    Real oldval;
    Real newval;
    Real linval; //value of linearized model for feasible (pojected) solution
    Real cutval; //value of cutting surface model for feasible (projected) solution
    Real modelval;
    Real augval;
    Real oldaugval;
    Real lastaugval;

    //counters 
    Integer innerit;    //number of nullsteps
    Integer suminnerit; //sum of innerit over all calls
    Integer cntobjeval; //number of calls to eigenvalue computation routine
                        //may differ from suminnerit because of additiona
                        //corrections
    Integer recomp;     //recomputations for the current oldval
    Integer sumrecomp;  //sum over all recomputations
    Integer qsdpfails;  //counts failed qsdp solves for one model
    Integer sumqsdpfails; //counts all failures to solve qsdp
    Integer augvalfails;  //counts, within a descent step, null steps
                        //that fail to increase augval 
                        //!(augval>lastaugval+eps_Real*abs(lastaugval))
    Integer sumaugvalfails;  //sum over all augval failures

    Integer maxrank;    //max dimension of subproblem matrices

    //for output
    ostream *out;
    const Clock myclock;
    const Clock* clockp;  //points usually to myclock but may be set to another one
  //time for building and solving quadratic model
    Microseconds time_coeff;
    Microseconds time_solve;
    Microseconds time_update;
    Microseconds time_resolve;
  //time for main components of iterations
  Microseconds time_augmod;
  Microseconds time_eval;
  Microseconds time_bunupd;
  Microseconds time_choosetau;
  Microseconds time_last_augmod;
  Microseconds time_last_eval;
  Microseconds time_last_bunupd;
  Microseconds time_last_choosetau;
  //time for main components in routine solve()
  Microseconds time_qsp;
  Microseconds time_subg;
  Microseconds time_lagupd;
  Microseconds time_model;
    Integer cntsolves;
    Integer cntresolves;
    Integer cntitersolves;
    Integer cntiterresolves;
    Integer calls;        //counts number of calls to inner_loop

    Integer bens,       //number of benign enforced null steps
            sens,       //number of significant enforced null steps
            dens,       //number of dangerous enforced null steps
            shallowcut; //number of null steps with shallow cut
    Real sensvkappasum;

    Integer nr_ineq;      //nr of inequalities
    Integer nr_lmult_act; //nr of active multipliers/bounds (inactive primal constraints)
    Integer nr_add;       //nr of new active multipliers
    Integer nr_del;       //nr of new inactive multipliers

    Real lower_bound;     //may be set to some value, has currently no influence
                          //except in some output routines


    int quadratic_model(
        Symmatrix &X,Symmatrix &Z,Real& s,Real& t, int& last_method_used,
        const Matrix& bundlevecs, Real tau,
        int do_scaling);

    int quadratic_restart(
        Symmatrix &X,Symmatrix &Z,Real& s,Real& t, int& last_method_used,
        const Indexmatrix& update_index, const Matrix& update_value,
        const Matrix& bundlevecs, Real tau, int do_scaling);

    void update_multipliers(
        Matrix& newy, Matrix& last_subgrad,
        Indexmatrix& update_index, Matrix& update_value);

    Integer delete_cuts(Matrix& newy,Matrix& last_subgrad,Matrix& y,
                        const Matrix& primalvecs,const Matrix& primaleigs);

    int solve_model(
        Matrix& newy, Real &modelval,
        Matrix& primalvecs,Matrix& primaleigs, Real& s,
        Matrix& last_subgrad,
        Matrix& y,const Matrix& bundlevecs, Real tau, Real oldval,
        int do_scaling,int resume);

    
public:
    inline SpectralBundle();
    inline ~SpectralBundle();
        
    void set_defaults();
    void init_size(Integer n, Integer m);
    //n is the dimension of the matrix, m the number of constraints (variables)
    
    inline void set_terminator(SBterminator* sbt);
    void set_modeleps(Real in_eps){modeleps=in_eps;}
    void set_choose_tau(SBchoosetau* sbct)
    {delete choose_tau; choose_tau=sbct;}
    void set_update_bundle(SBupdate_bundle* ub)
    {delete update_bundle; update_bundle=ub;}
    void set_mL(Real in_mL){mL=in_mL;}
    void set_mN(Real in_mN){mN=in_mN;}
    void set_use_linval(int i){use_linval=i;}
    void set_mult_stepsize(Real ms){mult_stepsize=ms;}
    void set_do_scaling(int ds){do_scaling=ds;}
    void set_clock(const Clock& myclock){clockp=&myclock;}
    void set_lower_bound(Real lb) {lower_bound=lb;}
    void set_out(ostream* o){out=o;}
    void set_intpoint_maxiter(Integer mi){intpoint_maxiter=mi;}
    void set_max_updates(Integer mu){max_updates=mu;}
    void set_qpeps(Real e){qpeps=e;}
    void set_remove_cuts(int rem_cuts){remove_cuts=rem_cuts;}
    
    int get_terminate() const {return terminate;}
    Real get_modeleps() const {return modeleps;}
    int get_do_scaling() const {return do_scaling;}
    
    Real get_last_subgrad_norm() const {return norm2(last_subgrad);}
    Integer get_cntobjeval() const {return cntobjeval;}
    Integer get_innerit() const {return innerit;}
    Integer get_suminnerit() const {return suminnerit;}
    Integer get_recomp() const {return recomp;}
    Integer get_sumrecomp() const {return sumrecomp;}
    Integer get_qsdpfails() const {return qsdpfails;}
    Integer get_sumqsdpfails() const {return sumqsdpfails;}
    Integer get_augvalfails() const {return augvalfails;}
    Integer get_sumaugvalfails() const {return sumaugvalfails;}
    Integer get_maxrank() const {return maxrank;}
    Real get_tau() const {return tau;}
    Real get_norm2subg() const {return norm2subg;}

    Microseconds get_time_coeff() const {return time_coeff;}
    Microseconds get_time_solve() const {return time_solve;}
    Microseconds get_time_update() const {return time_update;}
    Microseconds get_time_resolve() const {return time_resolve;}
    Microseconds get_time_augmod() const {return time_augmod;}
    Microseconds get_time_eval() const {return time_eval;}
    Microseconds get_time_bunupd() const {return time_bunupd;}
    Microseconds get_time_choosetau() const {return time_choosetau;}
    Microseconds get_time_last_augmod() const {return time_last_augmod;}
    Microseconds get_time_last_eval() const {return time_last_eval;}
    Microseconds get_time_last_bunupd() const {return time_last_bunupd;}
    Microseconds get_time_last_choosetau() const {return time_last_choosetau;}
    Integer get_cntsolves() const {return cntsolves;}
    Integer get_cntresolves() const {return cntresolves;}
    Integer get_cntitersolves() const {return cntitersolves;}
    Integer get_cntiterresolves() const {return cntiterresolves;}
    Integer get_bens() const {return bens;}
    Integer get_sens() const {return sens;}
    Integer get_dens() const {return dens;}
    Integer get_shallowcut() const {return shallowcut;}
    Real get_sensvkappasum() const{return sensvkappasum;}
    Integer get_nr_ineq() const {return nr_ineq;}
    Integer get_nr_lmult_act() const {return nr_lmult_act;}
    Integer get_nr_add() const {return nr_add;}
    Integer get_nr_del() const {return nr_del;}
    Integer get_calls() const {return calls;}
    Real get_lower_bound() const {return lower_bound;}
    
    Real get_oldval() const {return oldval;}
    Real get_newval() const {return newval;}
    Real get_modelval() const {return modelval;}
    int get_use_linval() const {return use_linval;}
    const Matrix& get_primalvecs() const {return primalvecs;}
    const Matrix& get_primaleigs() const {return primaleigs;}
    const Matrix& get_bundlevecs() const {return bundlevecs;}
    Real get_s() const {return s;}

    const SpectralBundleProblem* get_problem() const {return problem;}
    SBterminator* get_terminator() const {return terminator;}
    SBchoosetau*  get_choose_tau() const {return choose_tau;}
    SBupdate_bundle* get_update_bundle() const {return update_bundle;}
    const Clock& get_clock() const {return *clockp;}

    int constraints_changed(SpectralBundleProblem& problem,int rem_cuts=1);
    
    int inner_loop(SpectralBundleProblem& problem, int resume=0);

    ostream& print_line_summary(ostream& out) const;
    ostream& print_full_summary(ostream& out) const;

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
};

class SBterminator
{
    friend class SpectralBundle;
    //standard terminator, can be redefined by derivation
    //and using SpectralBundle.set_terminator(SBterminator *)
protected:
    Real termeps;
    const Clock* clockp;
    void (*save_function)(const SpectralBundle* sb); 
    //if this function is set, it is called on every execution of check_termination
    Microseconds timelimit; //upper limit 
    Integer recomplimit;    //upper limit on sumrecomp,  <0 if none
    Integer qsdpfailslimit; //upper limit on sumqsdpfail, <0 if none
    Integer augvalfailslimit; //upper limit on augvalfail, <0 if none
    long terminated; // 0    not terminated,
                     // 1    precision achieved,
                     // 2    timelimit exceeded
                     // 4    recomp limit exceeded
                     // 8    qsdpfailslimit exceeded
                     //16    augvalfailslimit exceeded
    
    virtual int check_termination(SpectralBundle* sb)
    {
     if (save_function) (*save_function)(sb);
     if ((sb->get_suminnerit()<10)&&(sb->get_norm2subg()>0.1)) return 0;
     Real abs_oldval=(sb->get_oldval()>0)?sb->get_oldval():-sb->get_oldval();
     terminated=(sb->get_oldval()-sb->get_modelval()<=
                 termeps*(abs_oldval+1.));
     if (clockp)
         terminated|=2*(clockp->time()>=timelimit);
     if (recomplimit>=0)
         terminated|=4*(recomplimit<=sb->get_sumrecomp());
     if (qsdpfailslimit>=0)
         terminated|=8*(qsdpfailslimit<=sb->get_sumqsdpfails());
     if (augvalfailslimit>=0)
         terminated|=16*(augvalfailslimit<=sb->get_sumaugvalfails());
     return terminated;
    }
public:
    SBterminator()
    {
     termeps=1e-5;clockp=0;timelimit.set_infinity(true);
     terminated=0;recomplimit=100;qsdpfailslimit=100;
     augvalfailslimit=10; save_function=0;
    }

    SBterminator(Real teps)
    {
     termeps=teps;clockp=0;timelimit.set_infinity(true);terminated=0;
     recomplimit=100;qsdpfailslimit=100;augvalfailslimit=10;save_function=0;
    }

    SBterminator(Real teps,const Clock* cp,Microseconds tl)
    {
     termeps=teps;clockp=cp;timelimit=tl;terminated=0;
     recomplimit=100;qsdpfailslimit=100;augvalfailslimit=10;save_function=0;
    }
    
    virtual ~SBterminator(){}

    virtual void set_termeps(Real teps){termeps=teps;}
    virtual Real get_termeps() const {return termeps;}
    virtual void set_timelimit(const Clock* cp,Microseconds tl){clockp=cp;timelimit=tl;}
    virtual Microseconds get_timelimit() const {return timelimit;}
    virtual void set_recomplimit(Integer rl){recomplimit=rl;}
    virtual Integer get_recomplimit() const {return recomplimit;}
    virtual void set_qsdpfailslimit(Integer ql){qsdpfailslimit=ql;}
    virtual Integer get_qsdpfailslimit() const {return qsdpfailslimit;}
    virtual void set_augvalfailslimit(Integer al){augvalfailslimit=al;}
    virtual Integer get_augvalfailslimit() const {return augvalfailslimit;}
    
    virtual int get_terminated() const {return terminated;}

    virtual void set_save_function(void (*sf)(const SpectralBundle*))
      { save_function=sf;}    

    virtual void print_status(ostream& out)
    {
      TERMINATION_OK = 1;
		out<<"termination status: "<<terminated;
      if (terminated==0) { out<<" (not terminated)"<<endl; return;}
      if (terminated & 1){ out<<", relative precision criterion satisfied"; }
      if (terminated & 2){ out<<", timelimit exceeded"; }
      if (terminated & 4){ out<<", function reevaluation limit exceeded";}
      if (terminated & 8){  TERMINATION_OK = 0; out<<", limit of QSPfailures exceeded";}
      if (terminated & 16){ TERMINATION_OK = 0; out<<", limit of augmented model failures exceeded";} 
      out<<endl;
    }
    

    virtual ostream& save(ostream& out) const
    {
     out.precision(20);
     out<<termeps<<"\n"<<timelimit<<"\n"<<recomplimit<<"\n"<<qsdpfailslimit<<"\n"<<augvalfailslimit<<"\n"<<terminated<<"\n";
     return out;
    }
    virtual istream& restore(istream& in)
    { 
      in>>termeps;
      in>>timelimit;
      in>>recomplimit;
      in>>qsdpfailslimit;
      in>>augvalfailslimit;
      in>>terminated; 
      return in;
    }
};



//-----------------------------------------------------------------------------
//----                              inline
//-----------------------------------------------------------------------------

inline SpectralBundle::SpectralBundle()
{terminator=0; choose_tau=0; update_bundle=0; out=0; set_defaults();}

inline SpectralBundle::~SpectralBundle()
{delete terminator; delete choose_tau;delete update_bundle;}

inline void SpectralBundle::set_terminator(SBterminator* sbt)
{delete terminator; terminator=sbt;}

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: spectral.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:45  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/spectral.h,v $
 --------------------------------------------------------------------------- */
