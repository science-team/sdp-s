/* -*-c++-*-
 * 
 *    Source     $RCSfile: bigmat.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:35 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __BIGMAT_H__
#define __BIGMAT_H__

#if __GNUC__ > 3 || \
	(__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
#include <ext/hash_map>
#include <ext/hash_set>
#else
#include <hash_map>
#endif

#include "memarray.h"
#include "lanczos.h"
#include "coeffmat.h"

class Sparsesym;
class Problem;
class Matrcons;

using namespace std;

#if __GNUC__ > 3 || \
        (__GNUC__ == 3 && (__GNUC_MINOR__ >= 1))
using namespace __gnu_cxx;
#endif

typedef hash_map<Integer,Real,hash<Integer>,equal_to<Integer> > Indexhash;
//typedef std::hash_map<Integer,Real,std::hash<Integer>,std::equal_to<Integer> > Indexhash;

class Bigmatrix:public Lanczosmatrix,protected Memarrayuser
{
private:
    Integer meminzp,mempmemjp,memjp,mempmemvp,memvalp,memmcp,memmcv;
    Integer dim;      //size of the matrix
    Integer nz;
    Integer *inzp;     //number of nonzeros in column i
    Integer *pmemjp;
    Integer *pmemvp;
    Integer **jp;     //array pointing to arrays of row indices
    Real    **valp;   //array pointing to arrays of corresponding values
    Matrix di;   //diagonal elements
    Integer nrowhash;
    Indexhash* rowhash;
    
    Integer n;        //number off nonsparse structured constraints
    const Coeffmat ** mcp;  //pointer to these constraints
                              //(matrix constraints pointer)
    Real *mcv;        //multiplier values for these constraints

    mutable Integer nmult;    //counts number of Mat*vec operations

    //---
    int use_dense;     //usually 0, set to 1 if matrix is sufficiently dense
    Symmatrix symrep;  //in this case the matrix is stored here
    
    //--- alternatively for testing
    const Problem* problem;
    const Matrix* yp;

    //--- temporary variables for lanczosmult
    mutable Matrix At;
    mutable Matrix Bt;
    Indexmatrix collecti;
    Indexmatrix collectj;
    Matrix collectval;
    Indexmatrix sval;
    Indexmatrix sind;
    

    void error(const char *s) const;
    int provide_sparse(Integer cnt);
    int provide_column(Integer i,Integer cnt);
    int provide_mat(Integer cnt);
    int add_ij(Integer i,Integer j,Real ij);
 public:
    Bigmatrix();
    Bigmatrix(Problem& problemin,const Matrix& yin);
    ~Bigmatrix();

    void clear();
    Integer get_nmult() const {return nmult;}
    void reset_nmult() {nmult=0;}
    
    int init(Problem& problemin,const Matrix& yin,int dense=0,int with_C=1);
    //dense should be set to some value !=0 if a dense
    //representation is desired

    int get_dense() const {return use_dense;}
    
    virtual Integer lanczosdim() const;
    virtual Integer lanczosflops() const;
    virtual int lanczosmult(const Matrix& A,Matrix& B) const;
        //computes  B = bigMatrix * A
        //A and B must not be the same object!
    int make_symmatrix(Symmatrix& S) const;
    
    friend ostream& operator<<(ostream& out,const Bigmatrix&);
};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: bigmat.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:35  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/bigmat.h,v $
 --------------------------------------------------------------------------- */
