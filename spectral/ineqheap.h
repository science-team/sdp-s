/* -*-c++-*-
 * 
 *    Source     $RCSfile: ineqheap.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __INEQHEAP_H__
#define __INEQHEAP_H__

//the main purpose of this class is to facilitate the selection of a few
//violated inequalities

#include "symmat.h"

class Coeffmat;

//a pointer to a class of this type must be passed to ineqheap. The
//value of the function eval is used for ranking the inequalities,
//a smaller value is better.

class Eval
{
public:
    Eval(){}
    virtual ~Eval(){}

    virtual Real eval(const Coeffmat& M,char sense,Real rhs)=0;
};

class Noeval:public Eval
{
public:
    Noeval(){}
    virtual ~Noeval(){}

    virtual Real eval(const Coeffmat& M,char sense,Real rhs){return 0.;}
};

//this is an example of an implementation for the abstract class Eval.
//It computes the violation with respect to a Symmatrix variable X.
//To avoid copying the variable only a pointer to this variable is stored.

class Violation:public Eval
{
private:
    Symmatrix* Xp;

    void error(const char *s);
    //{cout<<"Violation ERROR: "<<s<<endl; exit(1);}
    
public:
    Violation(){Xp=0;}
    Violation(Symmatrix* X){Xp=X;}
    virtual ~Violation(){}

    void init(Symmatrix* X){Xp=X;}
    virtual Real eval(const Coeffmat& M,char sense,Real rhs);
    /*
    { //computes slack, negative if violated
     if (Xp==0) error("Xp is not initialized");
     Real scale=M.norm();
     if (sense==GEQ) return (M.trprod(*Xp)-rhs)/scale;
     return (rhs-M.trprod(*Xp))/scale;
    }
    */
};


//this is a second example of an implementation for the abstract class Eval.
//It computes how far we have to go back form X along DX to satisfy the
//constraint. If the inequality is not violated, eval returns 1.
//To avoid copying the variable only a pointer to this variable is stored.

class Backtrack:public Eval
{
private:
    Symmatrix* Xp;
    Symmatrix* DXp;

    void error(const char *s);
    //{cout<<"Backtrack ERROR: "<<s<<endl; exit(1);}
    
public:
    Backtrack(){Xp=0;DXp=0;}
    Backtrack(Symmatrix* X,Symmatrix* DX){Xp=X;DXp=DX;}
    virtual ~Backtrack(){}

    void init(Symmatrix* X,Symmatrix* DX){Xp=X;DXp=DX;}
    virtual Real eval(const Coeffmat& M,char sense,Real rhs);
    /*
    { 
     if ((Xp==0)||(DXp==0)) error("Xp or DXp is not initialized");
     Real val=rhs-M.trprod(*Xp);
     if (((sense==GEQ)&&(val<=0.))
       ||((sense==LEQ)&&(val>=0.)))  return 1.;
     return val/M.trprod(*DXp);
    }
    */
};


//Ineqheap is designed to keep the "size" best constraints (best with respect
//to the ranking provided by Eval) which are added after an initialization.
//Depending on "delfalg" it deletes or overwrites the pointer to any
//inequality which is or was added but turned out to be too weak,
//even if the inequality is never really accepted in the heap.
//Inequalities which remain in the heap are never deleted. If init
//is called, old references will be overwritten by 0.
//The "cnt" accepted inequalities will always be stored in positions 0..cnt.

class Ineqheap
{
private:
    Integer matrpmem;      //number of entries allocated for matrp 
    Integer sensemem;      //number of entries allocated for sensemem
    Integer indmem;        //number of entries allocated for ind
    Integer valmem;        //number of entries allocated for val
    Integer size;          //maixmal number of inequalities kept in the heap
    Coeffmat **matrp;  //
    Real minviol;      //minimum amount of violation required, only
                       //smaller numbers (=larger violations) are considered
    Matrix rhsvec;     //stores the right hand sides of the inequalities
    char* sense;       //stores the sense of the inequalities
    Integer* ind;      //index for heapsort and heap
    Real* val;         //values for heapsort and heap
    Integer cnt;       //current number of inequalities in the heap
    Eval* evalp;       //points to the function used for ranking the ineqs

    int delflag;       //if delflag is 1 any inequality not accepted is deleted
                       //if delflag is 0 the objects are not destroyed.
    
    void error(const char *s);
    void first_init();

public:
    Ineqheap(){first_init();init(0,0,0.);}
    Ineqheap(Integer si,Eval* ev,Real mv,int delfl=1)
        {first_init();init(si,ev,mv,delfl);}
    ~Ineqheap();

    int init(Integer size,Eval* evalp,Real minviol,int delfl=1);

    void set_delflag(int i){delflag=i;}

    Real eval(const Coeffmat& M,char sense,Real rhs);
    // calls evalp->eval with these arguments

    int check(const Coeffmat& M,char sense,Real rhs,Real* result=0);
    // compares the "result" of evalp->eval with checkval()
    // the inequality would be added to the heap if return code is 1
    // if result!=0, the result of evalp->eval is stored there

    int add(Coeffmat* M,char sense,Real rhs,Real* result=0);
    // returns 1 if the inequality is added to the heap, 0 if not
    // if result!=0, the result of evalp->eval is stored there
 
    int add(Coeffmat* M,char sense,Real rhs,Real cheat);
    // returns 1 if the inequality is added to the heap, 0 if not
    // eval is not called, cheat is used instead
 
    Integer get_cnt() const {return (size<cnt)?size:cnt;}
    Coeffmat** get_matrp() const {return matrp;}
    const Matrix& get_rhsvec() const {return rhsvec;}
    char* get_sense() const {return sense;}

    Coeffmat* get_matrp(Integer i) const {return matrp[i];}
    Real get_rhsvec(Integer i) const {return rhsvec(i);}
    char get_sense(Integer i) const {return sense[i];}

    Real checkval();
    //returns the value an inequality has to beat to be added to the heap

};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: ineqheap.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/ineqheap.h,v $
 --------------------------------------------------------------------------- */
