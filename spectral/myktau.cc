/* -*-c++-*-
 * 
 *    Source     $RCSfile: myktau.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:38 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: myktau.cc,v 1.1.1.1.2.1 2000/02/28 13:55:38 bzfhelmb Exp $"

#include "myktau.h"
#include "mymath.h"

Myktau::Myktau(Real mRin,Integer iphists,Integer cuthists)
{
 taumin=taumax=tau=-1.;
 iphist_size=iphists;
 cuthist_size=cuthists;
 out=0;
 mR=mRin;
}

SBchoose_val Myktau::init(const Matrix& subgrad)
{
 tau=norm2(subgrad);
 if (taumin<=0) taumin=1e-5*tau;
 else tau=max(taumin,tau);
 if (taumax>0) tau=min(tau,taumax);  
 oldmodelval=1e30;
 epstau=1e30;
 itau=0;
 iphist_i=0;
 cuthist_i=0;
 olddy.init(subgrad.dim(),1,0.);
 ip_history.init(iphist_size,1,0.);
 cutval_history.init(cuthist_size,1,0.);
 tauchanged=1;
 return SBchoose_ok;
}

SBchoose_val Myktau::prob_changed(const Matrix& subgrad)
{
 epstau=1e30;
 itau=0;
 iphist_i=0;
 cuthist_i=0;
 olddy.init(subgrad.dim(),1,0.);
 ip_history.init(iphist_size,1,0.);
 cutval_history.init(cuthist_size,1,0.);
 return SBchoose_ok;
}

Real Myktau::get_tau() const
{return tau;}
    
int Myktau::tau_changed() const
{
 return tauchanged;
}

SBchoose_val Myktau::choose_serious(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)
{
 if (tau<0) return SBchoose_notinit;
 
 Real oldtau=tau;
 Real tauint=2*tau*(1.-(oldval-newval)/(oldval-modelval));
 if (oldmodelval>oldval) oldmodelval=oldval-norm2subg/tau;
 
 if (out) (*out)<<"  serious step, i_u="<<itau<<flush;

 Matrix tmpvec=newy-y;
 tmpvec/=norm2(tmpvec);
 ip_history(iphist_i)=ip(olddy,tmpvec);
 iphist_i++;
 iphist_i%=iphist_size;
 cutval_history(cuthist_i)=oldmodelval-modelval;
 cuthist_i++;
 cuthist_i%=cuthist_size;
 oldmodelval=modelval;
 olddy=tmpvec;
     
 if (((oldval-newval)>mR*(oldval-modelval))&&(itau>0)){
     if (out) (*out)<<" tauint="<<tauint<<flush;
     tau=tauint;
 }
 else if (((itau>=3)&&(min(ip_history)>.3))||(itau>=10)){
     //there were three consecutive serious steps 
     if (out) (*out)<<" itau>3 tau/5 "<<flush;
     tau/=5.;
 }
 else if ((min(ip_history)>.5)&&(min(cutval_history)>0)&&
          (oldmodelval-modelval>.6*(oldval-newval))){
     if (out) (*out)<<" hist tau/2 "<<flush;
     tau/=2.;
 }
         
 tau=max(oldtau/10.,tau);
 if (taumin>0) tau=max(tau,taumin);
 if (out) (*out)<<" newtau="<<tau<<endl;
 itau=max(itau+1,Integer(1));
 if (tau<oldtau) {
     itau=1;
     ip_history.init(iphist_size,1,0.);
     cutval_history.init(cuthist_size,1,0.);
     tauchanged=1;
 }
 else {
     tauchanged=0;
 }
 return SBchoose_ok;
}


SBchoose_val Myktau::choose_nullstep(Real newval,Real oldval,Real modelval,
                       const Matrix& y, const Matrix& newy, const Matrix& eigvecs,
                       const Matrix& last_subgrad, Real norm2subg,
                       SpectralBundleProblem *problem)
{
 if (tau<0) return SBchoose_notinit;
 
 Real oldtau=tau;
 //Real tauint=2*tau*(1.-(oldval-newval)/(oldval-modelval));
 if (oldmodelval>oldval) oldmodelval=oldval-norm2subg/tau;
 
 if (out) (*out)<<"    null step, itau="<<itau<<" epstau="<<epstau<<flush;
 epstau=min(epstau,norm2subg+oldval-modelval-ip(y-newy,last_subgrad));

 Matrix tmpvec;
 problem->get_rhs(tmpvec);
 tmpvec-=problem->opA(eigvecs.col(0),Matrix(1,1,problem->lmaxmult()));
 Real lin_approx=oldval-newval-ip(y-newy,tmpvec);

 if (out) (*out)<<" lapprox="<<lin_approx<<flush;
 if (lin_approx<-1e-12*(my_abs(oldval)+1.)){
         if (out) {
             (*out)<<"\n\n*** ERROR in Myktau::choose_nullstep():";
             (*out)<<" lin_approx negative"<<lin_approx<<endl;
         }
         return SBchoose_lapprox;
 }
 
 if ((itau<-10)&&(max(ip_history)<-.1)){
     if (out) (*out)<<" hist tau*2"<<flush;
     tau*=2.;
 }     
 tau=min(tau,2.*oldtau);
 if (taumax>0) tau=min(tau,taumax);
 if (out) (*out)<<" newtau="<<tau<<endl;
 itau=min(itau-1,Integer(-1));
 if (tau>oldtau) {
     itau=-1;
     tauchanged=1;
     ip_history.init(iphist_size,1,0.);
     cutval_history.init(cuthist_size,1,0.);
 }
 else {
     tauchanged=0;
 }
 return SBchoose_ok;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: myktau.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:38  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/myktau.cc,v $
 --------------------------------------------------------------------------- */
