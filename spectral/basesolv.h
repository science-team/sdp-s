/* -*-c++-*-
 * 
 *    Source     $RCSfile: basesolv.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:52 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __BASESOLV_H__
#define __BASESOLV_H__

#include <iostream>

#include "sbparams.h"

class BaseSolver
{
public:
  BaseSolver(){}
  virtual ~BaseSolver(){}
  
  virtual int solve(
		    Problem* problem,
		    SpectralBundle* bundle,
		    Clock* myclock,
		    SBparams* params,
		    int resume=0)=0;
  
  
  virtual void set_out(ostream* o) =0;      
  
  virtual int read(istream& in) {return 0;}
  //allows to read problem specific input data before reading the standard 
  //problem description for specifying the eigenvalue problem
  
  virtual int read_next_param(int argc,char** argv,int& argpos){return 0;}
  //sbparams is unable to process command line parameter argv[argpos] 
  //and passes it on.
  //check this parameter and process it if it is yours
  //Update argpos if your were able to process it 
  //return 0 on success (also if the parameter was not yours)
  //and 1 if the parameter was yours but could not be completed correctly
  //by the remaining arguments.
  
  virtual ostream& save(ostream& out) const =0;
  virtual istream& restore(istream& in) =0;

};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: basesolv.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/basesolv.h,v $
 --------------------------------------------------------------------------- */
