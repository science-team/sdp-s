/* -*-c++-*-
 * 
 *    Source     $RCSfile: cmsingle.h,v $ 
 *    Version    $Revision: 1.1.1.1 $ 
 *    Date       $Date: 1999/12/15 13:47:51 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __CMSINGLE_H__
#define __CMSINGLE_H__

//defines a base class for constraint matrices.
//the idea is to help exploiting special structures for
//the computation of tr(AiXAjZi) without having to know
//the special structure of both, Ai and Aj. 

#include "cmgramde.h"
#include "cmrankdd.h"
#include "sparssym.h"

class CMsingleton: public Coeffmat
{
private:
    Integer nr;
    Integer ii;
    Integer jj;
    Real    val;
public:
    CMsingleton(Integer innr, Integer ini, Integer inj, Real inval,Integer k=0)
    {nr=innr;ii=ini;jj=inj;val=inval;CM_type=CM_singleton;userkey=k;}
    virtual ~CMsingleton(){}

    //virtual CM_type get_type() const {return CM_type;} //no need to overload
    //virtual Integer get_userkey() const {return userkey;}
    //virtual void set_userkey(Integer k) const {userkey=k;}
    
    virtual Integer dim() const {return nr;}
    //returns the order of the represented symmetric matrix
    
    virtual Real operator()(Integer i,Integer j) const 
    { if (((i==ii)&&(j==jj))||((i==jj)&&(j==ii))) return val; return 0.; }

    virtual void make_symmatrix(Symmatrix& S) const
    {S.init(nr,0.); S(ii,jj)=val;}
    //return dense symmetric constraint matrix

    virtual Real norm(void) const
    {if (ii==jj) return abs(val); return sqrt(2.)*abs(val);}
    //compute Frobenius norm of matrix

    virtual Coeffmat* subspace(const Matrix& P) const
    {
     if (ii==jj){ return new CMgramdense(sqrt(abs(val))*(P.row(ii)).transpose(),val>0); }
     return new CMlowrankdd((P.row(ii)).transpose()*val,(P.row(jj)).transpose());
    }
    //delivers a new object on the heap corresponding
    //to the matrix P^TAP, the caller is responsible for deleting the object
    
    virtual void multiply(Real d) { val*=d; }
    //multiply constraint permanentely by d
    //this is to allow scaling or sign changes in the constraints

    virtual Real ip(const Symmatrix& S) const
    {if (ii==jj) return val*S(ii,ii); return 2.*val*S(ii,jj);}
    //=ip(*this,S)=trace(*this*S) trace inner product
    
    virtual Real gramip(const Matrix& P) const
    {
     if (ii==jj)
        return val*mat_ip(P.coldim(),P.get_store()+ii,P.rowdim(),P.get_store()+ii,P.rowdim());
     return 2.*val*mat_ip(P.coldim(),P.get_store()+ii,P.rowdim(),P.get_store()+jj,P.rowdim());  
    }
    //=ip(*this,PP^T)=trace P^T(*this)P

    virtual void addmeto(Symmatrix& S,Real d=1.) const {S(ii,jj)+=d*val;}
    //S+=d*(*this);

    virtual void addprodto(Matrix& B,const Matrix&C ,Real d=1.) const
    {
     mat_xpeya(C.coldim(),B.get_store()+ii,B.rowdim(),C.get_store()+jj,C.rowdim(),d*val);
     if (ii==jj) return;
     mat_xpeya(C.coldim(),B.get_store()+jj,B.rowdim(),C.get_store()+ii,C.rowdim(),d*val);
    }
    //B+=d*(*this)*C

    virtual void addprodto(Matrix& B,const Sparsemat&C ,Real d=1.) const
    {
      if (C.coldim()==1){
	B(ii)+=d*val*C(jj);
	if (ii!=jj) B(jj)+=d*val*C(ii);
        return;
      }
      Sparsemat tmp(C.row(jj));
      for(Integer i=0;i<tmp.nonzeros();i++){
	Integer indi, indj; Real v;
	tmp.get_edge(i,indi,indj,v);
	B(ii,indj)+=d*val*v;
      }
      if (ii==jj) return;
      tmp.init(C.row(ii));
      for(Integer i=0;i<tmp.nonzeros();i++){
	Integer indi, indj; Real v;
	tmp.get_edge(i,indi,indj,v);
	B(jj,indj)+=d*val*v;
      }
    }
    //B+=d*(*this)*C

    virtual Integer prodvec_flops() const 
    { return (ii==jj)?2:4; }
    //return estimate of number of flops to compute addprodto for a vector

    virtual int dense() const
    {return 0;}
    //returns 1 if its structure as bad as its dense symmetric representation,
    //otherwise 1
    
    virtual int sparse() const
    { return 1;}
    //returns 0 if not sparse otherwise 1
        
    virtual int sparse(Indexmatrix& I,Indexmatrix& J,Matrix& v,Real d=1.)const
    { I.init(1,1,ii); J.init(1,1,jj); v.init(1,1,val*d); return 1;}
    //returns 0 if not sparse. If it is spars it returns 1 and
    //the nonzero structure in I,J and v, where v is multiplied by d.
    //Only the upper triangle (including diagonal) is delivered
    
    virtual int support_in(const Sparsesym& S) const
    {return S.check_support(ii,jj);}
    //returns 0 if the support of the costraint matrix is not contained in the
    //support of the sparse symmetric matrix A, 1 if it is contained.

    virtual Real ip(const Sparsesym& S) const
    {if (ii==jj) return val*S(ii,jj); return 2.*val*S(ii,jj);}
    //returns the inner product of the constraint matrix with A
    
    virtual void project(Symmatrix& S,const Matrix& P) const // S=P^t*A*P
    {
     S.newsize(P.coldim());chk_set_init(S,1);
     const Real *ap=P.get_store()+ii-nr;
     Real *sp=S.get_store();
     if (ii==jj){
         for(Integer i=P.coldim();--i>=0;){
             const Real *aap=ap; Real a=*(aap=(ap+=nr));
             *sp++=a*a*val; a*=val;
             for(Integer j=i;--j>=0;) *sp++=a*(*(aap+=nr));
         }
         return;
     }
     const Real *bp=P.get_store()+jj-nr;
     for(Integer i=P.coldim();--i>=0;){
         const Real *aap; Real a=*(aap=(ap+=nr))*val;
         const Real *bbp; Real b=*(bbp=(bp+=nr));
         *sp++=2.*a*b; b*=val;
         for(Integer j=i;--j>=0;) *sp++=a*(*(bbp+=nr))+b*(*(aap+=nr));
     }
    }

    virtual ostream& display(ostream& o) const 
    {o<<"CMsingleton\n"; o<<nr<<" "<<ii<<" "<<jj<<" "<<val<<"\n"; return o;}
    //display constraint information
    
    virtual ostream& out(ostream& o) const
    {return o<<"SINGLETON\n"<<nr<<" "<<ii<<" "<<jj<<" "<<val<<"\n";}
    //put entire contents onto outstream with the class type in the beginning so
    //that the derived class can be recognized.

    virtual istream& in(istream& is)
    {
     if (!(is>>nr>>ii>>jj>>val)) { 
         cerr<<"*** ERROR: CMsingleton::in(): reading from input failed";
         is.clear(ios::failbit);
         return is;
     }
     if (nr<0) {
         cerr<<"*** ERROR: CMsingleton::in(): dimension of matrix must positive";
         cerr<<"but is "<<nr<<endl;
         is.clear(ios::failbit);
         return is;
     }
     if ((ii<0)||(ii>nr)){
         cerr<<"*** ERROR: CMsingleton::in(): row index outside range, ";
         cerr<<0<<"<="<<ii<<"<"<<nr<<endl;
         is.clear(ios::failbit);
         return is;
     }
     if ((jj<0)||(jj>nr)){
         cerr<<"*** ERROR: CMsingleton::in(): column index outside range, ";
         cerr<<0<<"<="<<jj<<"<"<<nr<<endl;
         is.clear(ios::failbit);
         return is;
     }
     return is;
    }
    //counterpart to out, does not read the class type, though.
    //This is assumed to have been read in order to generate the correct class

    CMsingleton(istream& is,Integer k=0)
    {CM_type=CM_singleton;userkey=k;in(is);}

    //--- specific routines
    int get_ijval(Integer& i,Integer &j, Real& v) const
    {i=ii; j=jj; v=val; return 0;}

};

#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: cmsingle.h,v $
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/cmsingle.h,v $
 --------------------------------------------------------------------------- */
