/* -*-c++-*-
 * 
 *    Source     $RCSfile: sbparams.h,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:42 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ifndef __SBPARAMS_H__
#define __SBPARAMS_H__

#include "spectral.h"
#include "problem.h"

class BaseSolver;

class SBparams {
private:
  int reading_parameter_file; //will be increased if parameter file is read
                              //at most 10 to avoid infinite recursions

public:
    Real termeps;  
    Integer timelimit;
    Integer maxkeepvecs;
    Integer maxaddvecs;
    Integer minkeepvecs;
    Integer maxiter;
    Real modeleps;
    Real aggregtol;
    Integer initial_scaling;
    Integer use_startheur;
    Integer do_scaling;
    Real mL;
    Real mN;
    Real mult_stepsize;
    Real mR;
    Integer ct_method;
    Integer fixu;
    Real fixuval;
    Real umin;
    Real umax;
    Integer terminator; 
    Integer exacteigs;
    Integer use_cutval;
    Integer bundle_update;
    Integer lanczossteps;
    Integer chebits;
#ifdef WITH_SEPARATION
    Integer first_sep;
    Integer last_sep;
    Integer step_sep;
    Real sep_mult;
    Real sepviol;
    Real first_sep_relprec;
#endif
    char *infile;
    char *instring;
    char *spfile;
    char *yfile;
    char *eigfile;
    char *etafile;
    char *Xfile;
    char *sXfile;
	 Real *Xvect;
    char *Pfile;
    char *sysfile;
    char *spsfile;
    char *probfile;
    ostream* outprob;
    ostream* outbundle;
    char* savefile;
    char* resume;
    
    SBparams();
    
    ~SBparams();

    ostream& save(ostream& out) const;
    istream& restore(istream& in);
    void usage();
    int message(const char *s,Integer retcode);
    int input(Integer argc,char **argv,BaseSolver* basesolver);
    int initialize(Problem* problem,SpectralBundle* bundle,Clock* myclock,BaseSolver* basesolver);
    int output(const Problem* problem,const SpectralBundle* bundle,const Clock* myclock);

};
    
#endif

/* ---------------------------------------------------------------------------
 *    Change log $Log: sbparams.h,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:42  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:52  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/sbparams.h,v $
 --------------------------------------------------------------------------- */
