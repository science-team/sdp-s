/* -*-c++-*-
 * 
 *    Source     $RCSfile: updheur.cc,v $ 
 *    Version    $Revision: 1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:36 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: updheur.cc,v 1.1.2.1 2000/02/28 13:55:36 bzfhelmb Exp $"

#include <cmath>
#include "updheur.h"
#include "mymath.h"
#include "heapsort.h"

// *****************************************************************************
//                               update_bundle
// *****************************************************************************

// First the bundlevectors are set to the vectors generated from the
// primal optimal solution (primalvecs). Vectors whose contribution
// is relatively small will be aggregated to keep the bundle small.
// If too many vectors are important, then the least important ones
// are aggregated to ensure that at most maxkeepvecs vectors remain.
// Afterwards at most maxaddvecs vectors obtained by the last
// function evaluation are added to the bundle to improve the model.
// Finally the vectors of the bundle are orthogonalized.
// input: eigvecs, primalvecs,primaleigs, s
// output: bundlevecs

void SBupdate_heuristic::update_bundle(SpectralBundle* sb,
				       SpectralBundleProblem *problem,
          Matrix& bundlevecs, const Matrix& eigvecs, const Matrix& eigvals,
	  Matrix& primalvecs, Matrix& primaleigs,Matrix& rayleighval, 
          Real& s)
{
 maxaddvecs=min(maxaddvecs,max(primalvecs.rowdim()/4,Integer(1)));
 maxkeepvecs=min(maxkeepvecs,primalvecs.rowdim()-maxaddvecs);
 minkeepvecs=min(minkeepvecs,maxkeepvecs);
 if ((s>0.3*problem->lmaxmult())||
     ((sb->get_cntobjeval()>=5)&&
      (double(sb->get_time_augmod())*.1>double(sb->get_time_eval())))
     ){
   maxkeepvecs=0;
   minkeepvecs=0;
   usemin=0;
   maxaddvecs=min(Integer(7),maxaddvecs);
 }   
 Real aggregval=aggregtol*max(primaleigs);
 if (out){
     (*out)<<" ";
     (*out).setf(ios::showpoint);
 }
 
 //--- determine number of new vectors
 Real new_threshold=eigvals(0)-
   max(1e-4*my_abs(eigvals(0)),
       5.*(eigvals(0)-max(rayleighval)));
 int nr_new=min(maxaddvecs,eigvecs.coldim());
 while ((nr_new>5)&&(eigvals(nr_new-1)<new_threshold)) nr_new--;

 //--- select columns to aggregate, aggregate them
 Indexmatrix ind;
 Integer i;
 if ((primalvecs.coldim()>min(maxkeepvecs,lastkeep+3))||(min(primaleigs)<aggregval)){
         
     //fill ind with column indices that are to be aggregated
     ind=sortindex(primaleigs);
     i=max(Integer(0),primalvecs.coldim()-min(maxkeepvecs,lastkeep+3));
     while((i<ind.dim())&&(primaleigs(ind(i))<aggregval)) i++;
     if ((i>=ind.dim()-minkeepvecs)&&
	 (double(sb->get_time_augmod())<double(sb->get_time_eval())/2.)&&
	 (double(sb->get_time_last_augmod())<double(sb->get_time_last_eval())/2.)
     ){
       if (ind.dim()>usemin) {
	 usemin=min(minkeepvecs,usemin+1);
       }
     }
     if (i>ind.dim()-usemin){
       i=max(Integer(0),ind.dim()-usemin);
     }
     if (i<ind.dim()){
         ind.delete_rows(Range(i,ind.dim()-1));
     }
         
     //aggregate
     if (out) {(*out)<<" aggrdim="<<ind.dim();out->precision(4);}
     if (ind.dim()>0){
         Real su;
         if (ind.dim()<primalvecs.coldim()){  //only a subset of the columns
	   su=sum(primaleigs(ind));
	   if (problem->subgrad_is_available()&&(s>0.)){
             if (out) (*out)<<"  add subgrad ("<<(s+su)/problem->lmaxmult()<<") "<<flush;
             problem->update_subgrad(primalvecs.cols(ind),primaleigs(ind),
				     su/(su+s));
	   }
	   else {
             if (out) (*out)<<" init subgrad ("<<su/problem->lmaxmult()<<") "<<flush;
             problem->init_subgrad(primalvecs.cols(ind),primaleigs(ind));
	   }
	   primalvecs.delete_cols(ind);
	   primaleigs.delete_rows(ind);
	   rayleighval.delete_rows(ind);
	 }
         else { //aggregate all columns
	   su=sum(primaleigs);
	   if (problem->subgrad_is_available()&&(s>0.)){
             if (out) (*out)<<"  add subgrad ("<<(s+su)/problem->lmaxmult()<<") "<<flush;
             problem->update_subgrad(primalvecs,primaleigs,su/(su+s));
	   }
	   else {
             if (out) (*out)<<" init subgrad ("<<su/problem->lmaxmult()<<") "<<flush;
             problem->init_subgrad(primalvecs,primaleigs);
	   }
	   primalvecs.init(0,0,0.);
	   primaleigs.init(0,0,0.);
           rayleighval.init(0,0,0.);
	 }
         s+=su;
     }
 }
 else if(s==0.){
     problem->clear_subgrad_available();
 }
 bundlevecs=primalvecs;
 lastkeep=bundlevecs.coldim();
     
 //--- enlarge bundle with new information
 if (out) {
   (*out)<<" nr_new="<<nr_new<<" usemin="<<usemin;
   if (bundlevecs.coldim()>0){
     tmpvec=eigvecs.col(0);
     tmpvec.transpose();
     tmpvec*=bundlevecs;
     tmpvec.transpose();
     (*out)<<" res="<<norm2(bundlevecs*tmpvec-eigvecs.col(0))<<endl;
   }
   else (*out)<<" res="<<1.<<endl;
   (*out)<<" ";
   for(i=0;i<nr_new;i++){
     out->precision(6);(*out)<<" "<<eigvals(i);
   }
   (*out)<<endl;
 }

 if (bundlevecs.coldim()==0){ //only new vectors
   bundlevecs.init(eigvecs.cols(Range(0,nr_new-1)));
   return;
 }

 //old and new vectors
 bundlevecs.concat_right(eigvecs.cols(Range(0,nr_new-1)));
 
 //--- orthogonalize bundlevecs
//     {
//         Integer i,k;
//         Integer n=bundlevecs.rowdim();
//         for(k=0;k<bundlevecs.coldim();k++){
//             Real *xk=bundlevecs.get_store()+k*n;
//             Real t,t1;
//             do{
//                 //compute projection of vector k on vector i and subtract this 
//                 t=0.;
//                 for(i=0;i<k;i++){
//                     Real *xi=bundlevecs.get_store()+i*n;
//                     t1=mat_ip(n,xi,xk);
//                     t+=t1*t1;
//                     mat_xpeya(n,xk,xi,-t1);
//                 }
//                 t1=mat_ip(n,xk,xk);
//                 t+=t1;
//             }while(t1<t/100.);
//             t1=sqrt(t1);
//             if (t1>1e-20) {
//                 //vector is not in the span, normalize it
//                 mat_xmultea(n,xk,1./t1);
//             }
//             else {
//                 //vector is in the span of previous vectors, delete it
//                 bundlevecs.delete_cols(Indexmatrix(1,1,k));
//                 k--;  //correct the value of k
//             }
//         }
//         primaleigs.init(bundlevecs.coldim(),1,1.);
//     }
     

 //if the old bundlevecs may be changed this is more stable
 tmpmat=bundlevecs;
 Indexmatrix piv;
 Integer r=tmpmat.QR_factor(piv);
 if (r<bundlevecs.coldim()) {
     if (out) {
         (*out)<<"\nWARNING: bundle update linearly dependent:";
         (*out)<<" cols="<<bundlevecs.coldim()<<" rank="<<r<<endl;
     }
     rankdefcnt++;
 }
 
 bundlevecs.init(tmpmat.rowdim(),r,0.);
 for(i=0;i<r;i++) bundlevecs(i,i)=1.;
 tmpmat.Q_times(bundlevecs,r);
}

// *****************************************************************************
//                             SBupdate_heuristic::save
// *****************************************************************************

ostream& SBupdate_heuristic::save(ostream& out) const
{
 out.precision(20);
 out<<maxkeepvecs<<"\n";
 out<<minkeepvecs<<"\n";
 out<<maxaddvecs<<"\n";
 out<<rankdefcnt<<"\n";
 out<<aggregtol<<"\n";
 out<<usemin<<"\n";
 return out;
}

// *****************************************************************************
//                             SBupdate_heuristic::restore
// *****************************************************************************

istream& SBupdate_heuristic::restore(istream& in)
{
 if (!(in>>maxkeepvecs)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading maxkeepvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>minkeepvecs)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading minkeepvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>maxaddvecs)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading maxaddvecs"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>rankdefcnt)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading rankdefcnt"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>aggregtol)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading aggregtol"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 if (!(in>>usemin)){
     cerr<<"*** ERROR:  SBupdate_heuristic::restore(): failed in reading usemin"<<endl;
     in.clear(ios::failbit);
     return in;
 }
 return in;
}


/* ---------------------------------------------------------------------------
 *    Change log $Log: updheur.cc,v $
 *    Change log Revision 1.1.2.1  2000/02/28 13:55:36  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:50  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/Attic/updheur.cc,v $
 --------------------------------------------------------------------------- */
 
