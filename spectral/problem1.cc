/* -*-c++-*-
 * 
 *    Source     $RCSfile: problem1.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:39 $ 
 *    Author     Christoph Helmberg 
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: problem1.cc,v 1.1.1.1.2.1 2000/02/28 13:55:39 bzfhelmb Exp $"

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <fstream>
#include "mymath.h"
#include "problem1.h"
#include "lanczpol.h"

#define BIGM_NO_INIT 0
#define BIGM_INIT_MODEL 1
#define BIGM_INIT_OBJ 2

#define DENSE_LIMIT 50

// ===========================================================================
//                                   error
// ===========================================================================

void Problem1::error(const char* s) const
{
 cerr<<"Problem1 error: "<<s<<endl;
 exit(1);
}

// ===========================================================================
//                                 Problem1()
// ===========================================================================

Problem1::Problem1()
{
 mempool=memsign=memrhs=memy=0;
 memscale=memsubgrad=memlmult=memdelstat=0;
 C=0;
 pool=0;
 signp=0;
 rhsp=0;
 yp=0;
 scalep=0;
 subgradp=0;
 lmultp=0;
 del_statp=0;
 n=0;
 p=0;
 subgrad_available=0;
 eigvaltime=Microseconds(long(0));
 matrixtime=Microseconds(long(0));
 subgtime=Microseconds(long(0));
 nr_eval=0;
 out=0;
 bigm_init=BIGM_NO_INIT;
 dense_constraints=0;
 exacteigs=0;
 use_startheur=1;
 store_symsubg=0;
 store_sparsesubg=0;
 lanczos=new Lanczpol;
}
     
// ===========================================================================
//                                 ~Problem1()
// ===========================================================================

Problem1::~Problem1()
{
 Integer i;
 delete C;
 for(i=0;i<n;i++) delete pool[i];
 delete[] pool;
 delete lanczos;
 memarray.free(signp);
 memarray.free(rhsp);
 memarray.free(yp);
 memarray.free(scalep);
 memarray.free(subgradp);
 memarray.free(lmultp);
 memarray.free(del_statp);
}

// ===========================================================================
//                                  provide
// ===========================================================================

//chekcs if enough memory is available to store k constraint matrices,
//if not, new storage is allocated and the old information is copied to it

int Problem1::provide(Integer k)
{
 if (mempool<k){
     Coeffmat** hp;
     
     //Integer newmem=memarray.get(2*k*Integer(sizeof(Coeffmat *)),(char *)hp);
     //if (hp==0){newmem=memarray.get(k*Integer(sizeof(Coeffmat *)),(char *)hp);}
     //newmem/=Integer(sizeof(Coeffmat *));

     Integer newmem=max(2*mempool,k);
     hp=new Coeffmat*[newmem];
     if (hp==0) hp=new Coeffmat*[newmem=k];
     if (hp==0) return 1;
     Integer i;
     for(i=0;i<n;i++) hp[i]=pool[i];
     for(;i<newmem;i++) hp[i]=0;
     delete[] pool;
     pool=hp;
     mempool=newmem;    
 }
 if (memsign<k){
     char* hp;
     Integer newmem=memarray.get(max(2*memsign,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 2;
     Integer i;
     for(i=0;i<n;i++) hp[i]=signp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(signp);
     signp=hp;
     memsign=newmem;    
 }
 if (memrhs<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memrhs,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 3;
     Integer i;
     for(i=0;i<n;i++) hp[i]=rhsp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(rhsp);
     rhsp=hp;
     memrhs=newmem;    
 }
 if (memy<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memy,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 6;
     Integer i;
     for(i=0;i<n;i++) hp[i]=yp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(yp);
     yp=hp;
     memy=newmem;    
 }
 if (memscale<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memscale,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 8;
     Integer i;
     for(i=0;i<n;i++) hp[i]=scalep[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(scalep);
     scalep=hp;
     memscale=newmem;    
 }
 if (memsubgrad<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memsubgrad,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 9;
     Integer i;
     for(i=0;i<n;i++) hp[i]=subgradp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(subgradp);
     subgradp=hp;
     memsubgrad=newmem;    
 }
if (memlmult<k){
     Real* hp;
     Integer newmem=memarray.get(max(2*memlmult,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 11;
     Integer i;
     for(i=0;i<n;i++) hp[i]=lmultp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(lmultp);
     lmultp=hp;
     memlmult=newmem;    
 }
 if (memdelstat<k){
     char* hp;
     Integer newmem=memarray.get(max(2*memdelstat,k),hp);
     if (hp==0){newmem=memarray.get(k,hp);}
     if (hp==0) return 12;
     Integer i;
     for(i=0;i<n;i++) hp[i]=del_statp[i];
     for(;i<newmem;i++) hp[i]=0;
     memarray.free(del_statp);
     del_statp=hp;
     memdelstat=newmem;    
 }
 return 0;
}

// ===========================================================================
//                                   clear
// ===========================================================================

//delete old problem and reset values to be prepared for a new one

void Problem1::clear()
{
 Integer i;
 delete C; C=0;
 for(i=0;i<n;i++) {
     delete pool[i];
     pool[i]=0;
     signp[i]=0;
     rhsp[i]=0.;
     yp[i]=0.;
     scalep[i]=0.;
     subgradp[i]=0.;
     lmultp[i]=0.;
     del_statp[i]=0;
 }
 n=p=0;
 bigm_init=BIGM_NO_INIT;
 dense_constraints=0;
 subgrad_available=0;
}

// ===========================================================================
//                                   set_y
// ===========================================================================

void Problem1::set_y(const Matrix& y,const Matrix& eigva,const Matrix& eigve)
{
 if (y.dim()!= n) error("set_y: y has wrong dimension");
 mat_xey(n,yp,y.get_store());
 eigval=eigva;
 eigvecs=eigve;
}
      
// ===========================================================================
//                                  set_scale
// ===========================================================================

void Problem1::set_scale(const Matrix& s)
{
 if (s.dim()!= n) error("set_scale: s has wrong dimension");
 mat_xey(n,scalep,s.get_store());
}

// ===========================================================================
//                                  set_lmult
// ===========================================================================

void Problem1::set_lmult(const Matrix& s)
{
 if (s.dim()!= n) error("set_lmult: s has wrong dimension");
 mat_xey(n,lmultp,s.get_store());
}

// ===========================================================================
//                                init_subgrad
// ===========================================================================

//Compute the subgradient corresponding to the matrix P*Diag(d)*P'/sum(d)
//and store it. P has orthonormal columns and d is a nonengative vector
//of appropriate size.
//If "store_symsubg" is set, then the full matrix itself is stored, as well.

void Problem1::init_subgrad(const Matrix& P,const Matrix& d)
{
 tmpvec.init(d,1./sum(d));
 tmpvec.sqrt();
 tmpmat=P;
 tmpmat.scale_cols(tmpvec);
  
 if (store_symsubg){
     rankadd(tmpmat,symsubg);
 }
 if (store_sparsesubg){
     support_rankadd(tmpmat,sparsesubg);
 }

 subgrad_Cvalue=gramip_C(tmpmat);
 gram_opA(tmpmat,tmpvec);
 mat_xey(n,subgradp,tmpvec.get_store());
 subgrad_available=1;

}

// ===========================================================================
//                                update_subgrad
// ===========================================================================

//Compute the subgradient corresponding to the matrix P*Diag(d)*P'/sum(d)
//and add it to the stored subgradient.  P has orthonormal columns and
//d is a nonengative vector of appropriate size.
//If "store_symsubg" is set, then the full supbgradient matrix is updated,
//as well.

void Problem1::update_subgrad(const Matrix& P,const Matrix& d,Real alpha)
{
 tmpvec.init(d,alpha/sum(d));
 tmpvec.sqrt();
 tmpmat=P;
 tmpmat.scale_cols(tmpvec);
  
 if (store_symsubg){
     rankadd(tmpmat,symsubg,1.,1.-alpha);
 }
 if (store_sparsesubg){
     support_rankadd(tmpmat,sparsesubg,1.,1.-alpha);
 }

 subgrad_Cvalue*=(1-alpha);
 subgrad_Cvalue+=gramip_C(tmpmat);
 mat_xmultea(n,subgradp,1-alpha);
 gram_opA(tmpmat,tmpvec);
 mat_xpey(n,subgradp,tmpvec.get_store());

}

// ===========================================================================
//                                gramip_C
// ===========================================================================

Real Problem1::gramip_C(const Matrix& P)
{
 myclock.start();
 Real d= C->gramip(P);
 subgtime+=myclock.time();
 return d;
}

// ===========================================================================
//                                ip_C
// ===========================================================================

Real Problem1::ip_C(const Matrix& P,const Matrix& di)
{
 tmpmat=P;
 tmpmat.scale_cols(sqrt(di));
 return gramip_C(tmpmat);
}

// ===========================================================================
//                                gram_opA
// ===========================================================================

//compute <Ai,PP'> for i=1,..,n

void Problem1::gram_opA(const Matrix& P,Matrix& vec)
{
 myclock.start();
 vec.newsize(n,Integer(1)); chk_set_init(vec,1);
 for(Integer i=0;i<n;i++){
     vec(i)=pool[i]->gramip(P);
 }
 subgtime+=myclock.time();
}

// ===========================================================================
//                                  opA(P,d,vec)
// ===========================================================================

void Problem1::opA(const Matrix& P,const Matrix& di,Matrix& vec)
{
 tmpmat=P;
 tmpmat.scale_cols(sqrt(di));
 gram_opA(tmpmat,vec);
}


// ===========================================================================
//                                  opA(P,d)
// ===========================================================================

Matrix Problem1::opA(const Matrix& P,const Matrix& di)
{
 Matrix vec;
 opA(P,di,vec);
 return vec;
}

// ===========================================================================
//                              compute_sdpqp_coeff
// ===========================================================================
 
void Problem1::compute_sdpqp_coeff(Symmatrix& Q, Matrix& Clin, Real& a,
                                   Real &dcoeff, const Matrix& P, Real regparam,
                                  int do_scaling)
{
 a=lmax_multiplier;
 Integer m=P.coldim();
 Integer mc2=(m*(m+1))/2;
 Q.init(mc2,0.);

 //--- Clin=-svec(P'*C*P);
 C->project(tmpsym,P);
 svec(tmpsym,Clin);
 Clin*=-1;

 //--- Q=1/g*sum(svec(P^T*Ai*P)*svec(P^T*Ai*P)^T);
 //    Clin-=sum(svec(P^T*Ai*P)*(1/g*rhs[i]-y[i]));
 Matrix sv;
 Real scale=1.;
 dcoeff=0.;
 
 for(Integer i=0;i<n;i++){
     
     if (do_scaling) scale=1./scalep[i];
     pool[i]->project(tmpsym,P);
     svec(tmpsym,sv);
     Real d=(rhsp[i]-lmultp[i]);
     dcoeff+=d*(scale*d/(2*regparam)-yp[i]);
     Clin.xpeya(sv,-(scale*d/regparam-yp[i]));
     
     //- add the dyadic product to Q
     if (do_scaling) sv*=sqrt(scale);
     Real *svp=sv.get_store();
     Real *hp=Q.get_store();
     Integer k;
     for(Integer j=mc2; (k=j--)>0; ){
         Real *svc;
         d=*(svc=svp++);
         for(;--k>=0;) (*hp++)+=d*(*svc++);
     }
 }
 Q/=regparam;
} 

// ===========================================================================
//                            compute_aggregqp_coeff
// ===========================================================================
 
void Problem1::compute_aggregqp_coeff(Symmatrix& Q1, Matrix& Q12, Real& q2,
                                     Matrix& Clin1, Real& c2, Real& a,
                                     Real& dcoeff,
                                     const Matrix& P, Real regparam,
                                     int do_scaling)
{
 a=lmax_multiplier;
 Integer m=P.coldim();
 Integer mc2=(m*(m+1))/2;
 Q1.init(mc2,0.);
 Q12.init(mc2,1,0.);
 q2=0.;
 //Clin1=-svec(P'*C*P);
 C->project(tmpsym,P);
 svec(tmpsym,Clin1);
 Clin1*=-1;

 c2=-subgrad_Cvalue;

 Matrix sv;
 Real scale=1.;
 dcoeff=0.;
 for(Integer i=0;i<n;i++){
     if (do_scaling) scale=1./scalep[i];
     pool[i]->project(tmpsym,P);
     Real sg=subgradp[i];
     Real d=(rhsp[i]-lmultp[i]);
     dcoeff+=d*(scale*d/(2.*regparam)-yp[i]);
     c2-=sg*(scale*d/regparam-yp[i]);
     q2+=scale*sg*sg;
     svec(tmpsym,sv);
     //Q12+=sv*sg;
     Q12.xpeya(sv,scale*sg);
     //Clin1-=sv*((rhsp[i]-lmultp[i])/regparam-yp[i]);
     Clin1.xpeya(sv,-(scale*d/regparam-yp[i]));

     //- add the dyadic product to Q1
     if (do_scaling) sv*=sqrt(scale);
     {
       Real *hp=Q1.get_store();
       Real *svp=sv.get_store();
       for(Integer j=mc2; j>0; j--){
         Real *svc=svp++;
         Real d=*svc;
         for(Integer k=0;k<j;k++) (*hp++)+=d*(*svc++);
       }
     }
 }
 Q1/=regparam;
 Q12/=regparam;
 q2/=regparam;
} 

// ===========================================================================
//                              update_sdpqp_coeff
// ===========================================================================
 
void Problem1::update_sdpqp_coeff(Matrix& dClin, Real & dd,
                                 const Matrix& P, Real regparam,
                                 const Indexmatrix& ind,const Matrix& val,
                                 int do_scaling)
{
 Integer m=P.coldim();
 Integer mc2=(m*(m+1))/2;
 dClin.init(mc2,1,0.);
 Matrix sv;
 Real scale=1.;
 dd=0.;
 for(Integer k=0;k<ind.dim();k++){
     Integer i=ind(k);
     if (do_scaling) scale=1./scalep[i];
     //attention: lmultp is already the new value, the old one is lmultp-val
     dd-=val(k)*(scale*(rhsp[i]-lmultp[i]+val(k)/2.)/regparam-yp[i]);
     pool[i]->project(tmpsym,P);
     svec(tmpsym,sv);
     dClin.xpeya(sv,scale*val(k)/regparam);
 }
} 

// ===========================================================================
//                            update_aggregqp_coeff
// ===========================================================================
 
void Problem1::update_aggregqp_coeff( Matrix& dClin1, Real& dc2, Real & dd,
                                     const Matrix& P, Real regparam,
                                     const Indexmatrix& ind,const Matrix& val,
                                     int do_scaling)
{
 Integer m=P.coldim();
 Integer mc2=(m*(m+1))/2;
 dClin1.init(mc2,1,0.);
 dc2=0.;
 Matrix sv;
 Real scale=1.;
 dd=0.;
 for(Integer k=0;k<ind.dim();k++){
     Integer i=ind(k);
     if (do_scaling) scale=1./scalep[i];
     //attention: lmultp is already the new value, the old one is lmultp-val
     dd-=val(k)*(scale*(rhsp[i]-lmultp[i]+val(k)/2.)/regparam-yp[i]);
     dc2+=val(k)*scale*subgradp[i]/regparam;
     pool[i]->project(tmpsym,P);
     svec(tmpsym,sv);
     dClin1.xpeya(sv,scale*val(k)/regparam);
 }
} 


// ===========================================================================
//                                objval
// ===========================================================================

//objective value of stored solution
//(value is currently not stored in order to avoid mixups after the 
// deletion or addition of new constraints
 
Real Problem1::objval() const
{
 return lmax_multiplier*eigval(0)+mat_ip(n,rhsp,yp);
}
      
// ===========================================================================
//                            objval(lmax,y)
// ===========================================================================

//obejctive value for given maxeigval and y 

Real Problem1::objval(Real maxeigval,const Matrix& y)
{
 return lmax_multiplier*maxeigval+mat_ip(n,rhsp,y.get_store());
}

// ===========================================================================
//                             model_compute
// ===========================================================================
 
//Computes the eigenvalues of the projected matrix P'*(C-opAt y)*P
//and at the same time the rayleighvalues of the columns of P for this matrix. 
//primalvecs (=P) has to be orthonormal.

Real Problem1::model_compute(const Matrix& y,const Matrix& primalvecs,
                             Matrix& rayleighval)
{
  myclock.start();
  bigm.init(*this,y,dense_constraints);
  matrixtime+=myclock.time();
 bigm_init=BIGM_INIT_MODEL;
 last_bigm_y=y;
 bigm.lanczosmult(primalvecs,tmpmat);
 Integer k=primalvecs.coldim();
 Integer m=primalvecs.rowdim();
 rayleighval.newsize(k,1);
 chk_set_init(rayleighval,1);
 tmpsym.newsize(k);
 chk_set_init(tmpsym,1);
 for(Integer i=0;i<k;i++){
     rayleighval(i)=tmpsym(i,i)=mat_ip(m,primalvecs.get_store()+i*m,tmpmat.get_store()+i*m);
     for(Integer j=i+1;j<k;j++){
         tmpsym(i,j)=mat_ip(m,primalvecs.get_store()+i*m,tmpmat.get_store()+j*m);
     }
 }

 tmpsym.eig(tmpmat,tmpvec);
 Real lmax=max(tmpvec);
 if (subgrad_available){
     Real h=subgrad_Cvalue-mat_ip(n,subgradp,y.get_store());
     if (h>lmax) lmax=h;
 }
 return lmax_multiplier*lmax+mat_ip(n,rhsp,y.get_store());
}

// ===========================================================================
//                             obj_compute
// ===========================================================================
 
//Compute the maximal nreig eigenvalues of the matrix (C-opAt y)
//and store them and possible further approximations to large
//eigenvalues in eigva and the according eigenvectors in eigve. 
//If the maximal value of a vector is above eigval_bound, then
//it is sufficient to return only these approximations.
//rel_prec gives the precision required to regard an vector
//as eigenvector. 
//The number of converged eigenvalues (and eigenvectors) is returned
//in nreig.

int Problem1::obj_compute(const Matrix& y, Matrix& eigva,Matrix& eigve,
                          Real eigval_bound,Real rel_prec,Integer& nreig)
{
 nr_eval++;
 if (bigm_init==BIGM_INIT_MODEL){
     tmpvec=last_bigm_y;
     tmpvec-=y;
     if (norm2(tmpvec)>1e-16) {
         myclock.start();
         bigm.init(*this,y,dense_constraints);
         matrixtime+=myclock.time();
         bigm_init=BIGM_INIT_OBJ;
         last_bigm_y=y;
     }
 }
 else {
     myclock.start();
     bigm.init(*this,y,dense_constraints);
     matrixtime+=myclock.time();
     bigm_init=BIGM_INIT_OBJ;
     last_bigm_y=y;
 }
 if (dim<DENSE_LIMIT){
     bigm.make_symmatrix(tmpsym);
     tmpsym*=-1.;
     myclock.start();
     tmpsym.eig(eigve,eigva);
     eigvaltime+=myclock.time();
     eigva*=-1.;
     nreig=dim;
     if (out){
         (*out)<<" dense: ";
         (*out)<<myclock;
         (*out)<<" eigmax=";out->precision(8);(*out)<<eigva(0)<<endl;
     }
     return 0;
 }
 lanczos->set_relprec(rel_prec);
 if (exacteigs==0) {
   nreig=1;
   lanczos->enable_stop_above(eigval_bound);
 }
 else {
   lanczos->enable_stop_above(1e40);
   nreig=exacteigs;
 }
 //lanczos->set_do_out(1);
 //lanczos->set_myout(cout);
 //lanczos->set_nblockmult(15);

#ifdef PRINT_MATRICES
 char fname[80];
 sprintf(fname,"lanmat%ld.dat",nr_eval-1);
 ofstream fout(fname);
 fout<<bigm;
 fout.close();
#endif

 bigm.reset_nmult();

 myclock.start();

 Integer blocksz=1;
 int status=lanczos->compute(&bigm,eigva,eigve,nreig,blocksz);

 eigvaltime+=myclock.time();

 nreig=eigva.dim();
 if (out){
     (*out)<<"  {"<<lanczos->get_iter()<<","<<bigm.get_nmult()<<","<<nreig<<"} ";
     (*out)<<myclock;
 }
 //if (nreig>1) cout<<" lasteig="<<eigva(nreig-1);
 
 lanczos->get_lanczosvecs(eigva,eigve);

 if (out){
   if (nreig>0) {(*out)<<" eigmax=";out->precision(8);(*out)<<eigva(0);}
   else {(*out)<<" lanczval=";out->precision(8);(*out)<<eigva(0);}
   (*out)<<" rp=";out->precision(2);(*out)<<rel_prec;
   (*out)<<" vecs="<<eigva.dim();
   out->precision(8);(*out)<<" bd="<<eigval_bound<<endl;
   //for(Integer i=0;i<min(eigva.dim(),Integer(7));i++){
   //    (*out)<<" ";out->precision(8);out->width(10);(*out)<<eigva(i);
   //}
   //(*out)<<endl;
 }
 
 return status;
}

// ===========================================================================
//                             starting_point()
// ===========================================================================
 
// computes for the starting point the maximal eigenvalue and eigenvector.
// *yp is assumed to hold the starting point. 
// If (use_startheur) is true the algorithm computes the largest two 
// eigenvalues and eigenvectros and if the gap between these two is large
// relative to the function value then a few steepest descent steps are made.
// returns -1 if a feasible descent direction to -infty is found
// returns status of Lanczos computation otherwise

int Problem1::starting_point()
{
 nr_eval++;
 myclock.start();
 tmpvec.init(n,1,yp);
 bigm.init(*this,tmpvec,dense_constraints);
 matrixtime+=myclock.time();
 bigm_init=BIGM_INIT_OBJ;
 last_bigm_y=tmpvec;

 Integer nreig;
 int status=0;
 if (dim<DENSE_LIMIT){
   bigm.make_symmatrix(tmpsym);
   tmpsym*=-1.;
   myclock.start();
   tmpsym.eig(eigvecs,eigval);
   eigvaltime+=myclock.time();
   eigval*=-1.;
   nreig=dim;
   if (out){
     (*out)<<" dense: ";
     (*out)<<myclock<<" eigmax=";out->precision(8);(*out)<<eigval(0);
     if (use_startheur) (*out)<<" eig2=";out->precision(8);(*out)<<eigval(1);
     (*out)<<endl;
   }
 }
 else {
   lanczos->set_relprec(1e-5);
   lanczos->enable_stop_above(1e40);
   if (use_startheur) nreig=2;
   else nreig=max(Integer(1),exacteigs);
   Integer blocksz=1;

   bigm.reset_nmult();

   myclock.start();
   eigval.init(Integer(0),Integer(0),0.); 
   eigvecs.init(Integer(0),Integer(0),0.);
   status=lanczos->compute(&bigm,eigval,eigvecs,nreig,blocksz);

   eigvaltime+=myclock.time();

   nreig=eigval.dim();
   lanczos->get_lanczosvecs(eigval,eigvecs);

   if (out){
     (*out)<<"  {"<<lanczos->get_iter()<<","<<bigm.get_nmult()<<","<<nreig<<"} ";
     (*out)<<myclock<<" eigmax=";out->precision(8);(*out)<<eigval(0);
     if (use_startheur) (*out)<<" eig2=";out->precision(8);(*out)<<eigval(1);
     (*out)<<endl;
   }
 }

 if (!use_startheur) return status;

 Real oldeig2=eigval(1);
 Real objval0=objval(); 
 Real objdiff=(eigval(0)-eigval(1))*lmax_multiplier;
 if (out){
   (*out)<<"  objval="<<objval0<<"  objdiff="<<objdiff<<endl;
 }

 Matrix dy(n,Integer(1)); chk_set_init(dy,1); //negative step direction
 tmpmat.init(Integer(0),Integer(0),0.); //eigenvectors of opAt(dy)
 Matrix dyeigval;
 Matrix dyeigvec;
 Real deig2=0.;
 Integer startiter=0;

 while ((objdiff>0.01*(my_abs(objval0)+1))&&(startiter<5)){
   startiter++;

   //--- compute dy=b-opA(lmax_multiplier*vv^T)
   tmpvec.init(eigvecs.col(0),sqrt(lmax_multiplier));
   Real sgnorm=0.;
   Real ipbdy=0.;
   for(Integer i=0;i<n;i++){
     Real d=rhsp[i]-pool[i]->gramip(tmpvec);  //subgradient
     if ((signp[i]==LEQ)&&(d>0.)){d=0.;}      //use feasible recession directions
     else if ((signp[i]==GEQ)&&(d<0.)){d=0.;}
     ipbdy+=rhsp[i]*d;                         //compute linear term change
     sgnorm+=d*d;
     dy(i)=d;
   }
   
   if (startiter==1){
     //--- compute lmax of opAt(dy) as approximation of change in eig 2
     nr_eval++;
     myclock.start();
     bigm.init(*this,dy,dense_constraints,0);
     bigm_init=BIGM_NO_INIT;
     matrixtime+=myclock.time();

     if (dim<DENSE_LIMIT){
       bigm.make_symmatrix(tmpsym);
       tmpsym*=-1.;
       myclock.start();
       tmpsym.eig(dyeigvec,dyeigval);
       eigvaltime+=myclock.time();
       dyeigval*=-1.;
       nreig=dim;
       if (out){
	 (*out)<<" dy : dense: ";
	 (*out)<<myclock;
	 (*out)<<" dyeigmax=";out->precision(8);(*out)<<dyeigval(0)<<endl;
       }
     }
     else {
       nreig=1;
       Integer blocksz=1;
       myclock.start();
       status=lanczos->compute(&bigm,dyeigval,dyeigvec,nreig,blocksz);
       eigvaltime+=myclock.time();
       if (out){
	 (*out)<<"  dy {"<<lanczos->get_iter()<<","<<bigm.get_nmult()<<","<<nreig<<"} ";
	 (*out)<<myclock<<" dyeigmax=";out->precision(8);(*out)<<dyeigval(0)<<endl;
       }
     }
     //--- compute stepsize guess and step
     if (lmax_multiplier*dyeigval(0)-ipbdy<-eps_Real) { 
       //feasible descent direction to -infty
       mat_xpeya(n,yp,dy.get_store(),-1e20);
       return -1;
     }
     deig2=dyeigval(0);
   }

   Real t=objdiff/(sgnorm+(lmax_multiplier*deig2-ipbdy));   
   if (out){
     (*out)<<"  objval="<<objval0<<"  objdiff="<<objdiff<<" deig2="<<deig2;
     (*out)<<" sgnorm="<<sgnorm<<" ipbdy="<<ipbdy<<" t="<<t<<endl;
   }

   //--- compute new point
   tmpvec.init(n,1,yp);
   mat_xpeya(n,tmpvec.get_store(),dy.get_store(),-t);
   
   //--- compute largest two eigenvalues at new point
   nr_eval++;
   myclock.start();
   bigm.init(*this,tmpvec,dense_constraints);
   matrixtime+=myclock.time();
   bigm_init=BIGM_NO_INIT;

   if (dim<DENSE_LIMIT){
     bigm.make_symmatrix(tmpsym);
     tmpsym*=-1.;
     myclock.start();
     tmpsym.eig(tmpmat,dyeigval);
     eigvaltime+=myclock.time();
     dyeigval*=-1.;
     nreig=dim;
     if (out){
       (*out)<<" dense: ";
       (*out)<<myclock;
       (*out)<<" eigmax=";out->precision(8);(*out)<<dyeigval(0)<<endl;
       (*out)<<" eig2=";out->precision(8);(*out)<<dyeigval(1);
       (*out)<<" oldeig2=";out->precision(8);(*out)<<oldeig2<<endl;
     }
   }
   else {
     nreig=2;
     Integer blocksz=1;
     dyeigval=eigval;
     tmpmat=eigvecs;     //use old vectors to generate starting vectors
     
     bigm.reset_nmult();
     
     myclock.start();
     status=lanczos->compute(&bigm,dyeigval,tmpmat,nreig,blocksz);
     
     eigvaltime+=myclock.time();
     
     nreig=dyeigval.dim();
     if (out){
       (*out)<<"  {"<<lanczos->get_iter()<<","<<bigm.get_nmult()<<","<<nreig<<"} ";
       (*out)<<myclock<<" eigmax=";out->precision(8);(*out)<<dyeigval(0);
       (*out)<<" eig2=";out->precision(8);(*out)<<dyeigval(1);
       (*out)<<" oldeig2=";out->precision(8);(*out)<<oldeig2<<endl;
     }
   }
   Real newobjval=lmax_multiplier*dyeigval(0)-t*ipbdy;
   if (objval0-newobjval<.01*t*sgnorm) {
     //progress is less than 10% of predicted value, linear approximation is bad   
     break;
   }

   //--- accept new point and continue
   objval0=newobjval;
   objdiff=(dyeigval(0)-dyeigval(1))*lmax_multiplier;
   deig2=max(0.,(dyeigval(1)-oldeig2)/t); //linear approximation to change in eig 2
   oldeig2=dyeigval(1);
   mat_xey(n,yp,tmpvec.get_store()); 
   bigm_init=BIGM_INIT_OBJ;
   last_bigm_y=tmpvec;
   if (dim<DENSE_LIMIT) {
     eigval=dyeigval;
     eigvecs=tmpmat;
   }
   else{
     lanczos->get_lanczosvecs(eigval,eigvecs);
   }
  

 }
 
 if (out){
   out->precision(8);
   (*out)<<"  objval="<<objval0<<"  objdiff="<<objdiff<<endl;
 }

 return status;
}

// ===========================================================================
//                                   add
// ===========================================================================

//add another constraint <A,X> ~ r
//  m   points to the coefficient matrix
//  s   is from GEQ LEQ EQU
//  r   is the right hand side 
// scaleval is per default 1.
// del_stat specifies whether the constraint may be deleted or not
// returns 0 on success, 1 if allocation failed
// if the subgradient is available explicitly, then the
// corresponding value is computed immediately, otherwise
// an existing subgradient is removed.
// the corresponding new y variable is initialized to zero.
 
int Problem1::add(Coeffmat* m,char s,Real r,Real scaleval,char del_stat)
{
 if (provide(n+1)) return 1;
 pool[n]=m;
 if (pool[n]->dense()) dense_constraints++;
 signp[n]=s;
 rhsp[n]=r;
 yp[n]=0.;
 scalep[n]=scaleval;
 if ((subgrad_available)&&(store_symsubg)&&(symsubg.rowdim()==dim)){
     subgradp[n]=m->ip(symsubg);
 }
 else if ((subgrad_available)&&(store_sparsesubg)&&(m->support_in(sparsesubg))){
     subgradp[n]=m->ip(sparsesubg);
 }
 else {
     subgradp[n]=0.; 
     subgrad_available=0;
 }
 bigm_init=BIGM_NO_INIT;
 lmultp[n]=0.;
 del_statp[n]=del_stat;
 n++;
 return 0;
}


// ===========================================================================
//                              remove
// ===========================================================================
 
// deletes all constraints whose index is contained in ind.
// produces an error and terminates if ind requires the deletion of
// a constraint with del_stat!=DEL_STAT_YES or if sind
// is infeasible (eg requires the deletion of one constaint twice)
// it returns 0 on success

int Problem1::remove(const Indexmatrix& ind)
{
 if (ind.dim()==0) return 0;
 Indexmatrix sind=sortindex(ind);
 if (ind(sind(0))<p) error("delete: ind is not feasible");
 Integer i;    //running index 
 Integer j;    //index to be copied to
 Integer k=0;    //index to next delete index
 Integer delind=ind(sind(0)); //index to be deleted next
 j=i=delind;
 for(;i<n;i++){
     if (i==delind) {
         if (del_statp[i]!=DEL_STAT_YES)
             error("delete: del_stat does not allow deletion");
         if (pool[i]->dense()) dense_constraints--; 
         delete pool[i];
         pool[i]=0;
         if (++k<ind.dim()) delind=ind(sind(k));
         else delind=n;
         if (delind<=i) error("delete: ind is not feasible");
         continue;
     }
     pool[j]=pool[i];
     signp[j]=signp[i];
     rhsp[j]=rhsp[i];
     yp[j]=yp[i];
     scalep[j]=scalep[i];
     subgradp[j]=subgradp[i];
     lmultp[j]=lmultp[i];
     del_statp[j]=del_statp[i];
     j++;
 }

 n-=ind.dim();
 bigm_init=BIGM_NO_INIT;
 
 return 0;
}

// ===========================================================================
//                            constraints_out
// ===========================================================================
 
//writes all constraints to o in the form A ~ rhs

int Problem1::constraints_out(ostream& o) const
{
 if (!o) return 1;
 o<<n<<"\n";o.precision(20);
 Integer i;
 for(i=0;i<n;i++){
     pool[i]->out(o);
     o<<signp[i]<<" "<<rhsp[i]<<"\n";
 }
 if (!o) return 1;
 return 0;
}
 
// ===========================================================================
//                            constraints_in
// ===========================================================================

//reads some additional constraints from is

int Problem1::constraints_in(istream& is)
{
 if (!is) {
     cerr<<"*** ERROR: Problem1::constraints_in(): ";
     cerr<<"istream broken without reading";
     is.clear(ios::failbit);
     return 1;
 } 
 Integer m;
 if (!(is>>m)){
     cerr<<"*** ERROR: Problem1::constraints_in(): ";
     cerr<<"reading number of constraints failed"<<endl;
     is.clear(ios::failbit);
     return 1;
 }
 if (m==0) return 0;
 if(m<0) {
     cerr<<"*** ERROR: Problem1::constraints_in(): ";
     cerr<<"number of constraints must be nonnegative, but is "<<m<<endl;
     is.clear(ios::failbit);
     return 1;
 }
 if (provide(n+m)) {
     cerr<<"*** ERROR: Problem1::constraints_in(): ";
     cerr<<"allocation of memory for "<<n<<" old and ";
     cerr<<m<<" new constraints failed"<<endl;
     is.clear(ios::failbit);
     return 1;
 }   
 provide(n+m);
 n+=m;
 for(Integer i=n-m;i<n;i++){
     pool[i]=coeffmat_read(is);
     if (pool[i]==0) {
         cerr<<"*** ERROR: Problem1::constraints_in(): ";
         cerr<<" failed in reading coefficient matrix #";
         cerr<<i-n<<endl;
         is.clear(ios::failbit);
         return 1;
     }   
     if (pool[i]->dense()) dense_constraints++;
     if (dim!=pool[i]->dim()) {
         cerr<<"*** ERROR: Problem1::constraints_in(): ";
         cerr<<" dimensions of constraint matrices do not match!\n";
         cerr<<"           dim of costmatrix = "<<dim;
         cerr<<", dim of constraint matrix #"<<i-n<<" = "<<pool[i]->dim();
         cerr<<endl;
         is.clear(ios::failbit);
         return 1;
     }   
     if (!(is>>signp[i])){
         cerr<<"*** ERROR: Problem1::constraints_in(): ";
         cerr<<" failed in reading constraint sign of constraint #";
         cerr<<i-n<<endl;
         is.clear(ios::failbit);
         return 1;
     }            
     if ((signp[i]!=EQU)&&(signp[i]!=LEQ)&&(signp[i]!=GEQ)){
         cerr<<"*** ERROR: Problem1::constraints_in(): ";
         cerr<<" failed in reading constraint sign of constraint #";
         cerr<<i-n<<"\n";
         cerr<<"           should be one of "<<EQU<<", "<<LEQ<<", "<<GEQ;
         cerr<<", but is "<<signp[i]<<endl;
         is.clear(ios::failbit);
         return 1;
     }   
     if (!(is>>rhsp[i])){
         cerr<<"*** ERROR: Problem1::constraints_in(): ";
         cerr<<" failed in reading right hand side of constraint #";
         cerr<<i-n<<endl;
         is.clear(ios::failbit);
         return 1;
     }            
     yp[i]=0.;
     scalep[i]=1.;
     lmultp[i]=0.;
     del_statp[i]=DEL_STAT_NO;
     if ((subgrad_available)&&(store_symsubg)&&(symsubg.rowdim()==dim)){
         subgradp[i]=pool[i]->ip(symsubg);
     }
     else if ((subgrad_available)&&(store_sparsesubg)&&(pool[n]->support_in(sparsesubg))){
         subgradp[n]=pool[i]->ip(sparsesubg);
     }
     else {
         subgradp[i]=0.; 
         subgrad_available=0;
     }
 }
 bigm_init=BIGM_NO_INIT;
 return 0;
}
        
// ===========================================================================
//                            read_problem
// ===========================================================================

//throws away and existing old problem and reads an entire problem, 
//cost matrix and constraints from is. 
//If do_scaling is not null all input constraints are scaled permanently
//to norm 1.
//all input constraints are marked as permanent constraints that may
//not be deleted. 

int Problem1::read_problem(istream& is,int do_scaling)
{
 clear();
 if (!is) {
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"istream broken without reading";
     is.clear(ios::failbit);
     return 1;
 }   
 if (!(is>>lmax_multiplier)){
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"reading multiplier for lmax failed"<<endl;
     is.clear(ios::failbit);
     return 1;
 }   
 if(lmax_multiplier<1e-10) {
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"multiplier for lmax too small: "<<lmax_multiplier<<endl;
     is.clear(ios::failbit);
     return 1;
 }
 C=coeffmat_read(is);
 if (C==0) {
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"failed in reading cost matrix ";
     is.clear(ios::failbit);
     return 1;
 }   
 dim=C->dim();
 if (C->dense()) dense_constraints++;
 Integer m;
 if (!(is>>m)){
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"reading number of constraints failed"<<endl;
     is.clear(ios::failbit);
     return 1;
 }   
 if(m<1) {
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"number of constraints must be positive, but is "<<m<<endl;
     is.clear(ios::failbit);
     return 1;
 }
 if (provide(m)) {
     cerr<<"*** ERROR: Problem1::read_problem(): ";
     cerr<<"allocation of memory for "<<m<<" constraints failed"<<endl;
     is.clear(ios::failbit);
     return 1;
 } 
 n=m;
 Integer i;
 for(i=0;i<n;i++){
     pool[i]=coeffmat_read(is);
     if (pool[i]==0) {
         cerr<<"*** ERROR: Problem1::read_problem(): ";
         cerr<<" failed in reading coefficient matrix #";
         cerr<<i<<endl;
         is.clear(ios::failbit);
         return 1;
     }
     if (pool[i]->dense()) dense_constraints++;
     if (dim!=pool[i]->dim()) {
         cerr<<"*** ERROR: Problem1::read_problem(): ";
         cerr<<" dimensions of constraint matrices do not match!\n";
         cerr<<"           dim of costmatrix = "<<dim;
         cerr<<", dim of constraint matrix #"<<i<<" = "<<pool[i]->dim();
         cerr<<endl;
         is.clear(ios::failbit);
         return 1;
     }   
     if (!(is>>signp[i])){
         cerr<<"*** ERROR: Problem1::read_problem(): ";
         cerr<<" failed in reading constraint sign of constraint #";
         cerr<<i<<endl;
         is.clear(ios::failbit);
         return 1;
     }            
     if ((signp[i]!=EQU)&&(signp[i]!=LEQ)&&(signp[i]!=GEQ)){
         cerr<<"*** ERROR: Problem1::read_problem(): ";
         cerr<<" failed in reading constraint sign of constraint #";
         cerr<<i<<"\n";
         cerr<<"           should be one of "<<EQU<<", "<<LEQ<<", "<<GEQ;
         cerr<<", but is "<<signp[i]<<endl;
         is.clear(ios::failbit);
         return 1;
     }   
     if (!(is>>rhsp[i])){
         cerr<<"*** ERROR: Problem1::read_problem(): ";
         cerr<<" failed in reading right hand side of constraint #";
         cerr<<i<<endl;
         is.clear(ios::failbit);
         return 1;
     }            
     yp[i]=0.;
     scalep[i]=1.;
     subgradp[i]=0.;
     lmultp[i]=0.;
     if (do_scaling){
         Real scale=1./pool[i]->norm();
         pool[i]->multiply(scale);
         rhsp[i]*=scale;
     }
     del_statp[i]=DEL_STAT_NO;
 }
 p=n;
 bigm_init=BIGM_NO_INIT;
 subgrad_available=0;
 eigvaltime=Microseconds(long(0));
 return 0;
}
        
// ===========================================================================
//                            write_problem
// ===========================================================================
 
int Problem1::write_problem(ostream& os) const
{
 if (!os) return 1;
 os.precision(20);os<<lmax_multiplier<<"\n";
 C->out(os);
 os<<"\n";
 os<<n<<"\n";
 for(Integer i=0;i<n;i++){
     pool[i]->out(os);
     os<<signp[i]<<" ";
     os.precision(20);os<<rhsp[i]<<"\n";
 }
 if (!os) return 1;
 return 0;
}
        
// ===========================================================================
//                              write_y
// ===========================================================================
 
int Problem1::write_y(ostream& os) const
{
 if (!os) return 1;
 tmpvec.init(n,1,yp);
 os<<tmpvec;
 if (!os) return 1;
 return 0;
}

// ===========================================================================
//                              read_y
// ===========================================================================
 
int Problem1::read_y(istream& in)
{
 if (!in)  {
     cerr<<"*** ERROR: Problem1::read_y(): instream bad even before starting"<<endl;
     return 1;
 }
 in>>tmpvec;
 if (!in) {
     cerr<<"*** ERROR: Problem1::read_y(): failed in reading y"<<endl;
     return 1;
 }
 if (tmpvec.dim()!=n) {
     cerr<<"*** ERROR: Problem1::read_y(): dimension of y is "<<tmpvec.dim();
     cerr<<", but number of constraints is "<<n<<endl;
     return 1;
 }
 int retval=0;
 for(Integer i=0;i<n;i++){
     Real d=yp[i]=tmpvec(i);
     if (rhsp[i]==GEQ){
         if (d>0.) {
             cerr<<"*** ERROR: Problem1::read_y(): y("<<i<<")="<<d;
             cerr<<">0, but constraint sense is "<<GEQ<<endl;
             retval=1;
         }
         continue;
     }
     if (rhsp[i]==LEQ){
         if (d<0.) {
             cerr<<"*** ERROR: Problem1::read_y(): y("<<i<<")="<<d;
             cerr<<"<0, but constraint sense is "<<LEQ<<endl;
             retval=1;
         }
         continue;
     }
 }
 
 return retval;
}

// ===========================================================================
//                                    save
// ===========================================================================

//call also lanczos->save
 
ostream& Problem1::save(ostream& os) const
{
 os.precision(20);
 write_problem(os);
 
 os.precision(20);
 os<<p<<"\n";
 {for(Integer i=0;i<n;i++) os<<yp[i]<<"\n";}
 {for(Integer i=0;i<n;i++) os<<scalep[i]<<"\n";}
 {for(Integer i=0;i<n;i++) os<<subgradp[i]<<"\n";}
 {for(Integer i=0;i<n;i++) os<<lmultp[i]<<"\n";}
 {for(Integer i=0;i<n;i++) os<<del_statp[i]<<"\n";}

 os<<eigval<<eigvecs;

 os<<store_symsubg<<"\n";
 os<<symsubg;
 os<<store_sparsesubg<<"\n";
 os<<sparsesubg;

 lanczos->save(os);
 os<<exacteigs<<"\n";  

 os<<dense_constraints<<"\n";
    
 os<<subgrad_available<<"\n"; 
 os<<subgrad_Cvalue<<"\n";

 os<< eigvaltime<<"\n";
 os<< nr_eval<<"\n";
 return os;
}
    
// ===========================================================================
//                                    restore
// ===========================================================================
 
//calls also lanczos->restore(), so the correct method has to be set already

istream& Problem1::restore(istream& in)
{
 bigm_init=0;
 read_problem(in);
 
 in>>p;
 {for(Integer i=0;i<n;i++) in>>yp[i];}
 {for(Integer i=0;i<n;i++) in>>scalep[i];}
 {for(Integer i=0;i<n;i++) in>>subgradp[i];}
 {for(Integer i=0;i<n;i++) in>>lmultp[i];}
 {for(Integer i=0;i<n;i++) in>>del_statp[i];}

 in>>eigval>>eigvecs;

 in>>store_symsubg;
 in>>symsubg;
 in>>store_sparsesubg;
 in>>sparsesubg;

 lanczos->restore(in);
 in>>exacteigs;  

 in>>dense_constraints;
    
 in>>subgrad_available; 
 in>>subgrad_Cvalue;

 in>> eigvaltime;
 in>> nr_eval;
 return in;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: problem1.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:39  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:49  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/problem1.cc,v $
 --------------------------------------------------------------------------- */
