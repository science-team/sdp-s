/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "singleton_matrix.h"


/*----------------------------------------------------------------------*/

void write_singleton(int dim, int i, int j, double val, FILE *fp, char **str)
{
 char buffer[40];
 int str_size;
 
 
 if(fp)
 	{
	 fprintf(fp, "SINGLETON\n");
	 fprintf(fp, "%d %d %d %f\n", dim, i, j, val);
	}
 if(str)
 	{
	 sprintf(buffer, "SINGLETON\n%d %d %d %f\n", dim, i, j, val);
	 str_size = strlen(buffer) + strlen(*str) + 1;
	 *str = (char *)realloc(*str, str_size*sizeof(char));
	 *str = strcat(*str, buffer);
	}
 if(!fp && !str)
 	{
	 printf("SINGLETON\n");
	 printf("%d %d %d %f\n", dim, i, j, val);
	}
}

/*----------------------------------------------------------------------*/

