/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include "annexes.h"
#include "SDP_pb.h"
#include "SDPS_main.h"

#define OUT stdout

char *filenameSB = NULL;
char *filename_resume = NULL;
int flag_fSB = 0;
int PRINT_DEBUG = 0;
char *filename_result = NULL;
char *master_file;
FILE *RES = NULL;
extern int NB_VAR;



/* ------------------------------------------------------------------------- */

void SDP_show_options()
{
  fprintf(OUT,"\tOptions are:\n");
  fprintf(OUT,"\t[ -fSB <filename> filename will be a problem file in SB format\n");
  fprintf(OUT,"\t  -fopt <filename> filename contains options for the SDP solver SB\n");
  fprintf(OUT,"\t  -fres <filename> filename will contain the ouput of SDP_S\n");
  fprintf(OUT,"\t  -nosolve write only a file in SB format\n");
  fprintf(OUT,"\t  -resume <filename> resumes an old process using the resume file filename ]\n");
}

/* ------------------------------------------------------------------------- */

char **SDP_read_option_file(char *filename, int *argc_SB)
{
  FILE *fp;
  char **argv_SB = NULL;
  char buffer[40];
 
 
 
  *argc_SB = 0;
  fp = fopen(filename, "r");
  if(fp == NULL)
    {
      fprintf(OUT,"Can't open file \"%s\".\nNo option read.\n");
      return NULL;
    }
 
  while(!feof(fp))
    {
      if(fscanf(fp, "%s", buffer) != EOF)
	{
	  (*argc_SB)++;
	  argv_SB = (char **)realloc(argv_SB, (*argc_SB)*sizeof(char *));
	  argv_SB[(*argc_SB)-1] = (char *)malloc((strlen(buffer)+1)*sizeof(char));
	  strcpy(argv_SB[(*argc_SB)-1], buffer);
	}
    }
  fclose(fp);
 
  return argv_SB;
}

/* ------------------------------------------------------------------------- */

int SDP_read_options(int argc, char **argv, int *flag_BB, int *flag_prim, char **filenameSB, 
		      int *argc_SB, char ***argv_SB, int *flag_nosolve, int *flag_fSB,int *flag_opt, char **filename_resume)
{
  int i;
  int retval;
 
  i = 2;
  retval = 0;
  while(i<argc)
    {
      if(same_strings(argv[i], "-nosolve"))  *flag_nosolve = 1;
      else if(same_strings(argv[i], "-debug"))  PRINT_DEBUG = 1;
      else if(same_strings(argv[i], "-fSB"))
	{
	  i++;
	  if(i>=argc)
	    {
	      fprintf(OUT,"Error : Option \"-fSB\" requires a filename.\nexiting.\n");
	      exit(1);
	    }
	  *filenameSB = (char *)malloc((strlen(argv[i])+1)*sizeof(char));
	  strcpy(*filenameSB, argv[i]);
	  *flag_fSB = 1;
	}
      else if(same_strings(argv[i], "-fres"))
	{
	  i++;
	  if(i>=argc)
	    {
	      fprintf(OUT,"Error : Option \"-fres\" requires a filename.\nexiting.\n");
	      exit(1);
	    }
	  filename_result = (char *)malloc((strlen(argv[i])+1)*sizeof(char));
	  strcpy(filename_result, argv[i]);
	  if ( (RES=fopen(filename_result,"w"))==NULL )
	   {
	 	  fprintf(OUT,"Warning : error while creating the output file.\n");
                  fprintf(OUT,"The ouput will not be saved.\n");
		  filename_result = NULL;
	   }
	 }
      else if(same_strings(argv[i], "-fopt"))
	{
	  i++;
	  if(i>=argc)
	    {
	      fprintf(OUT,"Error : Option \"-fopt\" requires a filename.\nexiting.\n");
	      exit(1);
	    }
	  *argv_SB = SDP_read_option_file(argv[i], argc_SB);
	  *flag_opt = 1;
	}
      else if(same_strings(argv[i], "-resume"))
        {
	  i++;
	  if(i>=argc)
	    {
	      fprintf(OUT,"Error : Option \"-resume\" requires a filename.\nexiting.\n");
	      exit(1);
	    }
	  *filename_resume = (char *)malloc((strlen(argv[i])+1)*sizeof(char));
	  strcpy(*filename_resume, argv[i]);   	
          retval = 1;
        }	      
      else
	{
	  fprintf(OUT,"Error : Invalid option \"%s\".\nexiting.\n", argv[i]);
	  exit(1);
	}
      i++;
    }
  return retval;
}

/* ------------------------------------------------------------------------ */

void get_vector()
{
  int sizePb,i,j;
  double res;
  FILE *fp;
  fp = fopen("result","r");
  if( fp == 0){
    fprintf(OUT,"Error: no result file\n");
    exit(-1);
  }
  fscanf(fp,"%d",&sizePb);
  fprintf(OUT,"Vector x : %d\n",sizePb);

  for(i=0; i<sizePb; i++){
    for(j=0; j<(sizePb-i);j++){
      fscanf(fp,"%lf",&res);
      if(j == 0)
	fprintf(OUT,"%lf ",res);
    }
  }
  fprintf(OUT,"\n");
}


/* ------------------------------------------------------------------------- */

int main(int argc, char **argv)
{
  SDP_pb *pb;
  SDP_sol *sol;
  int i;
  char **argv_SB = NULL;
  int argc_SB = 0;

  int flag_BB   = 0;
  int flag_prim = 0;
  int flag_nosolve = 0;
  int flag_opt = 0;
  char sys[200];

   fprintf(OUT,"\n\nSDP_S version 1.1, Copyright (C) 2002-2007 JP.Aubry,G.Delaporte,S.Jouteau,F.Roupin\n");
   
   fprintf(OUT,"\nSDP_S comes with ABSOLUTELY NO WARRANTY\n");
  if(argc < 2)
    {
      fprintf(OUT,"\nUsage : %s <problem filename> [options]\n", argv[0]);
      SDP_show_options();
      return 1;
    }
  if (SDP_read_options(argc, argv, &flag_BB, &flag_prim, &filenameSB, &argc_SB, &argv_SB,&flag_nosolve,&flag_fSB,&flag_opt,&filename_resume))
 {
// resume
   if (RES)
   {
    master_file = (char *)malloc((strlen(argv[1]))*sizeof(char));
    sprintf(master_file,"%s",argv[1]);	  
   }
   argc_SB = 3;
   argv_SB = (char **)malloc(argc_SB*sizeof(char *));
   argv_SB[1] = (char *)malloc((strlen("-resume")+1)*sizeof(char));
   argv_SB[2] = (char *)malloc((strlen(filename_resume)+1)*sizeof(char));   
   argv_SB[0] = (char *)malloc(5*sizeof(char));
   strcpy(argv_SB[0], "./sb");
   strcpy(argv_SB[1], "-resume");
   strcpy(argv_SB[2], filename_resume);
   
   if(filenameSB == NULL)
    {
      filenameSB = (char *)malloc((strlen(argv[1]))*sizeof(char));
      sprintf(filenameSB,"%s",argv[1]);
      sprintf(&(filenameSB[strlen(argv[1])-2]),"sb");
    }   
   if (RES) fclose(RES);
   pb = create_SDP_pb(argv[1], filenameSB);
   NB_VAR = pb->obj_f->N;
   sol = sb_main(argc_SB,argv_SB); 
   if (filename_result)
   {	   
     if ( (RES=fopen(filename_result,"a"))==NULL )
       {
	 fprintf(OUT,"Warning : error while opening the output file.\n");
	 fprintf(OUT,"The ouput will not be saved.\n");
	 filename_result = NULL;
       }	  
   }

   if(sol == NULL)
      {
        fprintf(OUT,"***** Problem can't be solved\n");
	if (filename_result) fprintf(RES,"***** Problem can't be solved\n");
	return 1;
      }
   fprintf(OUT,"\n -------- SDP bound : %lf\n\n", (pb->obj_f->min_max)*(sol->val));
   fprintf(OUT,"Vector x : \n");
   if (filename_result)
	 fprintf(RES,"\n -------- SDP bound : %lf\n\n", (pb->obj_f->min_max)*(sol->val));
   if (filename_result) fprintf(RES,"Vector x : \n");
   if(sol->x == NULL) fprintf(OUT,"x est nul :(\n");
   for(i=0;i<pb->obj_f->N;i++)
    {
     fprintf(OUT,"%lf  ", sol->x[i]);
     if(!((i+1)%5)) fprintf(OUT,"\n");
    }
  fprintf(OUT,"\n\n");
  if (filename_result)
  {
   for(i=0;i<pb->obj_f->N;i++)
    {
     fprintf(RES,"%lf  ", sol->x[i]);
     if(!((i+1)%5)) fprintf(RES,"\n");
    }
    fprintf(RES,"\n\n");	
  }
  
  free_SDP_sol(sol);
   
 }

// not a resume file  
 else  
 {
  if (RES)
   {
    master_file = (char *)malloc((strlen(argv[1]))*sizeof(char));
    sprintf(master_file,"%s",argv[1]);	  
   }
  if(filenameSB == NULL)
   {
    filenameSB = (char *)malloc((strlen(argv[1]))*sizeof(char));
    sprintf(filenameSB,"%s",argv[1]);
    sprintf(&(filenameSB[strlen(argv[1])-2]),"sb");
   }
   pb = create_SDP_pb(argv[1], filenameSB);

   if(pb == NULL)
   {
    fprintf(OUT,"Exiting\n#\n");
    if(!flag_fSB)
    {
      sprintf(sys, "rm %s",filenameSB);
      system(sys);
    }
    return -1;
   }
 
   if(flag_nosolve)
   {
    fprintf(OUT,"The problem hasn't been solved, filename in the SB format : \n%s\n#\n",filenameSB);
    return -1; 
   } 
   if(!flag_opt)
     {
      char filename_opt[100];
      FILE *tmp;
    
      sprintf(filename_opt,"%s",argv[1]);
      sprintf(&(filename_opt[strlen(argv[1])-2]),"opt");
      tmp = fopen(filename_opt,"r");

      if(tmp == 0)
      {
        tmp = fopen(filename_opt,"w");
        fprintf(tmp,"-si 1 -sh 1\n");
        fclose(tmp);
      }
      argv_SB = SDP_read_option_file(filename_opt, &argc_SB); 
     } 
   if (RES) fclose(RES);
   sol = get_SDP_sol(pb, argc_SB, argv_SB, 0,filenameSB);
   if (filename_result)
   {	   
     if ( (RES=fopen(filename_result,"a"))==NULL )
     {
  	  fprintf(OUT,"Warning : error while opening the output file.\n");
          fprintf(OUT,"The ouput will not be saved.\n");
 	  filename_result = NULL;
     }	  
   }
  
   if(sol == NULL)
      {
        fprintf(OUT,"***** Problem can't be solved\n");
	if (filename_result) fprintf(RES,"***** Problem can't be solved\n");

	return 1;
      }
   fprintf(OUT,"\n -------- SDP bound : %lf\n\n", sol->val);
   fprintf(OUT,"Vector x : \n");
   if (filename_result) fprintf(RES,"\n -------- SDP bound : %lf\n\n", sol->val);
   if (filename_result) fprintf(RES,"Vector x : \n");
   for(i=0;i<pb->obj_f->N;i++)
    {
     fprintf(OUT,"%lf  ", sol->x[i]);
     if(!((i+1)%5)) fprintf(OUT,"\n");
    }
  fprintf(OUT,"\n\n");  
  if (filename_result)
  {
   for(i=0;i<pb->obj_f->N;i++)
    {
     
     fprintf(RES,"%lf  ", sol->x[i]);
     if(!((i+1)%5)) fprintf(RES,"\n");
    }
    fprintf(RES,"\n\n");	
  }
  free_SDP_sol(sol);
 } 
 free_SDP_pb(pb);
 if(argv_SB)
    {
      for(i=0;i<argc_SB;i++) free(argv_SB[i]);
    }

  free(argv_SB);
  if(!flag_fSB)
  {
    sprintf(sys,"rm %s",filenameSB);
    system(sys);
  }
  if (RES)
   {
      fprintf(OUT,"Output was saved in file : %s\n", filename_result);
      fclose(RES);
   }
 
  fprintf(OUT,"\n#\n"); 
  return 0;
}
