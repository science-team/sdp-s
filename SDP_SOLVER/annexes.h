/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef annexes_h
#define annexes_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>


 int same_strings(char *s1, const char *s2);
 double difference(double d1, double d2, double e);
 int is_integer(double d, double e);
 int get_integer(double d, double e);
 double *get_x_from_fileW(char *fileW);
 char *file2string(char *filename);
 
#endif
