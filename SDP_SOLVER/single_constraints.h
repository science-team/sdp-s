/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef single_constraints_h
#define single_constraints_h

#include <stdio.h>
#include <stdlib.h>

#include "annexes.h"
#include "matrix_tools.h"

 typedef struct
	{
	 int i;
	 int j;
	 double val;
	 char op;
	} singleton_type;

 typedef struct
	{
	 int n;
	 int N;
	 int nb;
	 singleton_type **constraints;
	} singleton_constraints;


 singleton_constraints *create_singleton_constraints(int n, int N);
 void free_singleton_constr(singleton_constraints *s);
 void add_singleton(singleton_constraints *constr, int i, int j, char op, double val);
 void write_single_constr(int dim, singleton_constraints *constr, FILE *fp, char **str);
 int  valid_single_constr(singleton_constraints *constr, double **X, double e);
 
#endif
