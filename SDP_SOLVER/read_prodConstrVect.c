/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "constraints.h"

#define SPARSE 0
#define DENSE 1

#define LEFT 1
#define RIGHT 2



double *getVector(FILE *fp,int n,int sparse_dense)
{
  double buffer_d;
  double *vect; 
  int j;

  /* and then the vector */
  vect = (double *)malloc(n*sizeof(double));
  for(j=0;j<n;j++){
    vect[j] = 0.;
  }
  if(sparse_dense == DENSE){ /* Dense */
    for(j=0;j<n;j++){
      fscanf(fp, "%lf", &buffer_d);
      vect[j] = buffer_d;
    }
  }
  else{ /* Sparse */
    int nb_el;
    int ii;

    fscanf(fp, "%d", &nb_el);
    for(j=0;j<nb_el;j++){
      fscanf(fp, "%d %lf", &ii, &buffer_d);
      /* indexes are between 1 and n, and we want them between 0 and n-1 */
      vect[ii-1] = buffer_d;
    }
  }
  return vect;
}

void fill_constrProdVect(char *filename,quadratic_constraints *constr,int sparse_dense,int left_right)
{
  FILE *fp;
  char buffer[140];
  int buffer_int;
  double buffer_d;
 
  int i, j ;
  int ii, jj;
  int iii,jjj;
  int nb_el = 0;
  char type; /* 0 for '=', 1 for '<', 2 for '>' and 3 for '<>' */
  int n=0;
  int k=0;
  double value=0;
  double c1,c2;
  double *vect;
  quadratic_constraint **pt_constr;


  //printf("**reading prodConstrVect\n");

  /* READING OF THE HEADER : L= | L< | L> | L<>*/
  fp = fopen(filename, "r");
  fscanf(fp, "%s", buffer);
  //printf("*****\n%s left_right : %d\n",buffer,left_right);
  if(buffer[0] != 'l' && buffer[0] != 'L'){
    printf("ProductConstrVect is valid only for linear constraints\n");
    return;
  }
  if(strlen(buffer)>1)
    {
      if(buffer[1] == '=')      type = 0; /* dx = c         */
      else if(buffer[1] == '<')
	{
	  if(buffer[2] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[1] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }
  else
    {
      fscanf(fp, "%s", buffer);

      if(buffer[0] == '=')      type = 0; /* dx = c         */
      else if(buffer[0] == '<')
	{
	  if(buffer[1] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[0] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }

  /* READING OF N:dimension AND K:nb of constraints in this file */
  fscanf(fp, "%d", &n);
  fscanf(fp, "%d", &k);
  constr->n = n;

  /* ALLOCATION / REALLOCATION OF MEMORY TO STOCK CONSTRAINTS */
  buffer_int = constr->nb;
  constr->nb += n*k;
  printf("Adding %d product constraints made from file %s\n",n*k,filename);
  if ( ( constr->constraints = (quadratic_constraint **)realloc(constr->constraints, (constr->nb)*sizeof(quadratic_constraint *))) == NULL)
    printf("Memory Re-Allocation Error!!!\n");
  pt_constr = &(constr->constraints[buffer_int]);
 
  /* READING THE CONSTRAINTS */
  for(i=0;i<k;i++){
    fscanf(fp, "%lf", &value);
    if(type == 3){
      c1 = value;
      fscanf(fp,"%lf", &c2);
      if(left_right == LEFT){
	;
      }
      else{
	value = c2;
      }
    }

    vect = getVector(fp,n,sparse_dense);
    nb_el = 0;
    for(iii=0;iii<n;iii++){
      if(vect[iii] != 0)
	nb_el++;
    }


    for(j=0;j<n;j++){
      if ( (pt_constr[i*n+j] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint))) == NULL)
	printf("Memory Allocation Error!!!\n");
      /* then the vector */
      if ( (pt_constr[i*n+j]->l = (double *)malloc(n*sizeof(double))) == NULL)
	printf("Memory Allocation Error!!!\n");

      pt_constr[i*n+j]->dim = n;
      /* c | c1 c2 | c2 c1 */
      if(type == 0){
	pt_constr[i*n+j]->c1 = 0;
	pt_constr[i*n+j]->c2 = 0;
	pt_constr[i*n+j]->flag_ci = 3;
      }
      if(type == 1){
	pt_constr[i*n+j]->c1 = 0;
	pt_constr[i*n+j]->c2 = 0;
	pt_constr[i*n+j]->flag_ci = 2;
      }
      if(type == 2){
	pt_constr[i*n+j]->c1 = 0;
	pt_constr[i*n+j]->c2 = 0;
	pt_constr[i*n+j]->flag_ci = 1;
      }
      if(type == 3){
	pt_constr[i*n+j]->c1 = 0;
	pt_constr[i*n+j]->c2 = 0;
	if(left_right == LEFT)
	  pt_constr[i*n+j]->flag_ci = 2;
	else
	  pt_constr[i*n+j]->flag_ci = 1;
      }
      if(value != 0){
	if ( (pt_constr[i*n+j]->el_l = (int *)malloc(sizeof(int)))==NULL)
	  printf("Memory Allocation Error!!!\n");
	pt_constr[i*n+j]->nb_l = 1;
	pt_constr[i*n+j]->el_l[0] = j;
	pt_constr[i*n+j]->l[j] = -value;//-xi*c
      }
      else{
	pt_constr[i*n+j]->nb_l = 0;
	free(pt_constr[i*n+j]->l);
	pt_constr[i*n+j]->el_l = NULL;
	pt_constr[i*n+j]->l = NULL;
      }


      /* and the matrix **/
      pt_constr[i*n+j]->Q = create_sym_mat(n, nb_el);
      jjj = 0;
      for(jj=0;jj<n;jj++){ //constr*xi
	if(vect[jj] != 0){
	  pt_constr[i*n+j]->Q->index_i[jjj] = jj;
	  pt_constr[i*n+j]->Q->index_j[jjj] = j;
	  pt_constr[i*n+j]->Q->val[jjj] = (j == jj)?2*vect[jj]:vect[jj];
	  jjj ++;
	}
      }
    }
    free(vect);
  }
}




/** fpos of fp is at the beginning of the constraints ****/ 
void read_prodConstrVect(FILE *fp,constraints *constr)
{
  char buffer[200];
  char buffer2[200];
  char *ptr;
  fpos_t p,p_tmp;
  int res=0;
  int n = constr->n;
  int N = constr->N;
  int left_right=RIGHT;
  
  fgetpos(fp, &p);
  while(!feof(fp)){
    res = fscanf(fp, "%s", buffer);

    if(buffer[0] == '_')
      continue;

    if(((ptr = strstr(buffer, "ProductConstrVect")) != NULL) && res == 1){
      FILE *file_tmp;
      int type=0;
      int sparse_dense; /* 0 for sparse ; 1 for dense */
      unsigned char c;
      int found = 0;

      int i;

      if((ptr = strstr(buffer,"Left")) != NULL)
	left_right = LEFT;
      else if((ptr = strstr(buffer,"Right")) != NULL)
	left_right = RIGHT;

      if((ptr = strstr(buffer,"(")) == NULL){
	printf("*** error : Usage is : ProductConstrVect(<filename>) or ProductConstrVect Left/Right(<filename>)\n");
	return;
      }

      for(i=0;i<strlen(ptr);i++){
	if((ptr[i+1] != ')') && (ptr[i+1] != 0))
	  buffer2[i] = ptr[i+1];
	else{
	  buffer2[i] = 0;
	  break;
	}
      }



      fgetpos(fp,&p_tmp);
      fsetpos(fp,&p);
      
      while(!found){
	ptr = fgets(buffer,200,fp);
	if(ptr == NULL){
	  printf("*** error in ProductConstrVect, filename %s not found\n",buffer2);
	  fsetpos(fp,&p_tmp);
	  break;
	}
	if(strstr(buffer,buffer2) != NULL){
	  if(strstr(buffer,"Sparse")){
	    sparse_dense = 0;
	    fsetpos(fp,&p_tmp);
	    found = 1;
	  }
	  else{
	    if(strstr(buffer,"Dense")){
	      sparse_dense = 1;
	      fsetpos(fp,&p_tmp);
	      found = 1;
	    }
	  }
	}
      }

      if(found){
	if(!constr->quad){
	  constr->quad = create_quad_constr(n, N);
	}
	fill_constrProdVect(buffer2,constr->quad,sparse_dense,left_right);
      }
    }
  }


}


/************************************************************************************/
/*
 *  MODIF 18/12/2002:
 *  function constrProdVectbar
 * 
 * */




void fill_constrProdVectbar(char *filename,quadratic_constraints *constr,int sparse_dense,int left_right)
{
  FILE *fp;
  char buffer[140];
  int buffer_int;
  double buffer_d;
 
  int i, j ;
  int ii, jj;
  int iii,jjj;
  int nb_el = 0;
  char type; /* 0 for '=', 1 for '<', 2 for '>' and 3 for '<>' */
  int n=0;
  int k=0;
  double value=0;
  double c1,c2;
  double *vect;
  quadratic_constraint **pt_constr;


  /* READING OF THE HEADER : L= | L< | L> | L<>*/
  fp = fopen(filename, "r");
  fscanf(fp, "%s", buffer);
  if(buffer[0] != 'l' && buffer[0] != 'L'){
    printf("ProductConstrBarVect is valid only for linear constraints\n");
    return;
  }
  if(strlen(buffer)>1)
    {
      if(buffer[1] == '=')      type = 0; /* dx = c         */
      else if(buffer[1] == '<')
	{
	  if(buffer[2] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[1] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }
  else
    {
      fscanf(fp, "%s", buffer);

      if(buffer[0] == '=')      type = 0; /* dx = c         */
      else if(buffer[0] == '<')
	{
	  if(buffer[1] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[0] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }

  /* READING OF N:dimension AND K:nb of constraints in this file */
  fscanf(fp, "%d", &n);
  fscanf(fp, "%d", &k);
  constr->n = n;

  /* ALLOCATION / REALLOCATION OF MEMORY TO STOCK CONSTRAINTS */
  buffer_int = constr->nb;
  constr->nb += n*k;
  printf("Adding %d product (1-xi) constraints made from file %s\n",n*k,filename);
  if ( ( constr->constraints = (quadratic_constraint **)realloc(constr->constraints, (constr->nb)*sizeof(quadratic_constraint *))) == NULL)
    printf("Memory Re-Allocation Error!!!\n");
  pt_constr = &(constr->constraints[buffer_int]);
 
  /* READING THE CONSTRAINTS */
  for(i=0;i<k;i++){
    fscanf(fp, "%lf", &c1);
    value = c1;    
    if(type == 3)
    {
//      c1 = value;
      fscanf(fp,"%lf", &c2);
      if(left_right == LEFT){
	;
      }
      else{
	value = c2;
      }
    }

    vect = getVector(fp,n,sparse_dense);
    nb_el = 0;
    for(iii=0;iii<n;iii++){
      if(vect[iii] != 0)
	nb_el++;
    }


    for(j=0;j<n;j++){
      if ( (pt_constr[i*n+j] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint))) == NULL)
	printf("Memory Allocation Error!!!\n");
      /* then the vector */
      if ( (pt_constr[i*n+j]->l = (double *)malloc(n*sizeof(double))) == NULL)
	printf("Memory Allocation Error!!!\n");

      pt_constr[i*n+j]->dim = n;
      /* c | c1 c2 | c2 c1 */
      if(type == 0){
	pt_constr[i*n+j]->c1 = 2*value;
	pt_constr[i*n+j]->c2 = 2*value;
	pt_constr[i*n+j]->flag_ci = 3;
      }
      if(type == 1){
	pt_constr[i*n+j]->c1 = 2*value;
	pt_constr[i*n+j]->c2 = 2*value;
	pt_constr[i*n+j]->flag_ci = 2;
      }
      if(type == 2){
	pt_constr[i*n+j]->c1 = 2*value;
	pt_constr[i*n+j]->c2 = 2*value;
	pt_constr[i*n+j]->flag_ci = 1;
      }
      if(type == 3){
	if(left_right == LEFT)
	{
  	   pt_constr[i*n+j]->c1 = 2*c1;
	   pt_constr[i*n+j]->c2 = 2*c1;
	   pt_constr[i*n+j]->flag_ci = 2;
	}
	else
	{
  	   pt_constr[i*n+j]->c1 = 2*c2;
	   pt_constr[i*n+j]->c2 = 2*c2; 
	   pt_constr[i*n+j]->flag_ci = 1;
	}
      }
      // max of non-zero elements is n
	if ( (pt_constr[i*n+j]->el_l = (int *)malloc(sizeof(int)*n))==NULL)
		
	  printf("Memory Allocation Error!!!\n");
	
	// linear part : d.x+cxj i.e. vect.x+valuexj
	// vect.x
        jjj = 0;
	for(jj=0;jj<n;jj++)
	{
	  if (jj != j)
	  { 	
	     if (vect[jj] != 0)
	      {	  
		   pt_constr[i*n+j]->el_l[jjj] = jj;
		   pt_constr[i*n+j]->l[jj] = vect[jj];
        	   jjj++;
	      }
	  }
	  else //jj = j
	  {
	    if( (vect[j]+value) != 0)
            {
   	      pt_constr[i*n+j]->el_l[jjj] = jj;      
              pt_constr[i*n+j]->l[jj] = vect[jj]+value;//+xj*(c+vectj)
	      jjj++; 
            }  
	  }	   
	}
	
       pt_constr[i*n+j]->nb_l = jjj; //was = 1

      /* Quadratic part : -d.xxi i.e. -vect.xxi**/
      pt_constr[i*n+j]->Q = create_sym_mat(n, nb_el);
      jjj = 0;
      for(jj=0;jj<n;jj++){ //constr*(-xi)
	if(vect[jj] != 0){
	  pt_constr[i*n+j]->Q->index_i[jjj] = jj;
	  pt_constr[i*n+j]->Q->index_j[jjj] = j;
	  pt_constr[i*n+j]->Q->val[jjj] = (j == jj)?-2*vect[jj]:-vect[jj];
	  jjj ++;
	}
      }
    }
    free(vect);
  }
}




/** fpos of fp is at the beginning of the constraints ****/ 
void read_prodConstrVectbar(FILE *fp,constraints *constr)
{
  char buffer[200];
  char buffer2[200];
  char *ptr;
  fpos_t p,p_tmp;
  int res=0;
  int n = constr->n;
  int N = constr->N;
  int left_right=RIGHT;
  
  fgetpos(fp, &p);
  while(!feof(fp)){
    res = fscanf(fp, "%s", buffer);

    if(buffer[0] == '_')
      continue;

    if(((ptr = strstr(buffer, "ProductConstrBarVect")) != NULL) && res == 1){
      FILE *file_tmp;
      int type=0;
      int sparse_dense; /* 0 for sparse ; 1 for dense */
      unsigned char c;
      int found = 0;

      int i;

      if((ptr = strstr(buffer,"Left")) != NULL)
	left_right = LEFT;
      else if((ptr = strstr(buffer,"Right")) != NULL)
	left_right = RIGHT;

      if((ptr = strstr(buffer,"(")) == NULL){
	printf("*** error : use of ProductConstrBarVect is : ProductConstrBarVect(<filename>) or ProductConstrBarVect Left/Right(<filename>)\n");
	return;
      }

      for(i=0;i<strlen(ptr);i++){
	if((ptr[i+1] != ')') && (ptr[i+1] != 0))
	  buffer2[i] = ptr[i+1];
	else{
	  buffer2[i] = 0;
	  break;
	}
      }



      fgetpos(fp,&p_tmp);
      fsetpos(fp,&p);
      
      while(!found){
	ptr = fgets(buffer,200,fp);
	if(ptr == NULL){
	  printf("*** error in ProductConstrBarVect, filename %s hasn't been found\n",buffer2);
	  fsetpos(fp,&p_tmp);
	  break;
	}
	if(strstr(buffer,buffer2) != NULL){
	  if(strstr(buffer,"Sparse")){
	    sparse_dense = 0;
	    fsetpos(fp,&p_tmp);
	    found = 1;
	  }
	  else{
	    if(strstr(buffer,"Dense")){
	      sparse_dense = 1;
	      fsetpos(fp,&p_tmp);
	      found = 1;
	    }
	  }
	}
      }

      if(found){
	if(!constr->quad){
	  constr->quad = create_quad_constr(n, N);
	}
	fill_constrProdVectbar(buffer2,constr->quad,sparse_dense,left_right);
      }
    }
  }
}




