/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  

  
#ifndef product_constraints_h
#define product_constraints_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quadratic_constraints.h"
#include "linear_constraints.h"

 
 quadratic_constraints *add_productlinvar(quadratic_constraints *quad, linear_constraints *lin, int num, int min, int max, int n, int N);


#endif
