/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "sum_constraints.h"

/* ------------------------------------------------------------------------- */

quadratic_constraints *add_diagsum(quadratic_constraints *quad, char op, double val, int n, int N)
{
 quadratic_constraints *result = quad;
 int nb;
 sym_matrix *Q;
 int i;
 
 
 if(!result)
 	{
	 result = create_quad_constr(n, N);
	}

 nb = result->nb;
 result->constraints = (quadratic_constraint **)realloc(result->constraints, (nb+1)*sizeof(quadratic_constraint *));
 result->constraints[nb] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint));
 result->nb = nb+1;
 Q = create_sym_mat(N, N);
 for(i=0;i<N;i++)
 	{
	 Q->index_i[i] = i;
	 Q->index_j[i] = i;
	 Q->val[i]     = 1.;
	}
 result->constraints[nb]->dim = N;
 result->constraints[nb]->c1  = val;
 result->constraints[nb]->c2  = val;
 result->constraints[nb]->l   = NULL;
 result->constraints[nb]->Q   = Q;
 if(op == '=')      result->constraints[nb]->flag_ci = 3;
 else if(op == '<') result->constraints[nb]->flag_ci = 1;
 else if(op == '>') result->constraints[nb]->flag_ci = 2;
 
 return result;
}

/* ------------------------------------------------------------------------- */

linear_constraints *add_varsum(linear_constraints *lin, char op, double val, int min, int max, int n, int N)
{
 linear_constraints *result = lin;
 int nb;
 double *l;
 int i;
 
 
 if(op != '=')
 	{
	 printf("ERROR : varsum requires the operator '='. Any other operator is forbidden\n");
	 return lin;
	}
 if(!result)
 	{
	 result = create_lin_constr(n, N);
	}

 nb = result->nb;
 result->constraints = (linear_constraint **)realloc(result->constraints, (nb+1)*sizeof(linear_constraint *));
 result->constraints[nb] = (linear_constraint *)malloc(sizeof(linear_constraint));
 result->nb = nb+1;
 l = (double *)malloc(N*sizeof(double));
 for(i=0;i<N;i++)
 	{
	 l[i] = 0.;
	}
 for(i=min-1;i<max;i++) l[i]  = 1.;
 result->constraints[nb]->c1 = val;
 result->constraints[nb]->c2 = val;
 result->constraints[nb]->l  = l;
// CORRECTION 22/01/2002 FRED
 result->constraints[nb]->flag_ci = 3;

 
 return result;
}

/* ------------------------------------------------------------------------- */

quadratic_constraints *add_rowsum(quadratic_constraints *quad, char op, double val, int num_row, int n, int N)
{
 quadratic_constraints *result = quad;
 int nb;
 sym_matrix *Q;
 int i;
 
 
 if(!result)
 	{
	 result = create_quad_constr(n, N);
	}
 nb = result->nb;
 result->constraints = (quadratic_constraint **)realloc(result->constraints, (nb+1)*sizeof(quadratic_constraint *));
 result->constraints[nb] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint));
 result->nb = nb+1;
 Q = create_sym_mat(N, n);
 /* The matrix should be symmetric */
 for(i=0;i<n;i++)
 	{
	 Q->index_i[i] = i;
	 Q->index_j[i] = num_row-1;
	 Q->val[i]     = 1.;
	 if(i == num_row-1) Q->val[i] += 1.;
	}
 /* And then the sum will be counted 2 times */
 result->constraints[nb]->dim = N;
 result->constraints[nb]->c1  = 2.*val;
 result->constraints[nb]->c2  = 2.*val;
 result->constraints[nb]->l   = NULL;
 result->constraints[nb]->Q   = Q;
 if(op == '=')      result->constraints[nb]->flag_ci = 3;
 else if(op == '<') result->constraints[nb]->flag_ci = 1;
 else if(op == '>') result->constraints[nb]->flag_ci = 2;
 if(op == '=')      result->constraints[nb]->flag_ci = 3;
 else if(op == '<') result->constraints[nb]->flag_ci = 1;
 else if(op == '>') result->constraints[nb]->flag_ci = 2;
 
 return result;
}

/* ------------------------------------------------------------------------- */

quadratic_constraints *add_squaresum(quadratic_constraints *quad, char op, double val, int imin, int imax, int jmin, int jmax, int n, int N)
{
 quadratic_constraints *result = quad;
 int nb;
 sym_matrix *Q;
 int i, j;
 int nb_el = (imax-imax+1)*(jmax-jmax+1);
 int cpt;

 if(!result)
 	{
	 result = create_quad_constr(n, N);
	}
 nb = result->nb;
 result->constraints = (quadratic_constraint **)realloc(result->constraints, (nb+1)*sizeof(quadratic_constraint *));
 result->constraints[nb] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint));
 result->nb = nb+1;
 Q = create_sym_mat(N, nb_el);
 /* The matrix should be symmetric */
 cpt = 0;
 for(i=imin-1;i<imax;i++)
 	for(j=jmin-1;j<jmax;j++)
 		{
		 Q->index_i[cpt] = i;
		 Q->index_j[cpt] = j;
		 Q->val[cpt]     = 1.;
		 if(i==j) Q->val[cpt] += 1.;
		 cpt++;
		}
 /* And then the sum will be counted 2 times */
 result->constraints[nb]->dim = N;
 result->constraints[nb]->l   = NULL;
 result->constraints[nb]->Q   = Q;
 result->constraints[nb]->c1  = 2.*val;
 result->constraints[nb]->c2  = 2.*val;
 if(op == '=')      result->constraints[nb]->flag_ci = 3;
 else if(op == '<') result->constraints[nb]->flag_ci = 1;
 else if(op == '>') result->constraints[nb]->flag_ci = 2;
 
 return result;
}

/* ------------------------------------------------------------------------- */

quadratic_constraints *add_triangsum(quadratic_constraints *quad, char op, double val, int min, int max, int n, int N, int strict)
{
 quadratic_constraints *result = quad;
 int nb;
 sym_matrix *Q;
 int i, j;
 int nb_el;
 int cpt;
 
 
 cpt = max-min+1;
 nb_el = cpt*(cpt+1-2*strict);
 if(!result)
 	{
	 result = create_quad_constr(n, N);
	}
 nb = result->nb;
 result->constraints = (quadratic_constraint **)realloc(result->constraints, (nb+1)*sizeof(quadratic_constraint *));
 result->constraints[nb] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint));
 result->nb = nb+1;
 Q = create_sym_mat(N, nb_el);
 /* The matrix should be symmetric */
 cpt = 0;
 for(i=min-1;i<max;i++)
 	for(j=i+strict;j<max;j++)
 		{
		 Q->index_i[cpt] = i;
		 Q->index_j[cpt] = j;
		 Q->val[cpt]     = 1.;
		 if(i==j) Q->val[cpt] += 1.;
		 cpt++;
		}
 /* And then the sum will be counted 2 times */
 result->constraints[nb]->dim = N;
 result->constraints[nb]->c1  = 2.*val;
 result->constraints[nb]->c2  = 2.*val;
 result->constraints[nb]->l   = NULL;
 result->constraints[nb]->Q   = Q;
 if(op == '=')      result->constraints[nb]->flag_ci = 3;
 else if(op == '<') result->constraints[nb]->flag_ci = 1;
 else if(op == '>') result->constraints[nb]->flag_ci = 2;
 
 return result;
}

/* ------------------------------------------------------------------------- */


