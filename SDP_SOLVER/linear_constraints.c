/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "linear_constraints.h"

extern int PRINT_DEBUG;


/* ------------------------------------------------------------------------- */

linear_constraints *create_lin_constr(int n, int N)
{
  linear_constraints *result;
 
 
  result = (linear_constraints *)malloc(sizeof(linear_constraints));
  result->n  = n;
  result->N  = N;
  result->nb = 0;
  result->constraints = NULL;
 
  return result;
}

/* ------------------------------------------------------------------------- */

void free_lin_constr(linear_constraints *l)
{
  int i;
 
  for(i=0;i<l->nb;i++)
    {
      free(l->constraints[i]->l);
    }
  free(l->constraints);
  free(l);
}

/* ------------------------------------------------------------------------- */

int nb_lin_constr(linear_constraints *constr)
{
  int i;
  int flag;
  int result = 0;
  double c1, c2;
 
 
  for(i=0;i<constr->nb;i++)
    {
      flag = constr->constraints[i]->flag_ci;
      c1   = constr->constraints[i]->c1;
      c2   = constr->constraints[i]->c2;
      if(flag == 3 && c1 == c2) result += 2;
      else result++;
    }
 
  return result;
}

/* ------------------------------------------------------------------------- */

int read_lin_constr(char *filename, linear_constraints *constr, int sparse_dense)
{
  FILE *fp;
  char buffer[80];
  int buffer_int;
  double buffer_d;
 
  int i, j;
  char type; /* 0 for '=', 1 for '<', 2 for '>' and 3 for '<>' */
  int n;
  int k;
  linear_constraint **pt_constr;
 
 
  /* READING OF THE HEADER : L= | L< | L> | L<>*/
  if( !( fp = fopen(filename, "r"))) 	 printf("Error while opening the linear constraint file \"%s\".\n", filename);
  else  printf("Reading Linear constraints in file \"%s\".\n", filename);
  fflush(NULL);
  fscanf(fp, "%s", buffer);
  if(strlen(buffer)>1)
    {
      if(buffer[1] == '=')      type = 0; /* dx = c         */
      else if(buffer[1] == '<')
	{
	  if(buffer[2] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[1] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return 1;
	}
    }
  else
    {
      fscanf(fp, "%s", buffer);

      if(buffer[0] == '=')      type = 0; /* dx = c         */
      else if(buffer[0] == '<')
	{
	  if(buffer[1] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[0] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return 1;
	}
    }
 
  /* READING OF N:dimension AND K:nb of constraints in this file */
  fscanf(fp, "%d", &n);
  fscanf(fp, "%d", &k);
  constr->n = n;
 
  /* ALLOCATION / REALLOCATION OF MEMORY FOR CONSTRAINTS */
  buffer_int = constr->nb;
  constr->nb += k;
  constr->constraints = (linear_constraint **)realloc(constr->constraints, (constr->nb)*sizeof(linear_constraint *));
  pt_constr = &(constr->constraints[buffer_int]);
 
  /* READING OF THE CONSTRAINTS */
  for(i=0;i<k;i++)
    {
      pt_constr[i] = (linear_constraint *)malloc(sizeof(linear_constraint));
      /* c | c1 c2 | c2 c1 */
      if(type == 0)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->c2 = buffer_d;
	  pt_constr[i]->flag_ci = 3;
	}
      else if(type == 1) /* greater or equal to ...*/
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->flag_ci = 3;
	}
      else if(type == 2) /* less or equal to ... */
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c2 = buffer_d;
	  pt_constr[i]->flag_ci = 3;
	}
      else if(type == 3)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c2 = buffer_d;
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->flag_ci = 3;
	}
	 
      /* and then the vector */
      pt_constr[i]->l = (double *)malloc(n*sizeof(double));
      for(j=0;j<n;j++)
	{
	  pt_constr[i]->l[j] = 0.;
	}
      if(sparse_dense == 1) /* Dense */
	{
	  for(j=0;j<n;j++)
	    {
	      fscanf(fp, "%lf", &buffer_d);
	      pt_constr[i]->l[j] = buffer_d;
	    }
	}
      else /* Sparse */
	{
	  int nb_el;
	  int ii;
		 
	  fscanf(fp, "%d", &nb_el);
	  for(j=0;j<nb_el;j++)
	    {
	      fscanf(fp, "%d %lf", &ii, &buffer_d);
	      /* indexes are between 1 and n, and we want them between 0 and n-1 */
	      pt_constr[i]->l[ii-1] = buffer_d;
	    }
	}

      if(type == 1){
	double add;
	/* compute an upper bound s.t. : c2 = sum[for i st li>0] li */
	pt_constr[i]->c2 = 0;
	for(j=0;j<n;j++){
	  if((add=pt_constr[i]->l[j]) > 0)
	    pt_constr[i]->c2 += add;
	}
      }

      if(type == 2){
	double add;
	/* compute a low bound s.t. : c2 = sum[for i st li<0] li */
	pt_constr[i]->c1 = 0;
	for(j=0;j<n;j++){
	  if((add=pt_constr[i]->l[j]) < 0)
	    pt_constr[i]->c1 += add;
	}
      }      
    }
  printf("%d linear constraints read.\n",k);
  fclose(fp);
 
  return 0;
}

/* ------------------------------------------------------------------------- */

void write_lin_constr(int dim, linear_constraints *constr, FILE *fp, char **str)
{
  int i, j;
  double c1, c2;
  int flag_ci;
  int n = constr->n;
  double *vect;
  sparse_matrix *sp_mat;
  char buffer[40];
  int str_size;
 
 
  for(i=0;i<constr->nb;i++)
    {
      vect = constr->constraints[i]->l;
      c1 = constr->constraints[i]->c1;
      c2 = constr->constraints[i]->c2;
      flag_ci = constr->constraints[i]->flag_ci;
	 
      if((flag_ci < 3) || c1 == c2)
	{
	  sp_mat = vector_2_sparse_mat(n, vect);
	  sp_mat->n = dim;
	  for(j=0;j<sp_mat->nb_elements;j++) sp_mat->index_j[j] = n;
	  write_sym_mat((sym_matrix *)sp_mat, fp, str);
	  free_sparse_mat(sp_mat);
	}

      if ( (c1 == c2) && ( (flag_ci == 3) || (flag_ci == 4) ) ) 
      {
//	 printf("falg_ci = %d\n",flag_ci);
        if(flag_ci == 3)  /* equality with square*/
       	{
	  if(fp) fprintf(fp, "= %f\n", 2.*c1);
	  if(str)
	    {
	      sprintf(buffer, "= %f\n", 2.*c1);
	      str_size = strlen(buffer) + strlen(*str) + 1;
	      *str = (char *)realloc(*str, str_size*sizeof(char));
	      *str = strcat(*str, buffer);
	    }
	  if(!fp && !str) printf("= %f\n", 2.*c1);

	  sp_mat = vector_2_sparse_mat(n, vect);
	  sp_mat->n = dim;
	  write_lowrank_sparse_sparse(sp_mat, sp_mat, fp, str);
	  free_sparse_mat(sp_mat);
	  if(fp) fprintf(fp, "= %f\n", 2.*c1*c1);
	  if(str)
	    {
	      sprintf(buffer, "= %f\n", 2.*c1*c1);
	      str_size = strlen(buffer) + strlen(*str) + 1;
	      *str = (char *)realloc(*str, str_size*sizeof(char));
	      *str = strcat(*str, buffer);
	    }
	  if(!fp && !str) printf("= %f\n", 2.*c1*c1);
	 }
        //////////////////////////////////
	//  MODIF 7/4/2003
	//  GESTION DE LA FONCTION SIMPLE	
        if (flag_ci == 4) /* equality without square : function "Simple()"*/
	{
	  if(fp) fprintf(fp, "= %f\n", 2.*c1);
	  if(str)
	    {
	      sprintf(buffer, "= %f\n", 2.*c1);
	      str_size = strlen(buffer) + strlen(*str) + 1;
	      *str = (char *)realloc(*str, str_size*sizeof(char));
	      *str = strcat(*str, buffer);
	    }
	  if(!fp && !str) printf("= %f\n", 2.*c1);

/*	  sp_mat = vector_2_sparse_mat(n, vect);
	  sp_mat->n = dim;
	  write_lowrank_sparse_sparse(sp_mat, sp_mat, fp, str);
	  free_sparse_mat(sp_mat);
	  if(fp) fprintf(fp, "= %f\n", 2.*c1*c1);
	  if(str)
	    {
	      sprintf(buffer, "= %f\n", 2.*c1*c1);
	      str_size = strlen(buffer) + strlen(*str) + 1;
	      *str = (char *)realloc(*str, str_size*sizeof(char));
	      *str = strcat(*str, buffer);
	    }
	  if(!fp && !str) printf("= %f\n", 2.*c1*c1);
*/	 
	}
      }
      else /* inequality */
	{
	  double **mat;
	  int j, j2;
		 
		 
	  if(flag_ci == 1) /* greater or equal */
	    {
	      if(fp) fprintf(fp, "> %f\n", 2.*c1);
	      if(str)
		{
		  sprintf(buffer, "> %f\n", 2.*c1);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("> %f\n", 2.*c1);
	    }
		 
	  if(flag_ci == 2) /* less or equal */
	    {
	      if(fp) fprintf(fp, "< %f\n", 2.*c2);
	      if(str)
		{
		  sprintf(buffer, "< %f\n", 2.*c2);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("< %f\n", 2.*c2);
	    }
	  if(flag_ci == 3) /* c1 and c2 used in inequality */
	    {
	      mat = (double **)malloc((n+1)*sizeof(double *));
	      for(j=0;j<n+1;j++)
		{
		  mat[j] = (double *)malloc((n+1)*sizeof(double));
		  for(j2=0;j2<n+1;j2++) mat[j][j2] = 0.;
		}
	      /* Only the upper part of the matrix will be used */
	      for(j=0;j<n;j++)
		{
		  if(vect[j])
		    {
		      /* Linear term */
		      mat[j][n] =  - (c1+c2)*vect[j]*0.5;
					 
		      for(j2=j;j2<n;j2++)
			{
			  if(vect[j2])
			    {
			      mat[j][j2] = vect[j]*vect[j2];
			    }
			}
		    }
		}
	      write_sym_mat_mat(dim, n+1, mat, fp, str);
	      if(fp) fprintf(fp, "< %f\n", -c1*c2);
	      if(str)
		{
		  sprintf(buffer, "< %f\n", -c1*c2);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("< %f\n", -c1*c2);
	    }
	}
    }
  if(PRINT_DEBUG)
    printf("Linear constraints written\n");
}

/* ------------------------------------------------------------------------- */

int valid_lin_constr(linear_constraints *constr, double *x, double e)
{
  int N  = constr->N;
  int nb = constr->nb;
 
  double c1, c2;
  double *l;
 
  int i, j;
  double v;
 
 
  for(i=0;i<nb;i++)
    {
      c1 = constr->constraints[i]->c1;
      c2 = constr->constraints[i]->c2;
      l  = constr->constraints[i]->l;
	 
      v = 0.;
      for(j=0;j<N;j++) v += x[j]*l[j];
	 
      if(difference(c1,v,e)>0. || difference(c2,v,e)<0.) return 0;
    }
 
  return 1;
}

/* ------------------------------------------------------------------------- */


