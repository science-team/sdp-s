/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "constraints.h"

extern int PRINT_DEBUG;

/* ------------------------------------------------------------------------- */

constraints *create_constraints(int n, int N)
{
  constraints *result;
 
 
  result = (constraints *)malloc(sizeof(constraints));
  result->n = n;
  result->N = N;
  result->single = NULL;
  result->lin    = NULL;
  result->quad   = NULL;
 
  return result;
}

/* ------------------------------------------------------------------------- */

void free_constraints(constraints *c)
{
  if(c)
    {
      if(c->single) free_singleton_constr(c->single);
      if(c->lin)    free_lin_constr(c->lin);
      if(c->quad)   free_quad_constr(c->quad);
      free(c);
    }
}

/* ------------------------------------------------------------------------- */

int get_range(char *str_range, int *min, int *max, int n, int N)
{
  /* str_range is a string like "{a..b}" where 'a' and 'b' */
  /* are numbers or even 'n' (or 'N')                      */
  /* Returns a non-zero value if there's a problem         */
 
  int a, b;
 
  if(sscanf(str_range, "{%d..%d}", &a, &b) == 0)
    {
      char c;
	 
      if(sscanf(str_range, "{%c..%d}", &c, &b) == 2)
	{
	  if(c == 'n'|| c == 'N')
	    {
	      a = (c=='n')?n:N;
	    }
	  else
	    {
	      printf("Error. Wrong range value. \"{%c..%d}\" isn't allowed\n", c, b);
	      return 3;
	    }
	}
      else if(sscanf(str_range, "{%c..%d}", &c, &b) == 1)
	{
	  char c2;
		 
	  if(sscanf(str_range, "{%c..%c}", &c, &c2) == 2)
	    {
	      if((c == 'n' || c == 'N') && (c2 == 'n' || c2 == 'N'))
		{
		  a = (c=='n')?n:N;
		  b = (c2=='n')?n:N;
		}
	      else
		{
		  printf("Error. Wrong range value. \"{%c..%c}\" isn't allowed\n", c, c2);
		  return 3;
		}
	    }
	  else
	    {
	      printf("Error while reading the range of a parameter.\n");
	      printf("\"%s\" isn't valid.\n", str_range);
	      return 2;
	    }
	}
      else
	{
	  printf("Error while reading the range of a parameter.\n");
	  printf("\"%s\" isn't valid.\n", str_range);
	  return 1;
	}
    }

  if(sscanf(str_range, "{%d..%d}", &a, &b) == 1)
    {
      char c;
 
      if(sscanf(str_range, "{%d..%c}", &a, &c) != 2)
	{
	  printf("Error while reading the range of a parameter.\n");
	  printf("\"%s\" isn't valid.\n", str_range);
	  return 2;
	}
      if(c == 'n' || c == 'N')
	{
	  b = (c=='n')?n:N;
	}
      else
	{
	  printf("Error. Wrong range value. \"{%d..%c}\" isn't allowed\n", a, c);
	  return 3;
	}
    }

  *min = (a>b)?b:a;
  *max = (a>b)?a:b;
 
  return 0;
}

/* ------------------------------------------------------------------------- */

int get_range_2(char *str_range, int *min, int *max, int n, int N, char var)
{
  /* str_range is a string like "{a..b}" where 'a' and 'b' */
  /* are numbers or even 'n' (or 'N') or var               */
  /* Returns a negative value if there's a problem         */
  /* 1 if the first element is var                         */
  /* 2 if the second element is var                        */
  /* 3 if both elements are var                            */
 
  int a, b;
 
  if(sscanf(str_range, "{%d..%d}", &a, &b) == 0)
    {
      char c;
	 
      if(sscanf(str_range, "{%c..%d}", &c, &b) == 2)
	{
	  if(c == 'n'|| c == 'N')
	    {
	      a = (c=='n')?n:N;
	    }
	  else if(c == var)
	    {
	      *max = b;
	      return 1;
	    }
	  else
	    {
	      printf("Error. Wrong range value. \"{%c..%d}\" isn't allowed\n", c, b);
	      return -3;
	    }
	}
      else if(sscanf(str_range, "{%c..%d}", &c, &b) == 1)
	{
	  char c2;
		 
	  if(sscanf(str_range, "{%c..%c}", &c, &c2) == 2)
	    {
	      if((c == 'n' || c == 'N') && (c2 == 'n' || c2 == 'N'))
		{
		  a = (c=='n')?n:N;
		  b = (c2=='n')?n:N;
		}
	      else if((c == var) && (c2 == 'n' || c2 == 'N'))
		{
		  b = (c2=='n')?n:N;
		  *max = b;
		  return 1;
		}
	      else if((c == 'n' || c == 'N') && (c2 == var))
		{
		  a = (c=='n')?n:N;
		  *min = a;
		  return 2;
		}
	      else if((c == var) && (c2 == var))
		{
		  return 3;
		}
	      else
		{
		  printf("Error. Wrong range value. \"{%c..%c}\" isn't allowed\n", c, c2);
		  return -3;
		}
	    }
	  else
	    {
	      printf("Error while reading the range of a parameter.\n");
	      printf("\"%s\" isn't valid.\n", str_range);
	      return -2;
	    }
	}
      else
	{
	  printf("Error while reading the range of a parameter.\n");
	  printf("\"%s\" isn't valid.\n", str_range);
	  return -1;
	}
    }
  else if(sscanf(str_range, "{%d..%d}", &a, &b) == 1)
    {
      char c;
 
      if(sscanf(str_range, "{%d..%c}", &a, &c) != 2)
	{
	  printf("Error while reading the range of a parameter.\n");
	  printf("\"%s\" isn't valid.\n", str_range);
	  return -2;
	}
      if(c == 'n' || c == 'N')
	{
	  b = (c=='n')?n:N;
	}
      else if(c == var)
	{
	  *min = a;
	  return 2;
	}
      else
	{
	  printf("Error. Wrong range value. \"{%d..%c}\" isn't allowed\n", a, c);
	  return -3;
	}
    }

  *min = (a>b)?b:a;
  *max = (a>b)?a:b;
 
  return 0;
}

/* ------------------------------------------------------------------------- */

int read_constraints(char *filename, constraints *constr)
{
  FILE *fp;
  int found = 0;
  char buffer_main[360];
  char buffer[180];
  char buffer2[180];
  fpos_t p;
  int n = constr->n;
  int N = constr->N;
 
 
  printf("\n");
  fp = fopen(filename, "r");
  /* Search for */
  while(!found)
    {
      if(feof(fp))
	{
	  /* No constraints */
	  fclose(fp);
	  return 0;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "Constraints"))
	{
	  found = 1;
	}
    }

  fgetpos(fp, &p);
  /* *************** READING OF THE FILE CONSTRAINTS *************** */
  while(!feof(fp))
    {
      strcpy(buffer, "          ");
      if(fgets(buffer_main,360,fp) == NULL)
	continue;
      sscanf(buffer_main, "%s %s :", buffer, buffer2);

      //printf("%s",buffer_main);
      if(buffer[0] == '_'){
	//fgets(buffer,180,fp);
	continue;
      }

      if(same_strings(buffer, "File"))
	{
	  FILE *file_tmp;
	  int type;
	  int sparse_dense; /* 0 for sparse ; 1 for dense */
	  unsigned char c;


	  if(same_strings(buffer2, "Sparse"))     sparse_dense = 0;
	  else if(same_strings(buffer2, "Dense")) sparse_dense = 1;
	  else
	    {
	      printf("Error : File \"%s\" not allowed\n", buffer2);
	      printf("Only \"File Sparse\" and \"File Dense\" are possible.\n");
	      return 1;
	    }
		 
	  //fscanf(fp, "%s", buffer);
	  sscanf(&buffer_main[strlen(buffer)+strlen(buffer2)+3], "%s", buffer);
	  if(!(file_tmp = fopen(buffer, "r")))
	    {
	      printf("Error : file \"%s\" not found\n", buffer);
	      return 1;
	    }
	  fscanf(file_tmp, "%s :", buffer2);
	  c = buffer2[0];
	  if(c=='L' || c=='l')      type = 1;
	  else if(c=='Q' || c=='q') type = 2;
	  else                      type = 0;
	  fclose(file_tmp);
	  if(type==1)
	    {
	      if(!constr->lin)
		{
		  constr->lin = create_lin_constr(n, N);
		}
	      read_lin_constr(buffer, constr->lin, sparse_dense);
	    }
	  else if(type==2)
	    {
	      if(!constr->quad)
		{
		  constr->quad = create_quad_constr(n, N);
		}
	      read_quad_constr(buffer, constr->quad, sparse_dense);
	    }
	  else
	    {
	      printf("Wrong file format : \"%s\" isn't a valid constraint file.\n", buffer);
	      return 1;
	    }
	}
    }

  fsetpos(fp, &p);
  /* *************** READING OF THE SUM CONSTRAINTS *************** */
  while(!feof(fp))
    {
      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(buffer[0] == '_'){
	fgets(buffer,180,fp);
	continue;
      }

      if(same_strings(buffer, "Sum"))
	{
	  double val;
	  char op2[8];
	  char op;
		 
	  strcpy(buffer2, "          ");
	  fscanf(fp, "%s", buffer2);
	  if(same_strings(buffer2, "DiagSum"))
	    {
	      /* ---------- DIAGSUM ---------- */
	      if(fscanf(fp, "%s %lf", op2, &val) != 2)
		{
		  printf("Error : DiagSum requires a constant value\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      constr->quad = add_diagsum(constr->quad, op, val, n, N);
	    }
	  else if(same_strings(buffer2, "VarSum"))
	    {
	      /* ---------- VARSUM ---------- */
	      int min, max;
	      char buf_range[20];
			 
	      if(fscanf(fp, "%s %lf %s", op2, &val, buf_range) != 3)
		{
		  printf("Error : VarSum <operator> <value> <range>\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      if(get_range(buf_range, &min, &max, n, N))
		{
		  return 2;
		}
	      constr->lin = add_varsum(constr->lin, op, val, min, max, n, N);
	    }
	  else if(same_strings(buffer2, "RowSum"))
	    {
	      /* ---------- ROWSUM ---------- */
	      int num_row;
			 
	      if(fscanf(fp, "%d %s %lf", &num_row, op2, &val) != 3)
		{
		  printf("Error : RowSum requires a row number and a constant value\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      constr->quad = add_rowsum(constr->quad, op, val, num_row, n, N);
	    }
	  else if(same_strings(buffer2, "SquareSum"))
	    {
	      /* ---------- SQUARESUM ---------- */
	      int imin, imax;
	      int jmin, jmax;
	      char buf_range[20];
			 
	      /* i range */
	      fscanf(fp, "%s", buf_range);
	      if(get_range(buf_range, &imin, &imax, n, N))
		{
		  return 2;
		}
	      /* j range */
	      fscanf(fp, "%s", buf_range);
	      if(get_range(buf_range, &jmin, &jmax, n, N))
		{
		  return 2;
		}
	      /* val */
	      if(fscanf(fp, "%s %lf", op2, &val) != 2)
		{
		  printf("Error : SquareSum requires a constant value\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      constr->quad = add_squaresum(constr->quad, op, val, imin, imax, jmin, jmax, n, N);
	    }
	  else if(same_strings(buffer2, "TriangSum"))
	    {
	      /* ---------- TRIANGSUM ---------- */
	      int min, max;
	      char buf_range[20];

	      /* range */
	      fscanf(fp, "%s", buf_range);
	      if(get_range(buf_range, &min, &max, n, N))
		{
		  return 2;
		}
	      /* val */
	      if(fscanf(fp, "%s %lf", op2, &val) != 2)
		{
		  printf("Error : TriangSum requires a constant value\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      constr->quad = add_triangsum(constr->quad, op, val, min, max, n, N, 0);
	    }
	  else if(same_strings(buffer2, "StrictTriangSum"))
	    {
	      /* ---------- STRICTTRIANGSUM ---------- */
	      double val;
	      int min, max;
	      char buf_range[20];

	      /* range */
	      fscanf(fp, "%s", buf_range);
	      if(get_range(buf_range, &min, &max, n, N))
		{
		  return 2;
		}
	      /* val */
	      if(fscanf(fp, "%s %lf", op2, &val) != 2)
		{
		  printf("Error : StrictTriangSum requires a constant value\n");
		  return 2;
		}
	      op = op2[0];
	      if((op != '=') && (op != '<') && (op != '>'))
		{
		  printf("Error : The operator required must be '=', '<' or '>' and not '%c'", op);
		  return 2;
		}
	      constr->quad = add_triangsum(constr->quad, op, val, min, max, n, N, 1);
	    }
	  else
	    {
	      printf("Error : wrong type of sum.\n");
	      printf("\"%s\" isn't a valid keyword\n", buffer2);
	      return 2;
	    }
	}
    }
 
  fsetpos(fp, &p);
  /* *************** READING OF THE SINGLE CONSTRAINTS *************** */
  while(!feof(fp))
    {
      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(buffer[0] == '_'){
	fgets(buffer,180,fp);
	continue;
      }

      if(same_strings(buffer, "Single"))
	{
	  char buf1[10];
	  char buf2[10];
	  char buf3[10];
	  char buf_str[180];
	  int int1;
	  int int2;
	  double val;
	  char op2[8];
	  char op;
		 
	  if(!constr->single)
	    {
	      constr->single = create_singleton_constraints(n, N);
	    }
	  fscanf(fp, "%s %s %s %s", buf1, buf2, op2, buf3);
	  op = op2[0];
	  if((op != '=') && (op != '<') && (op != '>'))
	    {
	      printf("Error : The operator required must be '=', '<' or '>' and not '%s'", op2);
	      return 3;
	    }
	  sprintf(buf_str, "%s %s %s", buf1, buf2, buf3);
	  if(sscanf(buf_str, "%d %d %lf", &int1, &int2, &val) == 3)
	    {
	      add_singleton(constr->single, int1-1, int2-1, op, val);
	    }
	  else
	    {
	      if(sscanf(buf_str, "%d %s %lf", &int1, buf2, &val) == 3)
		{
		  int j;
		  int jmin, jmax;
		  char buf_range[20];
				 

		  fscanf(fp, "%s", buf_range);
		  if(get_range(buf_range, &jmin, &jmax, n, N))
		    {
		      return 3;
		    }

		  for(j=jmin-1;j<jmax;j++)
		    add_singleton(constr->single, int1-1, j, op, val);
		}
	      else
		{
		  if(sscanf(buf_str, "%s %d %lf", buf1, &int2, &val) == 3)
		    {
		      int i;
		      int imin, imax;
		      char buf_range[20];
					 
		      fscanf(fp, "%s", buf_range);
		      if(get_range(buf_range, &imin, &imax, n, N))
			{
			  return 3;
			}

		      for(i=imin-1;i<imax;i++)
			add_singleton(constr->single, i, int2-1, op, val);
		    }
		  else
		    {
		      if(sscanf(buf_str, "%s %s %lf", buf1, buf2, &val) == 3)
			{
			  if(!same_strings(buf1, buf2))
			    {
			      int i, j;
			      int imin, imax;
			      int jmin, jmax;
			      char buf_range[20];
			      int get_range_val;
							 
			      /* i range */
			      fscanf(fp, "%s", buf_range);
			      if(get_range(buf_range, &imin, &imax, n, N))
				{
				  return 3;
				}
					 
			      /* j range */
			      fscanf(fp, "%s", buf_range);
			      if((get_range_val = get_range_2(buf_range, &jmin, &jmax, n, N, 'i')) < 0)
				{
				  return 3;
				}
					 		 
			      if(get_range_val == 0)
				{
				  for(i=imin-1;i<imax;i++)
				    for(j=jmin-1;j<jmax;j++)
				      add_singleton(constr->single, i, j, op, val);
				}
			      else if(get_range_val == 1)
				{
				  for(i=imin-1;i<imax;i++)
				    for(j=i+1;j<jmax;j++)
				      add_singleton(constr->single, i, j, op, val);
				}
			      else if(get_range_val == 2)
				{
				  for(i=imin-1;i<imax;i++)
				    for(j=jmin-1;j<i;j++)
				      add_singleton(constr->single, i, j, op, val);
				}
			      else if(get_range_val == 3)
				{
				  for(i=imin-1;i<imax;i++)
				    add_singleton(constr->single, i, i, op, val);
				}
			    }
			  else
			    {
			      int i;
			      int min, max;
			      char buf_range[20];
							 
			      /* i j range */
			      fscanf(fp, "%s", buf_range);
			      if(get_range(buf_range, &min, &max, n, N))
				{
				  return 3;
				}
					 
			      for(i=min-1;i<max;i++)
				add_singleton(constr->single, i, i, op, val);
			    }
			}
		      else
			{
			  printf("The fourth parameter of a singleton must be a constant\n");
			  return 3;
			}
		    }
		}
	    }
	}
    }

  fsetpos(fp, &p);
  /* *************** READING OF THE PRODUCT CONSTRAINTS *************** */
  while(!feof(fp))
    {
      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(buffer[0] == '_'){
	fgets(buffer,180,fp);
	continue;
      }

      if(same_strings(buffer, "ProductLinVar"))
	{
	  int nb = constr->lin->nb;
	  char buf1[10];
	  char buf2[10];
	  char op2[8];
	  char op;
	  int lin_min, lin_max;
	  int var_min, var_max;
	  int get_range_val;
	  int buf_int;
	  int i;
		 
	  fscanf(fp, "%s %s %s", op2, buf1, buf2);
	  op = op2[0];
	  /* The operator */
	  if((op != '=') && (op != '<') && (op != '>'))
	    {
	      printf("Error : The operator required must be '=', '<' or '>' and not '%s'", op2);
	      return 4;
	    }
	  /* The range of the linear constraints */
	  if(!sscanf(buf1, "%d", &buf_int))
	    {
	      char buf_range[20];
			 
	      fscanf(fp, "%s", buf_range);
	      if((get_range_val = get_range_2(buf_range, &lin_min, &lin_max, n, N, 'x'))<0)
		{
		  return 4;
		}
	      if(get_range_val == 1)
		{
		  lin_min = lin_max;
		  lin_max = nb;
		}
	      else if(get_range_val == 2)
		{
		  lin_max = nb;
		}
	      else if(get_range_val == 3)
		{
		  lin_min = nb;
		  lin_max = nb;
		}
	    }
	  else
	    {
	      lin_min = buf_int;
	      lin_max = buf_int;			 
	    }
	  /* The range of the variable */
	  if(!sscanf(buf2, "%d", &buf_int))
	    {
	      char buf_range[20];
			 
	      fscanf(fp, "%s", buf_range);
	      if(get_range(buf_range, &var_min, &var_max, n, N))
		{
		  return 4;
		}
	    }
	  else
	    {
	      var_min = buf_int;
	      var_max = buf_int;			 
	    }
	  /* Add all */
	  for(i=lin_min-1;i<lin_max;i++)
	    {
	      if((constr->lin->constraints[i]->c1 == constr->lin->constraints[i]->c2 ) && (op=='=')){ 
		constr->quad = add_productlinvar(constr->quad, constr->lin, i, var_min-1, var_max-1, n, N); 
	      }
	    }
	}
    }
 
  fsetpos(fp, &p);
  /* *************** READING OF THE xi and (1-xi) PRODUCT CONSTRAINTS *************** */
  read_prodConstrVect(fp,constr);
  fsetpos(fp, &p);
  read_prodConstrVectbar(fp,constr);

/********************************************************************************************* */
//  MODIF 7/4/2003 : ajout de la fonction simple()
//  
  
  fsetpos(fp, &p);
  read_simple(fp,constr);

  return 0;
}

/* ------------------------------------------------------------------------- */

void write_constraints(int dim, constraints *constr, FILE *fp, char **str)
{
  if(constr->single) write_single_constr(dim, constr->single, fp, str);
  if(constr->lin)    write_lin_constr(dim, constr->lin, fp, str);
  if(constr->quad)   write_quad_constr(dim, constr->quad, fp, str);
}

/* ------------------------------------------------------------------------- */

int valid_constraints(constraints *constr, double **X, double *x, double e)
{
 
  if(constr->single) if(!valid_single_constr(constr->single, X, e)) return 0;
  if(constr->lin)    if(!valid_lin_constr(constr->lin, x, e))       return 0;
  if(constr->quad)   if(!valid_quad_constr(constr->quad, X, x, e))  return 0;
 
  return 1;
}

/* ------------------------------------------------------------------------- */


