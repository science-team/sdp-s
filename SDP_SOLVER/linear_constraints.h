/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef linear_constraints_h
#define linear_constraints_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "annexes.h"
#include "matrix_tools.h"

 typedef struct
	{
	 double c1;
	 double c2;
	 double *l;
	 int flag_ci;
	} linear_constraint;

 typedef struct
	{
	 int n;
	 int N;
	 int nb;
	 linear_constraint **constraints;
	} linear_constraints;

 linear_constraints *create_lin_constr(int n, int N);
 void free_lin_constr(linear_constraints *l);
 int read_lin_constr(char *filename, linear_constraints *constr, int sparse_dense);
 void write_lin_constr(int dim, linear_constraints *constr, FILE *fp, char **str);
 int valid_lin_constr(linear_constraints *constr, double *x, double e);
 int nb_lin_constr(linear_constraints *constr); 
 
#endif
