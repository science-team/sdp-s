/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef singleton_matrix_h
#define singleton_matrix_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* If 'n' is the dimension of a matrix/vector, */
/* all indexes given are between '0' and 'n-1' */

/* If the file pointer is NULL, then the result is written on the screen */

 void write_singleton(int dim, int i, int j, double val, FILE *fp, char **str);

#endif
