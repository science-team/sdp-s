/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "sparse_matrix.h"


/*----------------------------------------------------------------------*/

void free_sparse_mat(sparse_matrix *mat)
{
 free(mat->index_i);
 free(mat->index_j);
 free(mat->val);
 free(mat);
}

/*----------------------------------------------------------------------*/

void print_sparse_mat(sparse_matrix *mat)
{
 int i;
 
 printf("SPARSE MATRIX :\n");
 printf("n = %d\nk = %d\nnb_elements = %d\n", mat->n, mat->k, mat->nb_elements);
 for(i=0;i<mat->nb_elements;i++)
 	{
	 printf("%d\t", mat->index_i[i]);
	}
 printf("\n");
 for(i=0;i<mat->nb_elements;i++)
 	{
	 printf("%d\t", mat->index_j[i]);
	}
 printf("\n");
 for(i=0;i<mat->nb_elements;i++)
 	{
	 printf("%.4lf\t", mat->val[i]);
	}
 printf("\n");
}

/*----------------------------------------------------------------------*/

sparse_matrix *create_init_sparse_mat(int n, int k)
{
 sparse_matrix *mat;
 
 
 mat = (sparse_matrix *)malloc(sizeof(sparse_matrix));
 mat->n = n;
 mat->k = k;
 mat->index_i = NULL;
 mat->index_j = NULL;
 mat->val = NULL;
 
 return mat;
}

/*----------------------------------------------------------------------*/

sparse_matrix *vector_2_sparse_mat(int n, double *vect)
{
 sparse_matrix *mat;
 int nb_el = 0;
 int i;
 int cpt;
 
 
 for(i=0;i<n;i++)
 	{
	 if(vect[i]) nb_el++;
	}
 
 mat = (sparse_matrix *)malloc(sizeof(sparse_matrix));
 mat->n = n;
 mat->k = 1;
 mat->nb_elements = nb_el;
 mat->index_i = (int *)   malloc(nb_el*sizeof(int));
 mat->index_j = (int *)   malloc(nb_el*sizeof(int));
 mat->val     = (double *)malloc(nb_el*sizeof(double));
 
 cpt = 0;
 for(i=0;i<n;i++)
 	{
	 if(vect[i])
	 	{
		 mat->index_i[cpt] = i;
		 mat->index_j[cpt] = 0;
		 mat->val[cpt]     = vect[i];
		 cpt++;
		}
	}
 
 return mat;
}

/*----------------------------------------------------------------------*/

sparse_matrix *matrix_2_sparse_mat(int n, double **m)
{
 sparse_matrix *mat;
 int nb_el = 0;
 int i, j;
 int cpt;
 
 
 
 for(i=0;i<n;i++)
 	{
	 for(j=0;j<n;j++)
		{
		 if(m[i][j]) nb_el++;
		}
	}
 
 mat = (sparse_matrix *)malloc(sizeof(sparse_matrix));
 mat->n = n;
 mat->k = 1;
 mat->nb_elements = nb_el;
 mat->index_i = (int *)   malloc(nb_el*sizeof(int));
 mat->index_j = (int *)   malloc(nb_el*sizeof(int));
 mat->val     = (double *)malloc(nb_el*sizeof(double));
 
 cpt = 0;
 for(i=0;i<n;i++)
 	{
	 for(j=0;j<n;j++)
		{
		 if(m[i][j])
		 	{
			 mat->index_i[cpt] = i;
			 mat->index_j[cpt] = j;
			 mat->val[cpt]     = m[i][j];
			 cpt++;
			}
		}
	}
 
 return mat;
}

/*----------------------------------------------------------------------*/

void write_lowrank_sparse_sparse(sparse_matrix *m1, sparse_matrix *m2, FILE *fp, char **str)
{
 int i;
 char buffer[40];
 int str_size;
 
 
 if(fp)
 	{
	 fprintf(fp, "LOWRANK_SPARSE_SPARSE\n");
	 fprintf(fp, "%d %d %d\n", m1->n, m1->k, m1->nb_elements);
	 for(i=0;i<m1->nb_elements;i++)
	 	{
		 fprintf(fp, "%d %d %f\n", m1->index_i[i], m1->index_j[i], m1->val[i]);
		}
	 fprintf(fp, "%d %d %d\n", m2->n, m2->k, m2->nb_elements);
	 for(i=0;i<m2->nb_elements;i++)
	 	{
		 fprintf(fp, "%d %d %f\n", m2->index_i[i], m2->index_j[i], m2->val[i]);
		}
	}
 if(str)
 	{
	 sprintf(buffer, "LOWRANK_SPARSE_SPARSE\n%d %d %d\n", m1->n, m1->k, m1->nb_elements);
	 str_size = strlen(buffer) + strlen(*str) + 1;
	 *str = (char *)realloc(*str, str_size*sizeof(char));
	 *str = strcat(*str, buffer);
	 
	 for(i=0;i<m1->nb_elements;i++)
	 	{
		 sprintf(buffer, "%d %d %f\n", m1->index_i[i], m1->index_j[i], m1->val[i]);
		 str_size = strlen(buffer) + strlen(*str) + 1;
		 *str = (char *)realloc(*str, str_size*sizeof(char));
		 *str = strcat(*str, buffer);
		}

	 sprintf(buffer, "%d %d %d\n", m2->n, m2->k, m2->nb_elements);
	 str_size = strlen(buffer) + strlen(*str) + 1;
	 *str = (char *)realloc(*str, str_size*sizeof(char));
	 *str = strcat(*str, buffer);
	 
	 for(i=0;i<m2->nb_elements;i++)
	 	{
		 sprintf(buffer, "%d %d %f\n", m2->index_i[i], m2->index_j[i], m2->val[i]);
		 str_size = strlen(buffer) + strlen(*str) + 1;
		 *str = (char *)realloc(*str, str_size*sizeof(char));
		 *str = strcat(*str, buffer);
		}
	 
	}
 if(!fp && !str)
 	{
	 printf("LOWRANK_SPARSE_SPARSE\n");
	 printf("%d %d %d\n", m1->n, m1->k, m1->nb_elements);
	 for(i=0;i<m1->nb_elements;i++)
	 	{
		 printf("%d %d %f\n", m1->index_i[i], m1->index_j[i], m1->val[i]);
		}
	 printf("%d %d %d\n", m2->n, m2->k, m2->nb_elements);
	 for(i=0;i<m2->nb_elements;i++)
	 	{
		 printf("%d %d %f\n", m2->index_i[i], m2->index_j[i], m2->val[i]);
		}
	}
 
}

/*----------------------------------------------------------------------*/

