/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#ifndef SDP_pb_h
#define SDP_pb_h

#include <stdio.h>
#include <stdlib.h>

#include "objective_function.h"
#include "constraints.h"


typedef struct{
  obj_func *obj_f;
  constraints *constr;
  char *string_pb;
  int dim;
  int nb_constr;
} SDP_pb;

typedef struct{
  double *x;
  double val;
} SDP_sol;

typedef struct{
  int *x;
  int val;
} SDP_sol_int;


void free_SDP_pb(SDP_pb *pb);
void free_SDP_sol(SDP_sol *sol);
void free_SDP_sol_int(SDP_sol_int *sol);
void write_string_SDP_pb(SDP_pb *pb);
SDP_pb *create_SDP_pb(char *pb_filename, char *filenameSB);


#endif



