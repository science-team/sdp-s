/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef sym_matrix_h
#define sym_matrix_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sparse_matrix.h"


 typedef sparse_matrix sym_matrix;

/* If 'n' is the dimension of a matrix/vector, */
/* all indexes given are between '0' and 'n-1' */

/* If the file pointer is NULL, then the result is written on the screen */

 void free_sym_mat(sym_matrix *mat);
 sym_matrix *create_init_sym_mat(int n);
 sym_matrix *create_sym_mat(int n, int nb_el);
 sym_matrix *create_sym_mat_mat(int n, double **mat);
 void write_sym_mat(sym_matrix *mat, FILE *fp, char **str);
 void write_sym_mat_mat(int dim, int n, double **m, FILE *fp, char **str);


#endif
