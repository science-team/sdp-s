/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "diag_matrix.h"


/*----------------------------------------------------------------------*/

void write_diag_mat(int n, int i_begin, int i_end, double *diag, FILE *fp, char **str)
{
 /* Writes the matrix which diagonal is [0 0 ... 0 diag 0 0 ... 0] */
 /* where diag starts at index i_begin and finishes at index i_end */
 /* For the first element, i_begin = 1 and not 0                   */
 int i;
 int nb_el = 0;
 int dim = i_end - i_begin + 1;
 char buffer[40];
 int str_size;
 
 
 for(i=0;i<dim;i++)
 	{
	 if(diag[i]) nb_el++;
	}


 if(fp)
 	{
	 fprintf(fp, "SYMMETRIC_SPARSE\n");
	 fprintf(fp, "%d %d\n", n, nb_el);
	 for(i=0;i<dim;i++)
	 	{
		 if(diag[i]) fprintf(fp, "%d %d %f\n", i+i_begin-1, i+i_begin-1, diag[i]);
		}
	}
 if(str)
 	{
	 sprintf(buffer, "SYMMETRIC_SPARSE\n%d %d\n", n, nb_el);
	 str_size = strlen(buffer) + strlen(*str) + 1;
	 *str = (char *)realloc(*str, str_size*sizeof(char));
	 *str = strcat(*str, buffer);

	 for(i=0;i<dim;i++)
	 	{
		 if(diag[i])
		 	{
			 sprintf(buffer, "%d %d %f\n", i+i_begin-1, i+i_begin-1, diag[i]);
			 str_size = strlen(buffer) + strlen(*str) + 1;
			 *str = (char *)realloc(*str, str_size*sizeof(char));
			 *str = strcat(*str, buffer);
			}
		}
	}
 if(!fp && !str)
 	{
	 printf("SYMMETRIC_SPARSE\n");
	 printf("%d %d\n", n, nb_el);
	 for(i=0;i<dim;i++)
	 	{
		 if(diag[i]) printf("%d %d %f\n", i+i_begin-1, i+i_begin-1, diag[i]);
		}
	}
 
}

/*----------------------------------------------------------------------*/


