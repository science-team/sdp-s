/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "quadratic_constraints.h"

extern int PRINT_DEBUG;

/* ------------------------------------------------------------------------- */

quadratic_constraints *create_quad_constr(int n, int N)
{
  quadratic_constraints *result;
 
 
  result = (quadratic_constraints *)malloc(sizeof(quadratic_constraints));
  result->n  = n;
  result->N  = N;
  result->nb = 0;
  result->constraints = NULL;
 
  return result;
}

/* ------------------------------------------------------------------------- */

void free_quad_constr(quadratic_constraints *Q)
{
  int i;
 
  for(i=0;i<Q->nb;i++)
    {
      if(Q->constraints[i]->l) free(Q->constraints[i]->l);
      free_sym_mat(Q->constraints[i]->Q);
    }
  free(Q->constraints);
  free(Q);
}

/* ------------------------------------------------------------------------- */

int nb_quad_constr(quadratic_constraints *constr)
{
  int i;
  int flag;
  int result = 0;
  double c1, c2;
 
 
  for(i=0;i<constr->nb;i++)
    {
      flag = constr->constraints[i]->flag_ci;
      c1   = constr->constraints[i]->c1;
      c2   = constr->constraints[i]->c2;
      if(flag == 3 && c1 != c2) result += 2;
      else result++;
    }
 
  return result;
}

/* ------------------------------------------------------------------------- */

int read_quad_constr(char *filename, quadratic_constraints *constr, int sparse_dense)
{
  FILE *fp;
  char buffer[140];
  int buffer_int;
  double buffer_d;
 
  int i, j;
  int ii, jj;
  int nb_el;
  char type; /* 0 for '=', 1 for '<', 2 for '>' and 3 for '<>' */
  int n;
  int k;
  quadratic_constraint **pt_constr;
 
 
  /* READING OF THE HEADER : Q= | Q< | Q> | Q<>*/
  if( !( fp = fopen(filename, "r"))) 	 printf("Error while opening the quadratic constraint file \"%s\".\n", filename);
  else  printf("Reading Quadratic constraints in file \"%s\".\n", filename);
  fflush(NULL);
  fscanf(fp, "%s", buffer);
  if(strlen(buffer)>1 && buffer[0] == 'Q')
    {
      if(buffer[1] == '=')      type = 0; /* Q.X + lx = c         */
      else if(buffer[1] == '<')
	{
	  if(buffer[2] == '>')   type = 3; /* c1 <= Q.X + lx <= c2 */
	  else                   type = 1; /* c1 >= Q.X + lx       */
	}
      else if(buffer[1] == '>') type = 2; /* Q.X + lx >= c2       */
      else
	{
	  printf("Error while reading the quadratic constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return 1;
	}
    }
  else
    {
      fscanf(fp, "%s", buffer);
	 
      if(buffer[0] == '=')      type = 0; /* Q.X + lx = c         */
      else if(buffer[0] == '<')
	{
	  if(buffer[1] == '>')   type = 3; /* c1 <= Q.X + lx <= c2 */
	  else                   type = 1; /* c1 >= Q.X + lx       */
	}
      else if(buffer[0] == '>') type = 2; /* Q.X + lx >= c2       */
      else
	{
	  printf("Error while reading the quadratic constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return 1;
	}
    }

  /* READING OF N:dimension AND K:nb of constraints in this file */
  fscanf(fp, "%d", &n);
  fscanf(fp, "%d", &k);
  constr->n = n;
 
  /* ALLOCATION / REALLOCATION OF MEMORY TO STOCK CONSTRAINTS */
  buffer_int = constr->nb;
  constr->nb += k;
  if ( ( constr->constraints = (quadratic_constraint **)realloc(constr->constraints, (constr->nb)*sizeof(quadratic_constraint *))) == NULL)
    printf("Memory Re-Allocation Error!!!\n");
  pt_constr = &(constr->constraints[buffer_int]);
 
  /* READING OF THE CONSTRAINTS */
  for(i=0;i<k;i++)
    {
      if ( (pt_constr[i] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint))) == NULL)
	printf("Memory Allocation Error!!!\n");
      pt_constr[i]->dim = n;
      /* c | c1 c2 | c2 c1 */
      if(type == 0)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->c2 = buffer_d;
	  pt_constr[i]->flag_ci = 3;		 
	}
      else if(type == 1)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->flag_ci = 1;
	}
      else if(type == 2)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c2 = buffer_d;
	  pt_constr[i]->flag_ci = 2;
	}
      else if(type == 3)
	{
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c2 = buffer_d;
	  fscanf(fp, "%lf", &buffer_d);
	  pt_constr[i]->c1 = buffer_d;
	  pt_constr[i]->flag_ci = 3;
	}

      /* then the vector */
      if ( (pt_constr[i]->l = (double *)malloc(n*sizeof(double))) == NULL)
	printf("Memory Allocation Error!!!\n");
	
      /* and then the matrix */
      nb_el = 0;
	 
      if(sparse_dense == 1) /* Dense */
	{
	  /* Vector */
	  pt_constr[i]->nb_l = n;
	  if ( (pt_constr[i]->el_l = (int *)malloc(n*sizeof(int)))==NULL)
	    printf("Memory Allocation Error!!!\n");

	  for(j=0;j<n;j++)
	    {
	      fscanf(fp, "%lf", &buffer_d);
	      pt_constr[i]->l[j] = buffer_d;
	      pt_constr[i]->el_l[j] = j;
	    }
	  /* Matrix */
	  pt_constr[i]->Q = create_sym_mat(n, n*n);
	  for(ii=0;ii<n;ii++)
	    for(jj=0;jj<n;jj++)
	      {
		fscanf(fp, "%lf", &buffer_d);
		if(jj>=ii) /* Symmetric matrix */
		  if(buffer_d)
		    {
		      pt_constr[i]->Q->index_i[nb_el] = ii;
		      pt_constr[i]->Q->index_j[nb_el] = jj;
		      pt_constr[i]->Q->val[nb_el]     = buffer_d;
		      nb_el++;
		    }
	      }
	}
      else /* Sparse */
	{
	  /* Vector */
	  fscanf(fp, "%d", &nb_el);
	  if ( (pt_constr[i]->el_l = (int *)malloc(nb_el*sizeof(int)))==NULL)
	    printf("Memory Allocation Error!!!\n");
	  pt_constr[i]->nb_l = nb_el;
	  if(nb_el == 0)
	    {
	      free(pt_constr[i]->l);
	      free(pt_constr[i]->el_l);
	      pt_constr[i]->el_l = NULL;
	      pt_constr[i]->l = NULL;
	    }
	  else
	    {
	      for(j=0;j<nb_el;j++)
		{
		  fscanf(fp, "%d %lf", &ii, &buffer_d);
		  /* indexes are between 1 and n, and we want them between 0 and n-1 */
		  pt_constr[i]->el_l[j] = ii-1;
		  pt_constr[i]->l[ii-1] = buffer_d;
		}
	    }
	  /* Matrix */
	  fscanf(fp, "%d", &nb_el);
	  pt_constr[i]->Q = create_sym_mat(n, nb_el);
	  for(j=0;j<nb_el;j++)
	    {
	      fscanf(fp, "%d %d %lf", &ii, &jj, &buffer_d);
	      /* indexes are between 1 and n, and we want them between 0 and n-1 */
	      pt_constr[i]->Q->index_i[j] = ii-1;
	      pt_constr[i]->Q->index_j[j] = jj-1;
	      pt_constr[i]->Q->val[j]     = buffer_d;
	    }
	}
    }
  printf("%d quadratic constraints read.\n",k);
  fflush(NULL);

  return 0;
}

/* ------------------------------------------------------------------------- */

void write_quad_constr(int dim, quadratic_constraints *constr, FILE *fp, char **str)
{
  int i, j;
  double c1, c2;
  int flag_ci;
  int n;
  double *vect;
  int *v_el_l;
  sym_matrix *mat;
  sym_matrix *final_mat;
  char buffer[40];
  int str_size;
  int nb_l,indice;
 
  //printf("Writting quadratic contraints...\n");

  for(i=0;i<constr->nb;i++)
    {
      n       = constr->constraints[i]->dim;
      vect    = constr->constraints[i]->l;
      v_el_l  = constr->constraints[i]->el_l;
      nb_l	 = constr->constraints[i]->nb_l;
      mat     = constr->constraints[i]->Q;
      c1      = constr->constraints[i]->c1;
      c2      = constr->constraints[i]->c2;
      flag_ci = constr->constraints[i]->flag_ci;
      //	 printf("Quadratic constraint No %d\n read",i+1);

      if(vect)
	{
	  int nb = mat->nb_elements;
	  //		 nb_l = 0;
	  //	 	 for(j=0;j<n;j++) if(vect[j] != 0) nb_l++;
	  nb += nb_l;
	  final_mat = create_sym_mat(dim, nb);
	  final_mat->index_i = (int *)   realloc(final_mat->index_i, nb*sizeof(int));
	  final_mat->index_j = (int *)   realloc(final_mat->index_j, nb*sizeof(int));
	  final_mat->val     = (double *)realloc(final_mat->val,     nb*sizeof(double));

	  indice = 0;
	  // MODIF 7/04/2002
	  for(j=0;j<nb_l;j++)
	    {
	      //if(vect[v_el_l[j]] != 0)
		{
		  final_mat->index_i[indice] = v_el_l[j]; 
		  final_mat->index_j[indice] = n;
		  final_mat->val[indice]     = vect[v_el_l[j]];
		  // FIN MODIF
		  indice++;
		}
	    }
	}
      else
	{
	  nb_l = 0;
	  final_mat = create_sym_mat(dim, mat->nb_elements);
	}
      for(j=0;j<mat->nb_elements;j++)
	{
	  final_mat->index_i[j+nb_l] = mat->index_i[j];
	  final_mat->index_j[j+nb_l] = mat->index_j[j];
	  final_mat->val[j+nb_l]     = mat->val[j];

	}

      write_sym_mat(final_mat, fp, str);

      if(flag_ci == 3 && c1 == c2)
	{
	  if(fp) fprintf(fp, "= %f\n", c1);
	  if(str)
	    {
	      sprintf(buffer, "= %f\n", c1);
	      str_size = strlen(buffer) + strlen(*str) + 1;
	      *str = (char *)realloc(*str, str_size*sizeof(char));
	      *str = strcat(*str, buffer);
	    }
	  if(!fp && !str) printf("= %f\n", c1);
	}
      else
	{
	  if(flag_ci == 1)
	    {
	      if(fp) fprintf(fp, "< %f\n", c1);
	      if(str)
		{
		  sprintf(buffer, "< %f\n", c1);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("< %f\n", c1);
	    }
	  else if(flag_ci == 2)
	    {
	      if(fp) fprintf(fp, "> %f\n", c2);
	      if(str)
		{
		  sprintf(buffer, "> %f\n", c2);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("> %f\n", c2);
	    }
	  else if(flag_ci == 3)
	    {
	      if(fp) fprintf(fp, "< %f\n", c1);
	      if(str)
		{
		  sprintf(buffer, "< %f\n", c1);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("< %f\n", c1);
	      write_sym_mat(final_mat, fp, str);
	      if(fp) fprintf(fp, "> %f\n", c2);
	      if(str)
		{
		  sprintf(buffer, "> %f\n", c2);
		  str_size = strlen(buffer) + strlen(*str) + 1;
		  *str = (char *)realloc(*str, str_size*sizeof(char));
		  *str = strcat(*str, buffer);
		}
	      if(!fp && !str) printf("> %f\n", c2);
	    }
	}
      //if (! ((i+1)%100) ) printf("Quadratic Constraint %d written\n",i+1);

      free_sym_mat(final_mat);
    }
}

/* ------------------------------------------------------------------------- */

int valid_quad_constr(quadratic_constraints *constr, double **X, double *x, double e)
{
  int N  = constr->N;
  int nb = constr->nb;
 
  double c1, c2;
  double *l;
  int *el_l;
  double nb_l;
  sym_matrix *Q;
 
  int i, j;
  double v;
 
 
  for(i=0;i<nb;i++)
    {
      c1 = constr->constraints[i]->c1;
      c2 = constr->constraints[i]->c2;
      l  = constr->constraints[i]->l;
      el_l = constr->constraints[i]->el_l;
      nb_l = constr->constraints[i]->nb_l;
      Q  = constr->constraints[i]->Q;
	 
      v = 0.;
      for(j=0;j<nb_l;j++)
	{
	  v += x[el_l[j]]*l[el_l[j]];
	}
      for(j=0;j<Q->nb_elements;j++)
	{
	  int ii = Q->index_i[j];
	  int jj = Q->index_j[j];
		 
	  v += X[ii][jj]*Q->val[j];
	}
	 
      if(difference(c1,v,e)>0. || difference(c2,v,e)<0.) return 0;
    }
 
  return 1;
}

/* ------------------------------------------------------------------------- */

