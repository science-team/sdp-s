/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "objective_function.h"

extern int PRINT_DEBUG;
int flag_min_max;
int TRACE = 0;

/* ------------------------------------------------------------------------- */


void free_mat_file(mat_file *m)
{
  if(m)
    {
      if(m->name) free(m->name);
      free(m);
    }
}


/* ------------------------------------------------------------------------- */


obj_func *create_objective_function()
{
  obj_func *result;
 
 
  result = (obj_func *)malloc(sizeof(obj_func));
  result->min_max = 1;
  result->val     = 0;
  result->C       = NULL;
  result->l       = NULL;
 
  return result;
}


/* ------------------------------------------------------------------------- */


void free_objective_function(obj_func *f)
{
  if(f)
    {
      if(f->C) free_mat_file(f->C);
      if(f->l) free_mat_file(f->l);
      free(f);
    }
}


/* ------------------------------------------------------------------------- */


int read_type(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  int a;
  int b;
  int tmp;
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  printf("Error : can't get the type of the problem.\n");
	  printf("Must find \"Type : {0|-1,1}\"\n");
	  fclose(fp);
	  return -1;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);
	
      if(same_strings(buffer, "TYPE"))
	{
	  found = 1;
	}
    }

  fscanf(fp, "%s", buffer);
  tmp = sscanf(buffer, "{%d,%d}", &a, &b);
  if(tmp != 2)
    {
      printf("Error : wrong type of problem.\n");
      printf("\"%s\" is not valid\n", buffer);
      printf("(spaces must be removed)\n");
    }

  if(b != 1)
    {
      printf("Error : wrong type of problem.\n");
      printf("\"{%d,%d}\" is not valid\n", a, b);
      fclose(fp);
      return -2;
    }
 
  fclose(fp);
  if(a == -1) return 0;
  else if(a == 0) return 1;
  else
    {
      printf("Error : wrong type of problem.\n");
      printf("\"{%d,%d}\" is not valid\n", a, b);
      return -2;
    }
}


/* ------------------------------------------------------------------------- */


int read_dim(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  int n;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  printf("Error : can't get the dimension of the problem.\n");
	  printf("Must find \"Dim :\" 'n'\n");
	  fclose(fp);
	  return -1;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "DIM"))
	{
	  found = 1;
	}
    }

  fscanf(fp, "%d", &n);
  fclose(fp);

  if(n <= 0)
    {
      printf("Error : the dimension must be >0\n");
      return -2;
    }
	
  return n;
}

/* ------------------------------------------------------------------------- */


int read_n(char *filename, fpos_t p, int dim)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  int n;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  n = dim;
	  fclose(fp);
	  return n;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "NbBoolVar"))
	{
	  found = 1;
	}
      if(same_strings(buffer, "NbIntVar"))
	{
	  found = 2;
	}
    }

  fscanf(fp, "%d", &n);
  fclose(fp);

  if(n < 0 || n > dim)
    {
      printf("Error : the number 'n' of variables must verify 0 <= n <= dim\n");
      return -2;
    }
 
  if(found ==2) n = dim-n;
 
  return n;
}


/* ------------------------------------------------------------------------- */


int read_add_more_constr(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  int result;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  found = 1;
	  result = 0;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "AddMoreConstr"))
	{
	  found = 1;
	  fscanf(fp, "%d", &result);
	}
    }

  fclose(fp);
 
  return result;
}



/* ------------------------------------------------------------------------- */


int read_trace(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  int T;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  /* No trace constraint */
	  fclose(fp);
	  return 0;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "Trace"))
	{
	  found = 1;
	}
    }

  fscanf(fp, "%d", &T);
  fclose(fp);

  if(T < 0)
    {
      printf("Warning : the trace constraint is not valid, the trace must be >= 0\n");
      return -2;
    }
	
  return T;
}


/* ------------------------------------------------------------------------- */


mat_file *read_C(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  char buffer2[100];
  char tmp_buffer[100];
  mat_file *result;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  printf("Error : can't get the matrix C of the objective function.\n");
	  printf("Must find \"C : \"\n");
	  fclose(fp);
	  return NULL;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);
	 
      if(same_strings(buffer, "C"))
	{
	  found = 1;
	}
    }

  /* Read the format & the name of the file for C */
  if(fscanf(fp, "%s %s : %s ", tmp_buffer, buffer, buffer2) != 3)
    {
      printf("Error : must find \"C : file <type of file> : <filename> \n");
      fclose(fp);
      return NULL;
    }
 
  if(!same_strings(tmp_buffer, "file"))
    {
      printf("Error : must find \"C : file <type of file> : <filename> \n");
      printf("\"%s\" is not valid\n", tmp_buffer);
      fclose(fp);
      return NULL;
    }
 
  fclose(fp);
  result = (mat_file *)malloc(sizeof(mat_file));
 
  /* The file format */
  if(same_strings(buffer, "Sparse"))     result->format = SPARSE;
  else if(same_strings(buffer, "Dense")) result->format = DENSE;
  else
    {
      printf("The file format of C isn't valid.\n \"%s\" is unknown\n", buffer);
      return NULL;
    }

  /* The name */
  result->name = (char *)malloc((strlen(buffer2)+1)*sizeof(char));
  strcpy(result->name, buffer2);
  printf("Filename C : %s\n", result->name);
  if(PRINT_DEBUG)
    printf("Its format : %d\n", result->format);
  
  return result;
}


/* ------------------------------------------------------------------------- */


mat_file *read_l(char *filename, fpos_t p)
{
  FILE *fp;
  int found = 0;
  char buffer[100];
  char buffer2[100];
  char tmp_buffer[100];
  mat_file *result;
 
 
  fp = fopen(filename, "r");
  fsetpos(fp, &p);
  while(!found)
    {
      if(feof(fp))
	{
	  printf("Error : can't get the vector l of the objective function.\n");
	  printf("Must find \"l : \"\n");
	  fclose(fp);
	  return NULL;
	}

      strcpy(buffer, "          ");
      fscanf(fp, "%s :", buffer);

      if(same_strings(buffer, "l"))
	{
	  found = 1;
	}
    }

  /* Read the format & the name of the file for C */
  if(fscanf(fp, "%s %s : %s ", tmp_buffer, buffer, buffer2) != 3)
    {
      printf("Error : must find \"l : file <type of file> : <filename> \n");
      fclose(fp);
      return NULL;
    }
 
  if(!same_strings(tmp_buffer, "file"))
    {
      printf("Error : must find \"l : file <type of file> : <filename> \n");
      printf("\"%s\" is not valid\n", tmp_buffer);
      fclose(fp);
      return NULL;
    }
 
  fclose(fp);
  result = (mat_file *)malloc(sizeof(mat_file));
 
  /* The file format */
  if(same_strings(buffer, "Sparse"))     result->format = SPARSE;
  else if(same_strings(buffer, "Dense")) result->format = DENSE;
  else
    {
      printf("The file format of l isn't valid.\n \"%s\" is unknown\n", buffer);
      return NULL;
    }

  /* The name */
  result->name = (char *)malloc((strlen(buffer2)+1)*sizeof(char));
  strcpy(result->name, buffer2);
  printf("Filename l : %s\n", result->name);
  if(PRINT_DEBUG)
    printf("Its format : %d\n", result->format);
	
  return result;
}


/* ------------------------------------------------------------------------- */


double **return_C(obj_func *obj_f, int dim)
{
  FILE *fC;
  int i, j;
  double **C;
  double minmax = (double)obj_f->min_max;
  int n;
 
 
 
  if(obj_f->C == NULL)
    {
      C = (double **)malloc(dim*sizeof(double *));
      for(i=0;i<dim;i++)
	{
	  C[i] = (double *)malloc(dim*sizeof(double));
	  for(j=0;j<dim;j++) C[i][j] = 0.;
	}
      return C;
    }

  fC = fopen(obj_f->C->name, "r");
  if (!fC)
    {
      printf("Can not open file \"%s\", exiting\n", obj_f->C->name);
      exit(1);
    }

  fscanf(fC, "%d", &n);
  if (n != dim-2)
  {
    printf("WARNING : size of the matrix does not match\n          to the dimension declared in the master file !\n");
    printf("          size = %d dimension = %d\n",n,dim-2);
    printf("          Size set to %d\n",dim-2);
    n = dim-2;   
  }
  
  C = (double **)malloc(dim*sizeof(double *));
  for(i=0;i<dim;i++)
    {
      C[i] = (double *)malloc(dim*sizeof(double));
      for(j=0;j<dim;j++) C[i][j] = 0.;
    }
 
  if(obj_f->C->format == SPARSE)
    {
      int ii, jj;
      double vv;
      int m;
	 
      fscanf(fC, "%d", &m);
      for(i=0;i<m;i++)
	{
	  /* indi indj val */
	  fscanf(fC, "%d %d %lf", &ii, &jj, &vv);
	  /* Indexes are between 1 and n, and we want them between 0 and n-1 */
	  C[ii-1][jj-1] = minmax*vv;
	  C[jj-1][ii-1] = minmax*vv;
	}
    }
  else /* DENSE */
    {
      double val;

     
      for(i=0;i<n;i++)
	for(j=0;j<n;j++)
	  {
	    fscanf(fC, "%lf", &val);
	    C[i][j] = minmax*val;
	  }
    }
 
  fclose(fC);
 
  return C;
}


/* ------------------------------------------------------------------------- */


double  *return_l(obj_func *obj_f, int dim)
{
  FILE *fl;
  int i;
  double *l;
  double minmax = (double)obj_f->min_max;
  int n;
 
 
  if(obj_f->l == NULL) return NULL;
  fl = fopen(obj_f->l->name, "r");
  if (!fl)
    {
      printf(" could not open file \"%s\", exiting\n", obj_f->l->name);
      exit(1);
    }

  fscanf(fl, "%d", &n);
  n = dim;
  l = (double *)malloc(n*sizeof(double));
  for(i=0;i<n;i++)
    {
      l[i] = 0.;
    }
 
  if(obj_f->l->format == SPARSE)
    {
      int ii;
      double vv;
      int m;
	 
      fscanf(fl, "%d", &m);
      for(i=0;i<m;i++)
	{
	  /* ind val */
	  fscanf(fl, "%d %lf", &ii, &vv);
	  /* Indexes are between 1 and n, and we want them between 0 and n-1 */
	  l[ii-1] = minmax*vv;
	}
    }
  else /* DENSE */
    {
      double val;
	 
      for(i=0;i<n;i++)
	{
	  fscanf(fl, "%lf", &val);
	  l[i] = minmax*val;
	}
    }
 
  fclose(fl);
 
  return l;
}


/* ------------------------------------------------------------------------- */


void add_linear_part(obj_func *obj_f, double **C)
{
  FILE *fl;
  int n;
  int i,j;
  double tmp;
  double coef = (double)obj_f->min_max/2.;
 
 
  if(obj_f->l == NULL) return;

  fl = fopen(obj_f->l->name, "r");
  if (!fl)
    {
      printf(" could not open file \"%s\", exiting\n", obj_f->l->name);
      exit(1);
    }
  fscanf(fl, "%d", &n);
 		 		 			 		 			 
  switch(obj_f->l->format)
    {
    case DENSE :
      {
	for(i=0;i<n;i++)
	  {
	    fscanf(fl, "%lf", &tmp);
	    C[i][n] += coef*tmp;
	    C[n][i] += coef*tmp;
	  }
      }
      break;
    case SPARSE :
      {
	int m;
	int ii;
		 
	fscanf(fl, "%d", &m);
	for(i=0;i<m;i++)
	  {
	    fscanf(fl, "%d %lf", &ii, &tmp);
	    C[ii-1][n] += coef*tmp;
	    C[n][ii-1] += coef*tmp;
	  }
		 
      }
      break;
    default : exit(1);
      break;
    }
 
  fclose(fl);
}


/* ------------------------------------------------------------------------- */


void write_constr_obj_fun(obj_func *obj_f, FILE *fp, char **str)
{
  int i, j;
  int n = obj_f->n;
  char buffer[40];
  int str_size;
  int dim = n+2;
 
 
 
  switch(obj_f->type)
    {
    case 0 : /* {-1,1} */
      {
	for(i=0;i<=n;i++)
	  {
	    write_singleton(dim, i, i, 1., fp, str);
	    if(fp) fprintf(fp, "= 1\n");
	    if(str)
	      {
		sprintf(buffer, "= 1\n");
		str_size = strlen(buffer) + strlen(*str) + 1;
		*str = (char *)realloc(*str, str_size*sizeof(char));
		*str = strcat(*str, buffer);
	      }
	    if(!fp && !str) printf("= 1\n");
	  }
				 
	/* More constraints */
      }
      break;
    case 1 : /* {0,1} */
      {
	write_singleton(dim, n, n, 1., fp, str);
	if(fp) fprintf(fp, "= 1\n");
	if(str)
	  {
	    sprintf(buffer, "= 1\n");
	    str_size = strlen(buffer) + strlen(*str) + 1;
	    *str = (char *)realloc(*str, str_size*sizeof(char));
	    *str = strcat(*str, buffer);
	  }
	if(!fp && !str) printf("= 1\n");
				 
	for(i=0;i<n;i++)
	  {
	    int list_i1[2]      = {i,n};
	    int list_j1[2]      = {0,0};
	    double list_val1[2] = {1.,-1.};
	    sparse_matrix *mat1;
	    int list_i2[1]      = {i};
	    int list_j2[1]      = {0};
	    double list_val2[1] = {1.};
	    sparse_matrix *mat2;
					 
					 
	    mat1 = create_init_sparse_mat(dim, 1);
	    mat1->nb_elements = 2;
	    mat1->index_i = list_i1;
	    mat1->index_j = list_j1;
	    mat1->val     = list_val1;
	    mat2 = create_init_sparse_mat(dim, 1);
	    mat2->nb_elements = 1;
	    mat2->index_i = list_i2;
	    mat2->index_j = list_j2;
	    mat2->val     = list_val2;

	    write_lowrank_sparse_sparse(mat1, mat2, fp, str);
	    if(fp) fprintf(fp, "= 0\n");
	    if(str)
	      {
		sprintf(buffer, "= 0\n");
		str_size = strlen(buffer) + strlen(*str) + 1;
		*str = (char *)realloc(*str, str_size*sizeof(char));
		*str = strcat(*str, buffer);
	      }
	    if(!fp && !str) printf("= 0\n");
	  }

	printf("\n");
	/* More constraints */
	if(obj_f->more_constr & 0X01)
	  {
	    /* Xij >= 0 ; 0<=i<j<=n */
	    printf("Adding constraints : Xij >= 0 ; 1<=i<j<=n\n");
	    for(i=0;i<n;i++)
	      for(j=i+1;j<n;j++)
		{
		  write_singleton(dim, i, j, 1., fp, str);
		  if(fp) fprintf(fp, "> 0\n");
		  if(str)
		    {
		      sprintf(buffer, "> 0\n");
		      str_size = strlen(buffer) + strlen(*str) + 1;
		      *str = (char *)realloc(*str, str_size*sizeof(char));
		      *str = strcat(*str, buffer);
		    }
		  if(!fp && !str) printf("> 0\n");
		}
	  }
	if(obj_f->more_constr & 0X02)
	  {
	    /* Xij <= Xii */
	    printf("Adding constraints : Xij <= Xii 1<=i<=n 1<=j<=n i!=j\n");
	    for(i=0;i<n;i++)
	      for(j=0;j<n;j++)
		{
		  if (i!=j)
		  {	  
		   int list_i1[2]      = {i,j};
		   int list_j1[2]      = {0,0};
		   double list_val1[2] = {1.,-1.};
		   sparse_matrix *mat1;
		   int list_i2[1]      = {i};
		   int list_j2[1]      = {0};
		   double list_val2[1] = {1.};
		   sparse_matrix *mat2;
					 		
					 		
		   mat1 = create_init_sparse_mat(dim, 1);
		   mat1->nb_elements = 2;
		   mat1->index_i = list_i1;
		   mat1->index_j = list_j1;
		   mat1->val     = list_val1;
		   mat2 = create_init_sparse_mat(dim, 1);
		   mat2->nb_elements = 1;
		   mat2->index_i = list_i2;
		   mat2->index_j = list_j2;
		   mat2->val     = list_val2;
		 					 
		   write_lowrank_sparse_sparse(mat1, mat2, fp, str);
		   if(fp) fprintf(fp, "> 0\n");
		   if(str)
		    {
		      sprintf(buffer, "> 0\n");
		      str_size = strlen(buffer) + strlen(*str) + 1;
		      *str = (char *)realloc(*str, str_size*sizeof(char));
		      *str = strcat(*str, buffer);
		    }
		   if(!fp && !str) printf("> 0\n");
		  }
		}
	  }
	if(obj_f->more_constr & 0X04)
	  {
	    /* Xii+Xjj <= 1+Xij */
	    printf("Adding constraints : Xii+Xjj <= 1+Xij 1<=i<j<=n\n");
	    for(i=0;i<n;i++)
	      for(j=i+1;j<n;j++)
		{
		  int list_i[3]      = {i ,j ,i};
		  int list_j[3]      = {i ,j ,j};
		  double list_val[3] = {2.,2.,-1.};
		  sym_matrix *mat;
							 
		  mat = create_init_sym_mat(dim);
		  mat->nb_elements = 3;
		  mat->index_i = list_i;
		  mat->index_j = list_j;
		  mat->val     = list_val;
							 
		  write_sym_mat(mat, fp, str);
		  if(fp) fprintf(fp, "< 2\n");
		  if(str)
		    {
		      sprintf(buffer, "< 2\n");
		      str_size = strlen(buffer) + strlen(*str) + 1;
		      *str = (char *)realloc(*str, str_size*sizeof(char));
		      *str = strcat(*str, buffer);
		    }
		  if(!fp && !str) printf("< 2\n");
		}
	  }
	if(obj_f->more_constr & 0X08)
	  {
	    /* Xik+Xjk <= Xkk+Xij */
	    int k;
	    printf("Adding constraints : Xik+Xjk <= Xkk+Xij 1<=i<j<k<=n\n");					 
	    for(i=0;i<n;i++)
	      for(j=i+1;j<n;j++)
		for(k=0;k<n;k++)
		  {
		    if(k!=i && k!=j)
		      {
			int list_i[4]      = {i ,j ,k,  i};
			int list_j[4]      = {k ,k ,k,  j};
			double list_val[4] = {1.,1.,-2.,-1.};
			sym_matrix *mat;
									 
			if(k==i) list_val[0] = 2.;
			if(k==j) list_val[1] = 2.;
									 								 
			mat = create_init_sym_mat(dim);
			mat->nb_elements = 4;
			mat->index_i = list_i;
			mat->index_j = list_j;
			mat->val     = list_val;
									 
			write_sym_mat(mat, fp, str);
			if(fp) fprintf(fp, "< 0\n");
			if(str)
			  {
			    sprintf(buffer, "< 0\n");
			    str_size = strlen(buffer) + strlen(*str) + 1;
			    *str = (char *)realloc(*str, str_size*sizeof(char));
			    *str = strcat(*str, buffer);
			  }
			if(!fp && !str) printf("< 0\n");
		      }
		  }
	  }
	if(obj_f->more_constr & 0X10)
	  {
	    /* Xij+Xik+Xjk+1 >= Xii+Xjj+Xkk */
	    int k;
	    printf("Adding constraints : Xij+Xik+Xjk+1 >= Xii+Xjj+Xkk 1<=i<j<k<=n\n");					 
	    for(i=0;i<n;i++)
	      for(j=i+1;j<n;j++)
		for(k=j+1;k<n;k++)
		  {
		    int list_i[6]      = {i ,i ,j, i,  j,  k};
		    int list_j[6]      = {j ,k ,k, i,  j,  k};
		    double list_val[6] = {1.,1.,1.,-2.,-2.,-2.};
		    sym_matrix *mat;
								 
		    mat = create_init_sym_mat(dim);
		    mat->nb_elements = 6;
		    mat->index_i = list_i;
		    mat->index_j = list_j;
		    mat->val     = list_val;
								 
		    write_sym_mat(mat, fp, str);
		    if(fp) fprintf(fp, "> -2\n");
		    if(str)
		      {
			sprintf(buffer, "> -2\n");
			str_size = strlen(buffer) + strlen(*str) + 1;
			*str = (char *)realloc(*str, str_size*sizeof(char));
			*str = strcat(*str, buffer);
		      }
		    if(!fp && !str) printf("> -2\n");
		  }
	  }
      }
      break;
    default : printf("Error : objective function has unknown type\n");
      break;
    }
}


/* ------------------------------------------------------------------------- */


int read_objective_function(char *filename, obj_func *obj_f)
{
  FILE *pb_file;
  fpos_t p;
  fpos_t begin;
  char buffer[80];
  int again; /* Do we need to read again ? */
  double val = 0.;
 
  struct
  {
    unsigned char C   : 1; /* Do we use the matrix C     */
    unsigned char l   : 1; /* Do we use the vector l     */
    unsigned char val : 1; /* Do we use a constant value */
  } flag;
 
 

  printf("\n\n");
  pb_file = fopen(filename, "r");
  if(!pb_file)
    {
      printf("Error while opening the file \"%s\".\n", filename);
      printf("Abort.\n");
      return 1;
    }

  /* *************** Reading of the first lines : comments *************** */
  again = 1;
  while(again)
    {
      fgetpos(pb_file, &begin);
      fscanf(pb_file, "%s", buffer);
      if(buffer[0]=='/' && buffer[1]=='/')
	{
	  fscanf(pb_file, "%s", buffer);
	  while(!same_strings(buffer, "//"))
	    {
	      if(feof(pb_file))
		{
		  printf("Error : comments should end with \" //\".\n");
		  fclose(pb_file);
		  return 1;
		}
	      fscanf(pb_file, "%s", buffer);
	    }
	}
      else
	{
	  again = 0;
	  fsetpos(pb_file, &begin);
	}
    }
 
 
 
  /* *************** "Min" or "Max" *************** */
  fscanf(pb_file, "%s", buffer);
  /* The case doesn't matter */
  if(same_strings(buffer, "Max"))
    {
      //if(PRINT_DEBUG)
	printf("Maximizing\n");
      obj_f->min_max = 1;
    }
  else if(same_strings(buffer, "Min"))
    {
     //if(PRINT_DEBUG)
	printf("Minimizing\n");
      obj_f->min_max = -1;
    }
  else
    {
      printf("Error : neither \"Min\" nor \"Max\"\n");
      fclose(pb_file);
      return 2;
    }
  flag_min_max = obj_f->min_max;
  
  /* *************** The objective function *************** */
  fgetpos(pb_file, &p);
  again = 1;
  flag.C   = 0;
  flag.l   = 0;
  flag.val = 0;

  /* The first element of the function */
  fscanf(pb_file, "%s", buffer);
  /* Do we use the matrix C ? */
  if(buffer[0]=='C' || buffer[0]=='c')
    {
      flag.C = 1;
      fgetpos(pb_file, &p);

      /* Is there a '+' ? (another element) */
      fscanf(pb_file, "%s", buffer);
      if(!strcmp(buffer, "+"))
	{
	  /* There is another element */
	  fgetpos(pb_file, &p);
	}
      else
	{
	  /* There isn't anything else for the function */
	  again = 0;
	  fsetpos(pb_file, &p);
	}
    }
  else
    {
      /* We must go back in the file to reload the line */
      fsetpos(pb_file, &p);
    }

  /* Is there anything else to read ? */
  if(again)
    {
      /* Do we use the vector l ? */
      fscanf(pb_file, "%s", buffer);
      if(buffer[0]=='L' || buffer[0]=='l')
	{
	  flag.l = 1;
	  fgetpos(pb_file, &p);
	  /* Is there a '+' */
	  fscanf(pb_file, "%s", buffer);
	  if(!strcmp(buffer, "+"))
	    {
	      /* There is another element : the constant value */
	      if(fscanf(pb_file, "%lf", &val) != 1)
		{
		  printf("Error while reading the constant value of the objective function\n");
		  fclose(pb_file);
		  return 3;
		}
	      flag.val = 1;
	      fgetpos(pb_file, &p);
	    }
	  else
	    {
	      /* End of the function */
	      fsetpos(pb_file, &p);
	    }
	}
      else
	{
	  if(flag.C)
	    {
	      /* We must reload the string : it's the constant */
	      fsetpos(pb_file, &p);
	      if(fscanf(pb_file, "%lf", &val) != 1)
		{
		  printf("Error while reading the constant value of the objective function\n");
		  fclose(pb_file);
		  return 3;
		}
	      flag.val = 1;
	      fgetpos(pb_file, &p);
	    }
	}
    }
  /* *************** END OF READING : OBJECTIVE FUNCTION *************** */
 
  /* We must have C or l */
  if(!flag.C && !flag.l)
    {
      printf("The objective function isn't valid.\n");
      printf("There's neither a quadratic term nor a linear one.\n");
      printf("Abort\n");
      fclose(pb_file);
      return 4;
    }
  else
    {
      if(PRINT_DEBUG){
	printf("Quadratic term : %s\n", (flag.C)?"YES":"NO");
	printf("Linear term    : %s\n", (flag.l)?"YES":"NO");
	printf("Constant term  : %s", (flag.val)?"YES : ":"NO");
	if(flag.val)
	  printf("%f\n\n", val);
	else
	  printf("\n\n");
      }
    }
  fclose(pb_file);

  /* Get the type of problem : {-1,1} or {0,1} */
  if((obj_f->type = read_type(filename, begin)) < 0)
    {
      return 5;
    }
  if(PRINT_DEBUG)
    printf("Type OK : %d\n", obj_f->type);

  /* Get the dimension of the problem */
  if((obj_f->N = read_dim(filename, begin)) <= 0)
    {
      return 6;
    }
  //if(PRINT_DEBUG)
    printf("Dim : %d\n", obj_f->N);
 
  /* Get the number of boolean variables */
  if((obj_f->n = read_n(filename, begin, obj_f->N)) < 0)
    {
      return 7;
    }
  if(PRINT_DEBUG)
    printf("n OK : %d\n", obj_f->n);
 
  /* Some specific constraints to add ? */
  obj_f->more_constr = read_add_more_constr(filename, begin);
  if(obj_f->more_constr < 0)
    {
      return 8;
    }
  if(PRINT_DEBUG)
    printf("more constr OK : %d\n", obj_f->more_constr);
 
    TRACE = read_trace(filename, begin);
    if(TRACE>0) printf("The trace of X is set to : %d\n", TRACE);
//    else printf("The trace of X is not set %d \n",TRACE);


  /* We must now get the name of the files and their format */
  if(flag.C)
    {
      if(!(obj_f->C = read_C(filename, begin)))
	{
	  return 9;
	}
      if(PRINT_DEBUG)
	printf("C OK\n");
    }

  if(flag.l)
    {
      if(!(obj_f->l = read_l(filename, begin)))
	{
	  return 10;
	}
      if(PRINT_DEBUG)
	printf("l OK\n");
    }
 
  /* The constant value */
  obj_f->val = val;
 
  if(PRINT_DEBUG)
    printf("\n");
 
  /* Some constraints relative to the type of the problem are added */
  obj_f->nb_constraints = obj_f->n + 1;
  /* And some others with the label AddMoreConstr */
  if(obj_f->type == 1) /* {0,1} */
    {
      int n = obj_f->n;
      if(obj_f->more_constr & 0X01) obj_f->nb_constraints += (n*(n-1))/2;
      if(obj_f->more_constr & 0X02) obj_f->nb_constraints += n*(n-1);
      if(obj_f->more_constr & 0X04) obj_f->nb_constraints += (n*(n-1))/2;
      if(obj_f->more_constr & 0X08) obj_f->nb_constraints += ((n-2)*n*(n-1))/2;
      if(obj_f->more_constr & 0X10) obj_f->nb_constraints += (n*(n-1)*(n-2))/6;

    }
  else /* {-1,1} */
    {
    }
 
  return 0;
}


/* ------------------------------------------------------------------------- */


void write_objective_function(obj_func *obj_f, FILE *fp, char **str)
{
  /* This function writes the objective function, the number of constraints */
  /* and the constraints relative to the type of the problem                */

  int a;
  int dim;
  int i;
  double **C;
  sym_matrix *C_sym;
  char buffer[100];
  int str_size;

  dim = obj_f->N + 2;
  /* The trace */
 if (TRACE<=0)
 { 
  if(obj_f->n != obj_f->N) a = 0X7FFFFFFF;
  else                     a = obj_f->n + 1;
  if(fp)  fprintf(fp, "%d\n", a);
  if(str)
    {
      sprintf(buffer, "%d\n", a);
      str_size = strlen(buffer) + strlen(*str) + 1;
      *str = (char *)realloc(*str, str_size*sizeof(char));
      *str = strcat(*str, buffer);
    }
  if(!fp && !str) printf("%d\n", a);
 }
 else
 {
  a = TRACE+1; 
  if(fp)  fprintf(fp, "%d\n", a);
  if(str)
    {
      sprintf(buffer, "%d\n", a);
      str_size = strlen(buffer) + strlen(*str) + 1;
      *str = (char *)realloc(*str, str_size*sizeof(char));
      *str = strcat(*str, buffer);
    }
  if(!fp && !str) printf("%d\n", a);
 
 }
 /* Matrix C ('l' is included in it) */
  C = return_C(obj_f, dim);
  add_linear_part(obj_f, C);
  /* The constant value is also included in C */
  C[obj_f->N][obj_f->N] = obj_f->val*obj_f->min_max;

  C_sym = create_sym_mat_mat(obj_f->n+1, C);
  C_sym->n = dim;
  C_sym->k = dim;
  write_sym_mat(C_sym, fp, str);
  for(i=0;i<dim;i++) free(C[i]);
  free(C);


 /* The number of constraints ; Spaces are useful if you want to add constraints : */
 /* You just have to modify the file without inserting ( just modifying )          */
  if(fp) fprintf(fp, "%d          \n", obj_f->nb_constraints);
  if(str)
    {
      sprintf(buffer, "%d          \n", obj_f->nb_constraints);
      str_size = strlen(buffer) + strlen(*str) + 1;
      *str = (char *)realloc(*str, str_size*sizeof(char));
      *str = strcat(*str, buffer);
    }
  if(!fp && !str) printf("%d          \n", obj_f->nb_constraints);
 
 /* The constraints specific to the objective function */
  write_constr_obj_fun(obj_f, fp, str);
  // printf("Objective Function written.\n");
}
