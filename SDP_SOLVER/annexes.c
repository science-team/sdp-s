/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "annexes.h"



/* ------------------------------------------------------------------------- */

int same_strings(char *s1, const char *s2)
{
 /* The case doesn't matter                                */
 /* For example "TEST" and "TesT" are the same strings     */
 /* Return 1 if the strings are the same, 0 if they aren't */
 
 int n1 = strlen(s1);
 int n2 = strlen(s2);
 int i;
 
 
 if(n1 != n2) return 0;
 
 i=0;
 while(i<n1)
 	{
	 if(s1[i] != s2[i])
	 	{
		 int diff = s1[i] - s2[i];
		 
		 if(s1[i]>='A' && s1[i]<='Z')
		 	{
			 if(diff != 'A'-'a')
		 		{
				 /* The strings are different */
				 return 0;
				}
			}
		 else if(s1[i]>='a' && s1[i]<='z')
		 	{
			 if(diff != 'a'-'A')
		 		{
				 /* The strings are different */
				 return 0;
				}
			}
		 else return 0;
		}
	 i++;
	}
 
 return 1;
}

/* ------------------------------------------------------------------------- */

double difference(double d1, double d2, double e)
{
 double diff;
 
 
 diff = d1 - d2;
 if(fabs(diff) < e) return 0.;
 else               return diff;
 
}

/* --------------------------------------------------------------- */

int is_integer(double d, double e)
{
 double d2;
 
 
 d2 = (double)((int)d);
 if(difference(d, d2, e) && difference(d, d2+1., e) && difference(d, d2-1., e))
 	return 0;
 else
 	return 1;
}

/* ------------------------------------------------------------------------- */

int get_integer(double d, double e)
{
 double d2;
 int result;
 
 result = (int)d;
 d2 = (double)result;
 if     (!difference(d, d2+1., e)) result++;
 else if(!difference(d, d2-1., e)) result--;
 
 return result;
}

/* ------------------------------------------------------------------------- */

double *get_x_from_fileW(char *fileW)
{
 FILE *fW;
 int i, j;
 int n;
 double *x;
 double buf;
 

 fW = fopen(fileW, "r");
 fscanf(fW, "%d", &n);
 x = (double *)malloc(n*sizeof(double));
 for(i=0;i<n;i++)
 	{
	 fscanf(fW, "%lf", &buf);
	 x[i] = buf;
	 for(j=1;j<n-i;j++)
	 	{
		 fscanf(fW, "%lf", &buf);
		}
	}
 fclose(fW);
 
 return x;
}

/* --------------------------------------------------------------- */

char *file2string(char *filename)
{
 char *result;
 unsigned long len;
 FILE *fp;
 struct stat file_stats;
 char buf;
 int i;
 
 
 
 if(stat(filename, &file_stats))
 	{
	 printf("Error : can't read the length of the file \"%s\"\n", filename);
	 return NULL;
	}
 
 len = file_stats.st_size;
 result = (char *)malloc((len+1)*sizeof(char));
 if((fp = fopen(filename, "r")) == NULL)
 	{
	 printf("Error : can't open file \"%s\"\n", filename);
	}
 i = 0;
 while((buf=fgetc(fp)) != EOF)
 	{
	 result[i] = buf;
	 i++;
	}
 result[i] = 0;
 fclose(fp);
 
 return result;
}

/* --------------------------------------------------------------- */

