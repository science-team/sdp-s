/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef constraints_h
#define constraints_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "annexes.h"
#include "single_constraints.h"
#include "linear_constraints.h"
#include "quadratic_constraints.h"
#include "sum_constraints.h"
#include "product_constraints.h"

 typedef struct
	{
	 int n;
	 int N;
	 singleton_constraints *single;
	 linear_constraints    *lin;
	 quadratic_constraints *quad;
	} constraints;


 constraints *create_constraints(int n, int N);
 void free_constraints(constraints *c);
 int get_range(char *str_range, int *min, int *max, int n, int N);
 int get_range_2(char *str_range, int *min, int *max, int n, int N, char var);
 int read_constraints(char *filename, constraints *constr);
 void write_constraints(int dim, constraints *constr, FILE *fp, char **str);
 int valid_constraints(constraints *constr, double **X, double *x, double e);

 void read_prodConstrVect(FILE *fp,constraints *constr); 
 void read_prodConstrVectbar(FILE *fp,constraints *constr);
 void read_simple(FILE *fp,constraints *constr);

#endif
