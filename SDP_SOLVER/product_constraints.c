/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  

  
#include "product_constraints.h"


/* ------------------------------------------------------------------------- */

quadratic_constraints *add_productlinvar(quadratic_constraints *quad, linear_constraints *lin, int num, int min, int max, int n, int N)
{
 quadratic_constraints *result;
 int nb;
 sym_matrix *Q;
 double *l;
 int i, j;
 int cpt;
 
 result = quad;
 if(!result)
 	{
	 result = create_quad_constr(n, N);
         result->constraints = (quadratic_constraint **)malloc( (1+max-min)*sizeof(quadratic_constraint *));        
	}
	else
	{
         nb = result->nb;
         result->constraints = (quadratic_constraint **)realloc(result->constraints, (nb+1+max-min)*sizeof(quadratic_constraint *));
	}
 
 l = lin->constraints[num]->l; 
 nb = result->nb;
 for(i=min;i<max+1;i++)
 	{
	 nb = result->nb;
	 result->constraints[nb] = (quadratic_constraint *)malloc(sizeof(quadratic_constraint));
	 result->nb = nb+1;
	 Q = create_sym_mat(N+1, N+1);
	 cpt = 0;
	 for(j=0;j<N;j++)
	 	{
		 if(l[j])
		 	{
			 Q->index_i[cpt] = i;
			 Q->index_j[cpt] = j;
			 Q->val[cpt]     = l[j];
			 if(i==j) Q->val[cpt] += l[j];
			 cpt++;
			}
		}
	 if(lin->constraints[num]->c1)
	 	{
		 Q->index_i[cpt] = i;
		 Q->index_j[cpt] = N;
		 Q->val[cpt]     = -lin->constraints[num]->c1;
		 cpt++;
		}
	 
	 Q->index_i = (int *)   realloc(Q->index_i, cpt*sizeof(int));
	 Q->index_j = (int *)   realloc(Q->index_j, cpt*sizeof(int));
	 Q->val     = (double *)realloc(Q->val,     cpt*sizeof(double));
	 
	 result->constraints[nb]->dim = N+1;
	 result->constraints[nb]->c1  = 0.;
	 result->constraints[nb]->c2  = 0.;
	 result->constraints[nb]->l   = NULL;
	 result->constraints[nb]->Q   = Q;
	 result->constraints[nb]->flag_ci = 3;
	}
 printf("Product constraints done\n");
 fflush(NULL);
 return result;
}

/* ------------------------------------------------------------------------- */
