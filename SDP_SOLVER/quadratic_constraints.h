/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef quadratic_constraints_h
#define quadratic_constraints_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "annexes.h"
#include "matrix_tools.h"

 typedef struct
	{
	 double c1;
	 double c2;
	 double *l;
	 int *el_l;
	 int nb_l;
	 sym_matrix *Q;
	 int dim;
	 int flag_ci;
	} quadratic_constraint;

 typedef struct
	{
	 int n;
	 int N;
	 int nb;
	 quadratic_constraint **constraints;
	} quadratic_constraints;

 quadratic_constraints *create_quad_constr(int n, int N);
 void free_quad_constr(quadratic_constraints *Q);
 int read_quad_constr(char *filename, quadratic_constraints *constr, int sparse_dense);
 void write_quad_constr(int dim, quadratic_constraints *constr, FILE *fp, char **str);
 int valid_quad_constr(quadratic_constraints *constr, double **X, double *x, double e);
 
 
#endif
