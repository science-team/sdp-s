/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "single_constraints.h"


/* ------------------------------------------------------------------------- */

singleton_constraints *create_singleton_constraints(int n, int N)
{
  singleton_constraints *result;
 
 
  result = (singleton_constraints *)malloc(sizeof(singleton_constraints));
  result->n  = n;
  result->N  = N;
  result->nb = 0;
  result->constraints = NULL;
 
  return result;
}

/* ------------------------------------------------------------------------- */

void free_singleton_constr(singleton_constraints *s)
{
  free(s->constraints);
  free(s);
}

/* ------------------------------------------------------------------------- */

void add_singleton(singleton_constraints *constr, int i, int j, char op, double val)
{
  constr->constraints = (singleton_type **)realloc(constr->constraints, (constr->nb + 1)*sizeof(singleton_type *));
  constr->constraints[constr->nb] = (singleton_type *)malloc(sizeof(singleton_type));
  constr->constraints[constr->nb]->i   = i;
  constr->constraints[constr->nb]->j   = j;
  constr->constraints[constr->nb]->val = val;
  constr->constraints[constr->nb]->op  = op;
  constr->nb++;
}

/* ------------------------------------------------------------------------- */

void write_single_constr(int dim, singleton_constraints *constr, FILE *fp, char **str)
{
  int i;
  char buffer[100];
  int str_size;
  double coef;
 
 
  for(i=0;i<constr->nb;i++)
    {
      write_singleton(dim, constr->constraints[i]->i, constr->constraints[i]->j, 1., fp, str);
      if(constr->constraints[i]->i == constr->constraints[i]->j) coef = 1.;
      else                                                       coef = 2.;
      if(fp) fprintf(fp, "%c %f\n", constr->constraints[i]->op, constr->constraints[i]->val*coef);
      if(str)
	{
	  sprintf(buffer, "%c %f\n", constr->constraints[i]->op, constr->constraints[i]->val*coef);
	  str_size = strlen(buffer) + strlen(*str) + 1;
	  *str = (char *)realloc(*str, str_size*sizeof(char));
	  *str = strcat(*str, buffer);
	}
      if(!fp && !str) printf("%c %f\n", constr->constraints[i]->op, constr->constraints[i]->val*coef);
    }
  printf("Single constraints written.\n");
}

/* ------------------------------------------------------------------------- */

int  valid_single_constr(singleton_constraints *constr, double **X, double e)
{
  int nb = constr->nb;
 
  int ii, jj;
  double val;
  char op;
 
  int i;
  double v;
 
  for(i=0;i<nb;i++)
    {
      ii  = constr->constraints[i]->i;
      jj  = constr->constraints[i]->j;
      val = constr->constraints[i]->val;
      op  = constr->constraints[i]->op;
	 
      v = X[ii][jj];
	 
      if(op == '=')
	{
	  if(difference(v,val,e)) return 0;
	}
      else if(op == '<')
	{
	  if(difference(v,val,e)>0.) return 0;
	}
      else /* op == '>' */
	{
	  if(difference(v,val,e)<0.) return 0;
	}
    }
 
  return 1;
}

/* ------------------------------------------------------------------------- */

