 
/* -*-c++-*-
 * 
 *    Source     $RCSfile: sb_test.cc,v $ 
 *    Version    $Revision: 1.1.1.1.2.1 $ 
 *    Date       $Date: 2000/02/28 13:55:41 $ 
 *    Author     Christoph Helmberg 
 *    Modified in 2002 by G.Delaporte, S. Jouteau, F. Roupin
 * 
 --------------------------------------------------------------------------- */

// ****************************************************************************
//
//  This file is part of the class library "SBmethod", an implementation 
//  of the spectral bundle method for eigenvalue optimization.
//
//  Copyright (C) 2000  Christoph Helmberg
//
//     Konrad-Zuse-Zentrum fuer Informationstechnik Berlin
//     Takustrasse 7, D-14195 Berlin, Germany
//      
//     helmberg@zib.de     http://www.zib.de/helmberg 
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ****************************************************************************

#ident "$Id: sb_test.cc,v 1.1.1.1.2.1 2000/02/28 13:55:41 bzfhelmb Exp $"

#include "SDPS_main.h"
#include <fstream>

SBparams* paramsp;
SpectralBundle* bundlep;
BaseSolver* bsolver;
Problem* problemp;
Clock* clp;

int TERMINATION_OK;
int NB_VAR;
int SILENCE_MODE = 0;
int KILL = 0;
extern ofstream result;
extern FILE * RES;
extern char *filename_result;

//*************************************************************************
//                             save_and_exit
//*************************************************************************

void save_and_exit(const SpectralBundle* sbp)
 {
   KILL = 1;
     ofstream out;
     if (paramsp->savefile){
         out.open(paramsp->savefile);
     }
     else {
         out.open("SDPS_resume.dat");
     }
     out<<(sbp->get_clock()).time()<<"\n";
     paramsp->save(out);
     mat_randgen.save(out);
     sbp->get_problem()->save(out);
     sbp->get_choose_tau()->save(out);
     sbp->get_update_bundle()->save(out);
     sbp->get_terminator()->save(out);
     sbp->save(out);
     bsolver->save(out);
     out.close();
     cout<<"Terminated to resume later, intermediate output follows:"<<endl;
     int retval=paramsp->output(problemp,sbp,clp);
     if(filename_result)
     {
        cout<<"Output was saved in file : "<<filename_result<<"\n\n";
	result.close();
     }
     fflush(NULL);	     
     cout<<"#"; 
     exit(retval);
 }

//*************************************************************************
//                             summary_and_exit
//*************************************************************************

void summary_and_exit(const SpectralBundle* sbp)
 {
   KILL = 1;
   cout<<"Terminated prematurely by User signal 1!"<<endl;
   int retval;
   retval=paramsp->output(problemp,sbp,clp);
   if(filename_result)
   {
      cout<<"Output was saved in file : "<<filename_result<<"\n\n";	  
      result.close();
    }
   cout<<"#";
   exit(retval);
 }

//*************************************************************************
//                             summary_and_continue
//*************************************************************************

void summary_and_continue(const SpectralBundle* sbp)
 {
   KILL = 1;
   cout<<"Intermediate output caused by User signal 2:"<<endl;
   int retval;
   retval=paramsp->output(problemp,sbp,clp);
   sbp->get_terminator()->set_save_function(0);
 }

//*************************************************************************
//                             catch_signal
//*************************************************************************


void catch_signal(int sig){
  extern int flag_fSB;
  extern char *filenameSB;
  char sys[200];

  if(!flag_fSB){
    sprintf(sys,"rm %s",filenameSB);
    system(sys);
  }

  if (sig==SIGTERM){
    bundlep->get_terminator()->set_save_function(save_and_exit);
  }
  else if (sig==SIGUSR1) {
    bundlep->get_terminator()->set_save_function(summary_and_exit);
  }
  else if (sig==SIGUSR2) {
    bundlep->get_terminator()->set_save_function(summary_and_continue);
  }
  else if (sig==SIGFPE) {
    cout<<"\n\n**** ERROR: ARITHMETIC EXCEPTION, continuing anyways\n"<<endl;
  }
}


//*************************************************************************
//                              sb_main
//*************************************************************************

SDP_sol *sb_main(int argc,char **argv)
{
 SBparams params;
 paramsp=&params;

 Problem1 problem;
 problemp=&problem;
 SpectralBundle bundle;
 bundlep=&bundle;
 Clock myclock;
 clp=&myclock;
 Solver solver;
 bsolver=&solver;
 SDP_sol *result;
 Real sol;
 Real *vect_sol;
 
 
 if(params.input(argc,argv,bsolver)) return NULL;

 Microseconds offset(0,0);
 
 ifstream fresin;
 if (params.resume){
     fresin.open(params.resume);
     if (!fresin) {
         cout<<"*** ERROR: could not open resume file #"<<params.resume<<endl;
         exit(1);
     }
     if (!(fresin>>offset)) {
         cout<<"*** ERROR: failed in reading time offset from resume file #"<<params.resume<<endl;
         exit(1);
     }
     if (!params.restore(fresin)){
         cout<<"*** ERROR: failed in reading parameters from  resume file #"<<params.resume<<endl;
         exit(1);
     }
 }
 
 if(params.initialize(&problem,&bundle,&myclock,&solver)) return NULL;
 
 if (params.resume){
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring mat_randgen!#"<<endl;
         exit(1);
     }
     mat_randgen.restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring problem!#"<<endl;
         exit(1);
     }
     problem.restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring choose_tau!#"<<endl;
         exit(1);
     }
     bundle.get_choose_tau()->restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring update_bundle!#"<<endl;
         exit(1);
     }
     bundle.get_update_bundle()->restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring terminator!#"<<endl;
         exit(1);
     }
     bundle.get_terminator()->restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring bundle!#"<<endl;
         exit(1);
     }
     bundle.restore(fresin);
     if (!fresin){
         cerr<<"*** ERROR: sb_main(): resume file not good before restoring solver!#"<<endl;
         exit(1);
     }
     solver.restore(fresin);
     (fresin).close();
     myclock.start();
     myclock.set_offset(offset);
     cout<<"Resuming from file `"<<params.resume<<"'\n";
 }

 signal(SIGTERM,catch_signal);
 signal(SIGUSR1,catch_signal);
 signal(SIGUSR2,catch_signal);
 signal(SIGFPE,catch_signal);

 if(SILENCE_MODE)
 	{
	 solver.set_out(NULL);
	}
 else
 	{
	 solver.set_out(&cout);
	 myclock.elapsed_time(cout);cout<<"\n"<<endl;
	}
 
 
 if(solver.solve(&problem,&bundle,&myclock,&params,(params.resume!=0))) return NULL;
 if(params.output(&problem,&bundle,&myclock)) return NULL;
 if(!TERMINATION_OK) return NULL;
 sol = bundle.get_problem()->objval();
 vect_sol = params.Xvect;
 result = (SDP_sol *)malloc(sizeof(SDP_sol));
 result->x = vect_sol;
 result->val = sol;
 
 return result;
}


//*************************************************************************
//                              get_SDP_sol_str
//*************************************************************************

SDP_sol *get_SDP_sol_str(char *pb, int add_argc, char **add_argv)
{
 /* Before using this function, make sure that NB_VAR has the good value */
 int argc_SB;
 char **argv_SB;
 SDP_sol *result;
 int i;
 
 
 /* We create argc & argv to use the main() function of sb */
 argc_SB = 6;
 argv_SB = (char **)malloc((argc_SB+add_argc)*sizeof(char *));
 /* "./sb" */
 argv_SB[0] = (char *)malloc(5*sizeof(char));
 strcpy(argv_SB[0], "./sb");
 /* "-f2" */
 argv_SB[1] = (char *)malloc(4*sizeof(char));
 strcpy(argv_SB[1], "-f2");
 argv_SB[2] = pb;

 /* "-oXd2" */
 argv_SB[3] = (char *)malloc(6*sizeof(char));
 strcpy(argv_SB[3], "-oXd2");
 
 /* To get the vector associated with the result */
 /* "-oXd" */
 argv_SB[4] = (char *)malloc(5*sizeof(char));
 strcpy(argv_SB[4], "-oXd");
 /* "name_of_the_file_W" */
 argv_SB[5] = (char *)malloc(4*sizeof(char));
 strcpy(argv_SB[5], "_");

 for(i=argc_SB;i<argc_SB+add_argc;i++)
 	{
	 argv_SB[i] = add_argv[i-argc_SB];
	}
 argc_SB += add_argc;
 
 result = sb_main(argc_SB, argv_SB);

 return result;
}


//*************************************************************************
//                              get_SDP_sol_file
//*************************************************************************

SDP_sol *get_SDP_sol_file(char *filenameSB, int add_argc, char **add_argv)
{
 /* Before using this function, make sure that NB_VAR has the good value */
 int argc_SB;
 char **argv_SB;
 SDP_sol *result;
 int i;
 
 
 /* We create argc & argv to use the main() function of sb */
 argc_SB = 6;
 argv_SB = (char **)malloc((argc_SB+add_argc)*sizeof(char *));
 /* "./sb" */
 argv_SB[0] = (char *)malloc(5*sizeof(char));
 strcpy(argv_SB[0], "./sb");
 /* "-f2" */
 argv_SB[1] = (char *)malloc(4*sizeof(char));
 strcpy(argv_SB[1], "-f");
 argv_SB[2] = filenameSB;

 /* "-oXd2" */
 argv_SB[3] = (char *)malloc(6*sizeof(char));
 strcpy(argv_SB[3], "-oXd2");
 
 /* To get the vector associated with the result */
 /* "-oXd" */
 argv_SB[4] = (char *)malloc(5*sizeof(char));
 strcpy(argv_SB[4], "-oXd");
 /* "name_of_the_file_W" */
 argv_SB[5] = (char *)malloc(4*sizeof(char));
 strcpy(argv_SB[5], "_");

 for(i=argc_SB;i<argc_SB+add_argc;i++)
 	{
	 argv_SB[i] = add_argv[i-argc_SB];
	}
 argc_SB += add_argc;
 
 result = sb_main(argc_SB, argv_SB);

 return result;
}


//*************************************************************************
//                              get_SDP_sol
//*************************************************************************

SDP_sol *get_SDP_sol(SDP_pb *pb, int add_argc, char **add_argv, int silence,char *filenameSB)
{
 SDP_sol *result;
 double minmax = (double)pb->obj_f->min_max;
 
 NB_VAR = pb->obj_f->N;
 if(silence) SILENCE_MODE = 1;
 
 if(filenameSB) result = get_SDP_sol_file(filenameSB,add_argc,add_argv);

 if(minmax != 1. && result) result->val = result->val*minmax;
 
 return result;
}

/* ---------------------------------------------------------------------------
 *    Change log $Log: sb_test.cc,v $
 *    Change log Revision 1.1.1.1.2.1  2000/02/28 13:55:41  bzfhelmb
 *    Change log Reording inline functions and correction of minor details
 *    Change log
 *    Change log Revision 1.1.1.1  1999/12/15 13:47:51  bzfhelmb
 *    Change log Imported sources
 *    Change log
 *
 *    End of $Source: /kombadon/cvs/cvsroot/bzfhelmb/SBmethod/spectral/sb_test.cc,v $
 --------------------------------------------------------------------------- */
