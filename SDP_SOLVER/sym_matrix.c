/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "sym_matrix.h"

/*----------------------------------------------------------------------*/

void free_sym_mat(sym_matrix *mat)
{
 free(mat->index_i);
 free(mat->index_j);
 free(mat->val);
 free(mat);
}

/*----------------------------------------------------------------------*/

sym_matrix *create_init_sym_mat(int n)
{
 sym_matrix *mat;
 
 
 mat = (sym_matrix *)malloc(sizeof(sym_matrix));
 mat->n = n;
 mat->k = n;
 mat->nb_elements = 0;
 mat->index_i = NULL;
 mat->index_j = NULL;
 mat->val     = NULL;
 
 return mat;
}

/*----------------------------------------------------------------------*/

sym_matrix *create_sym_mat(int n, int nb_el)
{
 sym_matrix *mat;
 
 
 mat = (sym_matrix *)malloc(sizeof(sym_matrix));
 mat->n = n;
 mat->k = n;
 mat->nb_elements = nb_el;
 mat->index_i = (int *)   malloc(nb_el*sizeof(int));
 mat->index_j = (int *)   malloc(nb_el*sizeof(int));
 mat->val     = (double *)malloc(nb_el*sizeof(double));
 
 return mat;
}

/*----------------------------------------------------------------------*/

sym_matrix *create_sym_mat_mat(int n, double **m)
{
 /* mat is assumed to be symmetric (only the upper part is used) */
 sym_matrix *mat;
 int cpt = 0;
 int i, j;
 
 mat = (sym_matrix *)malloc(sizeof(sym_matrix));
 mat->n = n;
 mat->k = n;

 for(i=0;i<n;i++)
 	for(j=i;j<n;j++)
		{
		 if(m[i][j]) cpt++;
		}
 mat->nb_elements = cpt;
 mat->index_i = (int *)   malloc(cpt*sizeof(int));
 mat->index_j = (int *)   malloc(cpt*sizeof(int));
 mat->val     = (double *)malloc(cpt*sizeof(double));
 
 cpt = 0;
 for(i=0;i<n;i++)
 	for(j=i;j<n;j++)
		{
		 if(m[i][j])
		 	{
			 mat->index_i[cpt] = i;
			 mat->index_j[cpt] = j;
			 mat->val[cpt]     = m[i][j];
			 cpt++;
			}
		}
 
 return mat;
}

/*----------------------------------------------------------------------*/

void write_sym_mat(sym_matrix *mat, FILE *fp, char **str)
{
 int i;
 char buffer[40];
 int str_size;

// 17.02.2005 GESTION DE SINGLETON 
 
 if(fp)
 	{
	 if(mat->nb_elements==1)
 		{
		 fprintf(fp, "SINGLETON\n");
		 fprintf(fp, "%d ", mat->n);
		}
	else
		{
		 fprintf(fp, "SYMMETRIC_SPARSE\n");
		 fprintf(fp, "%d %d\n", mat->n, mat->nb_elements);
		}
	 for(i=0;i<mat->nb_elements;i++)
	 	{
		 fprintf(fp, "%d %d %f\n", mat->index_i[i], mat->index_j[i], mat->val[i]);
		}
	}
 if(str)
 	{
	 if(mat->nb_elements==1)
 		{
		  sprintf(buffer,"SINGLETON\n%d ",mat->n);
		}
	else
		{
		 sprintf(buffer, "SYMMETRIC_SPARSE\n%d %d\n", mat->n, mat->nb_elements);
		}
	str_size = strlen(buffer) + strlen(*str) + 1;
	*str = (char *)realloc(*str, str_size*sizeof(char));
	*str = strcat(*str, buffer);
		
	for(i=0;i<mat->nb_elements;i++)
	 	{
		 sprintf(buffer, "%d %d %f\n", mat->index_i[i], mat->index_j[i], mat->val[i]);
		 str_size = strlen(buffer) + strlen(*str) + 1;
		 *str = (char *)realloc(*str, str_size*sizeof(char));
		 *str = strcat(*str, buffer);
		}
	}
 if(!fp && !str) 
 	{
	 if(mat->nb_elements==1)
 		{
		 printf("SINGLETON\n");
		 printf("%d ", mat->n);
		}
	else
		{
		 printf("SYMMETRIC_SPARSE\n");
		 printf("%d %d\n", mat->n, mat->nb_elements);
		}
	 for(i=0;i<mat->nb_elements;i++)
	 	{
		 printf("%d %d %f\n", mat->index_i[i], mat->index_j[i], mat->val[i]);
		}
	}
 
}

/*----------------------------------------------------------------------*/

void write_sym_mat_mat(int dim, int n, double **m, FILE *fp, char **str)
{
 sym_matrix *mat;
 
 
 mat = create_sym_mat_mat(n, m);
 mat->n = dim;
 write_sym_mat(mat, fp, str);
 free_sym_mat(mat);
}

/*----------------------------------------------------------------------*/

