/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "SDP_pb.h"

extern int PRINT_DEBUG;

/* ------------------------------------------------------------------------- */

void free_SDP_pb(SDP_pb *pb)
{
  if(pb)
    {
      free_objective_function(pb->obj_f);
      free_constraints(pb->constr);
      if(pb->string_pb) free(pb->string_pb);
      free(pb);
    }
}

/* ------------------------------------------------------------------------- */

void free_SDP_sol(SDP_sol *sol)
{
  if(sol)
    {
      if(sol->x) free(sol->x);
      free(sol);
    }
}

/* ------------------------------------------------------------------------- */

void free_SDP_sol_int(SDP_sol_int *sol)
{
  if(sol)
    {
      if(sol->x) free(sol->x);
      free(sol);
    }
}

/* ------------------------------------------------------------------------- */

void write_string_SDP_pb(SDP_pb *pb)
{
  /************************************/
  /* Updates the field pb->string_pb  */
  /* If it is not NULL, then frees it */
  /* before writing it                */
  /************************************/
 
 
  if(pb->string_pb) free(pb->string_pb);
  pb->string_pb = (char *)malloc(sizeof(char));
  pb->string_pb[0] = '\0';

  write_objective_function(pb->obj_f, NULL, &(pb->string_pb));
  write_constraints(pb->dim, pb->constr, NULL, &(pb->string_pb));
}

/* ------------------------------------------------------------------------- */

SDP_pb *create_SDP_pb(char *pb_filename, char *filenameSB)
{
  SDP_pb *pb;
  obj_func *obj_f;
  constraints *constr;
  FILE *fileSB;
  int nb_constr;
  int dim;
 
 
 
  pb = (SDP_pb *)malloc(sizeof(SDP_pb));
  if(filenameSB) fileSB = fopen(filenameSB, "w");
  else           fileSB = NULL;
  pb->obj_f = create_objective_function();
  obj_f = pb->obj_f;

  if(read_objective_function(pb_filename, obj_f))
    {
      /* Error while reading the file of the problem */
      return NULL;
    }
  if(PRINT_DEBUG)
    printf("Objective function read\n");
  nb_constr = obj_f->nb_constraints;
  dim = obj_f->N + 2;
  pb->dim = dim;
 
  pb->constr = create_constraints(obj_f->n, obj_f->N);
  constr = pb->constr;
  if(read_constraints(pb_filename, constr))
    {
      /* Error while reading the file of the problem */
      return NULL;
    }
  if(PRINT_DEBUG)
    printf("Constraints read\n");
  if(constr->single)  nb_constr += constr->single->nb;
  if(constr->lin)     nb_constr += nb_lin_constr(constr->lin);
  if(constr->quad)    nb_constr += constr->quad->nb;

  obj_f->nb_constraints = nb_constr;

  write_objective_function(obj_f, fileSB, NULL);
  if(PRINT_DEBUG)
    printf("Objective function written\n");
  write_constraints(dim, constr, fileSB, NULL);
  if(PRINT_DEBUG)
    printf("Constraints written\n");
  printf("\n");

 
  if(fileSB)
    fclose(fileSB);
 
  return pb;
}


