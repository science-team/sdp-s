/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef SDPS_main_h
#define SDPS_main_h


#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include "problem1.h"
#include "solver.h"
#include "sbparams.h"
#include "spectral.h"
#include "SDP_pb.h"


 void save_and_exit(const SpectralBundle* sbp);
 void summary_and_exit(const SpectralBundle* sbp);
 void summary_and_continue(const SpectralBundle* sbp);
 void catch_signal(int sig);
 SDP_sol *sb_main(int argc,char **argv);
 SDP_sol *get_SDP_sol_str(char *pb, int add_argc, char **add_argv);
 SDP_sol *get_SDP_sol(SDP_pb *pb, int add_argc, char **add_argv, int silence,char *filenameSB);

#endif
