/* 
        Date       : 08/2002
        Authors    Geraud Delaporte, Sebastien Jouteau, Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#ifndef objective_function_h
#define objective_function_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "annexes.h"
#include "matrix_tools.h"


typedef enum{
  SPARSE = 0,
  DENSE
} enum_format;
 
typedef struct{
  enum_format format;
  char *name;
} mat_file;

typedef struct{
  int min_max; /* -1 to minimize, +1 to maximize */
  int n; /* Number of boolean variables */
  int N; /* Total number of variables   */
  int type; /* the type of boolean variables : 1 for a {0,1} problem, 0 for {-1,1} */
  int more_constr; /* if we need to use more constraints ({0,1} : like Xij >= 0) */
  int nb_constraints;
  double val;
  mat_file *C;
  mat_file *l;
} obj_func;
 

 /* List of functions */
 void free_mat_file(mat_file *m);
 int read_type(char *filename, fpos_t p);
 int read_dim(char *filename, fpos_t p);
 int read_n(char *filename, fpos_t p, int dim);
 int read_add_more_constr(char *filename, fpos_t p);
 int read_nb_constraints(char *filename, fpos_t p);
 mat_file *read_C(char *filename, fpos_t p);
 mat_file *read_l(char *filename, fpos_t p);
 double **return_C(obj_func *obj_f, int dim);
 double  *return_l(obj_func *obj_f, int dim);
 void add_linear_part(obj_func *obj_f, double **C);
 void write_constr_obj_fun(obj_func *obj_f, FILE *fp, char **str);
 
 /* Functions to be used */
 obj_func *create_objective_function();
 void free_objective_function(obj_func *f);
 int read_objective_function(char *filename, obj_func *obj_f);
 void write_objective_function(obj_func *obj_f, FILE *fp, char **str);
 
 
 
#endif
