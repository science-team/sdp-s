/* 
        Date       : 04/2003
	Author :   Frederic Roupin

  This file is part of SDP_S.

    SDP_S is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    SDP_S is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
  
#include "constraints.h"

#define SPARSE 0
#define DENSE 1

#define LEFT 1
#define RIGHT 2

extern double *getVector(FILE *fp,int n,int sparse_dense);

void fill_simple(char *filename,linear_constraints *constr,int sparse_dense,int left_right)
{
  FILE *fp;
  char buffer[140];
  int buffer_int;
  double buffer_d;
 
  int i, j ;
  int ii, jj;
  int iii,jjj;
  int nb_el = 0;
  char type; /* 0 for '=', 1 for '<', 2 for '>' and 3 for '<>' */
  int n=0;
  int k=0;
  double value=0;
  double c1,c2;
  double *vect;
  linear_constraint **pt_constr;


  /* READING OF THE HEADER : L= | L< | L> | L<>*/
  fp = fopen(filename, "r");
  fscanf(fp, "%s", buffer);
  if(buffer[0] != 'l' && buffer[0] != 'L'){
    printf("Simple() is valid only for linear constraints !\n");
    return;
  }
  if(strlen(buffer)>1)
    {
      if(buffer[1] == '=')      type = 0; /* dx = c         */
      else if(buffer[1] == '<')
	{
	  if(buffer[2] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[1] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }
  else
    {
      fscanf(fp, "%s", buffer);

      if(buffer[0] == '=')      type = 0; /* dx = c         */
      else if(buffer[0] == '<')
	{
	  if(buffer[1] == '>')   type = 3; /* c1 <= dx <= c2 */
	  else                   type = 2; /* dx <= c2       */
	}
      else if(buffer[0] == '>') type = 1; /* dx >= c1       */
      else
	{
	  printf("Error while reading the linear constraint file \"%s\".\n", filename);
	  printf("\"%s\" is not valid.\n", buffer);
	  return ;
	}
    }

  /* READING OF N:dimension AND K:nb of constraints in this file */
  fscanf(fp, "%d", &n);
  fscanf(fp, "%d", &k);
  constr->n = n;

  /* ALLOCATION / REALLOCATION OF MEMORY TO STOCK CONSTRAINTS */
  buffer_int = constr->nb;
  constr->nb += k;
  printf("Adding %d simple constraints from file %s\n",k,filename);
  if ( ( constr->constraints = (linear_constraint **)realloc(constr->constraints, (constr->nb)*sizeof(linear_constraint *))) == NULL)
    printf("Memory Re-Allocation Error!!!\n");
  pt_constr = &(constr->constraints[buffer_int]);
 
  /* READING THE CONSTRAINTS */ 
  // k = number of constraints
  for(i=0;i<k;i++)
  {
    fscanf(fp, "%lf", &value);
    if(type == 3)
     {
      c1 = value;
      fscanf(fp,"%lf", &c2);
      if(left_right != LEFT) value = c2;
     }

    vect = getVector(fp,n,sparse_dense);
    nb_el = 0;
    for(iii=0;iii<n;iii++)
     {
      if(vect[iii] != 0)
	nb_el++;
     }

      if ( (pt_constr[i] = (linear_constraint *)malloc(sizeof(linear_constraint))) == NULL)
	printf("Memory Allocation Error!!!\n");
      /* then the vector */
      if ( (pt_constr[i]->l = (double *)malloc(n*sizeof(double))) == NULL)
	printf("Memory Allocation Error!!!\n");

      /* c | c1 c2 | c2 c1 */
      if(type == 0){
	pt_constr[i]->c1 = value;
	pt_constr[i]->c2 = value;
	// flag_ci=4 pour distinguer le cas ou on ne
	// veut pas le carre de la contrainte
	pt_constr[i]->flag_ci = 4;
      }
      if(type == 1){
	pt_constr[i]->c1 = value;
	pt_constr[i]->c2 = value;
	pt_constr[i]->flag_ci = 1;
      }
      if(type == 2){
	pt_constr[i]->c1 = value;
	pt_constr[i]->c2 = value;
	pt_constr[i]->flag_ci = 2;
      }
      if(type == 3){
	pt_constr[i]->c1 = c1;
	pt_constr[i]->c2 = c2;
	if(left_right == LEFT)
	  pt_constr[i]->flag_ci = 1;
	else
	  pt_constr[i]->flag_ci = 2;
      }

// The vector
     for(j=0;j<n;j++)
	{
	  pt_constr[i]->l[j] = vect[j];
	}
    free(vect);
  }
}


/** fpos of fp is at the beginning of the constraints ****/ 
void read_simple(FILE *fp,constraints *constr)
{
  char buffer[200];
  char buffer2[200];
  char *ptr;
  fpos_t p,p_tmp;
  int res=0;
  int n = constr->n;
  int N = constr->N;
  int left_right=RIGHT;
  
  fgetpos(fp, &p);
  while(!feof(fp)){
    res = fscanf(fp, "%s", buffer);

    if(buffer[0] == '_')
      continue;

    if(((ptr = strstr(buffer, "Simple")) != NULL) && res == 1){
      FILE *file_tmp;
      int type=0;
      int sparse_dense; /* 0 for sparse ; 1 for dense */
      unsigned char c;
      int found = 0;

      int i;

      if((ptr = strstr(buffer,"Left")) != NULL)
	left_right = LEFT;
      else if((ptr = strstr(buffer,"Right")) != NULL)
	left_right = RIGHT;

      if((ptr = strstr(buffer,"(")) == NULL){
	printf("*** error : Usage is : Simple(<filename>) or Simple[Left|Right](<filename>)\n");
	return;
      }

      for(i=0;i<strlen(ptr);i++){
	if((ptr[i+1] != ')') && (ptr[i+1] != 0))
	  buffer2[i] = ptr[i+1];
	else{
	  buffer2[i] = 0;
	  break;
	}
      }

      fgetpos(fp,&p_tmp);
      fsetpos(fp,&p);
      
      while(!found){
	ptr = fgets(buffer,200,fp);
	if(ptr == NULL){
	  printf("*** error in Simple(), filename %s not found\n",buffer2);
	  fsetpos(fp,&p_tmp);
	  break;
	}
	if(strstr(buffer,buffer2) != NULL){
	  if(strstr(buffer,"Sparse")){
	    sparse_dense = 0;
	    fsetpos(fp,&p_tmp);
	    found = 1;
	  }
	  else{
	    if(strstr(buffer,"Dense")){
	      sparse_dense = 1;
	      fsetpos(fp,&p_tmp);
	      found = 1;
	    }
	  }
	}
      }

      if(found){
	if(!constr->lin){
	  constr->lin = create_lin_constr(n, N);
	}
	fill_simple(buffer2,constr->lin,sparse_dense,left_right);
      }
    }
  }
}


