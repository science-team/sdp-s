

CXX		=	g++
CPPFLAGS		=	-Wno-deprecated -Imatrix -Itools -Ispectral \
			-DWITH_MATRIX -DWITH_SYMMATRIX \
			-DWITH_SPARSESYM -DWITH_SPARSEMAT

GCCWARN		=#	
DEBU		=#	-g -DDEBUG
OPTI		=	-pipe -O6
LINK		=	-lm
OBJDIR		=	Object

# # TARGET		=	SDP_S res2Plot
TARGET		=	sdps sdps_res2Plot

CXXFLAGS		= 	$(OPTI) $(WARN) $(DEBU)
LDFLAGS		=	$(LINK)

VPATH	        =       matrix tools spectral SDP_SOLVER


# Fichiers sources
SRCS_matrix 			= $(wildcard   matrix/*.c matrix/*.cc )
SRCS_spectral 			= $(wildcard   spectral/*.c spectral/*.cc )
SRCS_SDP_SOLVER 			= $(wildcard   SDP_SOLVER/*.c SDP_SOLVER/*.cc )
# SRCS_tools			= $(wildcard   tools/*.c tools/*.cc )
SRCS			= $(SRCS_matrix) $(SRCS_spectral) $(SRCS_SDP_SOLVER)

# Fichiers objets
# transfome "	xxx/popo.cc"  en "Objects/popo.o"
ALL_OBJ = $(addsuffix .o, $(addprefix $(OBJDIR)/, $(notdir $(basename $(SRCS)))))


# Fichiers de d�pendance
DPDS = $(ALL_OBJ:.o=.d)

########################################################################

all:		$(TARGET)

sdps:		$(ALL_OBJ)
		$(CXX) $(CXXFLAGS) $(ALL_OBJ) $(LDFLAGS) -o $@
sdps_res2Plot: 	tools/res2Plot.o
		$(CXX) -lm tools/res2Plot.c -o $@

# various cleaning
clean:
		-rm -f Object/*.o tools/*.o  core
dc:	distclean
distclean:
		$(MAKE) clean
		@rm -f $(TARGET)
		

$(OBJDIR)/%.o:	%.cc
		$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(OBJDIR)/%.o:	%.c
		$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@



# Creation des fichiers de d�pendance 
# 
# Dans le but de g�n�rer une d�pendance correcte des fichiers de 
# d�pendance eux-m�me, la doc gmake (page 30) conseille d'utiliser 
# la commande ci -dessous.
# 
# Le sed a pour but de transforme les lignes du ".d" suivantes :
#    log.o:    log.cc log.h external.h
# en :
#    log.o log.d:    log.cc log.h external.h
# 
# NOTA : on peut utiliser l'option -M a la place de -MM pour lister 
# TOUTES les d�pendances (pour v�rifier le path des fichier syst�mes 
# inclus par exemple).
# 
# On affiche (grace a la variable automatique $? dans la commande @echo)
# le (ou les) fichiers recents,  responsables de la mise a jour du ".d",
# mais seul le  ".cc" est utilis� pour la reconstruction du ".d" : 
# 
# Exemple : si fichier a reconstruire est toto.d, la variable $?
# peut tres bien valoir titi.h si titi.h fait partie des d�pendance de 
# toto.cc  !
# 
# 
$(OBJDIR)/%.d: %.cc
	@echo '=> Refait .d "$@" car inexistant ou plus ancien que : $?'
	@if [ ! -d $(OBJDIR) ]; then mkdir $(OBJDIR); fi
	@set -e; rm -f $@; \
	$(CXX) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed  's,\($*\)\.o[ :]*,$(OBJDIR:/=)/\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$ 
$(OBJDIR)/%.d: %.c
	@echo '=> Refait .d "$@" car inexistant ou plus ancien que : $?'
	@if [ ! -d $(OBJDIR) ]; then mkdir $(OBJDIR); fi
	@set -e; rm -f $@; \
	$(CXX) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed  's,\($*\)\.o[ :]*,$(OBJDIR:/=)/\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$ 
	
deps: $(DPDS)

include $(DPDS)

########################################################################
# Visualisation des dependances completes de toutes les sources
visudep : 
	@echo 
	@for file in $(SRCS) ; \
	do \
	   echo ;\
	   echo "=> visu dependance de : $$file" ;\
	   echo ;\
	   $(CXX) -M $(DFLAGS) $(CPPFLAGS) $$file ;\
	done;

## Pour deboguage
trace_files:
	@echo "Nb de fichier dans variable SRCS" 
	@echo $(SRCS) | wc 
	@echo "Nb de fichier dans variable ALL_OBJ" 
	@echo $(ALL_DIR) | wc 
	@echo "Nb de fichier dans variable SRCS" 
	@echo  
	@echo $(sort $(notdir $(SRCS)))
	@echo "Nb de fichier dans variable ALL_OBJ" 
	@echo  
	@echo $(sort $(notdir $(ALL_OBJ))) 

########################################################################
# Archivage du r�pertoire courant dans le r�pertoire parent
# avec la date dans le nom du .zip cr��
# 
# Description des options de zip utilis�es :
#    -r   recurse into directories
#    -y   store symbolic links as the link instead of the referenced file
#    -9   compress better (-1 compress faster)
#    -v   verbose operation/print version info
#    -q   quiet operation
#    -o   make zipfile as old as latest entry
# Decompression par :
#    unzip leFichier.zip
#    le r�pertoire "projet" d'origine est alors recr�� (ATTENTION
#    s'il existe d�ja)
arc: zip
zip:
	$(MAKE) distclean
	date=`date +%Y%m%d-%Hh%Mmn`                     && \
	lpath=`pwd`                                     && \
	bname=`basename $$lpath`                        && \
	datename=$$bname-$$date                         && \
	cd ..                                           && \
	cp -Rp $$bname $$datename                       && \
	zip -r -y -o -q -9  $$datename.zip $$datename   && \
	\rm -rf $$datename                              && \
	cd $$lpath
	
tgz:
	$(MAKE) distclean
	date=`date +%Y%m%d-%Hh%Mmn`                     && \
	lpath=`pwd`                                     && \
	bname=`basename $$lpath`                        && \
	datename=$$bname-$$date                         && \
	cd ..                                           && \
	cp -Rp $$bname $$datename                       && \
	tar cf - $$datename | gzip > $$datename.tgz     && \
	\rm -rf $$datename                              && \
	cd $$lpath
	
tbz:
	$(MAKE) distclean
	date=`date +%Y%m%d-%Hh%Mmn`                     && \
	lpath=`pwd`                                     && \
	bname=`basename $$lpath`                        && \
	datename=$$bname-$$date                         && \
	cd ..                                           && \
	cp -Rp $$bname $$datename                       && \
	tar cf - $$datename | bzip2 > $$datename.tbz    && \
	\rm -rf $$datename                              && \
	cd $$lpath

## ./
